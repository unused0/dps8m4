/*
 * vim: filetype=c:tabstop=4:ai:expandtab
 * SPDX-License-Identifier: ICU
 * scspell-id: 8e66ce24-f62e-11ec-8690-80ee73e9b8e7
 *
 * ---------------------------------------------------------------------------
 *
 * Copyright (c) 2007-2013 Michael Mondy
 * Copyright (c) 2012-2016 Harry Reed
 * Copyright (c) 2013-2024 Charles Anthony
 * Copyright (c) 2021-2024 The DPS8M Development Team
 *
 * This software is made available under the terms of the ICU License.
 * See the LICENSE.md file at the top-level directory of this distribution.
 *
 * ---------------------------------------------------------------------------
 */

//typedef enum 
  //{
    //cm_LPW_init_state, // No TDCWs encountered; state is:
                       ////    PCW64 (pcw64_pge): on   PAGE CHAN
                       ////    PCW64 (pcw64_pge): off  EXT MODE CHAN
    //cm_real_LPW_real_DCW,
    //cm_ext_LPW_real_DCW,
    //cm_paged_LPW_seg_DCW
  //} chan_mode_t;

typedef enum chanStat
  {
    chanStatNormal          = 0,
    chanStatUnexpectedPCW   = 1,
    chanStatInvalidInstrPCW = 2,
    chanStatIncorrectDCW    = 3,
    chanStatIncomplete      = 4,
    chanStatUnassigned      = 5,
    chanStatParityErrPeriph = 6,
    chanStatParityErrBus    = 7
  } chanStat;

// Due to lack of documentation, chan_cmd is largely ignored
//
// iom_chan_control_words.incl.pl1
//
//   SINGLE_RECORD       init ("00"b3),
//   NONDATA             init ("02"b3),
//   MULTIRECORD         init ("06"b3),
//   SINGLE_CHARACTER    init ("10"b3)
//
// bound_tolts_/mtdsim_.pl1
//
//    idcw.chan_cmd = "40"b3;           /* otherwise set special cont. cmd */
//
// bound_io_tools/exercise_disk.pl1
//
//   idcw.chan_cmd = INHIB_AUTO_RETRY; /* inhibit mpc auto retries */
//   dcl     INHIB_AUTO_RETRY       bit (6) int static init ("010001"b);  // 021
//
// poll_mpc.pl1:
//  /* Build dcw list to get statistics from EURC MPC */
//  idcw.chan_cmd = "41"b3;            /* Indicate special controller command */
//  /* Build dcw list to get configuration and statistics from DAU MSP */
//  idcw.chan_cmd = "30"b3;                           /* Want list in dev# order */
//
// tape_ioi_io.pl1:
//   idcw.chan_cmd = "03"b3;          /* data security erase */
//   dcw.chan_cmd = "30"b3;           /* use normal values, auto-retry */

// iom_word_macros.incl.alm
//
// Channel control
#define CHAN_CTRL_TERMINATE 0
#define CHAN_CTRL_PROCEED   2
#define CHAN_CTRL_MARKER    3

// Channel command
#define CHAN_CMD_RECORD      0
#define CHAN_CMD_NONDATA     2
#define CHAN_CMD_MULTIRECORD 6
#define CHAN_CMD_CHARACTER   8

#define IS_IDCW(p)     ((p)->DCW_18_20_CP == 07)
#define IS_NOT_IDCW(p) ((p)->DCW_18_20_CP != 07)
#define IS_TDCW(p)     ((p)->DCW_18_20_CP !=  7 && (p)->DDCW_22_23_TYPE == 2)
#define IS_IOTD(p)     ((p)->DCW_18_20_CP !=  7 && (p)->DDCW_22_23_TYPE == 0)
#define IS_IONTP(p)    ((p)->DCW_18_20_CP !=  7 && (p)->DDCW_22_23_TYPE == 3)

// exercise_disk.pl1
#define CHAN_CMD_INHIB_AUTO_RETRY    021
// load_mpc.pl1
#define CHAN_CMD_SPECIAL_CTLR        040
// poll_mpc.pl1
#define CHAN_CMD_DEV_ORDER           030
#define CHAN_CMD_SPECIAL_CTLR2       041
// tape_ioi_io.pl1
#define CHAN_CMD_DATA_SECURITY_ERASE  03
#define CHAN_CMD_NORM_AUTO_TRY       030

typedef struct {

// simulator data
  volatile _Atomic uint myIomIdx;
  volatile _Atomic uint myChanIdx;

// scratch pad

  // packed LPW
  volatile _Atomic word36 LPW;
  // unpacked LPW
  volatile _Atomic word18 LPW_DCW_PTR;
  volatile _Atomic word1 LPW_18_RES;
  volatile _Atomic word1 LPW_19_REL;
  volatile _Atomic word1 LPW_20_AE;
  volatile _Atomic word1 LPW_21_NC;
  volatile _Atomic word1 LPW_22_TAL;
  volatile _Atomic word1 LPW_23_REL;
  volatile _Atomic word12 LPW_TALLY;

  // packed LPWX
  volatile _Atomic word36 LPWX;
  // unpacked LPWX
  volatile _Atomic word18 LPWX_BOUND; // MOD 2 (pg B16) 0-2^19; ie val = LPX_BOUND * 2
  volatile _Atomic word18 LPWX_SIZE;  // MOD 1 (pg B16) 0-2^18

// PCW_63_PTP indicates paging mode; indicates that a page table
// is available.
// XXX pg B11: cleared by a terminate interrupt service with the
// character size bit of the transaction command = 1. (bit 32)
// what is the 'transaction command.?

  // packed PCW
  volatile _Atomic word36 PCW0, PCW1;
  // unpacked PCW
  volatile _Atomic word6 PCW_CHAN;
  volatile _Atomic word6 PCW_AE;
  // Pg B2: "Absolute location (MOD 64) of the channels Page Table"
  volatile _Atomic word18 PCW_PAGE_TABLE_PTR;
  volatile _Atomic word1 PCW_63_PTP;
  volatile _Atomic word1 PCW_64_PGE;
  volatile _Atomic word1 PCW_65_AUX; // XXX
  volatile _Atomic word1 PCW_21_MSK; // Sometimes called 'M' // see 3.2.2, pg 25

  // packed DCW
  volatile _Atomic word36 DCW;
  // unpacked DCW
  // TDCW only
  word18 TDCW_DATA_ADDRESS;
  word1  TDCW_34_RES;
  word1  TDCW_35_REL;
  // TDCW, PCW 64 = 0
  word1  TDCW_33_EC;
  // TDCW, PCW 64 = 1
  word1  TDCW_31_SEG;
  word1  TDCW_32_PDTA;
  word1  TDCW_33_PDCW;
  // IDCW only
  word6  IDCW_DEV_CMD;
  word6  IDCW_DEV_CODE;
  word6  IDCW_AE;
  word1  IDCW_EC;
  word2  IDCW_CHAN_CTRL; // 0 terminate, 2 process, 3 marker
  word6  IDCW_CHAN_CMD;
  // POLTS sets this to one if there are IOTxes.
  word6  IDCW_COUNT;
  // DDCW only
  /*word18*/ uint DDCW_ADDR; // Allow overflow detection
  word12 DDCW_TALLY;
  word2  DDCW_22_23_TYPE; // '2' indicates TDCW
  // xDCW
  word3  DCW_18_20_CP; // '7' indicates IDCW
  // XXX pg 30; the indirect data service needs to use this.

  word6 ADDR_EXT; // 3.2.2, 3.2.3.1
  word1 SEG;  // pg B21

  enum { /* PGE */ cm1, cm2, cm3a, cm3b, cm4, cm5,
         /* EXT */ cm1e, cm2e } chanMode;

// XXX CP XXXX
// "Specifies the positions of the first character with the first word
// of the block. The byte size, defined by the channel, determines
// what CP values are valid/
//  6 bit: 0-5; 9 bit: 0-4; 18 bit: 0-1; 36 bit: 0-6
//
// For word channels, CP is sent to the channel during list service,
// and is zeros when placed in the mailbox for subsequent data
// services to the channel.
//
//  [CAC: I think that this can be elided. To implement correctly,
//  iom_list_service and/or doPayloadChannel would have to know the
//  word or sub-word functionality of the channel. But it would
//  be simpler to let the device handler just access the CP data,
//  and make it's own decisions about "zeros". After all, it is
//  not clear what a non-zero CP means for a word channel.]
//
// For sub-word channels which depend on IOM Central for packing and
// unpacking words, [IOM Central uses and updates the CP to access
// sub-words].
//
//  [CAC: Again, I think that this can be elided. It is simpler to
//  to have the device handler pack and unpack.]
//

// LPW addressing mode

  //enum { LPW_REAL, LPW_EXT, LPW_PAGED, LPW_SEG } lpwAddrMode;

  // pg B2: "Scratchpad area for two Page Table Words ... one
  // for the DCW List (PTW-LPW) and one for the data (PTW-DCW).

  // PTW format: (pg B8)
  //   4-17: Address of page table, mod 64
  //   31: WRC: Write control bit (1: page may be written)
  //   32: HSE: Housekeeping
  //   33: PGP: Page present
  // To read or write PGP must be 1
  // To write, WRC must be 1, HSE 0; system fault 15 on fail

  // pg b8: PTWs are used iff (indirect store and LPw 23 (segmented)), or
  // direct store.
  //
  //  ADDR  0-13 <- PTW 4-17
  //       14-23 <- LPW 8-17; DCW 8-17; direct channel address 14-23

  word36 PTW_DCW;  // pg B8.
  word36 PTW_LPW;  // pg B6.

  // pg b11 defines two PTW flags to indicate the validity of the
  // PTW_DCW and PTW_LPW; it is simpler to simply always fetch
  // the PTWs on demand.

  //  flag
  //chanMode_t chanMode;

  // true if the DCW is from a PCW
  bool isPCW;

  // Information accumulated for status service.
  word12 stati;
  uint dev_code;
  // Initialized to IDCW_COUNT; decremented when a IDCW that expects an IOTx is processed by the channel.
  // XXX POLTS console code drives this; if it turns out to be common across channels, the decrement should
  // be moved into the IOM.
  word6 recordResidue;
  word12 tallyResidue;
  word3 charPos;
  bool isRead;
  // isOdd can be ignored; see https://ringzero.wikidot.com/wiki:cac-2015-10-22
  // bool isOdd;
  bool initiate;

  chanStat chanStatus;

  bool lsFirst;

  bool wasTDCW;

  bool masked;

  bool in_use;

  bool start;

} chnlState_t;

extern chnlState_t iomChanData [N_IOM_UNITS_MAX] [MAX_CHANNELS];

// Indirect data service data type
typedef enum
  {
    idsTypeW36  // Incoming data is array of word36
  } idsType;

typedef enum
  {
    direct_load,
    direct_store,
    direct_read_clear,
  } iom_direct_data_service_op;

/* From AN70-1 May84
 *  ... The IOM determines an interrupt
 * number. (The interrupt number is a five bit value, from 0 to 31.
 * The high order bits are the interrupt level.
 *
 * 0 - system fault
 * 1 - terminate
 * 2 - marker
 * 3 - special
 *
 * The low order three bits determines the IOM and IOM channel
 * group.
 *
 * 0 - IOM 0 channels 32-63
 * 1 - IOM 1 channels 32-63
 * 2 - IOM 2 channels 32-63
 * 3 - IOM 3 channels 32-63
 * 4 - IOM 0 channels 0-31
 * 5 - IOM 1 channels 0-31
 * 6 - IOM 2 channels 0-31
 * 7 - IOM 3 channels 0-31
 *
 *   3  3     3   3   3
 *   1  2     3   4   5
 *  ---------------------
 *  | pic | group | iom |
 *  -----------------------------
 *       2       1     2
 */

enum iomImwPics
  {
    imwSystemFaultPic = 0,
    imwTerminatePic   = 1,
    imwMarkerPic      = 2,
    imwSpecialPic     = 3
  };


typedef void chnlInit_t (chnlState_t * chnlp);
int iomListService (chnlState_t * chnlp, bool * ptro, bool * sendp, bool * uffp);
void iomDirectDataService (chnlState_t * chnlp, word24 daddr, word36 * data, iom_direct_data_service_op op);
void iomIndirectDataService (chnlState_t * chnlp, word36 * data, uint * cnt, bool write);
int send_terminate_interrupt (chnlState_t * chnlp);
void iom_interrupt (uint scuUnitNum, uint iom_unit_idx);
void iom_init (void);
int send_marker_interrupt (chnlState_t * chnlp);
int send_general_interrupt (chnlState_t * chnlp, enum iomImwPics pic);
int send_special_interrupt (chnlState_t * chnlp, uint devCode, word8 status0, word8 status1);
int status_service (chnlState_t * chnlp, bool marker);
