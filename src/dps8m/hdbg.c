/*
 * vim: filetype=c:tabstop=4:ai:expandtab
 * SPDX-License-Identifier: ICU
 * scspell-id: 56a3950b-f62f-11ec-9ec8-80ee73e9b8e7
 *
 * ---------------------------------------------------------------------------
 *
 * Copyright (c) 2016-2024 Charles Anthony
 * Copyright (c) 2021-2024 The DPS8M Development Team
 *
 * This software is made available under the terms of the ICU License.
 * See the LICENSE.md file at the top-level directory of this distribution.
 *
 * ---------------------------------------------------------------------------
 */

// History Debugging

#if defined(HDBG)

# include <unistd.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>

# include "dps8m.h"
# include "iom_channel.h"
# include "utils.h"
# include "hdbg.h"
# include "threadz.h"

# include "faults.h"

enum hevtType {
  hevtEmpty = 0,
  hevtTrace,
  hevtM,
  hevtAPU,
  hevtFault,
  hevtIntrCellSet,
  hevtIntrSet,
  hevtIntr,
  hevtReg,
  hevtPAReg,
  hevtDSBRReg,
  hevtIEFP,
  hevtNote,
};

typedef struct {
  enum hevtType type;
  uint64_t time;
  uint cpu_idx;
#define CTX_SZ 32
  char ctx[CTX_SZ];
  bool rw; // F: read  T: write
  union {
    struct {
      addrModes_e addrMode;
      word15 segno;
      word18 ic;
      word3 ring;
      word36 inst;
    } trace;

    struct {
      word24 addr;
      word36 data;
    } memref;

    struct {
      _fault faultNumber;
      _fault_subtype subFault;
      char faultMsg [64];
    } fault;

    struct {
      uint cellnum;
      uint scuUnitIdx;
    } intrCellSet;

    struct {
      uint inum;
      uint cpuUnitIdx;
      uint scuUnitIdx;
    } intrSet;

    struct {
      uint intr_pair_addr;
    } intr;

    struct {
      enum hregs_t type;
      word36 data;
    } reg;

    struct {
      enum hregs_t type;
      par_s data;
    } par;

    struct {
      enum hdbgIEFP_e type;
      word15 segno;
      word18 offset;
    } iefp;

    struct {
      enum hregs_t type;
      dsbr_s data;
    } dsbr;

    struct {
      enum hregs_t type;
      word15 segno;
      word18 offset;
      word24 final;
      word36 data;
    } apu;
    struct {
# define NOTE_SZ 64
      char noteBody [NOTE_SZ];
    } note;
  };
} hev_t;

static hev_t * hevents = NULL;
static long hdbgSize = 0;
static long hevtPtr = 0;
static long hevtMark = 0;
static long hdbgSegNum = -1;
static bool blacklist[MAX18];
static long hdbgCPUMask = 0;
static bool hdbgLiveFlag = false;
static FILE * hdbgOut = NULL;

static void printM (hev_t * p, FILE * fp) {
  (void) fprintf (fp, "DBG(%"PRIu64")> CPU %d FINAL: %s %s %08o %012"PRIo64"\n",
           p->time,
           p->cpu_idx,
           p->ctx,
           p->rw ? "write" : "read ",
           p->memref.addr,
           p->memref.data);
}

static void printAPU (hev_t * p, FILE * fp) {
  (void) fprintf (fp, "DBG(%"PRIu64")> CPU %d APU: %s %s %05o:%06o %08o %012"PRIo64"\n",
           p->time,
           p->cpu_idx,
           p->ctx,
           p->rw ? "write" : "read ",
           p->apu.segno,
           p->apu.offset,
           p->apu.final,
           p->apu.data);
}

static void printTrace (hev_t * p, FILE * fp) {
  char buf[256];
  if (p -> trace.addrMode == ABSOLUTE_mode) {
    (void) fprintf (fp, "DBG(%"PRIu64")> CPU %d TRACE: %s %06o %o %012"PRIo64" (%s)\n",
             p->time,
             p->cpu_idx,
             p->ctx,
             p->trace.ic,
             p->trace.ring,
             p->trace.inst,
             disassemble (buf, p->trace.inst));
  } else {
    (void) fprintf (fp, "DBG(%"PRIu64")> CPU %d TRACE: %s %05o:%06o %o %012"PRIo64" (%s)\n",
             p->time,
             p->cpu_idx,
             p->ctx,
             p->trace.segno,
             p->trace.ic,
             p->trace.ring,
             p->trace.inst,
             disassemble (buf, p->trace.inst));
  }
}

static void printFault (hev_t * p, FILE * fp) {
  (void) fprintf (fp, "DBG(%"PRIu64")> CPU %d FAULT: %s Fault %d(0%o), sub %"PRIu64"(0%"PRIo64"), '%s'\n",
           p->time,
           p->cpu_idx,
           p->ctx,
           p->fault.faultNumber,
           p->fault.faultNumber,
           p->fault.subFault.bits,
           p->fault.subFault.bits,
           p->fault.faultMsg);
}

static void printIntrCellSet (hev_t * p, FILE * fp) {
  (void) fprintf (fp, "DBG(%"PRIu64")> SCU %o INTR_CELL_SET: %s cell number %d(0%o)\n",
           p->time,
           p->intrCellSet.scuUnitIdx,
           p->ctx,
           p->intrCellSet.cellnum,
           p->intrCellSet.cellnum);
}

static void printIntrSet (hev_t * p, FILE * fp) {
  (void) fprintf (fp, "DBG(%"PRIu64")> CPU %d INTR_SET: %s number %d(0%o), CPU %u SCU %u\n",
           p->time,
           p->cpu_idx,
           p->ctx,
           p->intrSet.inum,
           p->intrSet.inum,
           p->intrSet.cpuUnitIdx,
           p->intrSet.scuUnitIdx);
}

static void printIntr (hev_t * p, FILE * fp) {
  (void) fprintf (fp, "DBG(%"PRIu64")> CPU %d INTR: %s Interrupt pair address %o\n",
           p->time,
           p->cpu_idx,
           p->ctx,
           p->intr.intr_pair_addr);
}

// Keep sync'd with hregs_t
static char * regNames[] = {
   "A  ",
   "Q  ",
   "E  ",
   "X0 ", "X1 ", "X2 ", "X3 ", "X4 ", "X5 ", "X6 ", "X7 ",
   "AR0", "AR1", "AR2", "AR3", "AR4", "AR5", "AR6", "AR7",
   "PR0", "PR1", "PR2", "PR3", "PR4", "PR5", "PR6", "PR7",
   "Y  ", "Z  ",
   "IR ",
  "DSBR",
};

static void printReg (hev_t * p, FILE * fp) {
  if (p->reg.type == hreg_IR)
    (void) fprintf (fp, "DBG(%"PRIu64")> CPU %d REG: %s %s %s %012"PRIo64" Z%o N%o C %o O%o T%o \n",
             p->time,
             p->cpu_idx,
             p->ctx,
             p->rw ? "write" : "read ",
             regNames[p->reg.type],
             p->reg.data,
             TSTF (p->reg.data, I_ZERO),
             TSTF (p->reg.data, I_NEG),
             TSTF (p->reg.data, I_CARRY),
             TSTF (p->reg.data, I_OFLOW),
             TSTF (p->reg.data, I_TALLY));
  else if (p->reg.type >= hreg_X0 && p->reg.type <= hreg_X7)
    (void) fprintf (fp, "DBG(%"PRIu64")> CPU %d REG: %s %s %s %06"PRIo64"\n",
             p->time,
             p->cpu_idx,
             p->ctx,
             p->rw ? "write" : "read ",
             regNames[p->reg.type],
             p->reg.data);
  else
    (void) fprintf (fp, "DBG(%"PRIu64")> CPU %d REG: %s %s  %s %012"PRIo64"\n",
             p->time,
             p->cpu_idx,
             p->ctx,
             p->rw ? "write" : "read ",
             regNames[p->reg.type],
             p->reg.data);
}

static void printPAReg (hev_t * p, FILE * fp)
{
  if (p->reg.type >= hreg_PR0 && p->reg.type <= hreg_PR7)
    (void) fprintf (fp, "DBG(%"PRIu64")> CPU %d REG: %s %s %s %05o:%06o BIT %2o RNR %o\n",
             p->time,
             p->cpu_idx,
             p->ctx,
             p->rw ? "write" : "read ",
             regNames[p->reg.type],
             p->par.data.SNR,
             p->par.data.WORDNO,
             p->par.data.PR_BITNO,
             p->par.data.RNR);
  else
    (void) fprintf (fp, "DBG(%"PRIu64")> CPU %d REG: %s write %s %05o:%06o CHAR %o BIT %2o RNR %o\n",
             p->time,
             p->cpu_idx,
             p->ctx,
             regNames[p->reg.type],
             p->par.data.SNR,
             p->par.data.WORDNO,
             p->par.data.AR_CHAR,
             p->par.data.AR_BITNO,
             p->par.data.RNR);
}

static void printDSBRReg (hev_t * p, FILE * fp) {
  (void) fprintf (fp, "DBG(%"PRIu64")> CPU %d REG: %s %s %s %05o:%06o BIT %2o RNR %o\n",
           p->time,
           p->cpu_idx,
           p->ctx,
           p->rw ? "write" : "read ",
           regNames[p->reg.type],
           p->par.data.SNR,
           p->par.data.WORDNO,
           p->par.data.PR_BITNO,
           p->par.data.RNR);
}

static void printIEFP (hev_t * p, FILE * fp) {
  switch (p->iefp.type) {
    case hdbgIEFP_abs_bar_read:
      (void) fprintf (fp, "DBG(%"PRIu64")> CPU %d IEFP ABS BAR READ:  |%06o\n",
               p->time,
               p->cpu_idx,
               p->iefp.offset);
      break;

    case hdbgIEFP_abs_read:
      (void) fprintf (fp, "DBG(%"PRIu64")> CPU %d IEFP ABS     READ:  :%06o\n",
               p->time,
               p->cpu_idx,
               p->iefp.offset);
      break;

    case hdbgIEFP_bar_read:
      (void) fprintf (fp, "DBG(%"PRIu64")> CPU %d IEFP APP BAR READ:  %05o|%06o\n",
               p->time,
               p->cpu_idx,
               p->iefp.segno,
               p->iefp.offset);
      break;

    case hdbgIEFP_read:
      (void) fprintf (fp, "DBG(%"PRIu64")> CPU %d IEFP APP     READ:  %05o:%06o\n",
               p->time,
               p->cpu_idx,
               p->iefp.segno,
               p->iefp.offset);
      break;

    case hdbgIEFP_abs_bar_write:
      (void) fprintf (fp, "DBG(%"PRIu64")> CPU %d IEFP ABS BAR WRITE: |%06o\n",
               p->time,
               p->cpu_idx,
               p->iefp.offset);
      break;

    case hdbgIEFP_abs_write:
      (void) fprintf (fp, "DBG(%"PRIu64")> CPU %d IEFP ABS     WRITE: :%06o\n",
               p->time,
               p->cpu_idx,
               p->iefp.offset);
      break;

    case hdbgIEFP_bar_write:
      (void) fprintf (fp, "DBG(%"PRIu64")> CPU %d IEFP APP BAR WRITE: %05o|%06o\n",
               p->time,
               p->cpu_idx,
               p->iefp.segno,
               p->iefp.offset);
      break;

    case hdbgIEFP_write:
      (void) fprintf (fp, "DBG(%"PRIu64")> CPU %d IEFP APP     WRITE: %05o:%06o\n",
               p->time,
               p->cpu_idx,
               p->iefp.segno,
               p->iefp.offset);
      break;

    default:
      (void) fprintf (fp, "DBG(%"PRIu64")> CPU %d IEFP ??? ??? WRITE: %05o?%06o\n",
               p->time,
               p->cpu_idx,
               p->iefp.segno,
               p->iefp.offset);
      break;
  }
}

static void printNote (hev_t * p, FILE * fp) {
  (void) fprintf (fp, "DBG(%"PRIu64")> CPU %d Note: %s %s\n",
                  p->time,
                  p->cpu_idx,
                  p->ctx,
                  p->note.noteBody);
}

static void hdbgPrintEvent (hev_t * evtp, FILE * fp) {
  switch (evtp -> type) {
    case hevtEmpty:
      break;

    case hevtTrace:
      printTrace (evtp, fp);
      break;

    case hevtM:
      printM (evtp, fp);
      break;

    case hevtAPU:
      printAPU (evtp, fp);
      break;

# if 0
    case hevtIWBUpdate:
      printIWBUpdate (evtp, fp);
      break;
# endif

# if 0
    case hevtRegs:
      printRegs (evtp, fp);
      break;
# endif

    case hevtFault:
      printFault (evtp, fp);
      break;

    case hevtIntrCellSet:
      printIntrCellSet (evtp, fp);
      break;

    case hevtIntrSet:
      printIntrSet (evtp, fp);
      break;

    case hevtIntr:
      printIntr (evtp, fp);
      break;

    case hevtReg:
      printReg (evtp, fp);
      break;

    case hevtPAReg:
      printPAReg (evtp, fp);
      break;

    case hevtDSBRReg:
      printDSBRReg (evtp, fp);
      break;

    case hevtIEFP:
      printIEFP (evtp, fp);
      break;

    case hevtNote:
      printNote (evtp, fp);
      break;

    default:
      (void) fprintf (fp, "hdbgPrint ? %d\n", evtp -> type);
      break;
  }
}

static pthread_mutex_t hdbgLock = PTHREAD_MUTEX_INITIALIZER;

void hdbgPrint (void) {
  lock_ptr (& hdbgLock);
  simPrintf ("hdbg print\n");
  if (! hevents)
    goto done;
  hev_t * t = hevents;
  hevents = NULL;
  char fname[256];
  static int nlist = 0;
  sprintf (fname, "hdbg%04d.list", nlist);
  nlist ++;
  hdbgOut = fopen (fname, "w");
  if (! hdbgOut) {
    simPrintf ("can't open hdbg.list\n");
    goto done;
  }
  time_t curtime;
  time (& curtime);
  (void) fprintf (hdbgOut, "%s\n", ctime (& curtime));

  for (unsigned long p = 0; p < hdbgSize; p ++) {
    unsigned long q = (hevtPtr + p) % hdbgSize;
    hev_t * evtp = t + q;
    hdbgPrintEvent (evtp, hdbgOut);
  }
  fclose (hdbgOut);

  int fd = open ("M.dump", O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR);
  if (fd == -1) {
    simPrintf ("can't open M.dump\n");
    goto done;
  }
  // cast discards volatile
  atomic_thread_fence (memory_order_seq_cst);
  /* ssize_t n = */ write (fd, (const void *) M, MEMSIZE * sizeof (word36));
  close (fd);
done: ;
  unlock_ptr (& hdbgLock);
}

static void createBuffer (void) {
  lock_ptr (& hdbgLock);
  if (hevents) {
    FREE (hevents);
    hevents = NULL;
  }
  if (hdbgSize <= 0)
    return;
  hevents = malloc (sizeof (hev_t) * hdbgSize);
  if (! hevents) {
    simPrintf ("hdbg createBuffer failed\n");
    return;
  }
  (void) memset (hevents, 0, sizeof (hev_t) * hdbgSize);

  hevtPtr = 0;
  unlock_ptr (& hdbgLock);
}

static long hdbg_inc (void) {
  //hevtPtr = (hevtPtr + 1) % hdbgSize;
  long ret = __sync_fetch_and_add (& hevtPtr, 1l) % hdbgSize;

  if (hevtMark > 0) {
    long ret = __sync_fetch_and_sub (& hevtMark, 1l);
    if (ret <= 0)
      hdbgPrint ();
  }
  return ret;
}

//#ifdef SYNCTEST
#if 0
# define hev(t, tf, filter) \
  if (! hevents) \
    goto done; \
  if (0 && ! sys.syncClockMode) \
    goto done; \
  unsigned long p = hdbg_inc (); \
  hevents[p].type = t; \
  hevents[p].cpu_idx = cpup ? cpu.cpuIdx : 0; \
  hevents[p].time = cpup ? cpu.cycleCnt : 0; \
  strncpy (hevents[p].ctx, ctx, CTX_SZ - 1); \
  hevents[p].ctx[CTX_SZ - 1] = 0; \
  hevents[p].rw = tf;
#else
# define hev(t, tf, filter) \
  if (! hevents) \
    goto done; \
  if (0 && ! sys.syncClockMode) \
    goto done; \
  if (cpup && !TST_I_NBAR) \
    goto done; \
  if (cpup && filter && hdbgSegNum >= 0 && hdbgSegNum != cpu.PPR.PSR) \
    goto done; \
  if (cpup && filter && hdbgSegNum > 0 && blacklist[cpu.PPR.IC]) \
    goto done; \
  if (cpup && hdbgCPUMask && ((hdbgCPUMask & (1 << cpu.cpuIdx)) == 0)) \
    goto done; \
  unsigned long p = hdbg_inc (); \
  hevents[p].type = t; \
  hevents[p].cpu_idx = cpup ? cpu.cpuIdx : 0; \
  hevents[p].time = cpup ? cpu.cycleCnt : 0; \
  strncpy (hevents[p].ctx, ctx, CTX_SZ - 1); \
  hevents[p].ctx[CTX_SZ - 1] = 0; \
  hevents[p].rw = tf;
#endif

# define FILTER true
# define NO_FILTER false

# define WR true
# define RD false

void hdbgTrace (cpuState_t * cpup, const char * ctx) {
  //lock_ptr (& hdbgLock);
  hev (hevtTrace, RD, FILTER);
  hevents[p].trace.addrMode = get_addr_mode (cpup);
  hevents[p].trace.segno    = cpu.PPR.PSR;
  hevents[p].trace.ic       = cpu.PPR.IC;
  hevents[p].trace.ring     = cpu.PPR.PRR;
  hevents[p].trace.inst     = IWB_IRODD;
  if (hdbgLiveFlag)
   hdbgPrintEvent (hevents + p, stdout);
done: ;
  //unlock_ptr (& hdbgLock);
}

void hdbgMRead (cpuState_t * cpup, word24 addr, word36 data, const char * ctx) {
  hev (hevtM, RD, FILTER);
  hevents[p].memref.addr = addr;
  hevents[p].memref.data = data;
  if (hdbgLiveFlag)
   hdbgPrintEvent (hevents + p, stdout);
done: ;
}

void hdbgMWrite (cpuState_t * cpup, word24 addr, word36 data, const char * ctx) {
  hev (hevtM, WR, FILTER);
  hevents[p].memref.addr = addr;
  hevents[p].memref.data = data;
  if (hdbgLiveFlag)
   hdbgPrintEvent (hevents + p, stdout);
done: ;
}

void hdbgAPURead (cpuState_t * cpup, word15 segno, word18 offset, word24 final, word36 data, const char * ctx) {
  hev (hevtAPU, RD, FILTER);
  hevents[p].apu.segno  = segno;
  hevents[p].apu.offset = offset;
  hevents[p].apu.final  = final;
  hevents[p].apu.data   = data;
  if (hdbgLiveFlag)
   hdbgPrintEvent (hevents + p, stdout);
done: ;
}

void hdbgAPUWrite (cpuState_t * cpup, word15 segno, word18 offset, word24 final, word36 data, const char * ctx) {
  hev (hevtAPU, WR, FILTER);
  hevents[p].apu.segno  = segno;
  hevents[p].apu.offset = offset;
  hevents[p].apu.final  = final;
  hevents[p].apu.data   = data;
  if (hdbgLiveFlag)
   hdbgPrintEvent (hevents + p, stdout);
done: ;
}

void hdbgFault (cpuState_t * cpup, _fault faultNumber, _fault_subtype subFault, const char * faultMsg, const char * ctx) {
  hev (hevtFault, RD, FILTER);
  hevents[p].fault.faultNumber  = faultNumber;
  hevents[p].fault.subFault     = subFault;
  strncpy (hevents[p].fault.faultMsg, faultMsg, 63);
  hevents[p].fault.faultMsg[63] = 0;
  if (hdbgLiveFlag)
   hdbgPrintEvent (hevents + p, stdout);
done: ;
}

void hdbgIntrCellSet (cpuState_t * cpup, uint scuUnitIdx, uint cell, const char * ctx) {
  hev (hevtIntrCellSet, RD, FILTER);
  hevents[p].intrCellSet.cellnum       = cell;
  if (hdbgLiveFlag)
   hdbgPrintEvent (hevents + p, stdout);
done: ;
}

void hdbgIntrSet (cpuState_t * cpup, uint inum, uint cpuUnitIdx, uint scuUnitIdx, const char * ctx) {
  hev (hevtIntrSet, RD, FILTER);
  hevents[p].intrSet.inum       = inum;
  hevents[p].intrSet.cpuUnitIdx = cpuUnitIdx;
  hevents[p].intrSet.scuUnitIdx = scuUnitIdx;
  if (hdbgLiveFlag)
   hdbgPrintEvent (hevents + p, stdout);
done: ;
}

void hdbgIntr (cpuState_t * cpup, uint intr_pair_addr, const char * ctx) {
  hev (hevtIntr, RD, FILTER);
  hevents[p].cpu_idx             = cpu.cpuIdx;
  hevents[p].time                = cpu.cycleCnt;
  strncpy (hevents[p].ctx, ctx, CTX_SZ - 1);
  hevents[p].ctx[CTX_SZ - 1]             = 0;
  hevents[p].intr.intr_pair_addr = intr_pair_addr;
  if (hdbgLiveFlag)
   hdbgPrintEvent (hevents + p, stdout);
done: ;
}

void hdbgRegR (cpuState_t * cpup, enum hregs_t type, word36 data, const char * ctx) {
  hev (hevtReg, RD, FILTER);
  hevents[p].reg.type = type;
  hevents[p].reg.data = data;
  if (hdbgLiveFlag)
   hdbgPrintEvent (hevents + p, stdout);
done: ;
}

void hdbgRegW (cpuState_t * cpup, enum hregs_t type, word36 data, const char * ctx) {
  hev (hevtReg, WR, FILTER);
  hevents[p].reg.type = type;
  hevents[p].reg.data = data;
  if (hdbgLiveFlag)
   hdbgPrintEvent (hevents + p, stdout);
done: ;
}

void hdbgPARegR (cpuState_t * cpup, enum hregs_t type, par_s * data, const char * ctx) {
  hev (hevtPAReg, RD, FILTER);
  hevents[p].par.type = type;
  hevents[p].par.data = * data;
  if (hdbgLiveFlag)
   hdbgPrintEvent (hevents + p, stdout);
done: ;
}

void hdbgPARegW (cpuState_t * cpup, enum hregs_t type, par_s * data, const char * ctx) {
  hev (hevtPAReg, WR, FILTER);
  hevents[p].par.type = type;
  hevents[p].par.data = * data;
  if (hdbgLiveFlag)
   hdbgPrintEvent (hevents + p, stdout);
done: ;
}

# if 0
void hdbgDSBRRegR (enum hregs_t type, dsbr_s * data, const char * ctx) {
  hev (hevtDSBRReg, RD, FILTER);
  hevents[p].dsbr.type = type;
  hevents[p].dsbr.data = * data;
  if (hdbgLiveFlag)
   hdbgPrintEvent (hevents + p, stdout);
done: ;
}

void hdbgDSBRRegW (enum hregs_t type, dsbr_s * data, const char * ctx) {
  hev (hevtDSBRReg, WR, FILTER);
  hevents[p].dsbr.type = type;
  hevents[p].dsbr.data = * data;
  if (hdbgLiveFlag)
   hdbgPrintEvent (hevents + p, stdout);
done: ;
}
# endif

void hdbgIEFP (cpuState_t * cpup, enum hdbgIEFP_e type, word15 segno, word18 offset, const char * ctx) {
  hev (hevtIEFP, RD, FILTER);
  hevents [p].iefp.type   = type;
  hevents [p].iefp.segno  = segno;
  hevents [p].iefp.offset = offset;
  if (hdbgLiveFlag)
   hdbgPrintEvent (hevents + p, stdout);
done: ;
}

void hdbgNote (cpuState_t * cpup, const char * ctx, const char * fmt, ...) {
  hev (hevtNote, RD, FILTER);
#if 0
  if (! hevents)
    goto done;
  unsigned long p = hdbg_inc ();
  hevents[p].type = hevtNote;
  hevents[p].cpu_idx = 0;
  hevents[p].time = 0; /* cpu.cycleCnt; */
  strncpy (hevents[p].ctx, ctx, CTX_SZ-1 );
  hevents[p].ctx[CTX_SZ-1] = 0;
  hevents[p].rw = RD;
#endif

  va_list arglist;
  va_start (arglist, fmt);
  vsnprintf (hevents [p].note.noteBody, NOTE_SZ - 1, fmt, arglist);
  va_end (arglist);
  if (hdbgLiveFlag)
   hdbgPrintEvent (hevents + p, stdout);
done: ;
}

# if 0
void hdbg_mark (void) {
  hevtMark = hdbgSize;
  simPrintf ("hdbg mark set to %ld\n", (long) hevtMark);
}
# endif

simRc hdbg_cpu_mask (UNUSED int32_t arg, const char * buf)
  {
    hdbgCPUMask = strtoul (buf, NULL, 0);
    simPrintf ("hdbg CPU mask set to %ld\n", (long) hdbgCPUMask);
    return SCPE_OK;
  }

// set buffer size
simRc hdbg_size (UNUSED int32_t arg, const char * buf) {
  hdbgSize = strtoul (buf, NULL, 0);
  simPrintf ("hdbg size set to %ld\n", (long) hdbgSize);
  createBuffer ();
  return SCPE_OK;
}

// set target segment number
simRc hdbgSegmentNumber (UNUSED int32_t arg, const char * buf) {
  hdbgSegNum = strtoul (buf, NULL, 8);
  simPrintf ("hdbg target segment number set to %lu\n", hdbgSegNum);
  //createBuffer ();
  return SCPE_OK;
}

simRc hdbgBlacklist (UNUSED int32_t arg, const char * buf) {
  char work[strlen (buf) + 1];
  if (sscanf (buf, "%s", work) != 1)
    return SCPE_ARG;
  if (strcasecmp (work, "init") == 0) {
    memset (blacklist, 0, sizeof (blacklist));
    return SCPE_OK;
  }
  uint low, high;
  if (sscanf (work, "%o-%o", & low, & high) != 2)
    return SCPE_ARG;
  if (low > MAX18 || high > MAX18)
    return SCPE_ARG;
  for (uint addr = low; addr <= high; addr ++)
    blacklist[addr] = true;
  return SCPE_OK;
}

simRc hdbgLive (int32_t arg, const char * buf) {
  hdbgLiveFlag = arg == 0;
  return SCPE_OK;
}

simRc hdbg_print (UNUSED int32_t arg, const char * buf) {
  hdbgPrint ();
  return SCPE_OK;
}

#endif // if defined(HDBG)
