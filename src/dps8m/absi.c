/*
 * vim: filetype=c:tabstop=4:ai:expandtab
 * SPDX-License-Identifier: ICU
 * scspell-id: 336ce0b0-f62d-11ec-873c-80ee73e9b8e7
 *
 * ---------------------------------------------------------------------------
 *
 * Copyright (c) 2007-2013 Michael Mondy
 * Copyright (c) 2015-2024 Charles Anthony
 * Copyright (c) 2021-2024 The DPS8M Development Team
 *
 * This software is made available under the terms of the ICU License.
 * See the LICENSE.md file at the top-level directory of this distribution.
 *
 * ---------------------------------------------------------------------------
 */

#include <stdio.h>
#include <ctype.h>
#include <unistd.h>
#include <stdint.h>

#include "dps8m.h"
#include "iom_channel.h"
#include "iom.h"
#include "absi.h"
#include "cable.h"
#include "utils.h"

#include "udplib.h"

#if defined(FREE)
# undef FREE
#endif /* if defined(FREE) */
#define FREE(p) do  \
  {                 \
    free((p));      \
    (p) = NULL;     \
  } while(0)

#if defined(WITH_ABSI_DEV)
//# define DBG_CTR 1

static struct absi_state
  {
    char device_name [MAX_DEV_NAME_LEN];
    int link;
  } absi_state [N_ABSI_UNITS_MAX];

# define N_ABSI_UNITS 1 // default

# define UNIT_FLAGS ( UNIT_FIX | UNIT_ATTABLE | UNIT_ROABLE | UNIT_DISABLE | \
                     UNIT_IDLE )
UNIT absi_unit[N_ABSI_UNITS_MAX] =
  {
    {UDATA (NULL, UNIT_FLAGS, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL}
  };

# define ABSI_UNIT_IDX(uptr) ((uptr) - absi_unit)

static simRc absi_show_nunits (UNUSED FILE * st, UNUSED UNIT * uptr,
                                UNUSED int val, UNUSED const void * desc)
  {
    simPrintf ("Number of ABSI units in system is %d\n", absi_dev.numunits);
    return SCPE_OK;
  }

static simRc absi_set_nunits (UNUSED UNIT * uptr, UNUSED int32_t value,
                               const char * cptr, UNUSED void * desc)
  {
    if (! cptr)
      return SCPE_ARG;
    int n = atoi (cptr);
    if (n < 0 || n > N_ABSI_UNITS_MAX)
      return SCPE_ARG;
    absi_dev.numunits = (uint32_t) n;
    return SCPE_OK;
  }

static simRc absi_show_device_name (UNUSED FILE * st, UNIT * uptr,
                                    UNUSED int val, UNUSED const void * desc)
  {
    int n = (int) ABSI_UNIT_IDX (uptr);
    if (n < 0 || n >= N_ABSI_UNITS_MAX)
      return SCPE_ARG;
    if (absi_state[n].device_name[1] != 0)
      simPrintf("name     : %s", absi_state[n].device_name);
      else
        simPrintf("name     : ABSI%d", n);
    return SCPE_OK;
  }

static simRc absi_set_device_name (UNIT * uptr, UNUSED int32_t value,
                                   const char * cptr, UNUSED void * desc)
  {
    int n = (int) ABSI_UNIT_IDX (uptr);
    if (n < 0 || n >= N_ABSI_UNITS_MAX)
      return SCPE_ARG;
    if (cptr)
      {
        strncpy (absi_state[n].device_name, cptr, MAX_DEV_NAME_LEN-1);
        absi_state[n].device_name[MAX_DEV_NAME_LEN-1] = 0;
      }
    else
      absi_state[n].device_name[0] = 0;
    return SCPE_OK;
  }

# define UNIT_WATCH UNIT_V_UF

static MTAB absi_mod[] =
  {
# if !defined(SPEED)
    { UNIT_WATCH, 1, "WATCH",   "WATCH",   0, 0, NULL, NULL },
    { UNIT_WATCH, 0, "NOWATCH", "NOWATCH", 0, 0, NULL, NULL },
# endif /* if !defined(SPEED) */
    {
      MTAB_XTD | MTAB_VDV | MTAB_NMO | MTAB_VALR,  /* Mask               */
      0,                                           /* Match              */
      "NUNITS",                                    /* Print string       */
      "NUNITS",                                    /* Match string       */
      absi_set_nunits,                             /* Validation routine */
      absi_show_nunits,                            /* Display routine    */
      "Number of ABSI units in the system",        /* Value descriptor   */
      NULL                                         /* Help               */
    },
    {
      MTAB_XTD | MTAB_VUN | MTAB_VALR | MTAB_NC,   /* Mask               */
      0,                                           /* Match              */
      "NAME",                                      /* Print string       */
      "NAME",                                      /* Match string       */
      absi_set_device_name,                        /* Validation routine */
      absi_show_device_name,                       /* Display routine    */
      "Set the device name",                       /* Value descriptor   */
      NULL                                         /* Help               */
    },
    MTAB_eol
  };

static simRc absi_reset (UNUSED DEVICE * dptr)
  {
    //absiResetRX (0);
    //absiResetTX (0);
    return SCPE_OK;
  }

static simRc absiAttach (UNIT * uptr, const char * cptr)
  {
    if (! cptr)
      return SCPE_ARG;
    int unitno = (int) (uptr - absi_unit);

    //    ATTACH HIn llll:w.x.y.z:rrrr - connect via UDP to a remote

    simRc ret;
    char * pfn;
    //uint16 imp = 0; // we only support a single attachment to a single IMP

    // If we're already attached, then detach ...
    if ((uptr->flags & UNIT_ATT) != 0)
      detach_unit (uptr);

    // Make a copy of the "file name" argument.  udp_create() actually modifies
    // the string buffer we give it, so we make a copy now so we'll have
    // something to display in the "SHOW HIn ..." command.
    pfn = (char *) calloc ((CBUFSIZE + 1), sizeof (char));
    if (pfn == NULL)
      return SCPE_MEM;
    strncpy (pfn, cptr, CBUFSIZE);

    // Create the UDP connection.
    ret = udp_create (cptr, & absi_state[unitno].link);
    if (ret != SCPE_OK)
      {
        FREE (pfn);
        return ret;
      }

    uptr->flags |= UNIT_ATT;
    uptr->filename = pfn;
    return SCPE_OK;
  }

// Detach (connect) ...
static simRc absiDetach (UNIT * uptr)
  {
    int unitno = (int) (uptr - absi_unit);
    simRc ret;
    if ((uptr->flags & UNIT_ATT) == 0)
      return SCPE_OK;
    if (absi_state[unitno].link == NOLINK)
      return SCPE_OK;

    ret = udp_release (absi_state[unitno].link);
    if (ret != SCPE_OK)
      return ret;
    absi_state[unitno].link = NOLINK;
    uptr->flags &= ~ (unsigned int) UNIT_ATT;
    FREE (uptr->filename);
    return SCPE_OK;
  }

DEVICE absi_dev = {
    "ABSI",       /* Name                */
    absi_unit,    /* Units               */
    NULL,         /* Registers           */
    absi_mod,     /* Modifiers           */
    N_ABSI_UNITS, /* #units              */
    10,           /* Address radix       */
    24,           /* Address width       */
    1,            /* Address increment   */
    8,            /* Data radix          */
    36,           /* Data width          */
    NULL,         /* Examine             */
    NULL,         /* Deposit             */
    absi_reset,   /* Reset               */
    NULL,         /* Boot                */
    absiAttach,   /* Attach              */
    absiDetach,   /* Detach              */
    NULL,         /* Context             */
    DEV_DEBUG,    /* Flags               */
    0,            /* Debug control flags */
    NULL,         /* Debug flag names    */
    NULL,         /* Memory size change  */
    NULL,         /* Logical name        */
    NULL,         /* Help                */
    NULL,         /* Attach help         */
    NULL,         /* Attach context      */
    NULL,         /* Description         */
    NULL          /* End                 */
};

/*
 * absi_init()
 */

// Once-only initialization

void absi_init (void)
  {
    (void)memset (absi_state, 0, sizeof (absi_state));
    for (int i = 0; i < N_ABSI_UNITS_MAX; i ++)
      absi_state[i].link = NOLINK;
  }

static iomCmdRc absi_cmd (chnlState_t * chnlp)
  {
// simPrintf ("absi_cmd CHAN_CMD %o DEV_CODE %o DEV_CMD %o COUNT %o\n",
//chnlp->IDCW_CHAN_CMD, chnlp->IDCW_DEV_CODE, chnlp->IDCW_DEV_CMD, chnlp->IDCW_COUNT);
    //sim_debug (DBG_TRACE, & absi_dev,
               //"absi_cmd CHAN_CMD %o DEV_CODE %o DEV_CMD %o COUNT %o\n",
               //chnlp->IDCW_CHAN_CMD, chnlp->IDCW_DEV_CODE, chnlp->IDCW_DEV_CMD,
               //chnlp->IDCW_COUNT);

    // Not IDCW?
    if (IS_NOT_IDCW (chnlp))
      {
        simWarn ("%s: Unexpected IOTx\n", __func__);
        return IOM_CMD_ERROR;
      }

    switch (chnlp->IDCW_DEV_CMD)
      {
        case 000: // CMD 00 Request status
          {
            chnlp->stati = 04000;
simPrintf ("absi request status\n");
          }
          break;

        case 001: // CMD 01 Read
          {
            chnlp->stati = 04000;
simPrintf ("absi read\n");
          }
          break;

        case 011: // CMD 11 Write
          {
            chnlp->stati = 04000;
simPrintf ("absi write\n");
          }
          break;

        case 020: // CMD 20 Host switch down
          {
            chnlp->stati = 04000;
simPrintf ("absi host switch down\n");
          }
          break;

        case 040: // CMD 40 Reset status
          {
            chnlp->stati = 04000;
simPrintf ("absi reset status\n");
          }
          break;

        case 060: // CMD 60 Host switch up
          {
            chnlp->stati = 04000;
simPrintf ("absi host switch up\n");
          }
          break;

        default:
          {
            if (chnlp->IDCW_DEV_CMD != 051) // ignore bootload console probe
              simWarn ("%s: ABSI unrecognized device command  %02o\n", __func__, chnlp->IDCW_DEV_CMD);
            chnlp->stati = 04501; // cmd reject, invalid opcode
            chnlp->chanStatus = chanStatIncorrectDCW;
          }
          return IOM_CMD_ERROR;
      }

    if (chnlp->IDCW_CHAN_CMD == 0)
      return IOM_CMD_DISCONNECT; // Don't do DCW list
    return IOM_CMD_PROCEED;
  }

//  1 == ignored command
//  0 == ok
// -1 == problem
iomCmdRc absi_iom_cmd (chnlState_t * chnlp) {
  // Is it an IDCW?
  if (IS_IDCW (chnlp))
    return absi_cmd (chnlp);
  simPrintf ("%s expected IDCW\n", __func__);
  return IOM_CMD_ERROR;
}

void absi_process_event (void)
  {
# define psz 17000
    uint16_t pkt[psz];
    for (uint32_t unit = 0; unit < absi_dev.numunits; unit ++)
      {
        if (absi_state[unit].link == NOLINK)
          continue;
        //int sz = udp_receive ((int) unit, pkt, psz);
        int sz = udp_receive (absi_state[unit].link, pkt, psz);
        if (sz < 0)
          {
            (void)fprintf (stderr, "udp_receive failed\n");
          }
        else if (sz == 0)
          {
            //(void)fprintf (stderr, "udp_receive 0\n");
          }
        else
          {
            for (int i = 0; i < sz; i ++)
              {
                (void)fprintf (stderr, "  %06o  %04x  ", pkt[i], pkt[i]);
                for (int b = 0; b < 16; b ++)
                  (void)fprintf (stderr, "%c", pkt[i] & (1 << (16 - b)) ? '1' : '0');
                (void)fprintf (stderr, "\n");
              }
            // Send a NOP reply
            //int16_t reply[2] = 0x0040
            int rc = udp_send (absi_state[unit].link, pkt, (uint16_t) sz,
                               PFLG_FINAL);
            if (rc < 0)
              {
                (void)fprintf (stderr, "udp_send failed\n");
              }
          }
      }
  }
#endif /* if defined(WITH_ABSI_DEV) */
