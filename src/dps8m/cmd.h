/*
 * vim: filetype=c:tabstop=4:ai:expandtab
 * SPDX-License-Identifier: ICU
 * scspell-id: 5d03518d-f62d-11ec-a03b-80ee73e9b8e7
 *
 * ---------------------------------------------------------------------------
 *
 * Copyright (c) 2023-2024 Charles Anthony
 * Copyright (c) 2023-2024 The DPS8M Development Team
 *
 * This software is made available under the terms of the ICU License.
 * See the LICENSE.md file at the top-level directory of this distribution.
 *
 * ---------------------------------------------------------------------------
 */


// Longest parsable item
#define PARSE_LIMIT 4097

typedef char token[PARSE_LIMIT];

struct name_s {
  char name[4]; 
  bool deviceLetterSet;
  bool deviceLetter[26]; // 'a' -- 'z'
  bool deviceCodeSet;
  bool deviceCode[64]; // 00 -- 63
};
enum nameTypes_e { ntErr, ntDev, ntDevLtr, ntDevLtrDev };
typedef enum tokenType_e { tokenEmpty, tokenWord } tokenType;


void cmdParseError (const char * string, const char * p, const char * msg);
const char * cmdToken (const char * string, const char * p, token * t, tokenType * tt);
void cmdInit (void);

