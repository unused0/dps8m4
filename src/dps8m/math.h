/*
 * vim: filetype=c:tabstop=4:ai:expandtab
 * SPDX-License-Identifier: ICU
 * scspell-id: a3d21800-f62e-11ec-8838-80ee73e9b8e7
 *
 * ---------------------------------------------------------------------------
 *
 * Copyright (c) 2007-2013 Michael Mondy
 * Copyright (c) 2012-2016 Harry Reed
 * Copyright (c) 2013-2024 Charles Anthony
 * Copyright (c) 2021-2024 The DPS8M Development Team
 *
 * This software is made available under the terms of the ICU License.
 * See the LICENSE.md file at the top-level directory of this distribution.
 *
 * ---------------------------------------------------------------------------
 */

long double EAQToIEEElongdouble(cpuState_t * cpup);
#if defined(__MINGW32__) || defined(__MINGW64__)
double EAQToIEEEdouble(cpuState_t * cpup);
#endif
#if !defined(QUIET_UNUSED)
float72 IEEElongdoubleToFloat72(long double f);
void IEEElongdoubleToEAQ(long double f0);
double float36ToIEEEdouble(float36 f36);
float36 IEEEdoubleTofloat36(double f);
#endif /* if !defined(QUIET_UNUSED) */
void ufa (cpuState_t * cpup, bool sub, bool normalize);
void ufs (cpuState_t * cpup);
void fno (cpuState_t * cpup, word8 * E, word36 * A, word36 * Q);
void fno_ext (cpuState_t * cpup, int * e0, word8 * E, word36 * A, word36 * Q);

void fneg (cpuState_t * cpup);
void ufm (cpuState_t * cpup, bool normalize);
void fdv (cpuState_t * cpup);
void fdi (cpuState_t * cpup);
void frd (cpuState_t * cpup);
void fcmp(cpuState_t * cpup);
void fcmg(cpuState_t * cpup);

//void dufa (void);
//void dufs (void);
void dufa (cpuState_t * cpup, bool subtract, bool normalize);
void dufm (cpuState_t * cpup, bool normalize);
void dfdv (cpuState_t * cpup);
void dfdi (cpuState_t * cpup);
void dfrd (cpuState_t * cpup);
void dfcmp (cpuState_t * cpup);
void dfcmg (cpuState_t * cpup);

void dvf (cpuState_t * cpup);

void dfstr (cpuState_t * cpup, word36 *Ypair);
void fstr(cpuState_t * cpup, word36 *CY);
