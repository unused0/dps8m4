// MTB-652
//   flagbox @ 24000
#define FLAGBOX_ABSADDR 024000

// flagbox.incl.alm
//
//         equ     fgbx.slt_segno,33               " UPPER
#define fgbx_slt_segno 33
//         equ     fgbx.sentinel,36
#define fgbx_sentinel 36
//        equ     fgbx.sst_sdw,44         " DOUBLE
#define fgbx_sst_sdw 44
//        equ     fgbx.hc_dbr,46  
#define fgbx_hc_dpr 46

