/*
 * vim: filetype=c:tabstop=4:ai:expandtab
 * SPDX-License-Identifier: ICU
 * scspell-id: 79aba365-f62e-11ec-b567-80ee73e9b8e7
 *
 * ---------------------------------------------------------------------------
 *
 * Copyright (c) 2007-2013 Michael Mondy
 * Copyright (c) 2012-2016 Harry Reed
 * Copyright (c) 2013-2024 Charles Anthony
 * Copyright (c) 2015 Eric Swenson
 * Copyright (c) 2021-2024 The DPS8M Development Team
 *
 * This software is made available under the terms of the ICU License.
 * See the LICENSE.md file at the top-level directory of this distribution.
 *
 * ---------------------------------------------------------------------------
 */

void ReadAPUDataRead (cpuState_t * cpup, word18 addr, word36 *dat);
void ReadOperandRead (cpuState_t * cpup, word18 addr, word36 *dat);
void ReadOperandRMW (cpuState_t * cpup, word18 addr, word36 *dat);
void ReadAPUDataRMW (cpuState_t * cpup, word18 addr, word36 *dat);
void ReadInstructionFetch (cpuState_t * cpup, word18 addr, word36 *dat);
void ReadIndirectWordFetch (cpuState_t * cpup, word18 address, word36 * result);
void Read2OperandRead (cpuState_t * cpup, word18 address, word36 * result);
void Read2OperandRMW (cpuState_t * cpup, word18 address, word36 * result);
void Read2InstructionFetch (cpuState_t * cpup, word18 address, word36 * result);
void Read2RTCDOperandFetch (cpuState_t * cpup, word18 address, word36 * result);
void Read2IndirectWordFetch (cpuState_t * cpup, word18 address, word36 * result);
void WriteAPUDataStore (cpuState_t * cpup, word18 addr, word36 dat);
void WriteOperandStore (cpuState_t * cpup, word18 addr, word36 dat);
void Write2OperandStore (cpuState_t * cpup, word18 address, word36 * data);
void Write1 (cpuState_t * cpup, word18 address, word36 data, bool isAR);
void Write8 (cpuState_t * cpup, word18 address, word36 * data, bool isAR);
void Write16 (cpuState_t * cpup, word18 address, word36 * data);
void Write32 (cpuState_t * cpup, word18 address, word36 * data);
void Read8 (cpuState_t * cpup, word18 address, word36 * result, bool isAR);
void Read16 (cpuState_t * cpup, word18 address, word36 * result);
void WritePage (cpuState_t * cpup, word18 address, word36 * data, bool isAR);
void ReadPage (cpuState_t * cpup, word18 address, word36 * result, bool isAR);
void ReadIndirect (cpuState_t * cpup);
