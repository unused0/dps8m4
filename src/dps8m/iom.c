/*
 * vim: filetype=c:tabstop=4:ai:expandtab
 * SPDX-License-Identifier: ICU
 * SPDX-License-Identifier: Multics
 * scspell-id: 89f10936-f62e-11ec-b310-80ee73e9b8e7
 *
 * ---------------------------------------------------------------------------
 *
 * Copyright (c) 2007-2013 Michael Mondy
 * Copyright (c) 2012-2016 Harry Reed
 * Copyright (c) 2013-2024 Charles Anthony
 * Copyright (c) 2021-2023 The DPS8M Development Team
 *
 * This software is made available under the terms of the ICU License.
 * See the LICENSE.md file at the top-level directory of this distribution.
 *
 * ---------------------------------------------------------------------------
 *
 * This source file may contain code comments that adapt, include, and/or
 * incorporate Multics program code and/or documentation distributed under
 * the Multics License.  In the event of any discrepancy between code
 * comments herein and the original Multics materials, the original Multics
 * materials should be considered authoritative unless otherwise noted.
 * For more details and historical background, see the LICENSE.md file at
 * the top-level directory of this distribution.
 *
 * ---------------------------------------------------------------------------
 */

//
// IOM rework # 7
//

// Implementation notes:
//
// The IOM can be configured to operate in GCOS, Extended GCOS, and Paged modes.
// Only the Paged mode is supported.
//
// Backup list service not implemented
// Wraparound channel not implemented
// Snapshot channel not implemented
// Scratchpad Access channel not implemented

// Direct data service

// Pg: B20 "Address Extension bits will be used for core selection when the
//          "the IOM is [...] in Paged mode for a non-paged channel.

// Definitions:
//  unit number -- ambiguous: May indicate either the Multics
//                 designation for IOMA, IOMB, etc. or the scp
//                 UNIT table offset.
//    unit_idx --  always refers to the scp unit table offset.
//
//

// Functional layout:

// CPU emulator:
//   CIOC command -> SCU cable routing to IOM emulator "iom_connect"
//
// IOM emulator:
//   iom_connect performs the connect channel operations of fetching
//   the PCW, parsing out the channel number and initial DCW.
//
//   The channel number and initial DCW are passed to doPayloadChannel
//
//   Using the cable tables, doPayloadChannel determines the handler and
//   unit_idx for that channel number and calls
//     iom_unit[channel_number].iom_cmd (unit_idx, DCW)
//   It then walks the channel DCW list, passing DCWs to the handler.
//

//        make_idcw IDCW_name,
//                  Command,
//                  Device,
//                  {record,multirecord,character,nondata},
//                  {proceed,terminate,marker},
//                  [ChanData,]
//                  [Extension,]
//                  [mask,]
//
//   record      0
//   nondata     2
//   multirecord 6
//   character   8
//
//   terminate   0
//   proceed     2
//   marker      3
//
// Observed channel command dialects
//
// IOM boot
// CIOC 0
// Connect channel LPW 000000040000 (case c)
//   DCW pointer 000000
//     RES              0
//     REL              0
//     AE               0
//     NC 21            1
//     TAL 22           0
//     REL              0
//     TALLY         0000
//
// PCW 000000720201 012000000000
//   Chan info     0012
//   Addr ext        00
//   111              7
//   M                0
//   Chan info  22046447
//   Pg Tbl Ptr  000000
//   PTP              0
//   PGE              0
//   AUX              0
//
// MTP Tape channel LPW 000004020002 (case b)
//   DCW pointer 000004
//     RES              0
//     REL              0
//     AE               0
//     NC 21            0
//     TAL 22           1
//     REL              0
//     TALLY         0002
//
// DCW list dump:
//   IDCW 000000720201
//     cmd             00 Request Status
//     dev code        00
//     ctrl             2 (proceed)
//     chancmd          2 (nondata)
//   IDCW 050000700000
//     cmd             05 Rd Bin Rcrd
//     dev code        00
//     ctrl             0 (terminate)
//     chancmd          0 (record)
//   IOTD (0) 000030000000
//     tally       0000
//     addr            30
//     cp               0
//   IOTD (0) 400000000000000000000
//     tally       0000
//     addr            00
//     cp               0
//   IOTD (0) 400000000000000000000
//     tally       0000
//     addr            00
//     cp               0
//
// Tape commands done:
//   Tape Request Status
//   Tape Read Binary Record
//   IOTD (0) 000030000000
//     tally       0000
//     addr            30
//     cp               0
//   Tape IOT Read
//     Tape 0 reads record 1
//   ctrl == 0 in chan 10 (12) DCW

// Bootload check tape controller firmware loaded test
//
// CIOC 138
// Connect channel LPW 000000040000 (case c)
//   DCW pointer 000000
//     RES              0
//     REL              0
//     AE               0
//     NC 21            1
//     TAL 22           0
//     REL              0
//     TALLY         0000
//
// PCW 000000720201 012000000000
//   Chan info     0012
//   Addr ext        00
//   111              7
//   M                0
//   Chan info    22046447
//   Pg Tbl Ptr  000000
//   PTP              0
//   PGE              0
//   AUX              0
//
// MTP Tape channel LPW 000540007777 (case a)
//   DCW pointer 000540
//     RES              0
//     REL              0
//     AE               0
//     NC 21            0
//     TAL 22           0
//     REL              0
//     TALLY         7777
//
// DCW list dump:
//   IDCW 000000720201
//     cmd             00 Request Status
//     dev code        00
//     ctrl             2 (proceed)
//     chancmd          2 (nondata)
//   IDCW 510000700200
//     cmd             51 Reset Dev Stat/Alert
//     dev code        00
//     ctrl             0 (terminate)
//     chancmd          2 (nondata)
//   IDCW 000000700200
//     cmd             00 Request Status
//     dev code        00
//     ctrl             0 (terminate)
//     chancmd          2 (nondata)
//   IOTD (0) 200000000000
//     tally       0000
//     addr            200000
//     cp               0
//
// Tape commands done:
//   Tape Request Status
//   Tape Reset Device Status
//   ctrl == 0 in chan 10 (12) DCW

// Bootload tape read
//
// CIOC 7999826
// Connect channel LPW 000000040000 (case c)
//   DCW pointer 000000
//     RES              0
//     REL              0
//     AE               0
//     NC 21            1
//     TAL 22           0
//     REL              0
//     TALLY         0000
//
// PCW 000000720201 012000000000
//   Chan info     0012
//   Addr ext        00
//   111              7
//   M                0
//   Chan info    22046447
//   Pg Tbl Ptr  000000
//   PTP              0
//   PGE              0
//   AUX              0
//
// MTP Tape channel LPW 000621007777 (case a)
//   DCW pointer 000621
//     RES              0
//     REL              0
//     AE               0
//     NC 21            0
//     TAL 22           0
//     REL              0
//     TALLY         7777
//
// DCW list dump:
//   IDCW 000000720201
//     cmd             00 Request Status
//     dev code        00
//     ctrl             2 (proceed)
//     chancmd          2 (nondata)
//   IDCW 050000700000
//     cmd             05 Rd Bin Rcrd
//     dev code        00
//     ctrl             0 (terminate)
//     chancmd          0 (record)
//   IOTD (0) 060001002020
//     tally       2020
//     addr            60001
//     cp               0
//   IOTD (0) 024000000000
//     tally       0000
//     addr            24000
//     cp               0
//
// Tape commands done:
//   Tape Request Status
//   Tape Read Binary Record
//   IOTD (0) 060001002020
//     tally       2020
//     addr            60001
//     cp               0
//   Tape IOT Read
//   ctrl == 0 in chan 10 (12) DCW

// Bootload searching for console
//
// CIOC 8191828
// Connect channel LPW 037312020000 (case b)
//   DCW pointer 037312
//     RES              0
//     REL              0
//     AE               0
//     NC 21            0
//     TAL 22           1
//     REL              0
//     TALLY         0000
//
// PCW 510000700200 010000000000
//   Chan info     0010
//   Addr ext        00
//   111              7
//   M                0
//   Chan info 22046447
//   Pg Tbl Ptr  000000
//   PTP              0
//   PGE              0
//   AUX              0
//

// Bootload finding console
//
// CIOC 8324312
// Connect channel LPW 037312020000 (case b)
//   DCW pointer 037312
//     RES              0
//     REL              0
//     AE               0
//     NC 21            0
//     TAL 22           1
//     REL              0
//     TALLY         0000
//
// PCW 510000700200 036000000000
//   Chan info     0036
//   Addr ext        00
//   111              7
//   M                0
//   Chan info 22046447
//   Pg Tbl Ptr  000000
//   PTP              0
//   PGE              0
//   AUX              0
//
// CONSOLE: ALERT

// Bootload console read ID
//
// Connect channel LPW 037312020000 (case b)
//   DCW pointer 037312
//     RES              0
//     REL              0
//     AE               0
//     NC 21            0
//     TAL 22           1
//     REL              0
//     TALLY         0000
//
// PCW 570000700200 036000000000
//   Chan info     0036
//   Addr ext        00
//   111              7
//   M                0
//   Chan info 22046447
//   Pg Tbl Ptr  000000
//   PTP              0
//   PGE              0
//   AUX              0
//

// Bootload console write ASCII
//
// Connect channel LPW 037312020000 (case b)
//   DCW pointer 037312
//     RES              0
//     REL              0
//     AE               0
//     NC 21            0
//     TAL 22           1
//     REL              0
//     TALLY         0000
//
// PCW 330000700000 036000000000
//   Chan info     0036
//   Addr ext        00
//   111              7
//   M                0
//   Chan info 22046447
//   Pg Tbl Ptr  000000
//   PTP              0
//   PGE              0
//   AUX              0
//
// IOTD (0) 033624000022
//   tally       0022
//   addr            33624
//   cp               0
// Console channel LPW 033357007777 (case a)
//   DCW pointer 033357
//     RES              0
//     REL              0
//     AE               0
//     NC 21            0
//     TAL 22           0
//     REL              0
//     TALLY         7777
//
// DCW list dump:
//   IDCW 330000700000
//     cmd             33 Write ASCII
//     dev code        00
//     ctrl             0 (terminate)
//     chancmd          0 (record)
//   IOTD (0) 033624000022
//     tally       0022
//     addr            33624
//     cp               0
//   IDCW 510000700000
//     cmd             51 Reset Dev Stat/Alert
//     dev code        00
//     ctrl             0 (terminate)
//     chancmd          0 (record)

//
//  T&D Read block 3
//    Connect Channel LPW 006312040001 (case c)
//      DCW pointer 006312
//      RES              0
//      REL              0
//      AE               0
//      NC 21            1
//      TAL 22           0
//      REL              0
//      TALLY         0001
//    PCW 050000700001 012000000000
//      Chan info     0500
//      Addr ext        00
//      111              7
//      M                0
//      Chan info    00001
//      Chan            12
//      Pg Tbl Ptr  000000
//      PTP              0
//      PGE              0
//      AUX              0
//    IDCW 050000700001
//      cmd             05 (Read Binary Record)
//      dev             00
//      ctrl             0
//      chancmd          0
//
//    Payload channel
//      IOTD 010000000000
//        tally       0000
//        addr      010000
//

// Table 3.2.1 Updating of LPW address and tally and indication of PTRO and TRO
//
//  case   LPW 21   LPW 22  TALLY  UPDATE   INDICATE    INDICATE
//         NC       TALLY   VALUE  ADDR &   PTRO TO     TRO
//                                 TALLY    CONN. CH.   FAULT
//    a.     0       0       any    yes       no         no
//    b.     0       1       > 2    yes       no         no
//                            1     yes       yes        no
//                            0     no        no         yes
//    c.     1       x       any    no        yes        no
//
// Case a. is GCOS common Peripheral Interface payloads channels;
// a tally of 0 is not a fault since the tally field will not be
// used for that purpose.
//
// Case b. is the configuration expected of standard operation with real-time
// applications and/or direct data channels.
//
// Case c. will be used normally only with the connect channel servicing CPI
// type payload channels. [CAC: "INDICATE PTRO TO CONN. CH." means that the
// connect channel should not call list service to obtain additional PCW's]
//
// If case a. is used with the connect channels, a "System Fault: Tally
// Control Error, Connect Channel" will be generated. Case c. will be
// used in the bootload program.

// LPW bits
//
//  0-17  DCW pointer
//  18    RES - Restrict IDCW's [and] changing AE flag
//  19    REL - IOM controlled image of REL bit
//  20    AE - Address extension flag
//  21    NC
//  22    TAL
//  23    REL
//  24-35 TALLY
//

// LPWX (sometimes called LPWE)
//
//  0-8   Lower bound
//  9-17  Size
//  18-35 Pointer to DCW of most recent instruction
//

// PCW bits
//
// word 1
//   0-11 Channel information
//  12-17 Address extension
//  18-20 111
//  21    M
//  22-35 Channel Information
//
// word 2
//  36-38 SBZ
//  39-44 Channel Number
//  45-62 Page table ptr
//  63    PTP
//  64    PGE
//  65    AUX
//  66-21 not used

// IDCW
//
//   0-11 Channel information
//  12-17 Address extension
//  18-20 111
//  21    EC
//  22-35 Channel Information
//

// TDCW
//
// Paged, PCW 64 = 0
//
//   0-17 DCW pointer
//  18-20 Not 111
//  21    0
//  22    1
//  23    0
//  24-32 SBZ
//  33    EC
//  34    RES
//  35    REL
//
// Paged, PCW 64 = 1
//
//   0-17 DCW pointer
//  18-20 Not 111
//  21    0
//  22    1
//  23    0
//  24-30 SBZ
//  31    SEG
//  32    PDTA
//  33    PDCW
//  34    RES
//  35    REL
//

#include "dps8m.h"
#include "iom_channel.h"
#include "iom.h"
#include "faults.h"
#include "cable.h"
#include "opc.h"
#include "fnp.h"
#include "utils.h"
# include "threadz.h"

#ifdef TCPX
#define MBXTRACE

#ifdef MBXTRACE
#define mbase 06100
#define mhdrl 8
#define msubmbxl 8
#define msubmbxc 8
#define fsubmbxl 28
#define mbxcmdo 1
#define mcmda(n)  (mbase + mhdrl + msubmbxl * (n) + mbxcmdo)
#define mfcmda(n)  (mbase + mhdrl + msubmbxl * msubmbxc + fsubmbxl * (n) + mbxcmdo)

#define FTRC(a,d) ftrace (a, d)

static void ftracertx (uint n, word9 op_code, word36 w) {
  switch (op_code) {
    case 0005:
      simPrintf ("FNP RTX Input Accepted\n");
      break;
    default:
      simPrintf ("unk rtx op_code %o mbx %o w %012lo\n", op_code, n, w);
      break;
  }
}


static void ftracercd (uint n, word9 op_code, word36 w) {
  switch (op_code) {
    case 0100:
      simPrintf ("FNP RCD Accept New Terminal\n");
      break;
    case 0101:
      simPrintf ("FNP RCD Line Disconnected\n");
      break;
    case 0102:
      simPrintf ("FNP RCD Input to MBX\n");
      break;
    case 0105:
      simPrintf ("FNP RCD Send Output\n");
      break;
    case 0112:
      simPrintf ("FNP RCD Accept Input\n");
      break;
    case 0113:
      simPrintf ("FNP RCD Line Break\n");
      break;
    case 0114:
      simPrintf ("FNP RCD WRU Timeout\n");
      break;
    case 0115:
      simPrintf ("FNP RCD Error Message\n");
      break;
    case 0124:
      simPrintf ("FNP RCD Line Status\n");
      break;
    default:
      simPrintf ("unk rcd op_code %o mbx %o w %012lo\n", op_code, n, w);
      break;
  }
}

static void ftracem (uint n, word36 w) {
  word9 io_cmd = (word9) getbits36 (w, 27, 9);
  word9 op_code = (word9) getbits36 (w, 18, 9);
  switch (io_cmd) {
    case 1:
      ftracercd (n, op_code, w);
      break;
    case 2:
      ftracertx (n, op_code, w);
      break;
    default:
      simPrintf ("unk io_cmd %o mbx %o w %012lo\n", io_cmd, n, w);
      break;
  }
}

static void ftrace (word24 addr, word36 w) {
  for (uint n = 0; n < 4; n ++) {
    if (addr == mfcmda (n))
      ftracem (n, w);
  }
}

#define MTRC(a,d) mtrace (a, d)

static void mtracewtx (uint n, word9 op_code, word36 w) {
  switch (op_code) {
    case 0012:
      simPrintf ("CS WTX  Accept Output\n");
      break;
    case 0013:
      simPrintf ("CS WTX  Accept Last Output\n");
      break;
    default:
      simPrintf ("unk wtx op_code %o mbx %o w %012lo\n", op_code, n, w);
      break;
  }
}

static void mtracewcd (uint n, word9 op_code, word36 w) {
  switch (op_code) {
    case 0000:
      simPrintf ("CS WCD  Terminal Accecpted\n");
      break;
    case 0001:
      simPrintf ("CS WCD  Disconnect Line\n");
      break;
    case 0002:
      simPrintf ("CS WCD  Disconnect All Lines\n");
      break;
    case 0004:
      simPrintf ("CS WCD  Accept Calls\n");
      break;
    case 0006:
      simPrintf ("CS WCD  Set Line Type\n");
      break;
    case 0007:
      simPrintf ("CS WCD  Enter Receive Mode\n");
      break;
    case 0010:
      simPrintf ("CS WCD  Set Framing Characters\n");
      break;
    case 0023:
      simPrintf ("CS WCD  Dump Memory\n");
      break;
    case 0024:
      simPrintf ("CS WCD  Patch Memory\n");
      break;
    case 0026:
      simPrintf ("CS WCD  Line Control\n");
      break;
    case 0042:
      simPrintf ("CS WCD  Alter Parameters\n");
      break;
    case 0045:
      simPrintf ("CS WCD  Set Delay Table\n");
      break;
    default:
      simPrintf ("unk wcd op_code %o mbx %o w %012lo\n", op_code, n, w);
      break;
  }
}

static void mtracem (uint n, word36 w) {
  word9 io_cmd = (word9) getbits36 (w, 27, 9);
  word9 op_code = (word9) getbits36 (w, 18, 9);
  switch (io_cmd) {
    case 3:
      mtracewcd (n, op_code, w);
      break;
    case 4:
      mtracewtx (n, op_code, w);
      break;
    default:
      simPrintf ("unk io_cmd %o mbx %o w %012lo\n", io_cmd, n, w);
      break;
  }
}

void mtrace (word24 addr, word36 w) {
  for (uint n = 0; n < 4; n ++) {
    if (addr == mcmda (n))
      mtracem (n, w);
  }
}
#else
#define FTRC(a,d)
#define MTRC(a,d)
#endif  // MBXTRACE
#endif // TCPX

// Nomenclature
//
//  IDX index   refers to emulator unit
//  NUM         refers to the number that the unit is configured as ("IOMA,
//              IOMB,..."). Encoded in the low to bits of configSwMultiplexBaseAddress

// Default
#define N_IOM_UNITS 1

#define IOM_UNIT_IDX(uptr) ((uptr) - iom_unit)

iomState_t ioms[N_IOM_UNITS_MAX];

chnlState_t iomChanData[N_IOM_UNITS_MAX][MAX_CHANNELS];

typedef enum iom_status_t
  {
    iomStatNormal         = 0,
    iomStatLPWTRO         = 1,
    iomStat2TDCW          = 2,
    iomStatBoundaryError  = 3,
    iomStatAERestricted   = 4,
    iomStatIDCWRestricted = 5,
    iomStatCPDiscrepancy  = 6,
    iomStatParityErr      = 7
  } iom_status_t;

enum config_sw_OS_t
  {
    CONFIG_SW_STD_GCOS,
    CONFIG_SW_EXT_GCOS,
    CONFIG_SW_MULTICS  // "Paged"
  };

enum config_sw_model_t
  {
    CONFIG_SW_MODEL_IOM,
    CONFIG_SW_MODEL_IMU
  };

// Boot device: CARD/TAPE;
enum config_sw_bootload_device_e { CONFIG_SW_BLCT_CARD, CONFIG_SW_BLCT_TAPE };

typedef struct
  {
    // Configuration switches

    // Interrupt multiplex base address: 12 toggles
    word12 configSwIomBaseAddress;

    // Mailbox base aka IOM base address: 9 toggles
    // Note: The IOM number is encoded in the lower two bits

    // AM81, pg 60 shows an image of a Level 68 IOM configuration panel
    // The switches are arranged and labeled
    //
    //  12   13   14   15   16   17   18   --   --  --  IOM
    //                                                  NUMBER
    //   X    X    X    X    X    X    X                X     X
    //

    word9 configSwMultiplexBaseAddress;

    enum config_sw_model_t config_sw_model; // IOM or IMU

    // OS: Three position switch: GCOS, EXT GCOS, Multics
    enum config_sw_OS_t config_sw_OS; // = CONFIG_SW_MULTICS;

    // Bootload device: Toggle switch CARD/TAPE
    enum config_sw_bootload_device_e configSwBootloadCardTape; // = CONFIG_SW_BLCT_TAPE;

    // Bootload tape IOM channel: 6 toggles
    word6 configSwBootloadMagtapeChan; // = 0;

    // Bootload cardreader IOM channel: 6 toggles
    word6 configSwBootloadCardrdrChan; // = 1;

    // Bootload: pushbutton

    // Sysinit: pushbutton

    // Bootload SCU port: 3 toggle AKA "ZERO BASE S.C. PORT NO"
    // "the port number of the SC through which which connects are to
    // be sent to the IOM
    word3 configSwBootloadPort; // = 0;

    // 8 Ports: CPU/IOM connectivity
    // Port configuration: 3 toggles/port
    // Which SCU number is this port attached to
    uint configSwPortAddress[N_IOM_PORTS]; // = { 0, 1, 2, 3, 4, 5, 6, 7 };

    // Port interlace: 1 toggle/port
    uint configSwPortInterface[N_IOM_PORTS]; // = { 0, 0, 0, 0, 0, 0, 0, 0 };

    // Port enable: 1 toggle/port
    uint configSwPortEnable[N_IOM_PORTS]; // = { 0, 0, 0, 0, 0, 0, 0, 0 };

    // Port system initialize enable: 1 toggle/port // XXX What is this
    uint configSwPortSysinitEnable[N_IOM_PORTS]; // = { 0, 0, 0, 0, 0, 0, 0, 0 };

    // Port half-size: 1 toggle/port // XXX what is this
    uint configSwPortHalfsize[N_IOM_PORTS]; // = { 0, 0, 0, 0, 0, 0, 0, 0 };
    // Port store size: 1 8 pos. rotary/port
    uint configSwPortStoresize[N_IOM_PORTS]; // = { 0, 0, 0, 0, 0, 0, 0, 0 };

    // other switches:
    //   alarm disable
    //   test/normal
    iom_status_t iomStatus;

    uint invokingScuUnitIdx; // the unit number of the SCU that did the connect.

  } iom_unit_data_t;

static iom_unit_data_t iom_unit_data[N_IOM_UNITS_MAX];

typedef enum iomSysFaults_t
  {
    // List from 4.5.1; descr from AN87, 3-9
    iomNoFlt         = 000,
    iomIllChanFlt    = 001, // PCW to chan with chan number >= 40
                            // or channel without scratchpad installed
    iomIllSrvReqFlt  = 002, // A channel requested a service request code
                            // of zero, a channel number of zero, or
                            // a channel number >= 40
    // =003,                // Parity error scratchpad
    iomBndryVioFlt   = 003, // pg B36
    iom256KFlt       = 004, // 256K overflow -- address decremented to zero,
                            // but not tally
    iomLPWTRPConnFlt = 005, // tally was zero for an update LPW (LPW bit
                            // 21==0) when the LPW was fetched for the
                            // connect channel
    iomNotPCWFlt     = 006, // DCW for conn channel had bits 18..20 != 111b
    iomCP1Flt        = 007, // DCW was TDCW or had bits 18..20 == 111b
    // = 010,               // CP/CS-BAD-DATA DCW fetch for a 9 bit channel
                            // contained an illegal character position
    // = 011,               // NO-MEM-RES No response to an interrupt from
                            // a system controller within 16.5 usec.
    // = 012,               // PRTY-ERR-MEM Parity error on the read
                            // data when accessing a system controller.
    iomIllTalCChnFlt = 013, // LPW bits 21-22 == 00 when LPW was fetched
                            // for the connect channel
    // = 014,               // PTP-Fault: PTP-Flag= zero or PTP address
                            // overflow.
    iomPTWFlagFault  = 015, // PTW-Flag-Fault: Page Present flag zero, or
                            // Write Control Bit 0, or Housekeeping bit set,
    // = 016,               // ILL-LPW-STD LPW had bit 20 on in GCOS mode
    iomNoPortFault   = 017, // NO-PRT-SEL No port selected during attempt
                            // to access memory.
  } iomSysFaults_t;

typedef enum iomFaultServiceRequest
  {
    // Combined SR/M/D
    iomFsrFirstList =  (1 << 2) | 2,
    iomFsrList =       (1 << 2) | 0,
    iomFsrStatus =     (2 << 2) | 0,
    iomFsrIntr =       (3 << 2) | 0,
    iomFsrSPIndLoad =  (4 << 2) | 0,
    iomFsrDPIndLoad =  (4 << 2) | 1,
    iomFsrSPIndStore = (5 << 2) | 0,
    iomFsrDPIndStore = (5 << 2) | 1,
    iomFsrSPDirLoad =  (6 << 2) | 0,
    iomFsrDPDirLoad =  (6 << 2) | 1,
    iomFsrSPDirStore = (7 << 2) | 0,
    iomFsrDPDirStore = (7 << 2) | 1
  } iomFaultServiceRequest;

#if defined(IO_THREADZ)
__thread uint this_iom_idx;
__thread uint this_chan_num;
#endif

#if defined(TESTING)
static char * cmdNames [] =
  {
    "Request Status",        //  0
    "",                      //  1
    "Rd Ctrlr Mem",          //  2
    "Rd 9 Rcrd",             //  3
    "",                      //  4
    "Rd Bin Rcrd",           //  5
    "Initiate Rd Data Xfer", //  6
    "",                      //  7
    "",                      // 10
    "",                      // 11
    "",                      // 12
    "Wr 9 Rcrd",             // 13
    "",                      // 14
    "Wr Bin Rcrd",           // 15
    "Initiate Wr Data Xfer", // 16
    "",                      // 17
    "Release Ctlr",          // 20
    "",                      // 21
    "RdStatReg",             // 22
    "",                      // 23
    "",                      // 24
    "Read",                  // 25
    "",                      // 26
    "",                      // 27
    "Seek 512",              // 30
    "",                      // 31
    "MTP Wr Mem",            // 32
    "Write ASCII",           // 33
    "",                      // 34
    "",                      // 35
    "",                      // 36
    "",                      // 37
    "Reset Status",          // 40
    "Set 6250 CPI",          // 41
    "Restore",               // 42
    "",                      // 43
    "Forward Skip Rcrd",     // 44
    "Forward Skip File",     // 45
    "Backward Skip Rcrd",    // 46
    "Backspace File",        // 47
    "Request Status",        // 50
    "Reset Dev Stat/Alert",  // 51
    "",                      // 52
    "",                      // 53
    "",                      // 54
    "Write EOF",             // 55
    "",                      // 56
    "Survey Devs/ReadID",    // 57
    "Set 800 BPI",           // 60
    "Set 556 BPI",           // 61
    "",                      // 62
    "Set File Permit",       // 63
    "Set 200 BPI",           // 64
    "Set 1600 CPI",          // 65
    "",                      // 66
    "",                      // 67
    "Rewind",                // 70
    "",                      // 71
    "Rewind/Unload",         // 72
    "",                      // 73
    "",                      // 74
    "",                      // 75
    "",                      // 76
    "",                      // 77
  };
#endif

void iom_core_read (UNUSED uint iom_unit_idx, word24 addr, word36 *data) {
  word36 v;
  LOAD_ACQ_CORE_WORD(v, addr);
  * data = v & DMASK;
}

void iom_core_read2 (UNUSED uint iom_unit_idx, word24 addr, word36 *even, word36 *odd) {
  word36 v;
  LOAD_ACQ_CORE_WORD(v, addr);
  * even = v & DMASK;
  addr++;
  LOAD_ACQ_CORE_WORD(v, addr);
  * odd = v & DMASK;
}

void iom_core_write (uint iom_unit_idx, word24 addr, word36 data) {
  LOCK_CORE_WORD(addr, & ioms[iom_unit_idx].iomCoreLockState);
  STORE_REL_CORE_WORD(addr, data);
}

void iom_core_write2 (uint iom_unit_idx, word24 addr, word36 even, word36 odd) {
  LOCK_CORE_WORD(addr, & ioms[iom_unit_idx].iomCoreLockState);
  STORE_REL_CORE_WORD(addr, even);
  addr++;
  LOCK_CORE_WORD(addr, & ioms[iom_unit_idx].iomCoreLockState);
  STORE_REL_CORE_WORD(addr, odd);
}

void iom_core_read_lock (uint iom_unit_idx, word24 addr, word36 *data) {
  LOCK_CORE_WORD(addr, & ioms[iom_unit_idx].iomCoreLockState);
  word36 v;
  LOAD_ACQ_CORE_WORD(v, addr);
  * data = v & DMASK;
}

void iom_core_write_unlock (UNUSED uint iom_unit_idx, word24 addr, word36 data) {
  STORE_REL_CORE_WORD(addr, data);
}


static simRc iom_action (UNIT *up)
  {
    // Recover the stash parameters
    uint scuUnitIdx = (uint) (up -> u3);
    uint iom_unit_idx = (uint) (up -> u4);
    iom_interrupt (scuUnitIdx, iom_unit_idx);
    return SCPE_OK;
  }

static UNIT iom_unit[N_IOM_UNITS_MAX] = {
#if defined(NO_C_ELLIPSIS)
  { UDATA (iom_action, 0, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL },
  { UDATA (iom_action, 0, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL },
  { UDATA (iom_action, 0, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL },
  { UDATA (iom_action, 0, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL }
#else
  [0 ... N_IOM_UNITS_MAX - 1] = {
    UDATA (iom_action, 0, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL
  }
#endif
};

static simRc iom_show_mbx (UNUSED FILE * st,
                            UNUSED UNIT * uptr, UNUSED int val,
                            UNUSED const void * desc)
  {
    return SCPE_OK;
  }

static simRc iom_show_units (UNUSED FILE * st, UNUSED UNIT * uptr, UNUSED int val, UNUSED const void * desc)
  {
    simPrintf ("Number of IOM units in system is %d\n", iom_dev.numunits);
    return SCPE_OK;
  }

static simRc iom_set_units (UNUSED UNIT * uptr, UNUSED int value, const char * cptr, UNUSED void * desc)
  {
    if (! cptr)
      return SCPE_ARG;
    int n = atoi (cptr);
    if (n < 0 || n > N_IOM_UNITS_MAX)
      return SCPE_ARG;
    // This was true for early Multics releases.
    //if (n > 2)
      //simPrintf ("Warning: Multics supports 2 IOMs maximum\n");
    iom_dev.numunits = (unsigned) n;
    return SCPE_OK;
  }

static simRc iom_show_config (UNUSED FILE * st, UNIT * uptr, UNUSED int val,
                             UNUSED const void * desc)
  {
    uint iom_unit_idx = (uint) IOM_UNIT_IDX (uptr);
    if (iom_unit_idx >= iom_dev.numunits)
      {
        simPrintf ("error: Invalid unit number %lu\n", (unsigned long) iom_unit_idx);
        return SCPE_ARG;
      }

    simPrintf ("IOM unit number %lu\n", (unsigned long) iom_unit_idx);
    iom_unit_data_t * chnlp = iom_unit_data + iom_unit_idx;

    char * os = "<out of range>";
    switch (chnlp -> config_sw_OS)
      {
        case CONFIG_SW_STD_GCOS:
          os = "Standard GCOS";
          break;
        case CONFIG_SW_EXT_GCOS:
          os = "Extended GCOS";
          break;
        case CONFIG_SW_MULTICS:
          os = "Multics";
          break;
      }
    char * blct = "<out of range>";
    switch (chnlp -> configSwBootloadCardTape)
      {
        case CONFIG_SW_BLCT_CARD:
          blct = "CARD";
          break;
        case CONFIG_SW_BLCT_TAPE:
          blct = "TAPE";
          break;
      }

    simPrintf ("Allowed Operating System: %s\n", os);
    simPrintf ("IOM Base Address:         %03o(8)\n", chnlp -> configSwIomBaseAddress);
    simPrintf ("Multiplex Base Address:   %04o(8)\n", chnlp -> configSwMultiplexBaseAddress);
    simPrintf ("Bootload Card/Tape:       %s\n", blct);
    simPrintf ("Bootload Tape Channel:    %02o(8)\n", chnlp -> configSwBootloadMagtapeChan);
    simPrintf ("Bootload Card Channel:    %02o(8)\n", chnlp -> configSwBootloadCardrdrChan);
    simPrintf ("Bootload Port:            %02o(8)\n", chnlp -> configSwBootloadPort);
    simPrintf ("Port Address:            ");
    int i;
    for (i = 0; i < N_IOM_PORTS; i ++)
      simPrintf (" %03o", chnlp -> configSwPortAddress[i]);
    simPrintf ("\n");
    simPrintf ("Port Interlace:          ");
    for (i = 0; i < N_IOM_PORTS; i ++)
      simPrintf (" %3o", chnlp -> configSwPortInterface[i]);
    simPrintf ("\n");
    simPrintf ("Port Enable:             ");
    for (i = 0; i < N_IOM_PORTS; i ++)
      simPrintf (" %3o", chnlp -> configSwPortEnable[i]);
    simPrintf ("\n");
    simPrintf ("Port Sysinit Enable:     ");
    for (i = 0; i < N_IOM_PORTS; i ++)
      simPrintf (" %3o", chnlp -> configSwPortSysinitEnable[i]);
    simPrintf ("\n");
    simPrintf ("Port Halfsize:           ");
    for (i = 0; i < N_IOM_PORTS; i ++)
      simPrintf (" %3o", chnlp -> configSwPortHalfsize[i]);
    simPrintf ("\n");
    simPrintf ("Port Storesize:          ");
    for (i = 0; i < N_IOM_PORTS; i ++)
      simPrintf (" %3o", chnlp -> configSwPortStoresize[i]);
    simPrintf ("\n");

    return SCPE_OK;
  }

//
// set iom0 config=<blah> [;<blah>]
//
//    blah = iom_base=n
//           multiplex_base=n
//           os=gcos | gcosext | multics
//           boot=card | tape
//           tapechan=n
//           cardchan=n
//           scuport=n
//           port=n   // set port number for below commands
//             addr=n
//             interlace=n
//             enable=n
//             initenable=n
//             halfsize=n
//             storesize=n
//          bootskip=n // Hack: forward skip n records after reading boot record

static config_value_list_t cfg_model_list[] =
  {
    { "iom", CONFIG_SW_MODEL_IOM },
    { "imu", CONFIG_SW_MODEL_IMU }
  };

static config_value_list_t cfg_os_list[] =
  {
    { "gcos",    CONFIG_SW_STD_GCOS },
    { "gcosext", CONFIG_SW_EXT_GCOS },
    { "multics", CONFIG_SW_MULTICS  },
    { NULL,      0 }
  };

static config_value_list_t cfg_boot_list[] =
  {
    { "card", CONFIG_SW_BLCT_CARD },
    { "tape", CONFIG_SW_BLCT_TAPE },
    { NULL,   0 }
  };

static config_value_list_t cfg_base_list[] =
  {
    { "multics",  014 },
    { "multics1", 014 }, // boot iom
    { "multics2", 020 },
    { "multics3", 024 },
    { "multics4", 030 },
    { NULL, 0 }
  };

static config_value_list_t cfg_size_list[] =
  {
    { "32",    0 },
    { "64",    1 },
    { "128",   2 },
    { "256",   3 },
    { "512",   4 },
    { "1024",  5 },
    { "2048",  6 },
    { "4096",  7 },
    { "32K",   0 },
    { "64K",   1 },
    { "128K",  2 },
    { "256K",  3 },
    { "512K",  4 },
    { "1024K", 5 },
    { "2048K", 6 },
    { "4096K", 7 },
    { "1M",    5 },
    { "2M",    6 },
    { "4M",    7 },
    { NULL,    0 }
  };

static configList_t iom_config_list[] =
  {
    { "model",          1, 0,               cfg_model_list },
    { "os",             1, 0,               cfg_os_list    },
    { "boot",           1, 0,               cfg_boot_list  },
    { "iom_base",       0, 07777,           cfg_base_list  },
    { "multiplex_base", 0, 0777,            NULL           },
    { "tapechan",       0, 077,             NULL           },
    { "cardchan",       0, 077,             NULL           },
    { "scuport",        0, 07,              NULL           },
    { "port",           0, N_IOM_PORTS - 1, NULL           },
    { "addr",           0, 7,               NULL           },
    { "interlace",      0, 1,               NULL           },
    { "enable",         0, 1,               NULL           },
    { "initenable",     0, 1,               NULL           },
    { "halfsize",       0, 1,               NULL           },
    { "store_size",     0, 7,               cfg_size_list  },
    { NULL,             0, 0,               NULL           }
  };

static simRc iom_set_config (UNIT * uptr, UNUSED int value, const char * cptr, UNUSED void * desc)
  {
    uint iom_unit_idx = (uint) IOM_UNIT_IDX (uptr);
    if (iom_unit_idx >= iom_dev.numunits)
      {
        simPrintf ("error: %s: Invalid unit number %ld\n", __func__, (long) iom_unit_idx);
        return SCPE_ARG;
      }

    iom_unit_data_t * chnlp = iom_unit_data + iom_unit_idx;

    static uint port_num = 0;

    configState_t cfg_state = { NULL, NULL };

    for (;;)
      {
        int64_t v;
        int rc = cfgParse (__func__, cptr, iom_config_list, & cfg_state, & v);

        if (rc == CFG_DONE)
          break;

        if (rc == CFG_ERROR)
          {
            cfgParseDone (& cfg_state);
            continue;
          }
        const char * name = iom_config_list[rc].name;

        if (strcmp (name, "model") == 0)
          {
            chnlp -> config_sw_model = (enum config_sw_model_t) v;
            continue;
          }

        if (strcmp (name, "os") == 0)
          {
            chnlp -> config_sw_OS = (enum config_sw_OS_t) v;
            continue;
          }

        if (strcmp (name, "boot") == 0)
          {
            chnlp -> configSwBootloadCardTape = (enum config_sw_bootload_device_e) v;
            continue;
          }

        if (strcmp (name, "iom_base") == 0)
          {
            chnlp -> configSwIomBaseAddress = (word12) v;
            continue;
          }

        if (strcmp (name, "multiplex_base") == 0)
          {
            // The IOM number is in the low 2 bits
            // The address is in the high 7 bits which are mapped
            // to bits 12 to 18 of a 24 bit address
            //
//  0  1  2  3  4  5  6  7  8
//  x  x  x  x  x  x  x  y  y
//
//  Address
//  0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 29 20 21 22 23 24
//  0  0  0  0  0  0  0  0  0  0  0  0  x  x  x  x  x  x  x  0  0  0  0  0  0
//
// IOM number
//
//  0  1
//  y  y

            chnlp -> configSwMultiplexBaseAddress = (word9) v;
            continue;
          }

        if (strcmp (name, "tapechan") == 0)
          {
            chnlp -> configSwBootloadMagtapeChan = (word6) v;
            continue;
          }

        if (strcmp (name, "cardchan") == 0)
          {
            chnlp -> configSwBootloadCardrdrChan = (word6) v;
            continue;
          }

        if (strcmp (name, "scuport") == 0)
          {
            chnlp -> configSwBootloadPort = (word3) v;
            continue;
          }

        if (strcmp (name, "port") == 0)
          {
            port_num = (uint) v;
            continue;
          }

        if (strcmp (name, "addr") == 0)
          {
            chnlp -> configSwPortAddress[port_num] = (uint) v;
            continue;
          }

        if (strcmp (name, "interlace") == 0)
          {
            chnlp -> configSwPortInterface[port_num] = (uint) v;
            continue;
          }

        if (strcmp (name, "enable") == 0)
          {
            chnlp -> configSwPortEnable[port_num] = (uint) v;
            continue;
          }

        if (strcmp (name, "initenable") == 0)
          {
            chnlp -> configSwPortSysinitEnable[port_num] = (uint) v;
            continue;
          }

        if (strcmp (name, "halfsize") == 0)
          {
            chnlp -> configSwPortHalfsize[port_num] = (uint) v;
            continue;
          }

        if (strcmp (name, "store_size") == 0)
          {
            chnlp -> configSwPortStoresize[port_num] = (uint) v;
            continue;
          }

        simPrintf ("error: %s: Invalid cfgParse rc <%ld>\n", __func__, (long) rc);
        cfgParseDone (& cfg_state);
        return SCPE_ARG;
      } // process statements
    cfgParseDone (& cfg_state);
    return SCPE_OK;
  }

static simRc iom_reset_unit (UNIT * uptr, UNUSED int32_t value, UNUSED const char * cptr,
                       UNUSED void * desc)
  {
    uint iom_unit_idx = (uint) (uptr - iom_unit);
    iom_unit_reset_idx (iom_unit_idx);
    return SCPE_OK;
  }

static MTAB iom_mod[] =
  {
    {
       MTAB_XTD | MTAB_VUN | \
       MTAB_NMO | MTAB_NC,       /* mask               */
      0,                         /* match              */
      "MBX",                     /* print string       */
      NULL,                      /* match string       */
      NULL,                      /* validation routine */
      iom_show_mbx,              /* display routine    */
      NULL,                      /* value descriptor   */
      NULL                       /* help string        */
    },
    {
      MTAB_XTD | MTAB_VUN | \
      MTAB_NMO | MTAB_VALR,      /* mask               */
      0,                         /* match              */
      "CONFIG",                  /* print string       */
      "CONFIG",                  /* match string       */
      iom_set_config,            /* validation routine */
      iom_show_config,           /* display routine    */
      NULL,                      /* value descriptor   */
      NULL                       /* help string        */
    },
    {
      MTAB_XTD | MTAB_VUN | \
      MTAB_NMO | MTAB_VALR,      /* mask               */
      0,                         /* match              */
      (char *) "RESET",          /* print string       */
      (char *) "RESET",          /* match string       */
      iom_reset_unit,            /* validation routine */
      NULL,                      /* display routine    */
      (char *) "reset IOM unit", /* value descriptor   */
      NULL                       /* help               */
    },
    {
      MTAB_XTD | MTAB_VDV | \
      MTAB_NMO | MTAB_VALR,                 /* mask               */
      0,                                    /* match              */
      "NUNITS",                             /* print string       */
      "NUNITS",                             /* match string       */
      iom_set_units,                        /* validation routine */
      iom_show_units,                       /* display routine    */
      "Number of IOM units in the system",  /* value descriptor   */
      NULL                                  /* help string        */
    },
    {
      0, 0, NULL, NULL, 0, 0, NULL, NULL
    }
  };

//
// iom_unit_reset_idx ()
//
//  Reset -- Reset to initial state -- clear all device flags and cancel any
//  any outstanding timing operations. Used by RESET, RUN, and BOOT commands
//
//  Note that all reset()s run after once-only init().
//
//

simRc iom_unit_reset_idx (UNUSED uint iom_unit_idx)
  {
    return SCPE_OK;
  }

static simRc iom_reset (UNUSED DEVICE * dptr)
  {
    for (uint iom_unit_idx = 0; iom_unit_idx < iom_dev.numunits; iom_unit_idx ++)
      {
        iom_unit_reset_idx (iom_unit_idx);
      }
    return SCPE_OK;
  }

static simRc boot_svc (UNIT * unitp);
static UNIT boot_channel_unit[N_IOM_UNITS_MAX] = {
#if defined(NO_C_ELLIPSIS)
  { UDATA (& boot_svc, 0, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL },
  { UDATA (& boot_svc, 0, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL },
  { UDATA (& boot_svc, 0, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL },
  { UDATA (& boot_svc, 0, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL }
#else
  [0 ... N_IOM_UNITS_MAX - 1] = {
    UDATA (& boot_svc, 0, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL
  }
#endif
};

/*
 * init_memory_iom ()
 *
 * Load a few words into memory.   Simulates pressing the BOOTLOAD button
 * on an IOM or equivalent.
 *
 * All values are from bootload_tape_label.alm.  See the comments at the
 * top of that file.  See also doc #43A239854.
 *
 * NOTE: The values used here are for an IOM, not an IOX.
 * See init_memory_iox () below.
 *
 */

static void init_memory_iom (uint iom_unit_idx)
  {
    // The presence of a 0 in the top six bits of word 0 denote an IOM boot
    // from an IOX boot

    // " The channel number ("Chan#") is set by the switches on the IOM to be
    // " the channel for the tape subsystem holding the bootload tape. The
    // " drive number for the bootload tape is set by switches on the tape
    // " MPC itself.

    word12 base = iom_unit_data[iom_unit_idx].configSwIomBaseAddress;

    // bootload_io.alm insists that pi_base match
    // template_slt_$iom_mailbox_absloc

    //uint pi_base = iom_unit_data[iom_unit_idx].configSwMultiplexBaseAddress & ~3;
    word36 pi_base = (((word36)  iom_unit_data[iom_unit_idx].configSwMultiplexBaseAddress)  << 3) |
                     (((word36) (iom_unit_data[iom_unit_idx].configSwIomBaseAddress & 07700U)) << 6) ;
    word3 iom_num = ((word36) iom_unit_data[iom_unit_idx].configSwMultiplexBaseAddress) & 3;
    word36 cmd = 5;       // 6 bits; 05 for tape, 01 for cards
    word36 dev = 0;            // 6 bits: drive number

    // is-IMU flag
    word36 imu = iom_unit_data[iom_unit_idx].config_sw_model == CONFIG_SW_MODEL_IMU ? 1 : 0;       // 1 bit

    // Description of the bootload channel from 43A239854
    //    Legend
    //    BB - Bootload channel #
    //    C - Cmd (1 or 5)
    //    N - IOM #
    //    P - Port #
    //    XXXX00 - Base Addr -- 01400
    //    XXYYYY0 Program Interrupt Base

    enum config_sw_bootload_device_e bootdev = iom_unit_data[iom_unit_idx].configSwBootloadCardTape;

    word6 bootchan;
    if (bootdev == CONFIG_SW_BLCT_CARD)
      bootchan = iom_unit_data[iom_unit_idx].configSwBootloadCardrdrChan;
    else // CONFIG_SW_BLCT_TAPE
      bootchan = iom_unit_data[iom_unit_idx].configSwBootloadMagtapeChan;

    // 1
    // system fault vector; DIS 0 instruction (imu bit not mentioned by
    // 43A239854)

    word36 dis0 = W36_C (0616200);

    iom_core_write (iom_unit_idx, 010 + 2 * iom_num + 0, (imu << 34) | dis0);

    // Simulator specific hack; the simulator flags memory words as
    // being "uninitialized"; because the fault pair is fetched as a
    // pair, the odd word's uninitialized state may generate a
    // run-time warning, even though it is not executed.
    //
    // By zeroing it here, we clear the flag and avoid the message
    // Since the memory will have already been set to zero by
    // the "Initialize and clear", this will have no affect
    // on the boot operation.

    iom_core_write (iom_unit_idx, 010 + 2 * iom_num + 1, W36_C (0));

    // 2
    // terminate interrupt vector (overwritten by bootload)

    iom_core_write (iom_unit_idx, 030 + 2 * iom_num, dis0);

    // 3
    // tally word for sys fault status

    // XXX CAC: Clang -Wsign-conversion claims 'base<<6' is int
    word24 base_addr =  (word24) base << 6; // 01400
    iom_core_write (iom_unit_idx, base_addr + 7, ((word36) base_addr << 18) | W36_C (02000002));

    // 4
    // Fault channel DCW

    // bootload_tape_label.alm says 04000, 43A239854 says 040000.  Since
    // 43A239854 says "no change", 40000 is correct; 4000 would be a
    // large tally

    // Connect channel LPW; points to PCW at 000000
    iom_core_write (iom_unit_idx, base_addr + 010, W36_C (040000));

    // 5
    // Boot device LPW; points to IDCW at 000003

    word24 mbx = base_addr + 4u * bootchan;
    iom_core_write (iom_unit_idx, mbx, W36_C (03020003));

    // 6
    // Second IDCW: IOTD to loc 30 (startup fault vector)

    iom_core_write (iom_unit_idx, 4, W36_C (030) << 18);

    // 7
    // Default SCW points at unused first mailbox.

    // T&D tape overwrites this before the first status is saved, though.
    // CAC: But the status is never saved, only a $CON occurs, never
    // a status service

    // SCW

    iom_core_write (iom_unit_idx, mbx + 2, ((word36)base_addr << 18));

    // 8
    // 1st word of bootload channel PCW

    iom_core_write (iom_unit_idx, 0, W36_C (0720201));

    // 9
    // 2nd word of PCW pair

    // SCU port
    word3 port = iom_unit_data[iom_unit_idx].configSwBootloadPort; // 3 bits;

    // Why does bootload_tape_label.alm claim that a port number belongs in
    // the low bits of the 2nd word of the PCW?  The lower 27 bits of the
    // odd word of a PCW should be all zero.

    //[CAC] Later, bootload_tape_label.alm does:
    //
    //     cioc    bootload_info+1 " port # stuck in PCW
    //     lda     0,x5            " check for status
    //
    // So this is a bootloader kludge to pass the bootload SCU number
    //

    //[CAC] From Rev01.AN70.archive:
    //  In BOS compatibility mode, the BOS BOOT command simulates the IOM,
    //  leaving the same information.  However, it also leaves a config deck
    //  and flagbox (although bce has its own flagbox) in the usual locations.
    //  This allows Bootload Multics to return to BOS if there is a BOS to
    //  return to.  The presence of BOS is indicated by the tape drive number
    //  being non-zero in the idcw in the "IOM" provided information.  (This is
    //  normally zero until firmware is loaded into the bootload tape MPC.)

    iom_core_write (iom_unit_idx, 1, ((word36) (bootchan) << 27) | port);

    // 10
    // word after PCW (used by program)

    iom_core_write (iom_unit_idx, 2, ((word36) base_addr << 18) | (pi_base) | iom_num);

    // 11
    // IDCW for read binary

    iom_core_write (iom_unit_idx, 3, (cmd << 30) | (dev << 24) | W36_C (0700000));
  }

static simRc boot_svc (UNIT * unitp)
  {
    uint iom_unit_idx = (uint) (unitp - boot_channel_unit);
    // the docs say press sysinit, then boot; we don't have an
    // explicit "sysinit", so we treat "reset iom" as sysinit.
    // The docs don't say what the behavior is if you don't press
    // sysinit first so we won't worry about it.

    // This is needed to reset the interrupt mask registers; Multics tampers
    // with runtime values, and mucks up rebooting on multi-CPU systems.
    for (int port_num = 0; port_num < N_SCU_PORTS; port_num ++)
      {
        if (! cables->iom_to_scu[iom_unit_idx][port_num].socket_in_use)
          continue;
        uint scuUnitIdx = cables->iom_to_scu[iom_unit_idx][port_num].scuUnitIdx;
        scuResetUnit ((int) scuUnitIdx);
      }

    // initialize memory with boot program
    init_memory_iom (iom_unit_idx);

    // Start the remote console listener
    startRemoteConsole ();

    // Start the FNP dialup listener
    startFNPListener ();

    // simulate $CON
    // XXX Making the assumption that low memory is connected to port 0, ..., high to 3
    if (! cables->iom_to_scu[iom_unit_idx][0].socket_in_use)
      {
        simWarn ("boot iom can't find a SCU\n");
        return SCPE_ARG;
      }
    uint scuUnitIdx = cables->iom_to_scu[iom_unit_idx][0].scuUnitIdx;
    iom_interrupt (scuUnitIdx, iom_unit_idx);

    // returning OK from the BOOT command causes the CPU start
    return SCPE_OK;
  }

static simRc iom_boot (int unitNum, UNUSED DEVICE * dptr)
  {
    if (unitNum < 0 || unitNum >= (int) iom_dev.numunits)
      {
        simPrintf ("%s: Invalid unit number %d\n", __func__, unitNum);
        return SCPE_ARG;
      }
    //uint iom_unit_idx = (uint) unitNum;
    boot_svc (& boot_channel_unit[unitNum]);
    return SCPE_OK;
  }

DEVICE iom_dev =
  {
    "IOM",        /* name                */
    iom_unit,     /* units               */
    NULL,         /* registers           */
    iom_mod,      /* modifiers           */
    N_IOM_UNITS,  /* #units              */
    10,           /* address radix       */
    8,            /* address width       */
    1,            /* address increment   */
    8,            /* data radix          */
    8,            /* data width          */
    NULL,         /* examine routine     */
    NULL,         /* deposit routine     */
    iom_reset,    /* reset routine       */
    iom_boot,     /* boot routine        */
    NULL,         /* attach routine      */
    NULL,         /* detach routine      */
    NULL,         /* context             */
    0,            /* flags               */
    0,            /* debug control flags */
    NULL,         /* debug flag names    */
    NULL,         /* memory size change  */
    NULL,         /* logical name        */
    NULL,         /* help                */
    NULL,         /* attach help         */
    NULL,         /* help context        */
    NULL,         /* description         */
    NULL          /* end                 */
  };

static uint mbxLoc (chnlState_t * chnlp) {
  uint iomUnitIdx  = chnlp->myIomIdx;
  uint chan = chnlp->myChanIdx;
// IDX is correct here as computation is based on physical unit
// configuration switches
  word12 base      = iom_unit_data[iomUnitIdx].configSwIomBaseAddress;
  word24 base_addr = ((word24) base) << 6; // 01400
  word24 mbx       = base_addr + 4 * chan;
  return mbx;
  }

/*
 * status_service ()
 *
 * Write status info into a status mailbox.
 *
 * BUG: Only partially implemented.
 * WARNING: The diag tape will crash because we don't write a non-zero
 * value to the low 4 bits of the first status word.  See comments
 * at the top of mt.c. [CAC] Not true. The IIOC writes those bits to
 * tell the bootloader code whether the boot came from an IOM or IIOC.
 * The connect channel does not write status bits. The diag tape crash
 * was due so some other issue.
 *
 */

// According to gtss_io_status_words.incl.pl1
//
//,     3 WORD1
//,       4 Termination_indicator         bit(01)unal
//,       4 Power_bit                     bit(01)unal
//,       4 Major_status                  bit(04)unal
//,       4 Substatus                     bit(06)unal
//,       4 PSI_channel_odd_even_ind      bit(01)unal
//,       4 Marker_bit_interrupt          bit(01)unal
//,       4 Reserved                      bit(01)unal
//,       4 Lost_interrupt_bit            bit(01)unal
//,       4 Initiate_interrupt_ind        bit(01)unal
//,       4 Abort_indicator               bit(01)unal
//,       4 IOM_status                    bit(06)unal
//,       4 Address_extension_bits        bit(06)unal
//,       4 Record_count_residue          bit(06)unal
//
//,      3 WORD2
//,       4 Data_address_residue          bit(18)unal
//,       4 Character_count               bit(03)unal
//,       4 Read_Write_control_bit        bit(01)unal
//,       4 Action_code                   bit(02)unal
//,       4 Word_count_residue            bit(12)unal

// iom_stat.incl.pl1
//
// (2 t bit (1),             /* set to "1"b by IOM                         */
//  2 power bit (1),         /* non-zero if peripheral absent or power off */
//  2 major bit (4),         /* major status                               */
//  2 sub bit (6),           /* substatus                                  */
//  2 eo bit (1),            /* even/odd bit                               */
//  2 marker bit (1),        /* non-zero if marker status                  */
//  2 soft bit (2),          /* software status                            */
//  2 initiate bit (1),      /* initiate bit                               */
//  2 abort bit (1),         /* software abort bit                         */
//  2 channel_stat bit (3),  /* IOM channel status                         */
//  2 central_stat bit (3),  /* IOM central status                         */
//  2 mbz bit (6),
//  2 rcount bit (6),        /* record count residue                       */
//
//  2 address bit (18),      /* DCW address residue                        */
//  2 char_pos bit (3),      /* character position residue                 */
//  2 r bit (1),             /* non-zero if reading                        */
//  2 type bit (2),          /* type of last DCW                           */
//  2 tally bit (12)) unal;  /* DCW tally residue                          */

// Searching the Multics source indicates that
//   eo is  used by tape
//   marker is used by tape and printer
//   soft is set by tolts as /* set "timeout" */'
//   soft is reported by tape, but not used.
//   initiate is used by tape and printer
//   abort is reported by tape, but not used
//   char_pos is used by tape, console

// pg B26: "The DCW residues stored in the status in page mode will
// represent the next absolute address (bits 6-23) of data prior to
// the application of the Page Table Word"

int status_service (chnlState_t * chnlp, bool marker) {
  uint iom_unit_idx = chnlp->myIomIdx;
  // See page 33 and AN87 for format of y-pair of status info

  // BUG: much of the following is not tracked

  word36 word1, word2;
  word1 = 0;
  putbits36_12 (& word1, 0, chnlp -> stati);
  // isOdd can be set to zero; see
  //   https://ringzero.wikidot.com/wiki:cac-2015-10-22
  //putbits36_1 (& word1, 12, chnlp -> isOdd ? 0 : 1);
  putbits36_1 (& word1, 13, marker ? 1 : 0);
  putbits36_2 (& word1, 14, 0); // software status
  putbits36_1 (& word1, 16, chnlp -> initiate ? 1 : 0);
  putbits36_1 (& word1, 17, 0); // software abort bit
  putbits36_3 (& word1, 18, (word3) chnlp -> chanStatus);
  //putbits36_3 (& word1, 21, iom_unit_data[iom_unit_idx].iomStatus);
  putbits36_3 (& word1, 21, 0);
#if 0
  // BUG: Unimplemented status bits:
  putbits36_6 (& word1, 24, chan_status.addr_ext);
#endif
  putbits36_6 (& word1, 30, chnlp -> recordResidue);

  word2 = 0;
#if 0
  // BUG: Unimplemented status bits:
  putbits36_18 (& word2, 0, chan_status.addr);
  putbits36_2 (& word2, 22, chan_status.type);
#endif
  putbits36_3  (& word2, 18, chnlp -> charPos);
  putbits36_1  (& word2, 21, chnlp -> isRead ? 1 : 0);
  putbits36_12 (& word2, 24, chnlp -> tallyResidue);

  // BUG: need to write to mailbox queue

  // T&D tape does *not* expect us to cache original SCW, it expects us to
  // use the SCW loaded from tape.

  uint chanloc   = mbxLoc (chnlp);
  word24 scwAddr = chanloc + IOM_MBX_SCW;
  word36 scw;
  iom_core_read_lock (iom_unit_idx, scwAddr, & scw);
  word18 addr    = getbits36_18 (scw, 0);   // absolute
  uint lq        = getbits36_2 (scw, 18);
  uint tally     = getbits36_12 (scw, 24);

  if (lq == 3) {
    lq = 0;
  }
  iom_core_write2 (iom_unit_idx, addr, word1, word2);

  // XXX: PVS-Studio believes that tally is always == 0!?
  if (tally > 0 || (tally == 0 && lq != 0)) {
    switch (lq) {
      case 0:
        // list
        if (tally != 0) {
          tally --;
          addr += 2;
        }
        break;

      case 1:
        // 4 entry (8 word) queue
        if (tally % 8 == 1 /* || tally % 8 == -1 */)
          addr -= 8;
        else
          addr += 2;
        tally --;
        break;

      case 2:
        // 16 entry (32 word) queue
        if (tally % 32 == 1 /* || tally % 32 == -1 */)
          addr -= 32;
        else
          addr += 2;
        tally --;
        break;
    }

    tally &= 07777;

    putbits36_12 (& scw, 24, (word12) tally);
    putbits36_18 (& scw, 0, addr);
  }

  iom_core_write_unlock (iom_unit_idx, scwAddr, scw);

  // BUG: update SCW in core
  return 0;
}

static word24 UNUSED build_AUXPTW_address (uint iom_unit_idx, int chan)
  {

//    0      5 6            16 17  18              21  22  23
//   ---------------------------------------------------------
//   | Zeroes | IOM Base Address  |                          |
//   ---------------------------------------------------------
//                         |    Channel Number       | 1 | 1 |
//                         -----------------------------------
// XXX Assuming 16 and 17 are or'ed. Pg B4 doesn't specify

    word12 IOMBaseAddress  = iom_unit_data[iom_unit_idx].configSwIomBaseAddress;
    word24 addr            = (((word24) IOMBaseAddress) & MASK12) << 6;
    addr                  |= ((uint) chan & MASK6) << 2;
    addr                  |= 03;
    return addr;
  }

static word24 build_DDSPTW_address (word18 PCW_PAGE_TABLE_PTR, word8 pageNumber)
  {
//    0      5 6        15  16  17  18                       23
//   ----------------------------------------------------------
//   | Page Table Pointer | 0 | 0 |                           |
//   ----------------------------------------------------------
// or
//                        -------------------------------------
//                        |  Direct chan addr 6-13            |
//                        -------------------------------------
    if (PCW_PAGE_TABLE_PTR & 3)
      simWarn ("%s: pcw_ptp %#o page_no  %#o\n", __func__, PCW_PAGE_TABLE_PTR, pageNumber);
    word24 addr = (((word24) PCW_PAGE_TABLE_PTR) & MASK18) << 6;
    addr |= pageNumber;
    return addr;
  }

// Fetch Direct Data Service Page Table Word

static void fetch_DDSPTW (chnlState_t * chnlp, word18 addr)
  {
    uint iom_unit_idx = chnlp->myIomIdx;
    uint chan = chnlp->myChanIdx;

    //atomic_thread_fence (memory_order_acquire);
    word24 pgte         = build_DDSPTW_address (chnlp -> PCW_PAGE_TABLE_PTR,
                                      (addr >> 10) & MASK8);
    iom_core_read (iom_unit_idx, pgte, (word36 *) & chnlp -> PTW_DCW);
    if ((chnlp -> PTW_DCW & W36_C (0740000777747)) != W36_C (04))
      simWarn ("%s: chan %ld addr %#o pgte %08o ptw %012"PRIo64"\n", __func__, (long)chan, addr, pgte, chnlp -> PTW_DCW);
    //atomic_thread_fence (memory_order_release);
  }

static word24 build_IDSPTW_address (word18 PCW_PAGE_TABLE_PTR, word1 seg, word8 pageNumber)
  {
//    0      5 6        15  16  17  18                       23
//   ----------------------------------------------------------
//   | Page Table Pointer         |                           |
//   ----------------------------------------------------------
// plus
//                    ----
//                    | S |
//                    | E |
//                    | G |
//                    -----
// plus
//                        -------------------------------------
//                        |         DCW 0-7                   |
//                        -------------------------------------

    word24 addr  = (((word24) PCW_PAGE_TABLE_PTR) & MASK18) << 6;
    addr        += (((word24) seg) & 01) << 8;
    addr        += pageNumber;
    return addr;
  }

// Fetch Indirect Data Service Page Table Word

static void fetch_IDSPTW (chnlState_t * chnlp, word18 addr)
  {
    //atomic_thread_fence (memory_order_acquire);
    word24 pgte         = build_IDSPTW_address (chnlp -> PCW_PAGE_TABLE_PTR,
                                      chnlp -> SEG,
                                      (addr >> 10) & MASK8);
    iom_core_read (chnlp->myIomIdx, pgte, (word36 *) & chnlp -> PTW_DCW);
    if ((chnlp -> PTW_DCW & W36_C (0740000777747)) != W36_C (04))
      simWarn ("%s: chan %d addr %#o ptw %012"PRIo64"\n", __func__, chnlp->myChanIdx, addr, chnlp -> PTW_DCW);
    //atomic_thread_fence (memory_order_release);
  }

static word24 build_LPWPTW_address (word18 PCW_PAGE_TABLE_PTR, word1 seg, word8 pageNumber)
  {

//    0      5 6        15  16  17  18                       23
//   ----------------------------------------------------------
//   | Page Table Pointer         |                           |
//   ----------------------------------------------------------
// plus
//                    ----
//                    | S |
//                    | E |
//                    | G |
//                    -----
// plus
//                        -------------------------------------
//                        |         LPW 0-7                   |
//                        -------------------------------------

    word24 addr  = (((word24) PCW_PAGE_TABLE_PTR) & MASK18) << 6;
    addr        += (((word24) seg) & 01) << 8;
    addr        += pageNumber;
    return addr;
  }

static void fetch_LPWPTW (chnlState_t * chnlp)
  {
    //atomic_thread_fence (memory_order_acquire);
    uint iom_unit_idx = chnlp->myIomIdx;
    word24 addr         = build_LPWPTW_address (chnlp -> PCW_PAGE_TABLE_PTR,
                                      chnlp -> SEG,
                                      (chnlp -> LPW_DCW_PTR >> 10) & MASK8);
    iom_core_read (iom_unit_idx, addr, (word36 *) & chnlp -> PTW_LPW);
    if ((chnlp -> PTW_LPW & W36_C (0740000777747)) != W36_C (04))
      simWarn ("%s: chan %d addr %#o ptw %012"PRIo64"\n", __func__, chnlp->myChanIdx, addr, chnlp->PTW_LPW);
    //atomic_thread_fence (memory_order_release);
  }

// 'write' means peripheral write; i.e. the peripheral is writing to core after
// reading media.

void iomDirectDataService (chnlState_t * chnlp, word24 daddr, word36 * data,
                              iom_direct_data_service_op op)
  {
    uint iom_unit_idx = chnlp->myIomIdx;
    // The direct data service consists of one core storage cycle (Read Clear, Read
    // Restore or Clear Write, Double or Single Precision) using an absolute
    // 24-bit address supplied by the channel. This service is used by
    // "peripherals" capable of providing addresses from an external source,
    // such as the 355 Direct Interface Adapter.

    // The PCW received for the channel contains a Page Table Pointer
    // which is used as an absolute address pointing to the Page Table which
    // has been set up by software for the Direct Channel. All addresses
    // for the Direct Channel will be accessed using this Page Table if paging
    // has been requested in the PCW.

    //atomic_thread_fence (memory_order_acquire);

    if (chnlp -> masked)
      return;

    if (chnlp -> PCW_63_PTP && chnlp -> PCW_64_PGE)
      {
        fetch_DDSPTW (chnlp, daddr);
        daddr = ((uint) getbits36_14 (chnlp -> PTW_DCW, 4) << 10) | (daddr & MASK10);
      }

#ifdef TCPX
if (op == direct_store && daddr >= 6100 && daddr < 7000) FTRC (daddr, * data);
#endif
    if (op == direct_store)
      iom_core_write (iom_unit_idx, daddr, * data);
    else if (op == direct_load)
      iom_core_read (iom_unit_idx, daddr, data);
    else if (op == direct_read_clear)
      {
        iom_core_read_lock (iom_unit_idx, daddr, data);
        iom_core_write_unlock (iom_unit_idx, daddr, 0);
      }
  }

// 'tally' is the transfer size request by Multics.
// For write, '*cnt' is the number of words in 'data'.
// For read, '*cnt' will be set to the number of words transferred.
// Caller responsibility to allocate 'data' large enough to accommodate
// 'tally' words.
void iomIndirectDataService (chnlState_t * chnlp, word36 * data, uint * cnt, bool write) {
  //atomic_thread_fence (memory_order_acquire);

  if (chnlp->masked)
    return;

  // tape_ioi_io.pl1  "If the initiate bit is set in the status, no data was
  //                   transferred (no tape movement occurred)."
  chnlp->initiate = false;

  uint tally = chnlp->DDCW_TALLY;
  uint daddr = chnlp->DDCW_ADDR;
  if (tally == 0) {
    tally = 4096;
  }
  chnlp->tallyResidue = (word12) tally;

  if (write) {
    uint c = * cnt;
    while (chnlp->tallyResidue) {
      if (c == 0)
        break; // read buffer exhausted; returns w/tallyResidue != 0

      if (! IS_IONTP (chnlp)) {
        if (chnlp->PCW_63_PTP && chnlp->PCW_64_PGE) {
          fetch_IDSPTW (chnlp, daddr);
          word24 addr = ((word24) (getbits36_14 (chnlp -> PTW_DCW, 4) << 10)) | (daddr & MASK10);
          iom_core_write (chnlp->myIomIdx, addr, * data);
        } else {
          if (daddr > MASK18) { // 256K overflow
            simWarn ("%s 256K ovf\n", __func__); // XXX
            daddr &= MASK18;
          }

          // If PTP is not set, we are in cm1e or cm2e. Both are 'EXT DCW', so
          // we can elide the mode check here.
          uint daddr2 = daddr | (uint) chnlp->ADDR_EXT << 18;
          iom_core_write (chnlp->myIomIdx, daddr2, * data);
        }
      }
      daddr ++;
      data ++;
      chnlp->tallyResidue --;
      c --;
    }
  } else { // read
    uint c = 0;
    while (chnlp -> tallyResidue) {
      if (IS_IONTP (chnlp)) {
        * data = 0;
      } else {
        if (chnlp -> PCW_63_PTP  && chnlp -> PCW_64_PGE) {
          fetch_IDSPTW (chnlp, daddr);
          word24 addr = ((word24) (getbits36_14 (chnlp -> PTW_DCW, 4) << 10)) | (daddr & MASK10);
          iom_core_read (chnlp->myIomIdx, addr, data);
        } else {
          // XXX assuming DCW_ABS
          if (daddr > MASK18) { // 256K overflow
            simWarn ("%s 256K ovf\n", __func__); // XXX
            daddr &= MASK18;
          }

          // If PTP is not set, we are in cm1e or cm2e. Both are 'EXT DCW', so
          // we can elide the mode check here.
          uint daddr2 = daddr | (uint) chnlp -> ADDR_EXT << 18;
          iom_core_read (chnlp->myIomIdx, daddr2, data);
        }
      }
      daddr ++;
      chnlp->tallyResidue --;
      data ++;
      c ++;
    }
    * cnt = c;
  }
  //atomic_thread_fence (memory_order_release);
}

static void update_chan_mode (chnlState_t * chnlp, bool tdcw)
  {
    //atomic_thread_fence (memory_order_acquire);
    uint chan = chnlp->myChanIdx;
    if (chan == IOM_CONNECT_CHAN)
      {
        chnlp -> chanMode = cm1;
        return;
      }

    if (! tdcw)
      {
        if (chnlp -> PCW_64_PGE == 0)
          {

            if (chnlp -> LPW_20_AE == 0)
              {
                    chnlp -> chanMode = cm1e;  // AE 0
              }
            else
              {
                    chnlp -> chanMode = cm2e;  // AE 1
              }

          }
        else
          {
            if (chnlp -> LPW_20_AE == 0)
              {

                if (chnlp -> LPW_23_REL == 0)
                  {
                    chnlp -> chanMode = cm1;  // AE 0, REL 0
                  }
                else
                  {
                    chnlp -> chanMode = cm3a;  // AE 0, REL 1
                  }

              }
            else  // AE 1
              {

                if (chnlp -> LPW_23_REL == 0)
                  {
                    chnlp -> chanMode = cm3b;  // AE 1, REL 0
                  }
                else
                  {
                    chnlp -> chanMode = cm4;  // AE 1, REL 1
                  }

              }
          }
      }
    else // tdcw
      {
        switch (chnlp -> chanMode)
          {
            case cm1:
              if (chnlp -> TDCW_32_PDTA)
                {
                  chnlp -> chanMode = cm2;
                  break;
                }
              if (chnlp -> TDCW_33_PDCW)
                if (chnlp -> TDCW_35_REL)
                  chnlp -> chanMode = cm4; // 33, 35
                else
                  chnlp -> chanMode = cm3b; // 33, !35
              else
                if (chnlp -> TDCW_35_REL)
                  chnlp -> chanMode = cm3a; // !33, 35
                else
                  chnlp -> chanMode = cm2; // !33, !35
              break;

            case cm2:
              if (chnlp -> TDCW_33_PDCW)
                if (chnlp -> TDCW_35_REL)
                  chnlp -> chanMode = cm4; // 33, 35
                else
                  chnlp -> chanMode = cm3b; // 33, !35
              else
                if (chnlp -> TDCW_35_REL)
                  chnlp -> chanMode = cm3a; // !33, 35
                else
                  chnlp -> chanMode = cm2; // !33, !35
              break;

            case cm3a:
              if (chnlp -> TDCW_33_PDCW)
                chnlp -> chanMode = cm4;
              break;

            case cm3b:
              if (chnlp -> TDCW_35_REL)
                chnlp -> chanMode = cm4;
              break;

            case cm4:
              chnlp -> chanMode = cm5;
              break;

            case cm5:
              break;

            case cm1e:
              {
                if (chnlp -> chanMode == cm1e && chnlp -> TDCW_33_EC)
                  chnlp -> chanMode = cm2e;
              }
              break;

            case cm2e:
              break;
          } // switch
      } // tdcw
    //atomic_thread_fence (memory_order_release);
  }

static void write_LPW (chnlState_t * chnlp)
  {
    //atomic_thread_fence (memory_order_acquire);
    uint iom_unit_idx = chnlp->myIomIdx;
    uint chan = chnlp->myChanIdx;
    uint chanLoc = mbxLoc (chnlp);
    iom_core_write (iom_unit_idx, chanLoc + IOM_MBX_LPW, chnlp -> LPW);
    if (chan != IOM_CONNECT_CHAN)
      {
        iom_core_write (iom_unit_idx, chanLoc + IOM_MBX_LPWX, chnlp -> LPWX);
      }
  }

#if defined(TESTING)
void dumpDCW (word36 DCW, word1 LPW_23_REL) {
  //atomic_thread_fence (memory_order_acquire);
  static char * charCtrls[4] = {"terminate", "undefined", "proceed", "marker"};
  static char * chanCmds[16] = {"record", "undefined", "nondata", "undefined", "undefined", "undefined", "multirecord", "undefined", "character", "undefined", "undefined", "undefined", "undefined", "undefined", "undefined", "undefined"};
  word3 DCW_18_20_CP =      getbits36_3 (DCW, 18);

  if (DCW_18_20_CP == 07) { // IDCW
    word6 IDCW_DEV_CMD  =     getbits36_6 (DCW,  0);
    word6 IDCW_DEV_CODE =     getbits36_6 (DCW,  6);
 /* word6 IDCW_AE =           getbits36_6 (DCW,  12);
  * word1 IDCW_EC;
  * if (LPW_23_REL)
  *   IDCW_EC = 0;
  * else
  *   IDCW_EC =               getbits36_1 (DCW, 21); */
    word2 IDCW_CHAN_CTRL =    getbits36_2 (DCW, 22);
    word6 IDCW_CHAN_CMD  =    getbits36_6 (DCW, 24);
    word6 IDCW_COUNT     =    getbits36_6 (DCW, 30);
    simPrintf ("//   IDCW %012"PRIo64"\n", DCW);
    simPrintf ("//     cmd             %02o %s\n", IDCW_DEV_CMD, cmdNames[IDCW_DEV_CMD]);
    simPrintf ("//     dev code        %02o\n", IDCW_DEV_CODE);
    simPrintf ("//     ctrl             %o (%s)\n", IDCW_CHAN_CTRL, charCtrls[IDCW_CHAN_CTRL]);
    simPrintf ("//     chancmd          %o (%s)\n", IDCW_CHAN_CMD, IDCW_CHAN_CMD < 16 ? chanCmds[IDCW_CHAN_CMD] : "unknown");
    simPrintf ("//     count            %o\n", IDCW_COUNT);
  } else { // TDCW or DDCW
    word18 TDCW_DATA_ADDRESS = getbits36_18 (DCW,  0);
    word1  TDCW_31_SEG       = getbits36_1  (DCW, 31);
    word1  TDCW_32_PDTA      = getbits36_1  (DCW, 32);
    word1  TDCW_33_PDCW      = getbits36_1  (DCW, 33);
    word1  TDCW_33_EC        = getbits36_1  (DCW, 33);
    word1  TDCW_34_RES       = getbits36_1  (DCW, 34);
    word1  TDCW_35_REL       = getbits36_1  (DCW, 35);

    word12 DDCW_TALLY        = getbits36_12 (DCW, 24);
    word18 DDCW_ADDR         = getbits36_18 (DCW,  0);
    word2  DDCW_22_23_TYPE   = getbits36_2  (DCW, 22);
    static char * types[4]   = { "IOTD", "IOTP", "TDCW", "IONTP" };
    if (DDCW_22_23_TYPE == 2) {
      simPrintf ("//   TDCW (2) %012"PRIo64"\n", DCW);
      simPrintf ("//     dcw ptr   %06o\n", TDCW_DATA_ADDRESS);
      simPrintf ("//     seg            %o\n", TDCW_31_SEG);
      simPrintf ("//     pdta           %o\n", TDCW_32_PDTA);
      simPrintf ("//     pdcw           %o\n", TDCW_33_PDCW);
      simPrintf ("//     ec             %o\n", TDCW_33_EC);
      simPrintf ("//     res            %o\n", TDCW_34_RES);
      simPrintf ("//     rel            %o\n", TDCW_35_REL);
    } else {
      simPrintf ("//   %s (%o) %012"PRIo64"\n", types[DDCW_22_23_TYPE], DDCW_22_23_TYPE, DCW);
      simPrintf ("//     tally       %04o\n", DDCW_TALLY);
      simPrintf ("//     addr            %02o\n", DDCW_ADDR);
      simPrintf ("//     cp               %o\n", getbits36_3 (DCW, 18));
    }
  }
}

#if 0
static void dumpLPW (chnlState_t * chnlp) {
  //atomic_thread_fence (memory_order_acquire);
  chnlState_t * chnlp = & iomChanData[iom_unit_idx][chan];
  char updateCase;
  if (chnlp->LPW_21_NC)
    updateCase = 'c';
  else if (chnlp->LPW_22_TAL)
    updateCase = 'b';
  else
    updateCase = 'a';
  static char * chanName[64] = {
        "0.",           //  0.   0
        "1.",           //  1.   1
        "Connect",      //  2.   2
        "3.",           //  3.   3
        "4.",           //  4.   4
        "5.",           //  5.   5
        "6.",           //  6.   6
        "7.",           //  7.   7
        "8.",           //  8.  10
        "9.",           //  9.  11
        "MTP Tape",     // 10.  12
        "IPC Disk",     // 11.  13
        "MPC Disk",     // 12.  14
        "Card reader",  // 13.  15
        "Card punch",   // 14.  16
        "Printer",      // 15.  17
        "FNP",          // 16.  20
        "FNP",          // 17.  21
        "FNP",          // 18.  22
        "FNP",          // 19.  23
        "FNP",          // 20.  24
        "FNP",          // 21.  25
        "FNP",          // 22.  26
        "FNP",          // 23.  27
        "24.",          // 24.  30
        "25.",          // 25.  31
        "ABSI",         // 26.  32
        "27.",          // 27.  33
        "28.",          // 28.  34
        "29.",          // 29.  35
        "Console",      // 30.  36
        "31.",          // 31.  37
        "Socket",       // 32.  40
        "Socket",       // 33.  41
        "Socket",       // 34.  42
        "Socket",       // 35.  43
        "Socket",       // 36.  44
        "Socket",       // 37.  45
        "Socket",       // 38.  46
        "39.",
        "40.",
        "41.",
        "42.",
        "43.",
        "44.",
        "45.",
        "46.",
        "47.",
        "48.",
        "49.",
        "50.",
        "51.",
        "52.",
        "53.",
        "54.",
        "55.",
        "56.",
        "57.",
        "58.",
        "59.",
        "60.",
        "61.",
        "62.",
        "63."
  };
  simPrintf ("// %s channel LPW %012"PRIo64" (case %c)\n", chanName[chan], chnlp->LPW, updateCase);
  simPrintf ("//   DCW pointer %06o\n",      chnlp->LPW_DCW_PTR);
  simPrintf ("//     RES              %o\n", chnlp->LPW_18_RES);
  simPrintf ("//     REL              %o\n", chnlp->LPW_19_REL);
  simPrintf ("//     AE               %o\n", chnlp->LPW_20_AE);
  simPrintf ("//     NC 21            %o\n", chnlp->LPW_21_NC);
  simPrintf ("//     TAL 22           %o\n", chnlp->LPW_22_TAL);
  simPrintf ("//     REL              %o\n", chnlp->LPW_23_REL);
  simPrintf ("//     TALLY         %04o\n",  chnlp->LPW_TALLY);
  simPrintf ("// \n");
}
#endif
#endif

static void fetch_and_parse_LPW (chnlState_t * chnlp)
  {
    //atomic_thread_fence (memory_order_acquire);
    uint chanLoc        = mbxLoc (chnlp);
    uint iom_unit_idx = chnlp->myIomIdx;
    uint chan = chnlp->myChanIdx;

    iom_core_read (iom_unit_idx, chanLoc + IOM_MBX_LPW, (word36 *) & chnlp -> LPW);
    chnlp -> LPW_DCW_PTR = getbits36_18 (chnlp -> LPW,  0);
    chnlp -> LPW_18_RES  = getbits36_1  (chnlp -> LPW, 18);
    chnlp -> LPW_19_REL  = getbits36_1  (chnlp -> LPW, 19);
    chnlp -> LPW_20_AE   = getbits36_1  (chnlp -> LPW, 20);
    chnlp -> LPW_21_NC   = getbits36_1  (chnlp -> LPW, 21);
    chnlp -> LPW_22_TAL  = getbits36_1  (chnlp -> LPW, 22);
    chnlp -> LPW_23_REL  = getbits36_1  (chnlp -> LPW, 23);
    chnlp -> LPW_TALLY   = getbits36_12 (chnlp -> LPW, 24);

    if (chan == IOM_CONNECT_CHAN)
      {
        chnlp -> LPWX = 0;
        chnlp -> LPWX_BOUND = 0;
        chnlp -> LPWX_SIZE = 0;
      }
    else
      {
        iom_core_read (iom_unit_idx, chanLoc + IOM_MBX_LPWX, (word36 *) & chnlp -> LPWX);
        chnlp -> LPWX_BOUND = getbits36_18 (chnlp -> LPWX, 0);
        chnlp -> LPWX_SIZE  = getbits36_18 (chnlp -> LPWX, 18);
      }
    update_chan_mode (chnlp, false);
    //atomic_thread_fence (memory_order_release);
  }

static void unpack_DCW (chnlState_t * chnlp)
  {
    //atomic_thread_fence (memory_order_acquire);
    chnlp -> DCW_18_20_CP =      getbits36_3 (chnlp -> DCW, 18);

    if (IS_IDCW (chnlp)) // IDCW
      {
        chnlp -> IDCW_DEV_CMD   = getbits36_6 (chnlp -> DCW, 0);
        chnlp -> IDCW_DEV_CODE  = getbits36_6 (chnlp -> DCW, 6);
        chnlp -> IDCW_AE        = getbits36_6 (chnlp -> DCW, 12);
        if (chnlp -> LPW_23_REL)
          chnlp -> IDCW_EC      = 0;
        else
          chnlp -> IDCW_EC      = getbits36_1 (chnlp -> DCW, 21);
        if (chnlp -> IDCW_EC)
          chnlp -> SEG          = 1; // pat. step 45
        chnlp -> IDCW_CHAN_CTRL = getbits36_2 (chnlp -> DCW, 22);
        chnlp -> IDCW_CHAN_CMD  = getbits36_6 (chnlp -> DCW, 24);
        chnlp -> IDCW_COUNT     = getbits36_6 (chnlp -> DCW, 30);
        chnlp->recordResidue = chnlp->IDCW_COUNT;
      }
    else // TDCW or DDCW
      {
        chnlp -> TDCW_DATA_ADDRESS = getbits36_18 (chnlp -> DCW,  0);
        chnlp -> TDCW_31_SEG       = getbits36_1  (chnlp -> DCW, 31);
        chnlp -> TDCW_32_PDTA      = getbits36_1  (chnlp -> DCW, 32);
        chnlp -> TDCW_33_PDCW      = getbits36_1  (chnlp -> DCW, 33);
        chnlp -> TDCW_33_EC        = getbits36_1  (chnlp -> DCW, 33);
        chnlp -> TDCW_34_RES       = getbits36_1  (chnlp -> DCW, 34);
        chnlp -> TDCW_35_REL       = getbits36_1  (chnlp -> DCW, 35);
        chnlp -> DDCW_TALLY        = getbits36_12 (chnlp -> DCW, 24);
        chnlp -> DDCW_ADDR         = getbits36_18 (chnlp -> DCW,  0);
        chnlp -> DDCW_22_23_TYPE   = getbits36_2  (chnlp -> DCW, 22);
      }
    //atomic_thread_fence (memory_order_release);
  }

static void pack_DCW (chnlState_t * chnlp)
  {
    // DCW_18_20_CP is the only field ever changed.
    //atomic_thread_fence (memory_order_acquire);
    putbits36_3 ((word36 *) & chnlp -> DCW, 18, chnlp -> DCW_18_20_CP);
    //atomic_thread_fence (memory_order_release);
  }

static void pack_LPW (chnlState_t * chnlp)
  {
    //atomic_thread_fence (memory_order_acquire);
    putbits36_18 ((word36 *) & chnlp-> LPW,   0, chnlp -> LPW_DCW_PTR);
    putbits36_1  ((word36 *) & chnlp-> LPW,  18, chnlp -> LPW_18_RES);
    putbits36_1  ((word36 *) & chnlp-> LPW,  19, chnlp -> LPW_19_REL);
    putbits36_1  ((word36 *) & chnlp-> LPW,  20, chnlp -> LPW_20_AE);
    putbits36_1  ((word36 *) & chnlp-> LPW,  21, chnlp -> LPW_21_NC);
    putbits36_1  ((word36 *) & chnlp-> LPW,  22, chnlp -> LPW_22_TAL);
    putbits36_1  ((word36 *) & chnlp-> LPW,  23, chnlp -> LPW_23_REL);
    putbits36_12 ((word36 *) & chnlp-> LPW,  24, chnlp -> LPW_TALLY);
    putbits36_18 ((word36 *) & chnlp-> LPWX,  0, chnlp -> LPWX_BOUND);
    putbits36_18 ((word36 *) & chnlp-> LPWX, 18, chnlp -> LPWX_SIZE);
    //atomic_thread_fence (memory_order_release);
  }

static void fetch_and_parse_PCW (chnlState_t * chnlp)
  {
    //atomic_thread_fence (memory_order_acquire);
    uint iom_unit_idx = chnlp->myIomIdx;

    iom_core_read2 (iom_unit_idx, chnlp -> LPW_DCW_PTR, (word36 *) & chnlp -> PCW0, (word36 *) & chnlp -> PCW1);
    chnlp -> PCW_CHAN           = getbits36_6  (chnlp -> PCW1,  3);
    chnlp -> PCW_AE             = getbits36_6  (chnlp -> PCW0, 12);
    chnlp -> PCW_21_MSK         = getbits36_1  (chnlp -> PCW0, 21);
    chnlp -> PCW_PAGE_TABLE_PTR = getbits36_18 (chnlp -> PCW1,  9);
    chnlp -> PCW_63_PTP         = getbits36_1  (chnlp -> PCW1, 27);
    chnlp -> PCW_64_PGE         = getbits36_1  (chnlp -> PCW1, 28);
    chnlp -> PCW_65_AUX         = getbits36_1  (chnlp -> PCW1, 29);
    if (chnlp -> PCW_65_AUX)
      simWarn ("PCW_65_AUX\n");
    chnlp -> DCW = chnlp -> PCW0;
    unpack_DCW (chnlp);
    //atomic_thread_fence (memory_order_release);
  }

static void fetch_and_parse_DCW (chnlState_t * chnlp, UNUSED bool read_only)
  {
    uint iom_unit_idx = chnlp->myIomIdx;
    word24 addr         = chnlp -> LPW_DCW_PTR & MASK18;

    switch (chnlp -> chanMode)
      {
        // LPW ABS
        case cm1:
        case cm1e:
          {
            iom_core_read (iom_unit_idx, addr, (word36 *) & chnlp -> DCW);
          }
          break;

        // LPW EXT
        case cm2e:
          {
            // LPXW_BOUND is mod 2; ie. val is * 2
            //addr |= ((word24) chnlp -> LPWX_BOUND << 18);
            addr += ((word24) chnlp -> LPWX_BOUND << 1);
            iom_core_read (iom_unit_idx, addr, (word36 *) & chnlp -> DCW);
          }
          break;

        case cm2:
        case cm3b:
          {
            simWarn ("fetch_and_parse_DCW LPW paged\n");
          }
          break;

        case cm3a:
        case cm4:
        case cm5:
          {
            fetch_LPWPTW (chnlp);
            // Calculate effective address
            // PTW 4-17 || LPW 8-17
            word24 addr_ = ((word24) (getbits36_14 (chnlp -> PTW_LPW, 4) << 10)) | ((chnlp -> LPW_DCW_PTR) & MASK10);
            iom_core_read (iom_unit_idx, addr_, (word36 *) & chnlp -> DCW);
          }
          break;
      }
    unpack_DCW (chnlp);
    ioms[iom_unit_idx].dcwCnt ++;
    //atomic_thread_fence (memory_order_release);
  }

/*
 * send_general_interrupt ()
 *
 * Send an interrupt from the IOM to the CPU.
 *
 */

int send_general_interrupt (chnlState_t * chnlp, enum iomImwPics pic)
  {
    uint iom_unit_idx = chnlp->myIomIdx;
    uint chan = chnlp->myChanIdx;
    uint imw_addr;
    uint chan_group    = chan < 32 ? 1 : 0;
    uint chan_in_group = chan & 037;

    uint iomUnitNum =
      iom_unit_data[iom_unit_idx].configSwMultiplexBaseAddress & 3u;
    uint interrupt_num = iomUnitNum | (chan_group << 2) | ((uint) pic << 3);
    // Section 3.2.7 defines the upper bits of the IMW address as
    // being defined by the mailbox base address switches and the
    // multiplex base address switches.
    // However, AN-70 reports that the IMW starts at 01200.  If AN-70 is
    // correct, the bits defined by the mailbox base address switches would
    // have to always be zero.  We'll go with AN-70.  This is equivalent to
    // using bit value 0010100 for the bits defined by the multiplex base
    // address switches and zeros for the bits defined by the mailbox base
    // address switches.
    //imw_addr += 01200;  // all remaining bits
    uint pi_base = iom_unit_data[iom_unit_idx].configSwMultiplexBaseAddress & ~3u;
    imw_addr = (pi_base << 3) | interrupt_num;

    word36 imw;
    iom_core_read_lock (iom_unit_idx, imw_addr, &imw);
    // The 5 least significant bits of the channel determine a bit to be
    // turned on.
    putbits36_1 (& imw, chan_in_group, 1);
    iom_core_write_unlock (iom_unit_idx, imw_addr, imw);
    return scuSetInterrupt (iom_unit_data[iom_unit_idx].invokingScuUnitIdx, interrupt_num);
  }

static void iom_fault (chnlState_t * chnlp, UNUSED const char * who,
                      iomFaultServiceRequest req,
                      iomSysFaults_t signal)
  {
    uint iom_unit_idx = chnlp->myIomIdx;
    uint chan = chnlp->myChanIdx;

    simWarn ("iom_fault %s\n", who);

    // chnlState_t * chnlp = & iomChanData[iom_unit_idx][chan];
    // TODO:
    // For a system fault:
    // Store the indicated fault into a system fault word (3.2.6) in
    // the system fault channel -- use a list service to get a DCW to do so
    // For a user fault, use the normal channel status mechanisms

    // sys fault masks channel

    // signal gets put in bits 30..35, but we need fault code for 26..29

    // BUG: mostly unimplemented

    // User Faults: Pg 78: "A user fault is an abnormal conditions that can
    // be caused by a user program operating in the slave mode in the
    // processor."
    //  and
    // "User faults can be detected by the IOM Central of by a channel. If
    // a user fault is detected by the IOM Central, the fault is indicated to
    // the channel, and the channel is responsible for reporting the user
    // fault as status is its regular status queue.

    // This code only handles system faults
    //

    word36 faultWord = 0;
    putbits36_9 (& faultWord, 9, (word9) chan);
    putbits36_5 (& faultWord, 18, (word5) req);
    // IAC, bits 26..29
    putbits36_6 (& faultWord, 30, (word6) signal);

    uint chanloc = mbxLoc (& iomChanData[iom_unit_idx][IOM_SYSTEM_FAULT_CHAN]);

    word36 lpw;
    iom_core_read (iom_unit_idx, chanloc + IOM_MBX_LPW, & lpw);

    word36 scw;
    iom_core_read (iom_unit_idx, chanloc + IOM_MBX_SCW, & scw);

    word36 dcw;
    iom_core_read_lock (iom_unit_idx, chanloc + IOM_MBX_DCW, & dcw);

    iom_core_write (iom_unit_idx, (dcw >> 18) & MASK18, faultWord);

    uint tally = dcw & MASK12;
    if (tally > 1)
      {
        dcw -= W36_C (01);  // tally --
        dcw += W36_C (01000000); // addr ++
      }
    else
      dcw = scw; // reset to beginning of queue
    iom_core_write_unlock (iom_unit_idx, chanloc + IOM_MBX_DCW, dcw);

    send_general_interrupt (& iomChanData[iom_unit_idx][IOM_SYSTEM_FAULT_CHAN], imwSystemFaultPic);
  }

// 0 ok
// -1 fault
// There is a path through the code where no DCW is sent (IDCW && LPW_18_RES)
// Does the -1 return cover that?

int iomListService (chnlState_t * chnlp, bool * ptro, bool * sendp, bool * uffp)
  {
    //atomic_thread_fence (memory_order_acquire);
    uint iom_unit_idx = chnlp->myIomIdx;
    uint chan = chnlp->myChanIdx;

// initialize

    bool isConnChan = chan == IOM_CONNECT_CHAN;
    * ptro          = false; // assume not PTRO
    bool uff        = false; // user fault flag
    bool send       = false;

// Figure 4.3.1

// START

    // FIRST SERVICE?

    if (chnlp -> lsFirst)
      {
        // PULL LPW AND LPW EXT. FROM CORE MAILBOX

        fetch_and_parse_LPW (chnlp);
        chnlp -> wasTDCW = false;
        chnlp -> SEG     = 0; // pat. FIG. 2, step 44
      }
    // else lpw and lpwx are in chanData;

    // CONNECT CHANNEL?

    if (isConnChan)
      { // connect channel

        // LPW 21 (NC), 22 (TAL) is {00, 01, 1x}?

        if (chnlp -> LPW_21_NC == 0 && chnlp -> LPW_22_TAL == 0)
              {
                iom_fault (& iomChanData[iom_unit_idx][IOM_CONNECT_CHAN], __func__,
                          chnlp -> lsFirst ? iomFsrFirstList : iomFsrList,
                          iomIllTalCChnFlt);
                return -1;
              }

        if (chnlp -> LPW_21_NC == 0 && chnlp -> LPW_22_TAL == 1)
          { // 01

            // TALLY is {0, 1, >1}?

            if (chnlp -> LPW_TALLY == 0)
              {
                iom_fault (& iomChanData[iom_unit_idx][IOM_CONNECT_CHAN], __func__,
                          chnlp -> lsFirst ? iomFsrFirstList : iomFsrList,
                          iomIllTalCChnFlt);
                return -1;
              }

            if (chnlp -> LPW_TALLY > 1)
              { // 00

                // 256K OVERFLOW?
                if (chnlp -> LPW_20_AE == 0 &&
                    (((word36) chnlp -> LPW_DCW_PTR) + ((word36) chnlp -> LPW_TALLY)) >
                    W36_C (01000000))
                  {
                    iom_fault (& iomChanData[iom_unit_idx][IOM_CONNECT_CHAN], __func__,
                              chnlp -> lsFirst ? iomFsrFirstList : iomFsrList,
                              iom256KFlt);
                    return -1;
                  }
              }
            else if (chnlp -> LPW_TALLY == 1)
              * ptro = true;
          }
        else // 1x
          * ptro = true;

        // PULL PCW FROM CORE

// B

        fetch_and_parse_PCW (chnlp); // fills in DCW*

        // PCW 18-20 == 111?
        if (IS_NOT_IDCW (chnlp))
          {
            iom_fault (& iomChanData[iom_unit_idx][IOM_CONNECT_CHAN], __func__,
                      chnlp -> lsFirst ? iomFsrFirstList : iomFsrList,
                      iomNotPCWFlt);
            return -1;
          }

        // SELECT CHANNEL

        // chan = chnlp -> PCW_CHAN;

// detect unused slot as fault
// if (no handler in slot)
//  goto FAULT;

       // SEND PCW TO CHANNEL

        // chnlp -> DCW = chnlp -> DCW;
        send = true;

        goto D;
      }

    // Not connect channel

    // LPW 21 (NC), 22 (TAL) is {00, 01, 1x}?

    if (chnlp -> LPW_21_NC == 0 && chnlp -> LPW_22_TAL == 0)
      {
        // XXX see pat. 46-51 re: SEG
        // 256K OVERFLOW?
        if (chnlp -> LPW_20_AE == 0 &&
            (((word36) chnlp -> LPW_DCW_PTR) + ((word36) chnlp -> LPW_TALLY)) > W36_C (01000000))
          {
            iom_fault (& iomChanData[iom_unit_idx][IOM_CONNECT_CHAN], __func__,
                      chnlp -> lsFirst ? iomFsrFirstList : iomFsrList,
                      iom256KFlt);
            return -1;
          }
       }
    else if (chnlp -> LPW_21_NC == 0 && chnlp -> LPW_22_TAL == 1)
      { // 01
A:;
        // TALLY is {0, 1, >1}?

        if (chnlp -> LPW_TALLY == 0)
          {
            uff = true;
          }
        else if (chnlp -> LPW_TALLY > 1)
          { // 00

            // XXX see pat. 46-51 re: SEG
            // 256K OVERFLOW?
            if (chnlp -> LPW_20_AE == 0 &&
                (((word36) chnlp -> LPW_DCW_PTR) + ((word36) chnlp -> LPW_TALLY)) > W36_C (01000000))
              {
                iom_fault (& iomChanData[iom_unit_idx][IOM_CONNECT_CHAN], __func__,
                          chnlp -> lsFirst ? iomFsrFirstList : iomFsrList,
                          iom256KFlt);
                return -1;
              }
          }
      }

    // LPW 20? -- LPW_20 checked by fetch_and_parse_DCW

    // PULL DCW FROM CORE
    fetch_and_parse_DCW (chnlp, false);

// C

    // IDCW ?

    if (IS_IDCW (chnlp))
      {

        // LPW_18_RES?

        if (chnlp -> LPW_18_RES)
          {
            // SET USER FAULT FLAG
            uff = true; // XXX Why? uff isn't not examined later.
            // send = false; implicit...
          }
        else
          {
            // SEND IDCW TO CHANNEL
            // chnlp -> DCW = chnlp -> DCW;
            send = true;
          }
        chnlp -> LPWX_SIZE = chnlp -> LPW_DCW_PTR;
        goto D;
      }

// Not IDCW

    if (chnlp -> lsFirst)
      chnlp -> LPWX_SIZE = chnlp -> LPW_DCW_PTR;

// pg B16: "If the IOM is paged [yes] and PCW bit 64 is off, LPW bit 23
//          is ignored by the hardware. If bit 64 is set, LPW bit 23 causes
//          the data to become segmented."
// Handled in fetch_and_parse_LPW

    // LPW 23 REL?

    // TDCW ?

    if (IS_TDCW (chnlp))
      {
        // SECOND TDCW?
        if (chnlp -> wasTDCW)
          {
            uff = true;
            goto uffSet;
          }
        chnlp -> wasTDCW = true;

        // PUT ADDRESS N LPW

        chnlp -> LPW_DCW_PTR = chnlp -> TDCW_DATA_ADDRESS;
        // OR TDCW 33, 34, 35 INTO LPW 20, 18, 23
        // XXX is 33 bogus? its semantics change based on PCW 64...
        // should be okay; pg B21 says that this is correct; implies that the
        // semantics are handled in the LPW code. Check...
        chnlp -> LPW_20_AE  |= chnlp -> TDCW_33_EC; // TDCW_33_PDCW
        chnlp -> LPW_18_RES |= chnlp -> TDCW_34_RES;
        chnlp -> LPW_23_REL |= chnlp -> TDCW_35_REL;

// Pg B21: (TDCW_31_SEG)
// "SEG = This bit furnishes the 19th address bit (MSD) of a TDCW address
//  used for locating the DCW list in a 512 word page table. It will have
//  meaning only in the TDCW where:
//   (a) the DCW list is already paged and the TDCW calls for the
//       DCW [to be] segmented
//  or
//   (b) the data is already segmented and the TDCW calls for the
//       DCW list to be paged
//  or
//   (c) neither data is segmented nor DCW list is paged and the
//       TDCW calls for both.
//  and
//   (d) an auxiliary PTW in not being used.

//   (a) the DCW list is already paged   --  LPW PAGED: 3b, 4
//       and the TDCW calls for the
//       DCW [list to be] segmented      --  LPW SEG:       5

//   (b) the data is already segmented   --  DCW SEG:   3a, 4
//       and the TDCW calls for the
//       DCW list to be paged            --  DCW PAGED:  2, 3b

//   (c) neither data is segmented       -- DCW !SEG     1, 2, 3b
//       nor DCW list is paged           -- LPW !PAGED   1, 2, 3a
//                                       --              1, 2
//       and the TDCW calls for both.    -- DCW SEG & LPW PAGED
//                                                       4

//put that weird SEG logic in here

        if (chnlp -> TDCW_31_SEG)
          simWarn ("TDCW_31_SEG\n");

        update_chan_mode (chnlp, true);

        // Decrement tally
        chnlp -> LPW_TALLY = (chnlp -> LPW_TALLY - 1u) & MASK12;

        pack_LPW (chnlp);

        // AC CHANGE ERROR? (LPW 18 == 1 && DCW 33 == 1)

        if (chnlp -> LPW_18_RES && chnlp -> TDCW_33_EC) // same as TDCW_33_PDCW
          {
            uff = true;
            goto uffSet;
          }

        goto A;
      }

    chnlp -> wasTDCW = false;
    // NOT TDCW

    // CP VIOLATION?

    // 43A239854 3.2.3.3 "The byte size, defined by the channel, determines
    // what CP values are valid..."

    // If we get here, the DCW is not a IDCW and not a TDCW, therefore
    // it must be a DDCW. If the list service knew the sub-word size
    // size of the device, it could check the for valid values. Let
    // the device handler do that later.

    // if (cp decrepancy)
    //   {
    //      user_fault_flag = iomCsCpDiscrepancy;
    //      goto user_fault;
    //   }
    // if (cp violation)
    //   {
    //     uff = true;
    //     goto uffSet;
    //   }

    // USER FAULT FLAG SET?

    if (uff)
      {
uffSet:;
        // PUT 7 into DCW 18-20
        chnlp -> DCW_18_20_CP = 07u;
        pack_DCW (chnlp);
      }

    // WRITE DCW IN [SCRATCH PAD] MAILBOX

    // chnlp -> DVW  = P -> DCW;

    // SEND DCW TO CHANNEL

    // chnlp -> DCW = chnlp -> DCW;
    send = true;

    // goto D;

D:;

    // SEND FLAGS TO CHANNEL

    * uffp  = uff;
    * sendp = send;

    // LPW 21 ?

    if (chnlp -> LPW_21_NC == 0) // UPDATE
     {
       // UPDATE LPW ADDRESS & TALLY
       if (isConnChan)
         chnlp -> LPW_DCW_PTR = (chnlp -> LPW_DCW_PTR + 2u) & MASK18;
       else
         chnlp -> LPW_DCW_PTR = (chnlp -> LPW_DCW_PTR + 1u) & MASK18;
       //chnlp -> LPW_TALLY = (chnlp -> LPW_TALLY - 1u) & MASK12;
       // ubsan
       chnlp->LPW_TALLY = ((word12) (((word12s) chnlp->LPW_TALLY) - 1)) & MASK12;
       pack_LPW (chnlp);
     }

    // IDCW OR FIRST LIST
    if (chnlp -> DDCW_22_23_TYPE == 07u || chnlp -> lsFirst)
      {
        // WRITE LPW & LPW EXT. INTO BOTH SCRATCHPAD AND CORE MAILBOXES
        // scratch pad
        // chnlp -> lpw = chnlp -> lpw
        // core
        write_LPW (chnlp);
      }
    else
      {
        if (chnlp -> LPW_21_NC == 0 ||
            (IS_TDCW (chnlp)))
          {
            // WRITE LPW INTO BOTH SCRATCHPAD AND CORE MAILBOXES
            // scratch pad
            // chnlp -> lpw = chnlp -> lpw
            // core
            write_LPW (chnlp);
          }
      }

    chnlp -> lsFirst = false;
    // END

    //atomic_thread_fence (memory_order_release);
    return 0;
  }

// 0 ok
// -1 uff
static int doPayloadChannel (chnlState_t * chnlp) {
// A dubious assumption being made is that the device code will always
// be in bits 6-12 of the DCw. Normally, the controller would
// decipher the device code and route to the device, but we
// have elided the controllers and must do that ourselves.

// Loop logic
//
//   The DCW list can be terminated two ways; either by having the
//   tally run out, or a IDCW with control set to zero.
//
//   The device handler will absorb DDCWs
//
// loop until:
//
//  listService sets ptro, indicating that no more DCWs are available. or
//     control is 0, indicating last IDCW

#ifdef TANDD_DEBUG
sim_printf ("chan %d\n", chan);
#endif

  uint iomUnitIdx = chnlp->myIomIdx;
  uint chan = chnlp->myChanIdx;

  chnlp -> chanMode   = cm1;
  chnlp -> LPW_18_RES = 0;
  chnlp -> LPW_20_AE  = 0;
  chnlp -> LPW_23_REL = 0;

  unpack_DCW (chnlp);

  chnlp->isPCW = true;

  // Masked is set in doConnectChannel
  //chnlp->masked = !!chnlp->PCW_21_MSK;
  struct iom_to_ctlr_s * d = & cables->iom_to_ctlr[iomUnitIdx][chan];

#if 0
// A device command of 051 in the PCW is only meaningful to the operator console;
// all other channels should ignore it. We use (somewhat bogusly) a chanType of
// chanTypeCPI to indicate the operator console.
  if ((! d->isConsole) && chnlp->IDCW_DEV_CMD == 051) {
    chnlp->stati = 04501;
    send_terminate_interrupt (chnlp);
    return 0;
  }
#endif

  if ((!d->socket_in_use) || (!d->iom_cmd)) {
    chnlp -> stati = 06000; // t, power off/missing
    goto terminate;
  }

// 3.2.2. "Bits 12-17 [of the PCW] contain the address extension which is maintained by
//         the channel for subsequent use by the IOM in generating a 24-bit
//         address for list of data services for the extended address modes."
// see also 3.2.3.1
  chnlp -> ADDR_EXT     = chnlp -> PCW_AE;

  chnlp -> lsFirst      = true;

  chnlp -> tallyResidue = 0;
  chnlp -> isRead       = true;
  chnlp -> charPos      = 0;
// As far as I can tell, initiate false means that an IOTx succeeded in
// transferring data; assume it didn't since that is the most common
// code path.
  chnlp -> initiate     = true;
  chnlp -> chanStatus   = chanStatNormal;

//
// Send the PCW's DCW
//
//if (chan == 027) simPrintf ("PCW DCW %012lo\n", chnlp->DCW);
  int rc = d->iom_cmd (chnlp);

  if (rc < 0) {
    chnlp -> dev_code = getbits36_6 (chnlp -> DCW, 6);
    goto terminate;
  }

  if (rc == IOM_CMD_PENDING) // handler still processing command, don't set
    goto pending;                // terminate intrrupt.

  if (IS_IDCW (chnlp) && chnlp->IDCW_CHAN_CTRL == CHAN_CTRL_MARKER) { // IDCW marker bit set
    send_marker_interrupt (chnlp);
  }

  if (rc == IOM_CMD_DISCONNECT) {
    goto terminate;
  }

  if (chnlp->masked) {
    goto terminate;
  }

  bool ptro, send, uff;
  bool terminate = false;
  chnlp->isPCW       = false;

  (void)terminate;
  bool idcw_terminate = chnlp -> IDCW_CHAN_CTRL == CHAN_CTRL_TERMINATE;
  do {
    int rc2 = iomListService (chnlp, & ptro, & send, & uff);
    if (rc2 < 0) {
// XXX set status flags
      simWarn ("%s list service failed\n", __func__);
      return -1;
    }
    if (uff) {
      // We get a uff if the LPW tally hit 0
      goto terminate;
    }
    // List service failed to get a DCW
    if (! send) {
      simWarn ("%s nothing to send\n", __func__);
      return 1;
    }


// 3.2.3.1 "If EC - Bit 21 = 1, The channel will replace the present address
//          extension with the new address extension in bits 12-17. ... In
//          Multics and NSA systems, EC is inhibited from the payload channel
//          if LPW bit 23 = 1.

    if (IS_IDCW (chnlp)) { // IDCW
      idcw_terminate = chnlp -> IDCW_CHAN_CTRL == CHAN_CTRL_TERMINATE;
      if (chnlp -> LPW_23_REL == 0 && chnlp -> IDCW_EC == 1)
        chnlp -> ADDR_EXT = getbits36_6 (chnlp -> DCW, 12);

      chnlp -> tallyResidue = 0;
      chnlp -> isRead       = true;
      chnlp -> charPos      = 0;
      chnlp -> chanStatus   = chanStatNormal;
    }

// Send the DCW list's DCW

//if (chan == 027) simPrintf ("    DCW %012lo\n", chnlp->DCW);
    rc2 = d->iom_cmd (chnlp);

    if (rc2 < 0) {
      chnlp -> dev_code = getbits36_6 (chnlp -> DCW, 6);
      goto terminate;
    }

    if (rc2 == IOM_CMD_DISCONNECT) {
      terminate = true;
    }

    if (rc2 == IOM_CMD_PENDING) // handler still processing command, don't set
      goto pending;                // terminate interrupt.

    // If IDCW and terminate and nondata
    if (IS_IDCW (chnlp) && chnlp->IDCW_CHAN_CTRL == CHAN_CTRL_TERMINATE && chnlp->IDCW_CHAN_CMD == CHAN_CMD_NONDATA) {
      goto terminate;
    }
    // If IOTD and last IDCW was terminate
    if (IS_IOTD (chnlp) && idcw_terminate && rc2 != IOM_CMD_RESIDUE) {
      goto terminate;
    }

    // IOM_CMD_RESIDUE: Continue pushing DCWS until the record residue is 0

    if (IS_NOT_IDCW (chnlp) && rc2 == IOM_CMD_RESIDUE) {
      if (chnlp->recordResidue)
        chnlp->recordResidue --;
      if (chnlp->recordResidue == 0)
        goto terminate;
    }
  } while (! terminate);

terminate:;
//if (chan == 027) { simPrintf ("doPayloadChan sends terminate\n"); }
  //atomic_thread_fence (memory_order_release);
  send_terminate_interrupt (chnlp);
  return 0;

pending:;
  //atomic_thread_fence (memory_order_release);
  return 0;
}

static void doPayloadConnect (chnlState_t * chnlp) {
  uint iomUnitIdx = chnlp->myIomIdx;
  uint chan = chnlp->myChanIdx;
  struct iom_to_ctlr_s * d = & cables->iom_to_ctlr[iomUnitIdx][chan];

  // A device command of 051 in the PCW is only meaningful to the operator console;
  // all other channels should ignore it.
  if (d->socket_in_use && (! d->isConsole) && chnlp->IDCW_DEV_CMD == 051) {
    chnlp->stati = 04501;
    send_terminate_interrupt (chnlp);
    return;
  }

  if (d->socket_in_use && d->iom_connect)
    d->iom_connect (chnlp);
  else
    doPayloadChannel (chnlp);
}

// doConnectChan ()
//
// Process the "connect channel".  This is what the IOM does when it
// receives a $CON signal.
//
// Only called by iom_interrupt ()
//
// The connect channel requests one or more "list services" and processes the
// resulting PCW control words.
//

static int doConnectChan (uint iom_unit_idx) {
  // ... the connect channel obtains a list service from the IOM Central.
  // During this service the IOM Central will do a double precision read
  // from the core, under the control of the LPW for the connect channel.
  //
  // If the IOM Central does not indicate a PTRO during the list service,
  // the connect channel obtains another list service.
  //
  // The connect channel does not interrupt or store status.
  //
  // The DCW and SCW mailboxes for the connect channel are not used by
  // the IOM.
  //atomic_thread_fence (memory_order_acquire);

  ioms[iom_unit_idx].connectCnt ++;
  chnlState_t * chnlp = & iomChanData[iom_unit_idx][IOM_CONNECT_CHAN];
  chnlp -> lsFirst = true;
  bool ptro, send, uff;
  do {
    // Fetch the next PCW
    int rc = iomListService (& iomChanData[iom_unit_idx][IOM_CONNECT_CHAN], & ptro, & send, & uff);
    if (rc < 0) {
      simWarn ("connect channel connect failed\n");
      return -1;
    }
    if (uff) {
      simWarn ("connect channel ignoring uff\n"); // XXX
    }
    if (! send) {
      simWarn ("connect channel nothing to send\n");
    } else {
      // Copy the PCW's DCW to the payload channel
      chnlState_t * q = & iomChanData[iom_unit_idx][chnlp -> PCW_CHAN];

      q -> PCW0               = chnlp -> PCW0;
      q -> PCW1               = chnlp -> PCW1;
      q -> PCW_CHAN           = chnlp -> PCW_CHAN;
      q -> PCW_AE             = chnlp -> PCW_AE;
      q -> PCW_PAGE_TABLE_PTR = chnlp -> PCW_PAGE_TABLE_PTR;
      q -> PCW_63_PTP         = chnlp -> PCW_63_PTP;
      q -> PCW_64_PGE         = chnlp -> PCW_64_PGE;
      q -> PCW_65_AUX         = chnlp -> PCW_65_AUX;
      q -> PCW_21_MSK         = chnlp -> PCW_21_MSK;
      q -> DCW                = chnlp -> DCW;
      unpack_DCW (q);

      q -> masked = chnlp -> PCW_21_MSK;
      if (q -> masked) {
        if (q -> in_use)
          simWarn ("%s: chan %o masked while in use\n", __func__, chnlp -> PCW_CHAN);
        q -> in_use = false;
        q -> start  = false;
      } else {
        if (q -> in_use)
          simWarn ("%s: chan %o connect while in use\n", __func__, chnlp -> PCW_CHAN);
        q -> in_use = true;
        q -> start  = true;
#if 0
//if (chnlp->PCW_CHAN == 027) simPrintf ("CON 27\n");
#if defined(IO_THREADZ)
        setChnConnect (iom_unit_idx, chnlp -> PCW_CHAN);
#else
# if !defined(IO_ASYNC_PAYLOAD_CHAN) && !defined(IO_ASYNC_PAYLOAD_CHAN_THREAD)
        doPayloadChannel (& iomChanData[iom_unit_idx][chnlp -> PCW_CHAN]);
# endif
# ifdef IO_ASYNC_PAYLOAD_CHAN_THREAD
        pthread_cond_signal (& iomCond);
# endif
#endif
#else
	doPayloadConnect (& iomChanData[iom_unit_idx][chnlp -> PCW_CHAN]);
#endif

      }
    }
  } while (! ptro);
  //atomic_thread_fence (memory_order_release);
  return 0; // XXX
}

/*
 * send_marker_interrupt ()
 *
 * Send a "marker" interrupt to the CPU.
 *
 * Channels send marker interrupts to indicate normal completion of
 * a PCW or IDCW if the control field of the PCW/IDCW has a value
 * of three.
 */

int send_marker_interrupt (chnlState_t * chnlp)
  {
    //atomic_thread_fence (memory_order_acquire);
    if (chnlp->masked)
      return 0;
    status_service (chnlp, true);
    return send_general_interrupt (chnlp, imwMarkerPic);
  }

/*
 * send_special_interrupt ()
 *
 * Send a "special" interrupt to the CPU.
 *
 */

int send_special_interrupt (chnlState_t * chnlp, uint devCode, word8 status0, word8 status1)
  {
    //atomic_thread_fence (memory_order_acquire);
    uint iom_unit_idx = chnlp->myIomIdx;
    uint chan = chnlp->myChanIdx;
    uint chanloc = mbxLoc (& iomChanData[iom_unit_idx][IOM_SPECIAL_STATUS_CHAN]);

    if (chnlp->masked)
      return(0);

    lock_iom();

// Multics uses an 12(8) word circular queue, managed by clever manipulation
// of the LPW and DCW.
// Rather then goes through the mechanics of parsing the LPW and DCW,
// we will just assume that everything is set up the way we expect,
// and update the circular queue.
    word36 lpw;
    iom_core_read (iom_unit_idx, chanloc + IOM_MBX_LPW, & lpw);

    word36 scw;
    iom_core_read (iom_unit_idx, chanloc + IOM_MBX_SCW, & scw);

    word36 dcw;
    iom_core_read_lock (iom_unit_idx, chanloc + IOM_MBX_DCW, & dcw);

    word36 status  = 0400000000000;
    status        |= (((word36) chan)    & MASK6) << 27;
    status        |= (((word36) devCode) & MASK8) << 18;
    status        |= (((word36) status0) & MASK8) <<  9;
    status        |= (((word36) status1) & MASK8) <<  0;
    iom_core_write (iom_unit_idx, (dcw >> 18) & MASK18, status);

    uint tally = dcw & MASK12;
    if (tally > 1)
      {
        dcw -= W36_C (01);  // tally --
        dcw += W36_C (01000000); // addr ++
      }
    else
      dcw = scw; // reset to beginning of queue
    iom_core_write_unlock (iom_unit_idx, chanloc + IOM_MBX_DCW, dcw);

    unlock_iom();

    send_general_interrupt (& iomChanData[iom_unit_idx][IOM_SPECIAL_STATUS_CHAN], imwSpecialPic);
    //atomic_thread_fence (memory_order_release);
    return 0;
  }

/*
 * send_terminate_interrupt ()
 *
 * Send a "terminate" interrupt to the CPU.
 *
 * Channels send a terminate interrupt after doing a status service.
 *
 */

int send_terminate_interrupt (chnlState_t * chnlp) {
if (chnlp->myChanIdx == 0027) { printf (INV"dia terminate"RES"\n"); }
  if (chnlp->masked)
    return 0;
  status_service (chnlp, false);
  if (chnlp->in_use == false)
    simWarn ("%s: chan %o not in use\n", __func__, chnlp->myChanIdx);
  chnlp->in_use = false;
  send_general_interrupt (chnlp, imwTerminatePic);
  return 0;
}

void iom_interrupt (uint scuUnitIdx, uint iomUnitIdx) {
#ifdef TANDD_DEBUG
  sim_printf ("[%lld] iom_interrupt\n", cpu.cycleCnt);
#endif
  iom_unit_data[iomUnitIdx].invokingScuUnitIdx = scuUnitIdx;

#if defined(IO_THREADZ)
  setIOMInterrupt (iomUnitIdx);
  iomDoneWait (iomUnitIdx);
#else
  /* int ret = */ doConnectChan (iomUnitIdx);
  // XXX doConnectChan return value ignored
#endif
}

void * chnThreadMain (void * arg) {
  chnlState_t * chnlp = * (chnlState_t **) arg;
  uint iomUnitIdx  = chnlp->myIomIdx;
  uint chan = chnlp->myChanIdx;

  simPrintf(INV"IOM %c Channel %o thread created"RES"\n", iomUnitIdx + 'a', chan);

  setSignals ();
  while (1) {
simPrintf(INV"IOM %c Channel %o thread waiting"RES"\n", iomUnitIdx + 'a', chan);
    chnConnectWait (chnlp);
simPrintf(INV"IOM %c Channel %o thread running"RES"\n", iomUnitIdx + 'a', chan);
    doPayloadChannel (chnlp);
    chnConnectDone (chnlp);
  }
}

/*
 * iom_init ()
 *
 *  Once-only initialization
 */

void iom_init (void)
  {
    // void * discards volatile
    memset ((void *) & iomChanData, 0, sizeof (iomChanData));
    for (int i = 0; i < N_IOM_UNITS_MAX; i++)
      for (int c = 0; c < MAX_CHANNELS; c ++) {
        iomChanData[i][c].myIomIdx = i;
        iomChanData[i][c].myChanIdx = c;
    }
  }

#ifdef PANEL68
void do_boot (void)
  {
    boot_svc (& boot_channel_unit[0]);
  }
#endif

#if defined(IO_ASYNC_PAYLOAD_CHAN) || defined(IO_ASYNC_PAYLOAD_CHAN_THREAD)
void iomProcess (void)
  {
    //atomic_thread_fence (memory_order_acquire);
    for (uint i = 0; i < N_IOM_UNITS_MAX; i++)
      for (uint j = 0; j < MAX_CHANNELS; j++)
        {
          chnlState_t * chnlp = &iomChanData [i] [j];
          if (chnlp -> start)
            {
              chnlp -> start = false;
              //atomic_thread_fence (memory_order_release);
              doPayloadChannel (i, j);
            }
        }
  }
#endif

char iomChar (uint iomUnitIdx)
  {
    return (iom_unit_data[iomUnitIdx].configSwMultiplexBaseAddress & 3) + 'A';
  }

void iomStats (uint iomNo) {
  if (! ioms[iomNo].connectCnt)
    return;

  (void)fflush(stderr);
  (void)fflush(stdout);
  sim_msg ("\n");
  (void)fflush(stdout);
  (void)fflush(stderr);
  sim_msg ("+---------------------------------+\n");
  sim_msg ("|         IOM %c Statistics        |\n", 'A' + iomNo);
  sim_msg ("+---------------------------------+\n");
  (void)fflush(stdout);
  (void)fflush(stderr);
#if defined(WIN_STDIO)
  sim_msg ("|  connects      %15llu  |\n", (unsigned long long)ioms[iomNo].connectCnt);
  sim_msg ("|  DCWs          %15llu  |\n", (unsigned long long)ioms[iomNo].dcwCnt);
  (void)fflush(stdout);
  (void)fflush(stderr);
  sim_msg ("+---------------------------------+\n");
  sim_msg ("|  lockCnt       %15llu  |\n", (unsigned long long)ioms[iomNo].iomCoreLockState.lockCnt);
  sim_msg ("|  lockImmediate %15llu  |\n", (unsigned long long)ioms[iomNo].iomCoreLockState.lockImmediate);
  (void)fflush(stdout);
  (void)fflush(stderr);
  sim_msg ("+---------------------------------+\n");
  sim_msg ("|  lockWait      %15llu  |\n", (unsigned long long)ioms[iomNo].iomCoreLockState.lockWait);
  sim_msg ("|  lockWaitMax   %15llu  |\n", (unsigned long long)ioms[iomNo].iomCoreLockState.lockWaitMax);
  (void)fflush(stdout);
  (void)fflush(stderr);
# if !defined(SCHED_NEVER_YIELD)
  sim_msg ("|  lockYield     %15llu  |\n", (unsigned long long)ioms[iomNo].iomCoreLockState.lockYield);
  (void)fflush(stdout);
  (void)fflush(stderr);
# else
  sim_msg ("|  lockYield                ----  |\n");
  (void)fflush(stdout);
  (void)fflush(stderr);
# endif /* if !defined(SCHED_NEVER_YIELD) */
  sim_msg ("+---------------------------------+");
  (void)fflush(stdout);
  (void)fflush(stderr);
# if !defined(UCACHE)
#  if !defined(UCACHE_STATS)
  sim_msg ("\n");
#  endif
# endif
  (void)fflush(stdout);
  (void)fflush(stderr);
#else
  sim_msg ("|  connects      %'15llu  |\n", (unsigned long long)ioms[iomNo].connectCnt);
  sim_msg ("|  DCWs          %'15llu  |\n", (unsigned long long)ioms[iomNo].dcwCnt);
  (void)fflush(stdout);
  (void)fflush(stderr);
  sim_msg ("+---------------------------------+\n");
  sim_msg ("|  lockCnt       %'15llu  |\n", (unsigned long long)ioms[iomNo].iomCoreLockState.lockCnt);
  sim_msg ("|  lockImmediate %'15llu  |\n", (unsigned long long)ioms[iomNo].iomCoreLockState.lockImmediate);
  (void)fflush(stdout);
  (void)fflush(stderr);
  sim_msg ("+---------------------------------+\n");
  sim_msg ("|  lockWait      %'15llu  |\n", (unsigned long long)ioms[iomNo].iomCoreLockState.lockWait);
  sim_msg ("|  lockWaitMax   %'15llu  |\n", (unsigned long long)ioms[iomNo].iomCoreLockState.lockWaitMax);
  (void)fflush(stdout);
  (void)fflush(stderr);
# if !defined(SCHED_NEVER_YIELD)
  sim_msg ("|  lockYield     %'15llu  |\n", (unsigned long long)ioms[iomNo].iomCoreLockState.lockYield);
  (void)fflush(stdout);
  (void)fflush(stderr);
# else
  sim_msg ("|  lockYield                ----  |\n");
  (void)fflush(stdout);
  (void)fflush(stderr);
# endif /* if !defined(SCHED_NEVER_YIELD) */
  sim_msg ("+---------------------------------+");
  (void)fflush(stdout);
  (void)fflush(stderr);
# if !defined(UCACHE)
#  if !defined(UCACHE_STATS)
  sim_msg ("\n");
#  endif
# endif
  (void)fflush(stderr);
  (void)fflush(stdout);
#endif

}
