/*
 * vim: filetype=c:tabstop=4:ai:expandtab
 * SPDX-License-Identifier: ICU
 * SPDX-License-Identifier: Multics
 * scspell-id: 4bc3e31c-f62d-11ec-bf00-80ee73e9b8e7
 *
 * ---------------------------------------------------------------------------
 *
 * Copyright (c) 2012-2016 Harry Reed
 * Copyright (c) 2013-2024 Charles Anthony
 * Copyright (c) 2017 Michal Tomek
 * Copyright (c) 2021-2024 The DPS8M Development Team
 *
 * This software is made available under the terms of the ICU License.
 * See the LICENSE.md file at the top-level directory of this distribution.
 *
 * ---------------------------------------------------------------------------
 *
 * This source file may contain code comments that adapt, include, and/or
 * incorporate Multics program code and/or documentation distributed under
 * the Multics License.  In the event of any discrepancy between code
 * comments herein and the original Multics materials, the original Multics
 * materials should be considered authoritative unless otherwise noted.
 * For more details and historical background, see the LICENSE.md file at
 * the top-level directory of this distribution.
 *
 * ---------------------------------------------------------------------------
 */


#include <stdio.h>
#include "dps8m.h"
#include "iom_channel.h"
#include "iom.h"
#include "cable.h"
#include "addrmods.h"
#include "faults.h"
#include "append.h"
#include "utils.h"
# include "threadz.h"

/*
 * The appending unit ...
 */

#if 0
void set_apu_status (cpup, cpuState_t * cpup, apuStatusBits status)
  {
# if 1
    word12 FCT = cpu.cu.APUCycleBits & MASK3;
    cpu.cu.APUCycleBits = (status & 07770) | FCT;
# else
    cpu.cu.PI_AP = 0;
    cpu.cu.DSPTW = 0;
    cpu.cu.SDWNP = 0;
    cpu.cu.SDWP  = 0;
    cpu.cu.PTW   = 0;
    cpu.cu.PTW2  = 0;
    cpu.cu.FAP   = 0;
    cpu.cu.FANP  = 0;
    cpu.cu.FABS  = 0;
    switch (status)
      {
        case apuStatus_PI_AP:
          cpu.cu.PI_AP = 1;
          break;
        case apuStatus_DSPTW:
        case apuStatus_MDSPTW: // XXX this doesn't seem like the right solution.
                               // XXX there is a MDSPTW bit in the APU history
                               // register, but not in the CU.
          cpu.cu.DSPTW = 1;
          break;
        case apuStatus_SDWNP:
          cpu.cu.SDWNP = 1;
          break;
        case apuStatus_SDWP:
          cpu.cu.SDWP  = 1;
          break;
        case apuStatus_PTW:
        case apuStatus_MPTW: // XXX this doesn't seem like the right solution.
                             // XXX there is a MPTW bit in the APU history
                             // XXX register, but not in the CU.
          cpu.cu.PTW   = 1;
          break;
        case apuStatus_PTW2:
          cpu.cu.PTW2  = 1;
          break;
        case apuStatus_FAP:
          cpu.cu.FAP   = 1;
          break;
        case apuStatus_FANP:
          cpu.cu.FANP  = 1;
          break;
        case apuStatus_FABS:
          cpu.cu.FABS  = 1;
          break;
      }
# endif
  }
#endif

#if defined(TESTING)
static char *str_sdw (char * buf, sdw_s *SDW);
#endif

//
//
//  The Use of Bit 29 in the Instruction Word
//  The reader is reminded that there is a preliminary step of loading TPR.CA
//  with the ADDRESS field of the instruction word during instruction decode.
//  If bit 29 of the instruction word is set to 1, modification by pointer
//  register is invoked and the preliminary step is executed as follows:
//  1. The ADDRESS field of the instruction word is interpreted as shown in
//  Figure 6-7 below.
//  2. C(PRn.SNR) -> C(TPR.TSR)
//  3. maximum of ( C(PRn.RNR), C(TPR.TRR), C(PPR.PRR) ) -> C(TPR.TRR)
//  4. C(PRn.WORDNO) + OFFSET -> C(TPR.CA) (NOTE: OFFSET is a signed binary
//  number.)
//  5. C(PRn.BITNO) -> TPR.BITNO
//

// Define this to do error detection on the PTWAM table.
// Useful if PTWAM reports an error message, but it slows the emulator
// down 50%

#if defined(do_selftestPTWAM)
static void selftest_ptwaw (void)
  {
    int usages[N_NAX_WAM_ENTRIES];
    for (int i = 0; i < N_MODEL_WAM_ENTRIES; i ++)
      usages[i] = -1;

    for (int i = 0; i < N_MODEL_WAM_ENTRIES; i ++)
      {
        ptw_s * p = cpu.PTWAM + i;
        if (p->USE > N_MODEL_WAM_ENTRIES - 1)
          simPrintf ("PTWAM[%d].USE is %d; > %d!\n",
                      i, p->USE, N_MODEL_WAM_ENTRIES - 1);
        if (usages[p->USE] != -1)
          simPrintf ("PTWAM[%d].USE is equal to PTWAM[%d].USE; %d\n",
                      i, usages[p->USE], p->USE);
        usages[p->USE] = i;
      }
    for (int i = 0; i < N_MODEL_WAM_ENTRIES; i ++)
      {
        if (usages[i] == -1)
          simPrintf ("No PTWAM had a USE of %d\n", i);
      }
  }
#endif

/*
 * implement ldbr instruction
 */

void do_ldbr (cpuState_t * cpup, word36 * Ypair)
  {
    CPTUR (cptUseDSBR);
    if (cpu.tweaks.enable_wam)
      {
        if (cpu.cu.SD_ON)
          {
            // If SDWAM is enabled, then
            //   0 -> C(SDWAM(i).FULL) for i = 0, 1, ..., 15
            //   i -> C(SDWAM(i).USE) for i = 0, 1, ..., 15
            for (uint i = 0; i < N_MODEL_WAM_ENTRIES; i ++)
              {
                cpu.SDWAM[i].FE = 0;
                L68_ (cpu.SDWAM[i].USE = (word4) i;)
                DPS8M_ (cpu.SDWAM[i].USE = 0;)
              }
          }

        if (cpu.cu.PT_ON)
          {
            // If PTWAM is enabled, then
            //   0 -> C(PTWAM(i).FULL) for i = 0, 1, ..., 15
            //   i -> C(PTWAM(i).USE) for i = 0, 1, ..., 15
            for (uint i = 0; i < N_MODEL_WAM_ENTRIES; i ++)
              {
                cpu.PTWAM[i].FE = 0;
                L68_ (cpu.PTWAM[i].USE = (word4) i;)
                DPS8M_ (cpu.PTWAM[i].USE = 0;)
              }
#if defined(do_selftestPTWAM)
            selftest_ptwaw ();
#endif
          }
      }
    else
      {
        cpu.SDW0.FE  = 0;
        cpu.SDW0.USE = 0;
        cpu.PTW0.FE  = 0;
        cpu.PTW0.USE = 0;
      }

    // If cache is enabled, reset all cache column and level full flags
    // XXX no cache

    // C(Y-pair) 0,23 -> C(DSBR.ADDR)
    cpu.DSBR.ADDR  = (Ypair[0] >> (35 - 23)) & PAMASK;

    // C(Y-pair) 37,50 -> C(DSBR.BOUND)
    cpu.DSBR.BND   = (Ypair[1] >> (71 - 50)) & 037777;

    // C(Y-pair) 55 -> C(DSBR.U)
    cpu.DSBR.U     = (Ypair[1] >> (71 - 55)) & 01;

    // C(Y-pair) 60,71 -> C(DSBR.STACK)
    cpu.DSBR.STACK = (Ypair[1] >> (71 - 71)) & 07777;
  }

/*
 * fetch descriptor segment PTW ...
 */

// CANFAULT

static void fetch_dsptw (cpuState_t * cpup, word15 segno)
  {
    PNL (L68_ (cpu.apu.state |= apu_FDPT;))

    if (2 * segno >= 16 * (cpu.DSBR.BND + 1))
      {
        // generate access violation, out of segment bounds fault
        PNL (cpu.acvFaults |= ACV15;)
        PNL (L68_ (cpu.apu.state |= apu_FLT;))
        doFault (cpup, FAULT_ACV, fst_acv15,
                 "acvFault: fetch_dsptw out of segment bounds fault");
      }
    set_apu_status (cpup, apuStatus_DSPTW);

    //word24 y1 = (2u * segno) % 1024u;
    word24 x1 = (2u * segno) / 1024u; // floor

    PNL (cpu.lastPTWOffset = segno;)
    PNL (cpu.lastPTWIsDS = true;)

    word36 PTWx1;
    coreRead (cpup, (cpu.DSBR.ADDR + x1) & PAMASK, & PTWx1, __func__);

    //cpu.PTW0.ADDR = GETHI (PTWx1);
    //cpu.PTW0.U    = TSTBIT (PTWx1, 9);
    //cpu.PTW0.M    = TSTBIT (PTWx1, 6);
    //cpu.PTW0.DF   = TSTBIT (PTWx1, 2);
    //cpu.PTW0.FC   = PTWx1 & 3;
    cpu.PTW0.ADDR = getbits36 (PTWx1, B_PTW_ADDR);
    cpu.PTW0.U    = getbits36 (PTWx1, B_PTW_U);
    cpu.PTW0.M    = getbits36 (PTWx1, B_PTW_M);
    cpu.PTW0.DF   = getbits36 (PTWx1, B_PTW_F);
    cpu.PTW0.FC   = getbits36 (PTWx1, B_PTW_FC);

    L68_ (if (cpu.MR_cache.emr && cpu.MR_cache.ihr)
      addL68ApuHistory (cpup, APUH_FDSPTW);)
  }

/*
 * modify descriptor segment PTW (Set U=1) ...
 */

// CANFAULT

static void modify_dsptw (cpuState_t * cpup, word15 segno)
  {

    PNL (L68_ (cpu.apu.state |= apu_MDPT;))

    set_apu_status (cpup, apuStatus_MDSPTW);

    word24 x1 = (2u * segno) / 1024u; // floor

    word36 PTWx1;
    coreReadLock (cpup, (cpu.DSBR.ADDR + x1) & PAMASK, & PTWx1, __func__);
    //PTWx1 = SETBIT (PTWx1, 9);
    PTWx1 = setbits36 (PTWx1, B_PTW_U, 1);
    coreWriteUnlock (cpup, (cpu.DSBR.ADDR + x1) & PAMASK, PTWx1, __func__);

    cpu.PTW0.U = 1;
    L68_ (if (cpu.MR_cache.emr && cpu.MR_cache.ihr)
      addL68ApuHistory (cpup, APUH_MDSPTW);)
  }

static word6 calc_hit_am (word6 LRU, uint hit_level)
  {
    switch (hit_level)
      {
        case 0:  // hit level A
          return (LRU | 070);
        case 1:  // hit level B
          return ((LRU & 037) | 06);
        case 2:  // hit level C
          return ((LRU & 053) | 01);
        case 3:  // hit level D
          return (LRU & 064);
        default:
          simWarn ("%s: Invalid AM level\n", __func__);
          return 0;
     }
  }

static sdw_s * fetch_sdw_from_sdwam (cpuState_t * cpup, word15 segno) {
  if ((! cpu.tweaks.enable_wam || ! cpu.cu.SD_ON)) {
    return NULL;
  }

  if (cpu.tweaks.l68_mode) { // L68
    int nwam = N_L68_WAM_ENTRIES;
    for (int _n = 0; _n < nwam; _n++) {
      // make certain we initialize SDWAM prior to use!!!
      if (cpu.SDWAM[_n].FE && segno == cpu.SDWAM[_n].POINTER) {
        cpu.cu.SDWAMM = 1;
        cpu.SDWAMR = (word4) _n;
        cpu.SDW = & cpu.SDWAM[_n];

        // If the SDWAM match logic circuitry indicates a hit, all usage
        // counts (SDWAM.USE) greater than the usage count of the register
        // hit are decremented by one, the usage count of the register hit
        // is set to 15, and the contents of the register hit are read out
        // into the address preparation circuitry.

        for (int _h = 0; _h < nwam; _h++) {
          if (cpu.SDWAM[_h].USE > cpu.SDW->USE)
            cpu.SDWAM[_h].USE -= 1;
        }
        cpu.SDW->USE = N_L68_WAM_ENTRIES - 1;

        char buf[256];
        (void)buf;
        return cpu.SDW;
      }
    }
  }

  if (! cpu.tweaks.l68_mode) { // DPS8M
    uint setno = segno & 017;
    uint toffset;
    sdw_s *p;
    for (toffset = 0; toffset < 64; toffset += 16) {
      p = & cpu.SDWAM[toffset + setno];
      if (p->FE && segno == p->POINTER) {
        cpu.cu.SDWAMM = 1;
        cpu.SDWAMR = (word6) (toffset + setno);
        cpu.SDW = p; // export pointer for appending

        word6 u = calc_hit_am (p->USE, toffset >> 4);
        for (toffset = 0; toffset < 64; toffset += 16) { // update LRU
          p = & cpu.SDWAM[toffset + setno];
          if (p->FE)
            p->USE = u;
        }

        char buf[256];
        (void)buf;
        return cpu.SDW;
      }
    }
  }
  cpu.cu.SDWAMM = 0;
  return NULL;    // segment not referenced in SDWAM
}

/*
 * Fetches an SDW from a paged descriptor segment.
 */
// CANFAULT

static void fetch_psdw (cpuState_t * cpup, word15 segno)
  {
    PNL (L68_ (cpu.apu.state |= apu_FSDP;))

    set_apu_status (cpup, apuStatus_SDWP);
    word24 y1 = (2 * segno) % 1024;

    word36 SDWeven, SDWodd;

    coreRead2 (cpup, ((((word24) cpu.PTW0.ADDR & 0777760) << 6) + y1) & PAMASK, & SDWeven, & SDWodd, __func__);

    // even word
    //cpu.SDW0.ADDR  = (SDWeven >> 12) & 077777777;
    //cpu.SDW0.R1    = (SDWeven >> 9)  & 7;
    //cpu.SDW0.R2    = (SDWeven >> 6)  & 7;
    //cpu.SDW0.R3    = (SDWeven >> 3)  & 7;
    //cpu.SDW0.DF    = TSTBIT (SDWeven, 2);
    //cpu.SDW0.FC    = SDWeven & 3;
    cpu.SDW0.ADDR  = getbits36 (SDWeven, B_SDWE_ADDR);
    cpu.SDW0.R1    = getbits36 (SDWeven, B_SDWE_R1);
    cpu.SDW0.R2    = getbits36 (SDWeven, B_SDWE_R2);
    cpu.SDW0.R3    = getbits36 (SDWeven, B_SDWE_R3);
    cpu.SDW0.DF    = getbits36 (SDWeven, B_SDWE_F);
    cpu.SDW0.FC    = getbits36 (SDWeven, B_SDWE_FC);

    // odd word
    //cpu.SDW0.BOUND = (SDWodd >> 21) & 037777;
    //cpu.SDW0.R     = TSTBIT (SDWodd, 20);
    //cpu.SDW0.E     = TSTBIT (SDWodd, 19);
    //cpu.SDW0.W     = TSTBIT (SDWodd, 18);
    //cpu.SDW0.P     = TSTBIT (SDWodd, 17);
    //cpu.SDW0.U     = TSTBIT (SDWodd, 16);
    //cpu.SDW0.G     = TSTBIT (SDWodd, 15);
    //cpu.SDW0.C     = TSTBIT (SDWodd, 14);
    //cpu.SDW0.EB    = SDWodd & 037777;
    cpu.SDW0.BOUND = getbits36 (SDWodd,  B_SDWO_BOUND);
    cpu.SDW0.R     = getbits36 (SDWodd,  B_SDWO_R);
    cpu.SDW0.E     = getbits36 (SDWodd,  B_SDWO_E);
    cpu.SDW0.W     = getbits36 (SDWodd,  B_SDWO_W);
    cpu.SDW0.P     = getbits36 (SDWodd,  B_SDWO_P);
    cpu.SDW0.U     = getbits36 (SDWodd,  B_SDWO_U);
    cpu.SDW0.G     = getbits36 (SDWodd,  B_SDWO_G);
    cpu.SDW0.C     = getbits36 (SDWodd,  B_SDWO_C);
    cpu.SDW0.EB    = getbits36 (SDWodd,  B_SDWO_EB);

    L68_ (
      if (cpu.MR_cache.emr && cpu.MR_cache.ihr)
        addL68ApuHistory (cpup, APUH_FSDWP);
     )
  }

// Nonpaged SDW Fetch
// Fetches an SDW from an unpaged descriptor segment.
// CANFAULT

static void fetch_nsdw (cpuState_t * cpup, word15 segno)
  {
    PNL (L68_ (cpu.apu.state |= apu_FSDN;))

    set_apu_status (cpup, apuStatus_SDWNP);

    if (2 * segno >= 16 * (cpu.DSBR.BND + 1))
      {
        // generate access violation, out of segment bounds fault
        PNL (cpu.acvFaults |= ACV15;)
        PNL (L68_ (cpu.apu.state |= apu_FLT;))
        doFault (cpup, FAULT_ACV, fst_acv15,
                 "acvFault fetch_dsptw: out of segment bounds fault");
      }

    word36 SDWeven, SDWodd;
    coreRead2 (cpup, (cpu.DSBR.ADDR + 2u * segno) & PAMASK, & SDWeven, & SDWodd, __func__);

    // even word
    //cpu.SDW0.ADDR = (SDWeven >> 12) & 077777777;
    //cpu.SDW0.R1   = (SDWeven >> 9)  & 7;
    //cpu.SDW0.R2   = (SDWeven >> 6)  & 7;
    //cpu.SDW0.R3   = (SDWeven >> 3)  & 7;
    //cpu.SDW0.DF   = TSTBIT (SDWeven, 2);
    //cpu.SDW0.FC   = SDWeven & 3;
    cpu.SDW0.ADDR  = getbits36 (SDWeven, B_SDWE_ADDR);
    cpu.SDW0.R1    = getbits36 (SDWeven, B_SDWE_R1);
    cpu.SDW0.R2    = getbits36 (SDWeven, B_SDWE_R2);
    cpu.SDW0.R3    = getbits36 (SDWeven, B_SDWE_R3);
    cpu.SDW0.DF    = getbits36 (SDWeven, B_SDWE_F);
    cpu.SDW0.FC    = getbits36 (SDWeven, B_SDWE_FC);

    // odd word
    //cpu.SDW0.BOUND = (SDWodd >> 21) & 037777;
    //cpu.SDW0.R     = TSTBIT (SDWodd, 20);
    //cpu.SDW0.E     = TSTBIT (SDWodd, 19);
    //cpu.SDW0.W     = TSTBIT (SDWodd, 18);
    //cpu.SDW0.P     = TSTBIT (SDWodd, 17);
    //cpu.SDW0.U     = TSTBIT (SDWodd, 16);
    //cpu.SDW0.G     = TSTBIT (SDWodd, 15);
    //cpu.SDW0.C     = TSTBIT (SDWodd, 14);
    //cpu.SDW0.EB    = SDWodd & 037777;
    cpu.SDW0.BOUND = getbits36 (SDWodd,  B_SDWO_BOUND);
    cpu.SDW0.R     = getbits36 (SDWodd,  B_SDWO_R);
    cpu.SDW0.E     = getbits36 (SDWodd,  B_SDWO_E);
    cpu.SDW0.W     = getbits36 (SDWodd,  B_SDWO_W);
    cpu.SDW0.P     = getbits36 (SDWodd,  B_SDWO_P);
    cpu.SDW0.U     = getbits36 (SDWodd,  B_SDWO_U);
    cpu.SDW0.G     = getbits36 (SDWodd,  B_SDWO_G);
    cpu.SDW0.C     = getbits36 (SDWodd,  B_SDWO_C);
    cpu.SDW0.EB    = getbits36 (SDWodd,  B_SDWO_EB);

    L68_ (
      if (cpu.MR_cache.emr && cpu.MR_cache.ihr)
        addL68ApuHistory (cpup, 0 /* No fetch no paged bit */);
    )
  }

#if defined(TESTING)
static char *str_sdw (char * buf, sdw_s *SDW)
  {
    if (! SDW->FE)
      (void) sprintf (buf, "*** SDW Uninitialized ***");
    else
      (void) sprintf (buf,
                      "ADDR:%06o R1:%o R2:%o R3:%o BOUND:%o R:%o E:%o W:%o P:%o "
                      "U:%o G:%o C:%o CL:%o DF:%o FC:%o POINTER=%o USE=%d",
                      SDW->ADDR,    SDW->R1, SDW->R2, SDW->R3, SDW->BOUND,
                      SDW->R,       SDW->E,  SDW->W,  SDW->P,  SDW->U,
                      SDW->G,       SDW->C,  SDW->EB, SDW->DF, SDW->FC,
                      SDW->POINTER, SDW->USE);
    return buf;
  }

/*
 * dump SDWAM...
 */

simRc dump_sdwam (cpuState_t * cpup)
  {
    char buf[256];
    (void)buf;
    for (int _n = 0; _n < N_MODEL_WAM_ENTRIES; _n++)
      {
        sdw_s *p = & cpu.SDWAM[_n];

        if (p->FE)
          simPrintf ("SDWAM n:%d %s\n", _n, str_sdw (buf, p));
      }
    return SCPE_OK;
  }
#endif

static uint to_be_discarded_am (word6 LRU)
  {
#if 0
    uint cA=0,cB=0,cC=0,cD=0;
    if (LRU & 040) cB++; else cA++;
    if (LRU & 020) cC++; else cA++;
    if (LRU & 010) cD++; else cA++;
    if (cA==3) return 0;
    if (LRU & 04)  cC++; else cB++;
    if (LRU & 02)  cD++; else cB++;
    if (cB==3) return 1;
    if (LRU & 01)  return 3; else return 2;
#endif

    if ((LRU & 070) == 070) return 0;
    if ((LRU & 046) == 006) return 1;
    if ((LRU & 025) == 001) return 2;
    return 3;
  }

/*
 * load the current in-core SDW0 into the SDWAM ...
 */

static void load_sdwam (cpuState_t * cpup, word15 segno, bool nomatch)
  {
    cpu.SDW0.POINTER = segno;
    cpu.SDW0.USE = 0;

    cpu.SDW0.FE = true;     // in use by SDWAM

    cpu.SDW = & cpu.SDW0;

    if (nomatch || (! cpu.tweaks.enable_wam) || (! cpu.cu.SD_ON))
      {
        return;
      }

    if (cpu.tweaks.l68_mode) { // L68
      // If the SDWAM match logic does not indicate a hit, the SDW is fetched
      // from the descriptor segment in main memory and loaded into the SDWAM
      // register with usage count 0 (the oldest), all usage counts are
      // decremented by one with the newly loaded register rolling over from 0 to
      // 15, and the newly loaded register is read out into the address
      // preparation circuitry.

      for (int _n = 0; _n < N_L68_WAM_ENTRIES; _n++) {
        sdw_s * p = & cpu.SDWAM[_n];
        if (! p->FE || p->USE == 0) {
          * p = cpu.SDW0;
          p->POINTER = segno;
          p->USE = 0;
          p->FE = true;     // in use by SDWAM

          for (int _h = 0; _h < N_L68_WAM_ENTRIES; _h++) {
            sdw_s * q = & cpu.SDWAM[_h];
            q->USE -= 1;
            q->USE &= N_L68_WAM_MASK;
          }

          cpu.SDW = p;

          char buf[256];
          (void) buf;
          return;
        }
      }
      // if we reach this, USE is scrambled
#if defined(TESTING)
      simWarn ("%s(3) no USE=0 found for segment=%d\n", __func__, segno);
      dump_sdwam (cpup);
#endif
    }

    if (! cpu.tweaks.l68_mode) { // DPS8M
      uint setno = segno & 017;
      uint toffset;
      sdw_s *p;
      for (toffset = 0; toffset < 64; toffset += 16) {
        p = & cpu.SDWAM[toffset + setno];
        if (!p->FE)
          break;
      }
      if (toffset == 64) { // all FE==1
        toffset = to_be_discarded_am (p->USE) << 4;
        p = & cpu.SDWAM[toffset + setno];
      }
      word6 u = calc_hit_am (p->USE, toffset >> 4); // before loading the SDWAM!
      * p = cpu.SDW0; // load the SDW
      p->POINTER = segno;
      p->FE = true;  // in use
      cpu.SDW = p; // export pointer for appending

      for (uint toffset1 = 0; toffset1 < 64; toffset1 += 16) { // update LRU
        p = & cpu.SDWAM[toffset1 + setno];
        if (p->FE)
          p->USE = u;
      }

      char buf[256];
      (void) buf;
    } // DPS8M
  }

static ptw_s * fetch_ptw_from_ptwam (cpuState_t * cpup, word15 segno, word18 CA)
  {
    if ((! cpu.tweaks.enable_wam) || (! cpu.cu.PT_ON))
      {
        return NULL;
      }

    if (cpu.tweaks.l68_mode) { // L68
      int nwam = N_L68_WAM_ENTRIES;
      for (int _n = 0; _n < nwam; _n++)
        {
          if (cpu.PTWAM[_n].FE && ((CA >> 6) & 07760) == cpu.PTWAM[_n].PAGENO &&
              cpu.PTWAM[_n].POINTER == segno)   //_initialized
            {
              cpu.cu.PTWAMM = 1;
              cpu.PTWAMR = (word4) _n;
              cpu.PTW = & cpu.PTWAM[_n];

              // If the PTWAM match logic circuitry indicates a hit, all usage
              // counts (PTWAM.USE) greater than the usage count of the register
              // hit are decremented by one, the usage count of the register hit
              // is set to 15, and the contents of the register hit are read out
              // into the address preparation circuitry.

              for (int _h = 0; _h < nwam; _h++)
                {
                  if (cpu.PTWAM[_h].USE > cpu.PTW->USE)
                    cpu.PTWAM[_h].USE -= 1; //PTW->USE -= 1;
                }
              cpu.PTW->USE = N_L68_WAM_ENTRIES - 1;
#if defined(do_selftestPTWAM)
              selftest_ptwaw ();
#endif
              return cpu.PTW;
            }
        }
    }

    DPS8M_ (
      uint setno = (CA >> 10) & 017;
      uint toffset;
      ptw_s *p;
      for (toffset = 0; toffset < 64; toffset += 16)
        {
          p = & cpu.PTWAM[toffset + setno];

          if (p->FE && ((CA >> 6) & 07760) == p->PAGENO && p->POINTER == segno)
            {
              cpu.cu.PTWAMM = 1;
              cpu.PTWAMR = (word6) (toffset + setno);
              cpu.PTW = p; // export pointer for appending

              word6 u = calc_hit_am (p->USE, toffset >> 4);
              for (toffset = 0; toffset < 64; toffset += 16) // update LRU
                {
                  p = & cpu.PTWAM[toffset + setno];
                  if (p->FE)
                    p->USE = u;
                }

              return cpu.PTW;
            }
        }
     )
    cpu.cu.PTWAMM = 0;
    return NULL;    // page not referenced in PTWAM
  }

static void fetch_ptw (cpuState_t * cpup, sdw_s *sdw, word18 offset)
  {
    // AL39 p.5-7
    // Fetches a PTW from a page table other than a descriptor segment page
    // table and sets the page accessed bit (PTW.U)
    PNL (L68_ (cpu.apu.state |= apu_FPTW;))
    set_apu_status (cpup, apuStatus_PTW);

    //word24 y2 = offset % 1024;
    word24 x2 = (offset) / 1024; // floor

    word36 PTWx2;

    PNL (cpu.lastPTWOffset = offset;)
    PNL (cpu.lastPTWIsDS = false;)

    coreReadLock (cpup, (sdw->ADDR + x2) & PAMASK, & PTWx2, __func__);

    //cpu.PTW0.ADDR = GETHI  (PTWx2);
    //cpu.PTW0.U    = TSTBIT (PTWx2, 9);
    //cpu.PTW0.M    = TSTBIT (PTWx2, 6);
    //cpu.PTW0.DF   = TSTBIT (PTWx2, 2);
    //cpu.PTW0.FC   = PTWx2 & 3;
    cpu.PTW0.ADDR = getbits36 (PTWx2, B_PTW_ADDR);
    cpu.PTW0.U    = getbits36 (PTWx2, B_PTW_U);
    cpu.PTW0.M    = getbits36 (PTWx2, B_PTW_M);
    cpu.PTW0.DF   = getbits36 (PTWx2, B_PTW_F);
    cpu.PTW0.FC   = getbits36 (PTWx2, B_PTW_FC);

    // ISOLTS-861 02
    //PTWx2 = SETBIT (PTWx2, 9);
    PTWx2 = setbits36 (PTWx2, B_PTW_U, 1);
    coreWriteUnlock (cpup, (sdw->ADDR + x2) & PAMASK, PTWx2, __func__);
    cpu.PTW0.U = 1;

    L68_ (if (cpu.MR_cache.emr && cpu.MR_cache.ihr)
      addL68ApuHistory (cpup, APUH_FPTW);)
  }

static void loadPTWAM (cpuState_t * cpup, word15 segno, word18 offset, UNUSED bool nomatch)
  {
    cpu.PTW0.PAGENO  = (offset >> 6) & 07760;
    cpu.PTW0.POINTER = segno;
    cpu.PTW0.USE     = 0;
    cpu.PTW0.FE      = true;

    cpu.PTW = & cpu.PTW0;
    if (nomatch || (! cpu.tweaks.enable_wam) || (! cpu.cu.PT_ON))
      {
        return;
      }

    if (cpu.tweaks.l68_mode) { // L68
      // If the PTWAM match logic does not indicate a hit, the PTW is fetched
      // from main memory and loaded into the PTWAM register with usage count 0
      // (the oldest), all usage counts are decremented by one with the newly
      // loaded register rolling over from 0 to 15, and the newly loaded register
      // is read out into the address preparation circuitry.

      for (int _n = 0; _n < N_L68_WAM_ENTRIES; _n++)
        {
          ptw_s * p = & cpu.PTWAM[_n];
          if (! p->FE || p->USE == 0)
            {
              *p = cpu.PTW0;
              p->PAGENO = (offset >> 6) & 07760;
              p->POINTER = segno;
              p->USE = 0;
              p->FE = true;

              for (int _h = 0; _h < N_L68_WAM_ENTRIES; _h++)
                {
                  ptw_s * q = & cpu.PTWAM[_h];
                  q->USE -= 1;
                  q->USE &= N_L68_WAM_MASK;
                }

              cpu.PTW = p;
#if defined(do_selftestPTWAM)
              selftest_ptwaw ();
#endif
              return;
            }
        }
      // if we reach this, USE is scrambled
      simPrintf ("loadPTWAM(segno=%05o, offset=%012o): no USE=0 found!\n",
                  segno, offset);
    }

    DPS8M_ (
      uint setno = (offset >> 10) & 017;
      uint toffset;
      ptw_s *p;
      for (toffset = 0; toffset < 64; toffset += 16)
        {
          p = & cpu.PTWAM[toffset + setno];
          if (! p->FE)
            break;
        }
      if (toffset == 64) // all FE==1
        {
          toffset = to_be_discarded_am (p->USE) << 4;
          p = & cpu.PTWAM[toffset + setno];
        }

      word6 u = calc_hit_am (p->USE, toffset >> 4); // before loading the PTWAM
      * p = cpu.PTW0; // load the PTW
      p->PAGENO = (offset >> 6) & 07760;
      p->POINTER = segno;
      p->FE = true;  // in use
      cpu.PTW = p; // export pointer for appending

      for (uint toffset1 = 0; toffset1 < 64; toffset1 += 16) // update LRU
        {
          p = & cpu.PTWAM[toffset1 + setno];
          if (p->FE)
            p->USE = u;
        }

    )
  }

/*
 * modify target segment PTW (Set M=1) ...
 */

static void modify_ptw (cpuState_t * cpup, sdw_s *sdw, word18 offset)
  {
    PNL (L68_ (cpu.apu.state |= apu_MPTW;))
    //word24 y2 = offset % 1024;
    word24 x2 = offset / 1024; // floor

    word36 PTWx2;

    set_apu_status (cpup, apuStatus_MPTW);

    coreReadLock (cpup, (sdw->ADDR + x2) & PAMASK, & PTWx2, __func__);
    //PTWx2 = SETBIT (PTWx2, 6);
    PTWx2 = setbits36 (PTWx2, B_PTW_M, 1);
    coreWriteUnlock (cpup, (sdw->ADDR + x2) & PAMASK, PTWx2, __func__);
    cpu.PTW->M = 1;
    L68_ (if (cpu.MR_cache.emr && cpu.MR_cache.ihr)
      addL68ApuHistory (cpup, APUH_MPTW);)
  }

static void do_ptw2 (cpuState_t * cpup, sdw_s *sdw, word18 offset)
  {
    PNL (L68_ (cpu.apu.state |= apu_FPTW2;))
    set_apu_status (cpup, apuStatus_PTW2);

    //word24 y2 = offset % 1024;
    word24 x2 = (offset) / 1024; // floor

    word36 PTWx2n;

    coreRead (cpup, (sdw->ADDR + x2 + 1) & PAMASK, & PTWx2n, __func__);

    ptw_s PTW2;
    //PTW2.ADDR = GETHI (PTWx2n);
    //PTW2.U    = TSTBIT (PTWx2n, 9);
    //PTW2.M    = TSTBIT (PTWx2n, 6);
    //PTW2.DF   = TSTBIT (PTWx2n, 2);
    //PTW2.FC   = PTWx2n & 3;
    PTW2.ADDR = getbits36 (PTWx2n, B_PTW_ADDR);
    PTW2.U    = getbits36 (PTWx2n, B_PTW_U);
    PTW2.M    = getbits36 (PTWx2n, B_PTW_M);
    PTW2.DF   = getbits36 (PTWx2n, B_PTW_F);
    PTW2.FC   = getbits36 (PTWx2n, B_PTW_FC);

    L68_ (if (cpu.MR_cache.emr && cpu.MR_cache.ihr)
      addL68ApuHistory (cpup, APUH_FPTW2);)

    // check that PTW2 is the next page of the same segment
    // ISOLTS 875 02a
    if ((PTW2.ADDR & 0777760) == (cpu.PTW->ADDR & 0777760) + 16)
       //Is PTW2.F set ON?
       if (! PTW2.DF)
           // initiate a directed fault
           doFault (cpup, FAULT_DF0 + PTW2.FC, fst_zero, "PTW2.F == 0");
  }

/*
 * Is the instruction a SToRage OPeration ?
 */

#if !defined(QUIET_UNUSED)
static char *str_access_type (MemoryAccessType accessType)
  {
    switch (accessType)
      {
        case UnknownMAT:        return "Unknown";
        case OperandRead:       return "OperandRead";
        case OperandWrite:      return "OperandWrite";
        default:                return "???";
      }
  }
#endif

#if !defined(QUIET_UNUSED)
static char *str_acv (_fault_subtype acv)
  {
    switch (acv)
      {
        case ACV0:  return "Illegal ring order (ACV0=IRO)";
        case ACV1:  return "Not in execute bracket (ACV1=OEB)";
        case ACV2:  return "No execute permission (ACV2=E-OFF)";
        case ACV3:  return "Not in read bracket (ACV3=ORB)";
        case ACV4:  return "No read permission (ACV4=R-OFF)";
        case ACV5:  return "Not in write bracket (ACV5=OWB)";
        case ACV6:  return "No write permission (ACV6=W-OFF)";
        case ACV7:  return "Call limiter fault (ACV7=NO GA)";
        case ACV8:  return "Out of call brackets (ACV8=OCB)";
        case ACV9:  return "Outward call (ACV9=OCALL)";
        case ACV10: return "Bad outward call (ACV10=BOC)";
        case ACV11: return "Inward return (ACV11=INRET) XXX ??";
        case ACV12: return "Invalid ring crossing (ACV12=CRT)";
        case ACV13: return "Ring alarm (ACV13=RALR)";
        case ACV14: return "Associative memory error XXX ??";
        case ACV15: return "Out of segment bounds (ACV15=OOSB)";
        //case ACDF0: return "Directed fault 0";
        //case ACDF1: return "Directed fault 1";
        //case ACDF2: return "Directed fault 2";
        //case ACDF3: return "Directed fault 3";
        default:
            break;
      }
  return "unhandled acv in str_acv";
  }
#endif


word24 do_append_cycle (cpuState_t * cpup, processor_cycle_type thisCycle, word36 * data, uint nWords) {
  switch (thisCycle) {
    case OPERAND_STORE:
      return doAppendCycleOperandStore (cpup, data, nWords);
    case OPERAND_READ:
      return doAppendCycleOperandRead (cpup, data, nWords);
    case INDIRECT_WORD_FETCH:
      return doAppendCycleIndirectWordFetch (cpup, data, nWords);
    case RTCD_OPERAND_FETCH:
      return doAppendCycleRTCDOperandFetch (cpup, data, nWords);
    case INSTRUCTION_FETCH:
      return doAppendCycleInstructionFetch (cpup, data, nWords);
    case APU_DATA_READ:
      return doAppendCycleAPUDataRead (cpup, data, nWords);
    case APU_DATA_STORE:
      return doAppendCycleAPUDataStore (cpup, data, nWords);
    case ABSA_CYCLE:
      return doAppendCycleABSA (cpup, data, nWords);
    case OPERAND_RMW:
      return doAppendCycleOperandRMW (cpup, data, nWords);
    case APU_DATA_RMW:
      return doAppendCycleAPUDataRMW (cpup, data, nWords);
    case UNKNOWN_CYCLE:
    default:
      (void)fprintf (stderr, "\nFATAL: APU unknown cycle %llu! Aborting at %s[%s:%d]\n",
                     (long long unsigned)thisCycle, __func__, __FILE__, __LINE__);
      BACKTRACE;
      abort();
  }
}

# include "doAppendCycleOperandStore.h"
# include "doAppendCycleOperandRead.h"
# include "doAppendCycleIndirectWordFetch.h"
# include "doAppendCycleRTCDOperandFetch.h"
# include "doAppendCycleInstructionFetch.h"
# include "doAppendCycleAPUDataRead.h"
# include "doAppendCycleAPUDataStore.h"
# include "doAppendCycleABSA.h"
#  include "doAppendCycleOperandRMW.h"
#  include "doAppendCycleAPUDataRMW.h"

// Translate a segno:offset to a absolute address.
// Return 0 if successful.

#if defined(TESTING)
int dbgLookupAddress (cpuState_t * cpup, word18 segno, word18 offset, word24 * finalAddress,
                      char * * msg)
  {
    // Local copies so we don't disturb machine state

    ptw_s PTW1;
    sdw_s SDW1;

   if (2u * segno >= 16u * (cpu.DSBR.BND + 1u))
     {
       if (msg)
         * msg = "DSBR boundary violation.";
       return 1;
     }

    if (cpu.DSBR.U == 0)
      {
        // fetch_dsptw

        word24 y1 = (2 * segno) % 1024;
        word24 x1 = (2 * segno) / 1024; // floor

        word36 PTWx1;
        coreRead (cpup, (cpu.DSBR.ADDR + x1) & PAMASK, & PTWx1, __func__);

        //PTW1.ADDR = GETHI (PTWx1);
        //PTW1.U = TSTBIT (PTWx1, 9);
        //PTW1.M = TSTBIT (PTWx1, 6);
        //PTW1.DF = TSTBIT (PTWx1, 2);
        //PTW1.FC = PTWx1 & 3;
        PTW1.ADDR = getbits36 (PTWx1, B_PTW_ADDR);
        PTW1.U    = getbits36 (PTWx1, B_PTW_U);
        PTW1.M    = getbits36 (PTWx1, B_PTW_M);
        PTW1.DF   = getbits36 (PTWx1, B_PTW_F);
        PTW1.FC   = getbits36 (PTWx1, B_PTW_FC);

        if (! PTW1.DF)
          {
            if (msg)
              * msg = "!PTW0.F";
            return 2;
          }

        // fetch_psdw

        y1 = (2 * segno) % 1024;

        word36 SDWeven, SDWodd;

        coreRead2 (cpup, ((((word24)PTW1. ADDR & 0777760) << 6) + y1) & PAMASK, & SDWeven, & SDWodd, __func__);

        // even word
        //SDW1.ADDR = (SDWeven >> 12) & 077777777;
        //SDW1.R1   = (SDWeven >> 9)  & 7;
        //SDW1.R2   = (SDWeven >> 6)  & 7;
        //SDW1.R3   = (SDWeven >> 3)  & 7;
        //SDW1.DF   = TSTBIT (SDWeven, 2);
        //SDW1.FC   = SDWeven & 3;
        SDW1.ADDR  = getbits36 (SDWeven, B_SDWE_ADDR);
        SDW1.R1    = getbits36 (SDWeven, B_SDWE_R1);
        SDW1.R2    = getbits36 (SDWeven, B_SDWE_R2);
        SDW1.R3    = getbits36 (SDWeven, B_SDWE_R3);
        SDW1.DF    = getbits36 (SDWeven, B_SDWE_F);
        SDW1.FC    = getbits36 (SDWeven, B_SDWE_FC);

        // odd word
        //SDW1.BOUND = (SDWodd >> 21) & 037777;
        //SDW1.R     = TSTBIT (SDWodd, 20);
        //SDW1.E     = TSTBIT (SDWodd, 19);
        //SDW1.W     = TSTBIT (SDWodd, 18);
        //SDW1.P     = TSTBIT (SDWodd, 17);
        //SDW1.U     = TSTBIT (SDWodd, 16);
        //SDW1.G     = TSTBIT (SDWodd, 15);
        //SDW1.C     = TSTBIT (SDWodd, 14);
        //SDW1.EB    = SDWodd & 037777;
        SDW1.BOUND = getbits36 (SDWodd,  B_SDWO_BOUND);
        SDW1.R     = getbits36 (SDWodd,  B_SDWO_R);
        SDW1.E     = getbits36 (SDWodd,  B_SDWO_E);
        SDW1.W     = getbits36 (SDWodd,  B_SDWO_W);
        SDW1.P     = getbits36 (SDWodd,  B_SDWO_P);
        SDW1.U     = getbits36 (SDWodd,  B_SDWO_U);
        SDW1.G     = getbits36 (SDWodd,  B_SDWO_G);
        SDW1.C     = getbits36 (SDWodd,  B_SDWO_C);
        SDW1.EB    = getbits36 (SDWodd,  B_SDWO_EB);
      }
    else // ! DSBR.U
      {
        // fetch_nsdw

        word36 SDWeven, SDWodd;

        coreRead2 (cpup, (cpu.DSBR.ADDR + 2 * segno) & PAMASK, & SDWeven, & SDWodd, __func__);

        // even word
        //SDW1.ADDR = (SDWeven >> 12) & 077777777;
        //SDW1.R1   = (SDWeven >> 9)  & 7;
        //SDW1.R2   = (SDWeven >> 6)  & 7;
        //SDW1.R3   = (SDWeven >> 3)  & 7;
        //SDW1.DF   = TSTBIT (SDWeven, 2);
        //SDW1.FC   = SDWeven & 3;
        SDW1.ADDR  = getbits36 (SDWeven, B_SDWE_ADDR);
        SDW1.R1    = getbits36 (SDWeven, B_SDWE_R1);
        SDW1.R2    = getbits36 (SDWeven, B_SDWE_R2);
        SDW1.R3    = getbits36 (SDWeven, B_SDWE_R3);
        SDW1.DF    = getbits36 (SDWeven, B_SDWE_F);
        SDW1.FC    = getbits36 (SDWeven, B_SDWE_FC);

        // odd word
        //SDW1.BOUND = (SDWodd >> 21) & 037777;
        //SDW1.R     = TSTBIT (SDWodd, 20);
        //SDW1.E     = TSTBIT (SDWodd, 19);
        //SDW1.W     = TSTBIT (SDWodd, 18);
        //SDW1.P     = TSTBIT (SDWodd, 17);
        //SDW1.U     = TSTBIT (SDWodd, 16);
        //SDW1.G     = TSTBIT (SDWodd, 15);
        //SDW1.C     = TSTBIT (SDWodd, 14);
        //SDW1.EB    = SDWodd & 037777;
        SDW1.BOUND = getbits36 (SDWodd,  B_SDWO_BOUND);
        SDW1.R     = getbits36 (SDWodd,  B_SDWO_R);
        SDW1.E     = getbits36 (SDWodd,  B_SDWO_E);
        SDW1.W     = getbits36 (SDWodd,  B_SDWO_W);
        SDW1.P     = getbits36 (SDWodd,  B_SDWO_P);
        SDW1.U     = getbits36 (SDWodd,  B_SDWO_U);
        SDW1.G     = getbits36 (SDWodd,  B_SDWO_G);
        SDW1.C     = getbits36 (SDWodd,  B_SDWO_C);
        SDW1.EB    = getbits36 (SDWodd,  B_SDWO_EB);

      }

    if (SDW1.DF == 0)
      {
        if (msg)
          * msg = "!SDW0.F != 0";
        return 3;
      }

    if (((offset >> 4) & 037777) > SDW1.BOUND)
      {
        if (msg)
          * msg = "C(TPR.CA)0,13 > SDW.BOUND";
        return 4;
      }

    // is segment C(TPR.TSR) paged?
    if (SDW1.U)
      {
        * finalAddress = (SDW1.ADDR + offset) & PAMASK;
      }
    else
      {
        // fetch_ptw
        word24 y2 = offset % 1024;
        word24 x2 = (offset) / 1024; // floor

        word36 PTWx2;

        coreRead (cpup, (SDW1.ADDR + x2) & PAMASK, & PTWx2, __func__);

        //PTW1.ADDR = GETHI (PTWx2);
        //PTW1.U    = TSTBIT (PTWx2, 9);
        //PTW1.M    = TSTBIT (PTWx2, 6);
        //PTW1.DF   = TSTBIT (PTWx2, 2);
        //PTW1.FC   = PTWx2 & 3;
        PTW1.ADDR = getbits36 (PTWx2, B_PTW_ADDR);
        PTW1.U    = getbits36 (PTWx2, B_PTW_U);
        PTW1.M    = getbits36 (PTWx2, B_PTW_M);
        PTW1.DF   = getbits36 (PTWx2, B_PTW_F);
        PTW1.FC   = getbits36 (PTWx2, B_PTW_FC);

        if (! PTW1.DF)
          {
            if (msg)
              * msg = "!PTW0.F";
            return 5;
          }

        y2 = offset % 1024;

        * finalAddress = ((((word24)PTW1.ADDR & 0777760) << 6) + y2) & PAMASK;
      }
    if (msg)
      * msg = "";
    return 0;
  }
#endif
