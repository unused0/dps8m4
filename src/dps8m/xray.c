#include <stdio.h>
#include <ctype.h>


#include "dps8m.h"
#include "xray.h"
#include "xrayMultics.h"


// flagbox.incl.alm, flagbox.incl.pl1
//
//     2 slt_segno bit (18),  /* segment # of the SLT */
//     2 sst_sdw bit (72),    /* set by init_sst */
//     2 hc_dbr bit (72),     /* set by start_cpu, idle DBR */
//
//         equ     fgbx.slt_segno,33               " UPPER
//         equ     fgbx.sst_sdw,44         " DOUBLE
//         equ     fgbx.hc_dbr,46          " DOUBLE
//
// lrn
//    5 flagbox (ring 0)
//
// systembook
//
// flagbox   5  (0, 0, 0) read write wired
//                        wired length: 1024 words;
//
// MTB-652   New Initialization SDN
//
//     [24000]    [4000]        [(bootload Multics) toehold]
//     [24000]    [2000]        [flagbox (overlays the toehold)]
//
//       The  absolute  addresses  of  most  of  these  segments  is
// arbitrary.   Hardware known  data bases  must be  at their proper
// places, though; also, the toeholds  are placed at addresses known
// to operators.   Except for these exceptions,  the segments may be
// moved.  Their addresses  are contained in bootload_equs.incl.alm.

char filebuf [MAX3(PATH_MAX, _POSIX_PATH_MAX, _XOPEN_PATH_MAX)];
bool verbose = 1;

struct system_state_s * system_state;
sdw_s sstSDW;
word24 sstPageTableAddr;
word18 sstPageTableBound;

static bool running;

int xrayQuit (void) {
  running = false;
  return 0;
}

// addr space   d0 space d1 space d2 space d3 space c0 space c1 space c2 space c3
//  8     1     12   1   12   1   12   1   12   1    4   1   4    1   4   1    4
//  0     8     9   21   22  34   35  47   48  60   61  65   66  70  71  75   76  80
#define BUFSZ 82
#define COLS 4

int xrayDump (long start, long cnt) {
  int i;

  char buf [BUFSZ];
  char buf12 [13];
  char buf5 [6];
  for (i = 0; i < cnt; i ++) {
    if (i % COLS == 0) {
      memset (buf, ' ', sizeof (buf));
      buf [BUFSZ - 1] = 0;
      sprintf (buf5, "%5lo", start + i);
      memcpy (buf + 0, buf5, 5);
    }

    word36 data = coreReadWord (start + i);
    sprintf (buf12, "%012"PRIo36, data);
    memcpy (buf + 9 + (i % COLS) * 13, buf12, 12);

    char buf4 [5] = "...."; 
    word9 ch;

    ch = (data >> 27) & 0777;
    if (ch < 128 && isprint (ch))
      buf4[0] = ch;
      
    ch = (data >> 18) & 0777;
    if (ch < 128 && isprint (ch))
      buf4[1] = ch;
      
    ch = (data >>  9) & 0777;
    if (ch < 128 && isprint (ch))
      buf4[2] = ch;
      
    ch = (data >>  0) & 0777;
    if (ch < 128 && isprint (ch))
      buf4[3] = ch;
      
    memcpy (buf + 61 + (i % COLS) * 5, buf4, 4);

    if (i % COLS == COLS - 1) {
      printf ("%s\n", buf);
    }
  }
  if (i % COLS != 0) {
    printf ("%s\n", buf);
  }
  return 0;
}


int xrayPrintSLT (void) {
  printf ("need slt segment ptr\n");
  return -1;
}

static void analyzeHCDBR (void) {
  word36 even, odd;
  coreReadDWord (FLAGBOX_ABSADDR + fgbx_hc_dpr, & even, & odd);
  printf ("hc_dpr %012"PRIo36" %012"PRIo36"\n", even, odd);
  dsbr_s dsbr;
  fetchDSBR (FLAGBOX_ABSADDR + fgbx_hc_dpr, & dsbr);
}


static void analyzeSST (void) {
  // The SST SDW address is at flagbox.sst_sdw
  word36 even, odd;
  coreReadDWord (FLAGBOX_ABSADDR + fgbx_sst_sdw, & even, & odd);
  printf ("sst_sdw %012"PRIo36" %012"PRIo36"\n", even, odd);
  fetchSDW (FLAGBOX_ABSADDR + fgbx_sst_sdw, & sstSDW);
  printf ("ADDR    %08o\n", sstSDW.ADDR);
  printf ("R1      %o\n", sstSDW.R1);
  printf ("R2      %o\n", sstSDW.R2);
  printf ("R3      %o\n", sstSDW.R3);
  printf ("BOUND   %05o\n", sstSDW.BOUND);
  printf ("R       %o\n", sstSDW.R);
  printf ("E       %o\n", sstSDW.E);
  printf ("W       %o\n", sstSDW.W);
  printf ("P       %o\n", sstSDW.P);
  printf ("U       %o %s\n", sstSDW.U, sstSDW.U ? "Unpaged" : "Paged");
  printf ("G       %o\n", sstSDW.G);
  printf ("C       %o\n", sstSDW.C);
  printf ("EB      %o\n", sstSDW.EB);
  printf ("POINTER %o\n", sstSDW.POINTER);
  printf ("DF      %o\n", sstSDW.DF);
  printf ("FE      %o\n", sstSDW.FE);
  printf ("USE     %o\n", sstSDW.USE);

  if (sstSDW.U != 0) {
    printf ("SST SDW not paged; bailing\n");
    return;
  }

return;
  sstPageTableAddr = sstSDW.ADDR;
  sstPageTableBound = sstSDW.BOUND << 4; 

  for (int n = 0; n <= sstPageTableBound; n ++) {
    //word36 ptw = coreReadWord (sstPageTableAddr + n);
    ptw_s ptw;
    fetchPTW (sstPageTableAddr + n, & ptw);
printf ("%5o %08o %012"PRIo36"\n", n, sstPageTableAddr + n, coreReadWord (sstPageTableAddr + n));
    if (ptw.U) {
      printf ("%08o  ADDR %08o U %o M %o F %o FC %o\n",
              n * 1024, ptw.ADDR << 6, ptw.U, ptw.M, ptw.DF, ptw.FC);
    }
  }
}

void simPrintf (const char* fmt, ...) {
  va_list args;
  va_start(args, fmt);
  (void) vprintf(fmt, args);
  va_end(args);
}

int main (int argc, char * argv[]) {

  if (argc > 2) {
    (void) snprintf (filebuf, sizeof (filebuf), "%s", argv[2]);
  } else {
    (void) snprintf (filebuf, sizeof (filebuf), "dps8m.state");
  }

  system_state = (struct system_state_s *) openShm (filebuf);
  if (! system_state) {
    (void)fprintf (stderr, "FATAL: system state open_shm failed: %s (Error %d)\r\n", strerror (errno), errno);
    return EXIT_FAILURE;
  }

  word36 sentinal1 = coreReadWord (fgbx_sentinel);
  printf ("sentinal1: %012"PRIo36"\n", sentinal1);

  analyzeSST ();
  analyzeHCDBR ();

  running = true;
  while (running) {
    printf ("xray> ");
    fflush (stdout);
    char * line = NULL;
    size_t len = 0;
    ssize_t nread = getline (& line, & len, stdin);
    if (nread < 0)
      break;
    //fwrite (line, nread, 1, stdout);
    int rc = parse_string (line);
    if (rc) {
      printf ("parser returned %d\n", rc);
    }
  }
  return 0;
}
