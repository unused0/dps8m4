/*
 * vim: filetype=c:tabstop=4:ai:expandtab
 * SPDX-License-Identifier: ICU
 * scspell-id: b39ac98e-f62e-11ec-98a8-80ee73e9b8e7
 *
 * ---------------------------------------------------------------------------
 *
 * Copyright (c) 2023-2024 Charles Anthony
 * Copyright (c) 2021-2024 The DPS8M Development Team
 *
 * This software is made available under the terms of the ICU License.
 * See the LICENSE.md file at the top-level directory of this distribution.
 *
 * ---------------------------------------------------------------------------
 */

#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#include "dps8m.h"
#include "cmd.h"
#include "iom_channel.h"
#include "iom.h"
#include "tap.h"
#include "utils.h"

//#define PARSER_TEST


#if 1

typedef enum cmdRC_e { cmdRCOk = 0, cmdRCErr = -1 } cmdRC;

void cmdParseError (const char * string, const char * p, const char * msg) {
  simPrintf ("Command parsing error:\n");
  long n = p - string;
  if (n >= 0 && n < PARSE_LIMIT) {
    simPrintf ("%s\n", string);
    for (long i = 0; i < n; i ++)
      simPrintf (" ");
    simPrintf ("^\n");
  }
  simPrintf ("%s\n", msg);
}

// Token:
//   tapa_01
//   tapa_01
//   tapa_[00-15]
//   tapa_[00,01,04]
//   tap[a-z]
//   tap[a,b,c]

//   Alpha [ Alphanumeric | '_' | '['  ]* 
//   " [^"] "
// <whitespace> [ <non-whitespace> | <quoted string> [whitespace | EOF]
// return
//   NULL: error
//   zero length string: EOF

const char * cmdToken (const char * string, const char * p, token * t, tokenType * tt) {
  memset (t, 0, sizeof (token));
  char * tptr = & (*t)[0];
  //*tptr = 0;
  char * tlast = & (*t)[PARSE_LIMIT - 1]; // trailing null
  *tt = tokenEmpty;
  if (! p)
    return p;

  char ch;
  // Skip white space
  while (1) {
    ch = *p;
    // End of string?
    if (! ch)
      return p;
    if (! isspace (ch))
      break;
    if (ch > ' ')
      break;
    p ++;
  } // while (1) skip white space

  if (isalpha (ch)) {
    *tt = tokenWord;
    while (1) { // traverse token body
      // append ch to token
      if (tptr == tlast) {
        cmdParseError (string, p, "Token too long");
        tptr ++;
      } else if (tptr < tlast) {
       * tptr ++ = ch;
      }
      // advance to next character
      p ++;
      ch = * p;
    
#define TOK_CHARS "_[]-,"
      if (isalnum (ch) || index (TOK_CHARS, ch))
        continue;
      break;
    } // while (1) traverse token body
    return p;
  } // is alpha


  cmdParseError (string, p, "Unhandled character");
  return p;
} // cmdToken

static char * names[] = {
  "tap", 
};
#define N_NAMES (sizeof (names) / sizeof (char *))

//  tap
//  tapa
//  tapa_01

const char * deviceLetters = "abcdefghijklmnopqrstuvwxyz";

// 0 ok
static enum nameTypes_e parseName (token * t, struct name_s * name) {
  memset (name, 0, sizeof (struct name_s));

  char * p = & (*t)[0];

  int i;
  for (i = 0; i < N_NAMES; i ++)
    if (strncmp (names[i], p, 3) == 0)
      break;

  if (i >= N_NAMES) {
    cmdParseError (*t, p, "Unknown name");
    return ntErr;
  }
  strncpy (name->name, p, 3);
  p += 3;

  char deviceLetter = *p;
  if (! deviceLetter)
    return ntDev; // just "dev"

  char * dlp = index (deviceLetters, deviceLetter);

  if (! dlp) {
    cmdParseError (*t, p, "Invalid device letter");
    return ntErr;
  }
  long offset = dlp - deviceLetters;
  if (offset < 0 || offset > 25) {
    cmdParseError (*t, p, "Invalid device letter");
    return ntErr;
  }
  name->deviceLetterSet = true;
  name->deviceLetter[offset] = true;
  // consume device letter
  p += 1;

  char underscore = *p;
  if (! underscore) {
    return ntDevLtr;
  }

  if (underscore != '_') {
    cmdParseError (*t , p, "Device syntax error; expected '_'");
    return ntErr;
  }

  // consume underscore
  p += 1;
  
  char d1 = *p;
  if (! isdigit (d1)) {
    cmdParseError (*t , p, "Device unit number expected");
    return ntErr;
  }
  int n1 = d1 - '0';

  // consume d1
  p += 1;

  int n;
  char d2 = *p;
  if (d2) {
    if (! isdigit (d2)) {
      cmdParseError (*t , p, "Device unit number expected");
      return ntErr;
    }
    int n2 = d2 - '0';
    n = n1 * 10 + n2;
    // consume d2
    p += 1;
  } else {
    n = n1;
  }

  if (n < 0 || n > 63) {
    cmdParseError (*t , p, "Device unit number not in 00..63");
    return ntErr;
  }

  name->deviceCodeSet = true;
  name->deviceCode[n] = true;

  if (*p) {
    cmdParseError (*t , p, "Name has trailing chacters");
    return ntErr;
  }

  return ntDevLtrDev;

} // parseName;

cmdRC cmdString (const char * cmdLine) {
  const char * p = cmdLine;
  token t;
  tokenType tt;
  const char * q = cmdToken (cmdLine, p, & t, & tt);

  //simPrintf ("'%s' %d\n", t, tt);

  if (tt != tokenWord) {
    cmdParseError (cmdLine, q, "Expected name");
    return cmdRCErr;
  }
  struct name_s name;
  enum nameTypes_e nt = parseName (& t, & name);
  //simPrintf ("%d %s\n", rc, name.name);
  if (nt == ntErr) {
    return cmdRCErr;
  }
  if (strcmp (name.name, "tap") == 0) {
    tapCmd (cmdLine, & t, & name, q);
  }

  return cmdRCOk;
}

#ifdef PARSER_TEST
static void cmdTest (void) {
  cmdRC rc;
#if 0
  rc = cmdString ("fooa_01 watch");
  if (rc) {
    simPrintf ("cmdTest cmdString returned %d\n", rc);
  }
  rc = cmdString ("tapa_01 watch");
  if (rc) {
    simPrintf ("cmdTest cmdString returned %d\n", rc);
  }
#endif
  char * line = NULL;
  size_t len = 0;
  while (1) {
    simPrintf (">>>");
    ssize_t nread = getline (& line, & len, stdin);
    if (nread == -1)
      break;
    size_t l = strlen (line);
    if (l) {
      if (line[l - 1] == '\n' || line[l-1] == '\r') {
        line[l - 1] = '\0';
      }
    }
    rc = cmdString (line);
    if (rc != cmdRCOk) {
      simPrintf ("err %d\n", rc);
    }
  }
  free (line);
}
#endif

void cmdInit (void) {
#ifdef PARSER_TEST
  cmdTest ();
#endif
}

#else
#define SOURCE_STACK_DEPTH 16


enum cmdSrcType { ST_CLOSED = 0, ST_STRING };

struct cmdSrc {
  int (* getc) (struct cmdSrc * this);
  void (* ungetc) (struct cmdSrc * this, int ch);
  void (* close) (struct cmdSrc * this);
  // Private
  enum cmdSrcType cmdSrcType;
  int ungotc;
  char parseBuffer[PARSE_LIMIT];
  //  ST_STRING
  char * cmdSrcStr;
  char * cmdSrcStrPtr;
};


static struct cmdSrc cmdSrcStack[SOURCE_STACK_DEPTH];
static int cmdSrcStackPtr;

#ifdef PARSER_TEST
static void cmdSrcTest (void);
#endif

void cmdInit (void) {
  // for (int i = 0; i < SOURCE_STACK_DEPTH; i ++)
  //   cmdSrcStack[i]cmdSrcType = ST_CLOSED;
  memset (cmdSrcStack, 0, sizeof (cmdSrcStack));
  cmdSrcStackPtr = -1;
#ifdef PARSER_TEST
  cmdSrcTest ();
#endif
}



static int cmdSrcGetcString (struct cmdSrc * this) {
  if (! this) {
    simWarn ("%s passed null  this\n", __func__);
    return -1;
  }
  if (this->cmdSrcType != ST_STRING) {
    simWarn ("%s passed not ST_STRING this\n", __func__);
    return -1;
  }
  if (this->ungotc != -1) {
    int ungotc = this->ungotc;
    this->ungotc = -1;
    return ungotc;
  }
  if (! this->cmdSrcStrPtr) {
    simWarn ("%s passed null this->cmdSrcStrPtr\n", __func__);
    return -1;
  }
  if (! * this->cmdSrcStrPtr) {
    return -1;  // EOF
  }
  return * this->cmdSrcStrPtr ++;
}

static void cmdSrcUngetcString (struct cmdSrc * this, int ch) {
  if (! this) {
    simWarn ("%s passed null  this\n", __func__);
    return;
  }
  if (this->cmdSrcType != ST_STRING) {
    simWarn ("%s passed not ST_STRING this\n", __func__);
    return;
  }
  if (this->ungotc != -1) {
    simWarn ("%s dosen't have room\n", __func__);
    return;
  }
  this->ungotc = ch;
}

void cmdSrcCloseString (struct cmdSrc * this) {
  if (! this) {
    simWarn ("%s passed null  this\n", __func__);
    return;
  }
  if (this->cmdSrcType != ST_STRING) {
    simWarn ("%s passed not ST_STRING this\n", __func__);
    return;
  }
  if (! this->cmdSrcStrPtr) {
    simWarn ("%s passed null this->cmdSrcStrPtr\n", __func__);
    return;
  }
  this->getc = NULL;
  this->ungetc = NULL;
  this->close = NULL;
  this->cmdSrcType = ST_CLOSED;
  this->ungotc = -1;
  free (this->cmdSrcStr);
  this->cmdSrcStr = NULL;
  this->cmdSrcStrPtr = NULL;
  if (cmdSrcStackPtr < 0) {

  cmdSrcStackPtr --;
    simWarn ("%s not popping empty stack\n", __func__);
    return;
  }
}

int cmdSrcOpenString (char * string) {
  if (cmdSrcStackPtr >= SOURCE_STACK_DEPTH) {
    simWarn ("%s stack full\n", __func__);
    return -1;
  }
  cmdSrcStackPtr ++;
  struct cmdSrc * this = cmdSrcStack + cmdSrcStackPtr;
  if (this->cmdSrcType != ST_CLOSED) {
    simWarn ("%s found unclosed this\n", __func__);
  }
  this->getc = cmdSrcGetcString;
  this->ungetc = cmdSrcUngetcString;
  this->close = cmdSrcCloseString;
  this->cmdSrcType = ST_STRING;
  this->ungotc = -1;
  this->cmdSrcStr = this->cmdSrcStrPtr = strdup (string);
  return 0; 
}

void cmdSrcClose (void) {
  if (cmdSrcStackPtr < 0) {
    simWarn ("%s stack empty\n", __func__);
    return;
  }
  struct cmdSrc * this = cmdSrcStack + cmdSrcStackPtr;
  this->close (this);
}

static int iswhite (int ch) {
  return ((! isprint (ch)) || isblank (ch));
}

static void skipWhitespace (struct cmdSrc * this) {
  while (1) {
    int ch = this->getc (this);
    if (ch == -1) // EOF
      return;
    if (! iswhite (ch)) {
      this->ungetc (this, ch);
      return;
    }
  }
}

// <whitespace> [ <non-whitespace> | <quoted string> [whitespace | EOF]
// return
//   NULL: error
//   zero length string: EOF
static char * cmdSrcParse (void) {
  if (cmdSrcStackPtr < 0) {
    simWarn ("%s empty stack\n", __func__);
    return NULL;
  }
  struct cmdSrc * this = cmdSrcStack + cmdSrcStackPtr;
  skipWhitespace (this);
  size_t len = 0;
  while (1) {
    int ch = this->getc (this);
    if (ch == -1) {
      break; // EOF
    }
    if (iswhite (ch)) {
      this->ungetc (this, ch);
      break;
    }
    if (len >= PARSE_LIMIT - 1) {
      simPrintf ("%s breaking long token\n", __func__);
      this->ungetc (this, ch);
      break;
    }
    this->parseBuffer[len ++] = (char) ch;
  }
  this->parseBuffer[len] = 0;
  return this->parseBuffer;
}

static int doCmdEc (char * cmd) {
  simPrintf ("!!!!\n");
  return 0;
}

struct cmdTblEntry {
  char * cmd;
  int (* doCmd) (char * cmd);
};

static struct cmdTblEntry cmdTbl [] = {
  {"ec", doCmdEc },
  { NULL, NULL }
};

static void cmdFlush (bool show) {
  // advance to end of line
  // XXX if show, print the buffered line
}

int doCmd (void) {
  char * cmdStatic = cmdSrcParse ();
  //strdupl (cmd, cmdStatic);
  char cmd [strlen (cmdStatic) + 1];
  strcpy (cmd, cmdStatic);
  for (struct cmdTblEntry * entry = cmdTbl; entry->cmd; entry ++) {
    if (strcasecmp (entry->cmd, cmd) == 0) {
      return entry->doCmd (cmd);
    }
  }
  simWarn ("Command '%s' not recognized\n", cmd);
  cmdFlush (true);
  return -1; // cmd not found
}

void doCmdStr (char * string) {
  cmdSrcOpenString (string);
  doCmd ();
  cmdSrcClose ();
}

#ifdef PARSER_TEST
static void cmdSrcTest (void) {
  cmdSrcOpenString ("this is a parser test.\n");
  while (1) {
    char * word = cmdSrcParse ();
    if (! word) {
      simPrintf ("%s error\n", __func__);
      return;
    }
    if (strlen (word)) {
      simPrintf ("<%s>\n", word);
    } else {
      simPrintf ("EOF\n");
      break;
    }
  }
  cmdSrcClose ();


  doCmdStr ("fubar");
  doCmdStr ("ec do boot.ini");
}
#endif
#endif
