//#define DDBG(x) x
#define DDBG(x)
/*
 * vim: filetype=c:tabstop=4:ai:expandtab
 * SPDX-License-Identifier: ICU
 * SPDX-License-Identifier: Multics
 * scspell-id: 134751ac-f62e-11ec-b67e-80ee73e9b8e7
 *
 * ---------------------------------------------------------------------------
 *
 * Copyright (c) 2007-2013 Michael Mondy
 * Copyright (c) 2012-2016 Harry Reed
 * Copyright (c) 2013-2024 Charles Anthony
 * Copyright (c) 2021-2023 The DPS8M Development Team
 *
 * This software is made available under the terms of the ICU License.
 * See the LICENSE.md file at the top-level directory of this distribution.
 *
 * ---------------------------------------------------------------------------
 *
 * This source file may contain code comments that adapt, include, and/or
 * incorporate Multics program code and/or documentation distributed under
 * the Multics License.  In the event of any discrepancy between code
 * comments herein and the original Multics materials, the original Multics
 * materials should be considered authoritative unless otherwise noted.
 * For more details and historical background, see the LICENSE.md file at
 * the top-level directory of this distribution.
 *
 * ---------------------------------------------------------------------------
 */

#include <stdio.h>
#include <ctype.h>

#include "dps8m.h"
#include "iom_channel.h"
#include "iom.h"
#include "faults.h"
#include "dia.h"
#include "cable.h"
#include "utils.h"
#include "dnpkt.h"
#include "fnp.h"
#include "threadz.h"

#define ASSUME0 0 // XXX

// libuv interface

//
// allocBuffer: libuv callback handler to allocate buffers for incomingd data.
//

static void allocBuffer (UNUSED uv_handle_t * handle, size_t suggestedSize, uv_buf_t * buf) {
  suggestedSize = sizeof (dnPkt);
  char * p = (char *) malloc (suggestedSize);
  if (! p) {
     simWarn ("%s malloc fail\n", __func__);
  }
  * buf = uv_buf_init (p, (uint) suggestedSize);
}

/* simParseAddress       host:port

   Presumption is that the input, if it doesn't contain a ':' character is a port specifier.
   If the host field contains one or more colon characters (i.e. it is an IPv6 address),
   the IPv6 address MUST be enclosed in square bracket characters (i.e. Domain Literal format)

   Inputs:
        cptr    =       pointer to input string
        defaultHost
                =       optional pointer to default host if none specified
        hostLen =      length of host buffer
        defaultPort
                =       optional pointer to default port if none specified
        portLen =      length of port buffer
        validateAddr = optional name/addr which is checked to be equivalent
                        to the host result of parsing the other input.  This
                        address would usually be returned by sim_accept_conn.
   Outputs:
        host    =       pointer to buffer for IP address (may be NULL), 0 = none
        port    =       pointer to buffer for IP port (may be NULL), 0 = none
        result  =       status (0 on complete success or -1 if
                        parsing can't happen due to bad syntax, a value is
                        out of range, a result can't fit into a result buffer,
                        a service name doesn't exist, or a validation name
                        doesn't match the parsed host)
*/

static int simParseAddress (const char *cptr,
                            char *host, size_t hostLen, const char *defaultHost,
                            char *port, size_t portLen, const char *defaultPort,
                            const char *validateAddr) {
  char gbuf[CBUFSIZE], defaultPbuf[CBUFSIZE];
  const char *hostp;
  char *portp;
  char *endc;
  unsigned long portval;

  if ((host != NULL) && (hostLen != 0))
    memset (host, 0, hostLen);
  if ((port != NULL) && (portLen != 0))
    memset (port, 0, portLen);
  if ((cptr == NULL) || (*cptr == 0)) {
    if (((defaultHost == NULL) || (*defaultHost == 0)) || ((defaultPort == NULL) || (*defaultPort == 0)))
      return -1;
    if ((host == NULL) || (port == NULL))
      return -1;                                  /* no place */
    if ((strlen(defaultHost) >= hostLen) || (strlen(defaultPort) >= portLen))
      return -1;                                  /* no room */
    strcpy (host, defaultHost);
    strcpy (port, defaultPort);
    return 0;
  }
  memset (defaultPbuf, 0, sizeof (defaultPbuf));
  if (defaultPort)
    strncpy (defaultPbuf, defaultPort, sizeof (defaultPbuf)-1);
  gbuf[sizeof (gbuf)-1] = '\0';
  strncpy (gbuf, cptr, sizeof (gbuf)-1);
  hostp = gbuf;                                           /* default addr */
  portp = NULL;
  if ((portp = strrchr (gbuf, ':')) &&                    /* x:y? split */
      (NULL == strchr (portp, ']'))) {
    *portp++ = 0;
    if (*portp == '\0')
      portp = defaultPbuf;
  } else {                                                  /* No colon in input */
    portp = gbuf;                                       /* Input is the port specifier */
    hostp = (const char *)defaultHost;                 /* host is defaulted if provided */
  }
  if (portp != NULL) {
    portval = strtoul (portp, &endc, 10);
    if ((*endc == '\0') && ((portval == 0) || (portval > 65535)))
      return -1;                                      /* numeric value too big */
    if (*endc != '\0') {
      struct servent *se = getservbyname (portp, "tcp");

      if (se == NULL)
        return -1;                                  /* invalid service name */
    }
  }
  if (port)                                               /* port wanted? */
    if (portp != NULL) {
      if (strlen (portp) >= portLen)
        return -1;                                  /* no room */
      else
        strcpy (port, portp);
    }
  if (hostp != NULL) {
    if (']' == hostp[strlen (hostp)-1]) {
      if ('[' != hostp[0])
        return -1;                                  /* invalid domain literal */
      /* host may be the const defaultHost so move to temp buffer before modifying */
      strncpy (gbuf, hostp+1, sizeof (gbuf)-1);         /* remove brackets from domain literal host */
      gbuf[strlen (gbuf)-1] = '\0';
      hostp = gbuf;
    }
  }
  if (host) {                                             /* host wanted? */
    if (hostp != NULL) {
      if (strlen (hostp) >= hostLen)
        return -1;                                  /* no room */
      else
        if (('\0' != hostp[0]) || (defaultHost == NULL))
          strcpy (host, hostp);
        else if (strlen (defaultHost) >= hostLen)
          return -1;                          /* no room */
        else
          strcpy (host, defaultHost);
    } else {
      if (defaultHost) {
        if (strlen (defaultHost) >= hostLen)
          return -1;                              /* no room */
        else
          strcpy (host, defaultHost);
      }
    }
  }
  if (validateAddr) {
    struct addrinfo *ai_host, *ai_validate, *ai, *aiv;
    int status;

    if (hostp == NULL)
      return -1;
    if (getaddrinfo (hostp, NULL, NULL, & ai_host))
      return -1;
    if (getaddrinfo (validateAddr, NULL, NULL, & ai_validate))
      return -1;
    status = -1;
    for (ai = ai_host; ai != NULL; ai = ai->ai_next) {
      for (aiv = ai_validate; aiv != NULL; aiv = aiv->ai_next) {
        if ((ai->ai_addrlen == aiv->ai_addrlen) &&
            (ai->ai_family == aiv->ai_family) &&
            (0 == memcmp (ai->ai_addr, aiv->ai_addr, ai->ai_addrlen))) {
          status = 0;
          break;
        }
      }
    }
    if (status != 0) {
      /* be generous and allow successful validations against variations of localhost addresses */
      if (((0 == strcmp ("127.0.0.1", hostp)) &&
           (0 == strcmp ("::1", validateAddr))) ||
          ((0 == strcmp ("127.0.0.1", validateAddr)) &&
           (0 == strcmp ("::1", hostp))))
        status = 0;
    }
    return status;
  }
  return 0;
}

// From sihm udplib

static int tcpParseRemote (const char * premote, char * rhost, size_t rhostl, int32_t * rport) {
  // This routine will parse a remote address string in any of these forms -
  //
  //            :w.x.y.z:rrrr
  //            :name.domain.com:rrrr
  //            :rrrr
  //            w.x.y.z:rrrr
  //            name.domain.com:rrrr
  //
  // In all examples, "rrrr" is the remote port number that we use for transmitting.
  // "w.x.y.z" is a dotted IP for the remote machine
  // and "name.domain.com" is its name (which will be looked up to get the IP).
  // If the host name/IP is omitted then it defaults to "localhost".

  char host [64], port [16];
  if (* premote == '\0')
    return -1;
  * rhost = '\0';

  if (simParseAddress (premote, host, sizeof (host), "localhost", port, sizeof (port), NULL, NULL))
    return -1;
  strncpy (rhost, host, rhostl);
  * rport = atoi (port);
  return 0;
}

#define DIA_UNIT_IDX(uptr) ((uptr) - diaUnit)

static configList_t diaConfigList [] = {
  /*  0 */ { "mailbox", 0, 07777, NULL },
  { NULL, 0, 0, NULL }
};

static simRc setConfig (UNIT * uptr, UNUSED int value, const char * cptr, UNUSED void * desc) {
  uint diaUnitIdx = (uint) DIA_UNIT_IDX (uptr);
  //if (diaUnitIdx >= diaDevice.numunits)
  if (diaUnitIdx >= N_DIA_UNITS_MAX) {
    simPrintf ("error: DIA SET CONFIG: invalid unit number %d\n", diaUnitIdx);
    return SCPE_ARG;
  }

  diaUnitData_t * dudp = diaData.diaUnitData + diaUnitIdx;

  configState_t cfgState = { NULL, NULL };

  for (;;) {
    int64_t v;
    int rc = cfgParse ("DIA SET CONFIG", cptr, diaConfigList, & cfgState, & v);
    switch (rc) {
      case CFG_ERROR:
        cfgParseDone (& cfgState);
        return SCPE_ARG;

      case CFG_DONE:
        break;

      case 0: // mailbox
        dudp -> mailboxAddress = (uint) v;
        break;

      default:
        simPrintf ("error: DIA SET CONFIG: invalid cfgParse rc <%d>\n", rc);
        cfgParseDone (& cfgState);
        return SCPE_ARG;
    } // switch
    if (rc < 0)
      break;
  } // process statements
  cfgParseDone (& cfgState);
  return SCPE_OK;
}

static simRc showConfig (UNUSED FILE * st, UNIT * uptr, UNUSED int val, UNUSED const void * desc) {
  long unit_idx = DIA_UNIT_IDX (uptr);
  if (unit_idx >= (long) N_DIA_UNITS_MAX) {
    simPrintf ("error: invalid unit number %ld\n", unit_idx);
    return SCPE_ARG;
  }

  simPrintf ("DIA unit number %ld\n", unit_idx);
  diaUnitData_t * dudp = diaData.diaUnitData + unit_idx;

  simPrintf ("DIA Mailbox Address:         %04o(8)\n", dudp -> mailboxAddress);

  return SCPE_OK;
}


static simRc showStatus (UNUSED FILE * st, UNIT * uptr, UNUSED int val, UNUSED const void * desc) {
  long diaUnitIdx = DIA_UNIT_IDX (uptr);
  if (diaUnitIdx >= (long) N_DIA_UNITS_MAX) {
    simPrintf ("error: invalid unit number %ld\n", diaUnitIdx);
    return SCPE_ARG;
  }

  simPrintf ("DIA unit number %ld\n", diaUnitIdx);
  diaUnitData_t * dudp = diaData.diaUnitData + diaUnitIdx;

  simPrintf ("mailboxAddress:              %04o\n", dudp->mailboxAddress);
  return SCPE_OK;
}

static simRc showUnits (UNUSED FILE * st, UNUSED UNIT * uptr, UNUSED int val, UNUSED const void * desc) {
  simPrintf ("Number of DIA units in system is %d\n", diaDevice.numunits);
  return SCPE_OK;
}

static simRc setNunits (UNUSED UNIT * uptr, UNUSED int32_t value, const char * cptr, UNUSED void * desc) {
  if (! cptr)
    return SCPE_ARG;
  int n = atoi (cptr);
  if (n < 0 || n > N_DIA_UNITS_MAX)
    return SCPE_ARG;
  diaDevice.numunits = (uint32_t) n;
  return SCPE_OK;
}

static MTAB diaMod [] = {
  {
    MTAB_XTD | MTAB_VUN | MTAB_NMO | MTAB_VALR, /* mask */
    0,            /* match */
    "CONFIG",     /* print string */
    "CONFIG",         /* match string */
    setConfig,         /* validation routine */
    showConfig, /* display routine */
    NULL,          /* value descriptor */
    NULL   // help string
  },

  {
    MTAB_XTD | MTAB_VUN | MTAB_NMO | MTAB_VALR, /* mask */
    0,            /* match */
    "STATUS",     /* print string */
    "STATUS",         /* match string */
    NULL,         /* validation routine */
    showStatus, /* display routine */
    NULL,          /* value descriptor */
    NULL   // help string
  },

  {
    MTAB_XTD | MTAB_VDV | MTAB_NMO | MTAB_VALR, /* mask */
    0,            /* match */
    "NUNITS",     /* print string */
    "NUNITS",         /* match string */
    setNunits, /* validation routine */
    showUnits, /* display routine */
    "Number of DIA units in the system", /* value descriptor */
    NULL          // help
  },
  { 0, 0, NULL, NULL, NULL, NULL, NULL, NULL }
};


UNIT diaUnit [N_DIA_UNITS_MAX] = {
  {UDATA (NULL, UNIT_DISABLE | UNIT_IDLE, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL}
};

static simRc diaReset (UNUSED DEVICE * dptr) {
  return SCPE_OK;
}

static void dnSendCB (uv_write_t * req, int status) {
  if (status)
    simPrintf ("send cb %d\n", status);
}

static void dnSend (uint diaUnitIdx, dnPkt * pkt) {
#ifdef PKT_TRACE
  simPrintf ("DEBUG: >%s\n", (char *) pkt);
#endif
  diaUnitData_t * dudp = & diaData.diaUnitData[diaUnitIdx];
  uv_write_t * req = malloc (sizeof (uv_write_t));
  if (! req) {
    simPrintf ("%s req malloc fail\n", __func__);
    return;
  }
  // set base to null
  memset (req, 0, sizeof (uv_write_t));

  void * p = malloc (sizeof (dnPkt));
  if (p == NULL) {
    simPrintf ("%s buf malloc fail\n", __func__);
    return;
  }
  uv_buf_t buf = uv_buf_init ((char *) p, (uint) sizeof (dnPkt));

  memcpy (buf.base, pkt, sizeof (dnPkt));
  int rc = uv_write (req, (uv_stream_t *) & dudp->tcpHandle, & buf, 1, dnSendCB);
  if (rc < 0) {
    fprintf (stderr, "%s uv_udp_send failed %d\n", __func__, rc);
  }
}

static void processCmdR (uv_stream_t * req, dnPkt * pkt) {
  diaClientData * cdp = (diaClientData *) req->data;
  if (cdp->magic != DIA_CLIENT_MAGIC)
    simPrintf ("ERROR: %s no magic\n", __func__);
  //diaUnitData_t * dudp = & diaData.diaUnitData[cdp->diaUnitIdx];
  word24 addr;
  int n = sscanf (pkt->cmdRead.addr, "%o", & addr);
  if (n != 1) {
    simPrintf ("%s unable to extract address\n", __func__);
  } else {
    word36 data;
    iomDirectDataService (& iomChanData[cdp->iomUnitIdx][cdp->chan], addr, & data, direct_load);

    dnPkt pkt;
    memset (& pkt, 0, sizeof (pkt));
    pkt.cmd = DN_CMD_DATA;
    sprintf (pkt.cmdData.data, "%08o:%012"PRIo64"", addr, data);
    dnSend (cdp->diaUnitIdx, & pkt);
  }
}

// 'Z' Read and clear

static void processCmdRC (uv_stream_t * req, dnPkt * pkt) {
  diaClientData * cdp = (diaClientData *) req->data;
  if (cdp->magic != DIA_CLIENT_MAGIC)
    simPrintf ("ERROR: %s no magic\n", __func__);
  //diaUnitData_t * dudp = & diaData.diaUnitData[cdp->diaUnitIdx];
  word24 addr;
  int n = sscanf (pkt->cmdRead.addr, "%o", & addr);
  if (n != 1) {
    simPrintf ("%s unable to extract address\n", __func__);
  } else {
    word36 data;
//simPrintf ("direct_read_clear at %06o was %036lo\n", addr, M[addr] & MASK36);
    iomDirectDataService (& iomChanData[cdp->iomUnitIdx][cdp->chan], addr, & data, direct_read_clear);
//simPrintf ("direct_read_clear now %036lo\n", M[addr] & MASK36);

    dnPkt pkt;
    memset (& pkt, 0, sizeof (pkt));
    pkt.cmd = DN_CMD_DATA;
    sprintf (pkt.cmdData.data, "%08o:%012"PRIo64"", addr, data);
    dnSend (cdp->diaUnitIdx, & pkt);
  }
}

static void processCmdW (uv_stream_t * req, dnPkt * pkt) {

// IDB: 000033000100 100501017600 000000000001
//for (uint i = 0; i < 513000; i ++) {
  //word36 w = M[i] & MASK36;
  //if (w == 0000033000100 || w == 0100501017600 || w == 0000000000001) {
    //simPrintf ("%08o:%012"PRIo64"\n", i, w);
  //}
//}

  diaClientData * cdp = (diaClientData *) req->data;
  if (cdp->magic != DIA_CLIENT_MAGIC)
    simPrintf ("ERROR: %s no magic\n", __func__);
  word24 addr;
  word36 data;
  int n = sscanf (pkt->cmdWrite.data, "%08o:%012"PRIo64"", & addr, & data);
  if (n != 2) {
    simPrintf ("%s unable to extract address\n", __func__);
  } else {
    iomDirectDataService (& iomChanData[cdp->iomUnitIdx][cdp->chan], addr, & data, direct_store);
//simPrintf ("dds %05o %012lo\n", addr, data);
//simPrintf ("DEBUG: %s store %08o:%012"PRIo64"\n", __func__, addr, data);
  }
}

static void processCmdSEC (uv_stream_t * req, dnPkt * pkt) {
  //simPrintf ("DEBUG:disconnect\n");
  diaClientData * cdp = (diaClientData *) req->data;
  if (cdp->magic != DIA_CLIENT_MAGIC)
    simPrintf ("ERROR: %s no magic\n", __func__);
  uint cell = 3;
  int n = sscanf (pkt->cmdSet.cell, "%o", & cell);
  if (n != 1) {
    simPrintf ("%s unable to extract cell\n", __func__);
  }
  chnlState_t * chnlp = & iomChanData[cdp->iomUnitIdx][cdp->chan];
  if (cell == 7) {
    send_special_interrupt (chnlp, 0, 0, 0);
  } else if (cell == 3) {
simPrintf (INV"status"RES"\n");
    //send_general_interrupt (chnlp, imwTerminatePic);
    chnlp->stati = 04000;
    //chnlp->in_use = false;
    //status_service (chnlp, false);
    send_general_interrupt (chnlp, imwTerminatePic);
  } else {
    simPrintf ("Error: SEC %o\n", cell);
  }
}

static void processCmdIntrCS (uv_stream_t * req, dnPkt * pkt) {
  diaClientData * cdp = (diaClientData *) req->data;
  if (cdp->magic != DIA_CLIENT_MAGIC)
    simPrintf ("ERROR: %s no magic\n", __func__);
  uint cell;
  chnlState_t * chnlp = & iomChanData[cdp->iomUnitIdx][cdp->chan];
  int n = sscanf (pkt->cmdIntrCS.intrLvl, "%o", & cell);
  if (n != 1) {
    simPrintf ("%s unable to extract cell\n", __func__);
  } else if (cell == 7) {
    send_special_interrupt (chnlp, 0, 0, 0);
  } else if (cell == 3) {
    send_general_interrupt (chnlp, imwTerminatePic);
  } else {
    simPrintf ("Error: Interrupt CS %o\n", cell);
  }
}

static void processCmdDis (uv_stream_t * req, dnPkt * pkt) {
  //simPrintf ("DEBUG:disconnect\n");
  diaClientData * cdp = (diaClientData *) req->data;
  if (cdp->magic != DIA_CLIENT_MAGIC)
    simPrintf ("ERROR: %s no magic\n", __func__);


//simPrintf ("DIS unit %o chan %o\n", cdp->iomUnitIdx, cdp->chan);
#if 0
  //iomChanData[cdp->iomUnitIdx][cdp->chan].in_use = false;
  send_general_interrupt (cdp->iomUnitIdx, cdp->chan, imwTerminatePic);
#else
//simPrintf ("dia sends terminate\n");
  //iomChanData[cdp->iomUnitIdx][cdp->chan].stati = 04000;
  send_terminate_interrupt (& iomChanData[cdp->iomUnitIdx][cdp->chan]);
#endif
}

static void processRecv (uv_stream_t * req, dnPkt * pkt, ssize_t nread) {
  if (nread != sizeof (dnPkt)) {
    simPrintf ("%s incoming packet size %ld, expected %ld\n", __func__, nread, sizeof (dnPkt));
for (uint i = 0; i < nread; i ++) {
  unsigned char ch = ((unsigned char *) pkt) [i];
  simPrintf ("%4d %03o ", i, ch);
  if (isprint (ch))
    simPrintf ("'%c'\n", ch);
  else
    simPrintf ("'\%03o'\n", ch);
}
  }
#ifdef PKT_TRACE
simPrintf ("DEBUG: <%s\n", (char *) pkt);
#endif
  if (pkt->cmd == DN_CMD_READ) {
    processCmdR (req, pkt);
  } else if (pkt->cmd == DN_CMD_READ_CLEAR) {
    processCmdRC (req, pkt);
  } else if (pkt->cmd == DN_CMD_WRITE) {
    processCmdW (req, pkt);
  } else if (pkt->cmd == DN_CMD_DISCONNECT) {
    processCmdDis (req, pkt);
  } else if (pkt->cmd == DN_CMD_SET_EXEC_CELL) {
    processCmdSEC (req, pkt);
  } else if (pkt->cmd == DN_CMD_INTERRUPT_CS) {
    processCmdIntrCS (req, pkt);
  } else {
    simPrintf ("Ignoring cmd %c %o\n", isprint (pkt->cmd) ? pkt->cmd : '.', pkt->cmd);
  }
}

static void onRead (uv_stream_t* req, ssize_t nread, const uv_buf_t* buf) {
  if (nread < 0) {
    fprintf (stderr, "Read error!\n");
    if (! uv_is_closing ((uv_handle_t *) req))
      uv_close ((uv_handle_t *) req, NULL);
    free (buf->base);
    return;
  }
  if (nread > 0) {
    if (! req) {
      simPrintf ("bad req\n");
      return;
    } else {
      //simPrintf ("recv %ld %c\n", nread, buf->base[0]);
      processRecv (req, (dnPkt *) buf->base, nread);
    }
  } else {
    //simPrintf ("recv empty\n");
  }
  if (buf->base)
    free (buf->base);
  return;
}

static void onConnect (uv_connect_t * server, int status) {
simPrintf (INV"connected"RES"\n");
  if (status < 0) {
    simPrintf (INV"connected %d"RES"\n", status);
    return;
  }
  diaClientData * cdp = (diaClientData *) server->data;
  if (cdp->magic != DIA_CLIENT_MAGIC)
    simPrintf ("ERROR: %s no magic\n", __func__);
  diaUnitData_t * dudp = & diaData.diaUnitData[cdp->diaUnitIdx];
  //uv_tcp_nodelay ((uv_tcp_t *) & dudp->tcpHandle, 1);
  //uv_stream_set_blocking ((uv_stream_t *) & dudp->tcpHandle, 1);
  uv_read_start ((uv_stream_t *) & dudp->tcpHandle, allocBuffer, onRead);
}

static int dia_connect (chnlState_t * chnlp) {
simPrintf (INV"trying to connect"RES"\n");
  uint diaUnitIdx  = get_ctlr_idx (chnlp);
simPrintf (INV"dia_connect diaUnitIdx %u"RES"\n", diaUnitIdx);
  diaUnitData_t * dudp = & diaData.diaUnitData[diaUnitIdx];
  UNIT * uptr = diaUnit + diaUnitIdx;

// Set up send

  int rc = uv_tcp_init (& dudp->loop, & dudp->tcpHandle);
  if (rc) {
    simPrintf ("%s unable to init tcpHandle %d\n", __func__, rc);
    return -1;
  }

  struct sockaddr_in raddr;
  uv_ip4_addr (dudp->rhost, dudp->rport, & raddr);

  rc = uv_tcp_init (& dudp->loop, & dudp->tcpHandle);
  if (rc) {
    simPrintf ("%s unable to connect tcpHandle %d\n", __func__, rc);
    return -1;
  }

  struct ctlr_to_iom_s * there = & cables->dia_to_iom[diaUnitIdx][ASSUME0];
//simPrintf ("DIAA at %u %u\n", there->iom_unit_idx, there->chan_num);
  dudp->clientData.magic = DIA_CLIENT_MAGIC;
  dudp->clientData.iomUnitIdx = there->iom_unit_idx;
  dudp->clientData.chan = there->chan_num;
  dudp->clientData.diaUnitIdx = diaUnitIdx;
//simPrintf ("dia_connect saving iomUnitIdx %u chan %u diaUnitIdx %u\n", dudp->clientData.iomUnitIdx, dudp->clientData.chan, dudp->clientData.diaUnitIdx);
  dudp->reqConnect.data = & dudp->clientData;
  dudp->tcpHandle.data = & dudp->clientData;

  rc = uv_tcp_connect (& dudp->reqConnect, & dudp->tcpHandle, (struct sockaddr *) & raddr, onConnect);
  if (rc) {
    simPrintf ("%s unable to connect udpSendHandle %d\n", __func__, rc);
    return -1;
  }
  uptr->flags |= UNIT_ATT;
simPrintf (INV"trying to connect; succeeded"RES"\n");
  return 0;
}

static simRc dia_attach (UNIT * uptr, const char * cptr) {
  if (! cptr)
    return SCPE_ARG;
  int diaUnitIdx = (int) (uptr - diaUnit);
  diaUnitData_t * dudp = & diaData.diaUnitData[diaUnitIdx];


  // ATTACH DNn w.x.y.z:rrrr - connect via TCp to a remote simh host

  // If we're already attached, then detach ...
  if ((uptr->flags & UNIT_ATT) != 0)
    detach_unit (uptr);

  // Make a copy of the "file name" argument.  dn_udp_create() actually modifies
  // the string buffer we give it, so we make a copy now so we'll have
  // something to display in the "SHOW DNn ..." command.
  strncpy (dudp->attachAddress, cptr, ATTACH_ADDRESS_SZ);
  uptr->filename = dudp->attachAddress;

  int rc = tcpParseRemote (cptr, dudp->rhost, sizeof (dudp->rhost), & dudp->rport);
  if (rc) {
    simWarn ("%s unable to parse address %d\n", __func__, rc);
    return SCPE_ARG;
  }
  // simPrintf ("%d %d:%s:%d\n", rc, lport, dudp->rhost, dudp->rport);

  //rc = dia_connect (uptr);
  //if (rc == 0) {
    //uptr->flags |= UNIT_ATT;
    //dudp->connected = true;
  //}
  return SCPE_OK;
}

static void close_cb (uv_handle_t * stream) {
//simPrintf ("close cb\n");
  diaClientData * cdp = (diaClientData *) stream->data;
  if (cdp->magic != DIA_CLIENT_MAGIC)
    simPrintf ("ERROR: %s no magic\n", __func__);
  diaUnit[cdp->diaUnitIdx].flags &= ~ (unsigned int) UNIT_ATT;
  free (stream);
}

// Detach (connect) ...
static simRc dia_detach (UNIT * uptr) {
  int diaUnitIdx = (int) (uptr - diaUnit);
  diaUnitData_t * dudp = & diaData.diaUnitData[diaUnitIdx];
  if ((uptr->flags & UNIT_ATT) == 0)
    return SCPE_OK;
  //if (! dudp->connected)
    //return SCPE_OK;

  if (! uv_is_closing ((uv_handle_t *) & dudp->tcpHandle))
    uv_close ((uv_handle_t *) & dudp->tcpHandle, close_cb);

  //dudp->connected = false;
  uptr->flags &= ~ (unsigned int) UNIT_ATT;
  //free (uptr->filename);
  //uptr->filename = NULL;
  return SCPE_OK;
}

DEVICE diaDevice = {
  "DIA",           /* name */
  diaUnit,        /* units */
  NULL,            /* registers */
  diaMod,         /* modifiers */
  N_DIAS,          /* #units */
  10,              /* address radix */
  31,              /* address width */
  1,               /* address increment */
  8,               /* data radix */
  9,               /* data width */
  NULL,            /* examine routine */
  NULL,            /* deposit routine */
  diaReset,           /* reset routine */
  NULL,            /* boot routine */
  dia_attach,          /* attach routine */
  dia_detach,          /* detach routine */
  NULL,            /* context */
  0,               /* flags */
  0,               /* debug control flags */
  NULL,            /* debug flag names */
  NULL,            /* memory size change */
  NULL,            /* logical name */
  NULL,            // attach help
  NULL,            // help
  NULL,            // help context
  NULL,            // device description
  NULL
};

diaData_t diaData;


//
// Once-only initialization
//

void diaInit (void) {
  memset(& diaData, 0, sizeof (diaData));
}

//
// Execute a DIA IDCW
//

static iomCmdRc dia_cmd (chnlState_t * chnlp) {
  uint diaUnitIdx = get_ctlr_idx (chnlp);
simPrintf (INV"dia_cmd diaUnitIdx %u"RES"\n", diaUnitIdx);
  UNIT * uptr = diaUnit + diaUnitIdx;
  if ((uptr->flags & UNIT_ATT) == 0) {
    int rc = dia_connect (chnlp);
    if (rc) {
      simPrintf (INV"Can't connect to FNP"RES"\n");
      return IOM_CMD_ERROR;
    }
  }
  chnlp -> stati = 0;
  //simPrintf ("fnp cmd %d %012lo\n", chnlp -> IDCW_DEV_CMD, M[06100]);
   simPrintf (INV"fnp cmd %d"RES"\n", chnlp->IDCW_DEV_CMD);
  switch (chnlp -> IDCW_DEV_CMD)
    {
      case 000: // CMD 00 Request status
        {
          chnlp -> stati = 04000;
          DDBG (simPrintf ("Request status\n"));
          // Send a connect message to the coupler
          dnPkt pkt;
          memset (& pkt, 0, sizeof (pkt));
          pkt.cmd = DN_CMD_CONNECT;
          dnSend (diaUnitIdx, & pkt);
        }
        break;

      default:
        {
          chnlp -> stati = 04501;
          chnlp -> chanStatus = chanStatIncorrectDCW;
          if (chnlp->IDCW_DEV_CMD != 051) // ignore bootload console probe
            simWarn ("dia daze %o\n", chnlp->IDCW_DEV_CMD);
        }
        return IOM_CMD_ERROR;
    }

  return IOM_CMD_PENDING; // did command, don't want more
}

//
// Execute DIA DCW
//

iomCmdRc diaIomCmd (chnlState_t * chnlp) {
  simPrintf (INV"diaIomCmd"RES"\n");
  DDBG (simPrintf ("diaIomCmd %u %u\n", iomUnitIdx, chan));
// Is it an IDCW?

  if (IS_IDCW (chnlp))
    return dia_cmd (chnlp);

  // else // DDCW/TDCW
  simPrintf ("%s expected IDCW %012"PRIo64"\n", __func__, chnlp->DCW);
  return IOM_CMD_ERROR;
}

//
// DIA controller thread
//

void * diaMain (void * arg) {
  chnlState_t * chnlp = * (chnlState_t **) arg;
  //uint iomUnitIdx  = chnlp->myIomIdx;
  //uint chan = chnlp->myChanIdx;
  uint diaUnitIdx = get_ctlr_idx (chnlp);
  diaUnitData_t * dudp = diaData.diaUnitData + diaUnitIdx;

  while (1) {
    simPrintf (INV"Calling uv_run"RES"\n");
    uv_run (& dudp->loop, UV_RUN_DEFAULT);
  }
  return NULL;
}


void diaAsyncCB (uv_async_t * handle) {
  simPrintf (INV"diaAsyncCB %p"RES"\n", handle->data);
  diaIomCmd ((chnlState_t *) handle->data);
}


//
// Start DIA controller thread
//

void diaThreadLaunch (diaUnitData_t * diaUnitp) {
  chnlState_t * chnlp = diaUnitp -> chnlp;
  uint iomNum = chnlp->myIomIdx;
  uint chnNum = chnlp->myChanIdx;
  uint diaUnitIdx = get_ctlr_idx (chnlp);
  diaUnitData_t * dudp = diaData.diaUnitData + diaUnitIdx;

  simPrintf (INV"diaThreadLaunch"RES"\n");

  uv_loop_init (& dudp->loop);
  // Used to signal CIOC connect to the channel
  uv_async_init (& dudp->loop, & dudp->async, diaAsyncCB);
  dudp->async.data = (void *) chnlp;
  simPrintf (INV"async data %p"RES"\n", chnlp);

  enum ctlr_type_e ctlr_type = cables->iom_to_ctlr[iomNum][chnNum].ctlr_type;
  createChnThread (diaUnitp->chnlp, ctlr_type_strs [ctlr_type], diaMain);
  //chnRdyWait (diaUnitp->chnlp);
}

// Issue a connect to a DIA.

void diaIomConnect (chnlState_t * chnlp) {
  int rc;

  simPrintf (INV"diaIomConnect"RES"\n");

  // Find DIA unit index
  uint ctlrUnitIdx  = get_ctlr_idx (chnlp);
  diaUnitData_t * dudp = & diaData.diaUnitData[ctlrUnitIdx];
  
  // Launch channel thread if needed
  if (! dudp->launched) {
    dudp->chnlp = chnlp;
    diaThreadLaunch (dudp);
    dudp->launched = true;
  }
  // Send a connect signal to the thread
simPrintf (INV"uv_async_send"RES"\n");
  rc = uv_async_send (& dudp->async);
  if (rc) {
    simPrintf ("uv_async_send returned %d\n", rc);
    return;
  }
}

