/*
 * vim: filetype=c:tabstop=4:ai:expandtab
 * SPDX-License-Identifier: ICU
 * scspell-id: 3b4b8be2-171d-11ee-84fd-80ee73e9b8e7
 *
 * ---------------------------------------------------------------------------
 *
 * Copyright (c) 2022-2024 Charles Anthony
 * Copyright (c) 2022-2023 Jeffrey H. Johnson
 * Copyright (c) 2022-2024 The DPS8M Development Team
 *
 * This software is made available under the terms of the ICU License.
 * See the LICENSE.md file at the top-level directory of this distribution.
 *
 * ---------------------------------------------------------------------------
 */

//
//        A:   fetch sdw
//        B:   check RB consistency
//        B1:  Is CALL6? Yes---------------------------------------+
//               No                                                |
//             Is transfer? Yes---------------+                E: Check RB
//               No                           |                    |
//             Check Read RB             F: Check Execute RB       |
//                |                      D: Check RALR             |
//                |                           |                    |
//                +<--------------------------+<-------------------+
//                |
//                V
//        G:   Check bound
//             Paged? No----------------------+
//               Yes                          |
//             Fetch PTW                      |
//        I:   Compute final address     H:   Compute Final Address
//                |                           |
//                +<--------------------------+
//                |
//                V
//        HI:  Read operand
//             CALL6? Yes--------------------------------------+
//               No                                            |
//             Transfer? Yes -----------------+                |
//               No                           |                |
//                |                           |                |
//                |                      L:  Handle TSPn   N: Handle CALL6
//                |                      KL: Set IC           Set IC
//                |                           |                |
//                |                           +<---------------+
//                |                           |
//                |                      M: Set P
//                |                           |
//                +<--------------------------+
//                |
//                V
//              Exit

word24 doAppendCycleOperandRead (cpuState_t * cpup, word36 * data, uint nWords) {
static int evcnt = 0;
  DCDstruct * i = & cpu.currentInstruction;
  (void)evcnt;

  uint this = UC_OPERAND_READ;
  if (i->info->flags & TRANSFER_INS)
    this = UC_OPERAND_READ_TRA;
  if (i->info->flags & CALL6_INS)
    this = UC_OPERAND_READ_CALL6;

  word24 finalAddress = 0;
  word24 pageAddress = 0;
  word3 RSDWH_R1 = 0;
  word14 bound = 0;
  word1 p = 0;
  bool paged;

// Is this cycle a candidate for ucache?

//#define TEST_UCACHE
#if defined(TEST_UCACHE)
  bool cacheHit;
  cacheHit = false; // Assume skip...
#endif /* if defined(TEST_UCACHE) */

#if 1
  // Is OPCODE call6?

  // See E1; The TRR needs to be checked and set to R2; this will vary across different
  // CALL6 calls.
  if (i->info->flags & CALL6_INS) {
# if defined(UCACHE_STATS)
    cpu.uCache.call6Skips ++;
# endif /* if defined(UCACHE_STATS) */
    goto skip;
  }
#endif

#if 0
  // Transfer or instruction fetch?
  if (i->info->flags & TRANSFER_INS) {
    //cpu.uCache.uc_xfer_skip ++;
    goto skip;
  }
#endif

  // Transfer?
  if (i->info->flags & TRANSFER_INS) {
    // check ring alarm to catch outbound transfers
    if (cpu.rRALR && (cpu.PPR.PRR >= cpu.rRALR)) {
#if defined(UCACHE_STATS)
      cpu.uCache.ralrSkips ++;
#endif /* if defined(UCACHE_STATS) */
      goto skip;
    }
  }

// Yes; check the ucache

#if defined(TEST_UCACHE)
  word24 cachedAddress;
  word3 cachedR1;
  word14 cachedBound;
  word1 cachedP;
  bool cachedPaged;
  cacheHit =
      ucCacheCheck (cpup, this, cpu.TPR.TSR, cpu.TPR.CA, & cachedBound, & cachedP, & cachedAddress, & cachedR1, & cachedPaged);
  HDBGNote (cpup, "doAppendCycleOperandRead.h", "test cache check %s %d %u %05o:%06o %05o %o %08o %o %o",
            cacheHit ? "hit" : "miss", evcnt, this, cpu.TPR.TSR, cpu.TPR.CA, cachedBound,
            cachedP, cachedAddress, cachedR1, cachedPaged);
  goto miss;
#else
  if (! ucCacheCheck (cpup, this, cpu.TPR.TSR, cpu.TPR.CA, & bound, & p, & pageAddress, & RSDWH_R1, & paged)) {
    HDBGNote (cpup, "doAppendCycleOperandRead.h", "miss %d %05o:%06o", evcnt, cpu.TPR.TSR, cpu.TPR.CA);
    goto miss;
  }
#endif /* if defined(TEST_UCACHE) */

  if (paged) {
    finalAddress = pageAddress + (cpu.TPR.CA & OS18MASK);
  } else {
    finalAddress = pageAddress + cpu.TPR.CA;
  }
  cpu.RSDWH_R1 = RSDWH_R1;

// ucache hit; housekeeping...
  //simPrintf ("hit  %d %05o:%06o\n", evcnt, cpu.TPR.TSR, cpu.TPR.CA);
  HDBGNote (cpup, "doAppendCycleOperandRead.h", "hit  %d %05o:%06o\n", evcnt, cpu.TPR.TSR, cpu.TPR.CA);

  cpu.apu.lastCycle = OPERAND_READ;
  goto HI;

#if 1
skip:;
  //simPrintf ("miss %d %05o:%06o\n", evcnt, cpu.TPR.TSR, cpu.TPR.CA);
  HDBGNote (cpup, "doAppendCycleOperandRead.h", "skip %d %05o:%06o\n", evcnt, cpu.TPR.TSR, cpu.TPR.CA);
# if defined(UCACHE_STATS)
  cpu.uCache.skips[this] ++;
# endif /* if defined(UCACHE_STATS) */
#endif /* if defined(TEST_UCACHE) */

miss:;

  bool nomatch = true;
  if (cpu.tweaks.enable_wam) {
    // AL39: The associative memory is ignored (forced to "no match") during
    // address preparation.
    // lptp,lptr,lsdp,lsdr,sptp,sptr,ssdp,ssdr
    // Unfortunately, ISOLTS doesn't try to execute any of these in append mode.
    // XXX should this be only for OPERAND_READ and OPERAND_STORE?
#if 1
    nomatch = i->opcode10 == 01232 ||
              i->opcode10 == 01254 ||
              i->opcode10 == 01154 ||
              i->opcode10 == 01173 ||
              i->opcode10 == 00557 ||
              i->opcode10 == 00257;
#else
    nomatch = ((i->opcode == 0232 || i->opcode == 0254 ||
                i->opcode == 0154 || i->opcode == 0173) &&
                i->opcodeX ) ||
               ((i->opcode == 0557 || i->opcode == 0257) &&
                ! i->opcodeX);
#endif
  }

  processor_cycle_type lastCycle = cpu.apu.lastCycle;
  cpu.apu.lastCycle = OPERAND_READ;

  PNL (L68_ (cpu.apu.state = 0;))

  cpu.RSDWH_R1 = 0;

  cpu.acvFaults = 0;

//#define FMSG(x) x
#define FMSG(x)
  FMSG (char * acvFaultsMsg = "<unknown>";)

////////////////////////////////////////
//
// Sheet 1: "START APPEND"
//
////////////////////////////////////////

// START APPEND
  word3 n = 0; // PRn to be saved to TSN_PRNO

////////////////////////////////////////
//
// Sheet 2: "A"
//
////////////////////////////////////////

//
//  A:
//    Get SDW

  //PNL (cpu.APUMemAddr = address;)
  PNL (cpu.APUMemAddr = cpu.TPR.CA;)

  // is SDW for C(TPR.TSR) in SDWAM?
  if (nomatch || ! fetch_sdw_from_sdwam (cpup, cpu.TPR.TSR)) {
    // No

    if (cpu.DSBR.U == 0) {
      fetch_dsptw (cpup, cpu.TPR.TSR);

      if (! cpu.PTW0.DF)
        doFault (cpup, FAULT_DF0 + cpu.PTW0.FC, fst_zero, "doAppendCycleOperandRead(A): PTW0.F == 0");

      if (! cpu.PTW0.U)
        modify_dsptw (cpup, cpu.TPR.TSR);

      fetch_psdw (cpup, cpu.TPR.TSR);
    } else
      fetch_nsdw (cpup, cpu.TPR.TSR); // load SDW0 from descriptor segment table.

    if (cpu.SDW0.DF == 0) {
      // initiate a directed fault ...
      doFault (cpup, FAULT_DF0 + cpu.SDW0.FC, fst_zero, "SDW0.F == 0");
    }
    // load SDWAM .....
    load_sdwam (cpup, cpu.TPR.TSR, nomatch);
  }

  // Yes...
  RSDWH_R1 = cpu.RSDWH_R1 = cpu.SDW->R1;

////////////////////////////////////////
//
// Sheet 3: "B"
//
////////////////////////////////////////

//
// B: Check the ring
//

  // check ring bracket consistency

  //C(SDW.R1) <= C(SDW.R2) <= C(SDW .R3)?
  if (! (cpu.SDW->R1 <= cpu.SDW->R2 && cpu.SDW->R2 <= cpu.SDW->R3)) {
    // Set fault ACV0 = IRO
    cpu.acvFaults |= ACV0;
    PNL (L68_ (cpu.apu.state |= apu_FLT;))
    FMSG (acvFaultsMsg = "acvFaults(B) C(SDW.R1) <= C(SDW.R2) <= " "C(SDW .R3)";)
  }

  // lastCycle == RTCD_OPERAND_FETCH
  // if a fault happens between the RTCD_OPERAND_FETCH and the INSTRUCTION_FETCH
  // of the next instruction - this happens about 35 time for just booting  and
  // shutting down multics -- a stored lastCycle is useless.
  // the opcode is preserved across faults and only replaced as the
  // INSTRUCTION_FETCH succeeds.
  if (lastCycle == RTCD_OPERAND_FETCH)
    simWarn ("%s: lastCycle == RTCD_OPERAND_FETCH opcode %0#o\n", __func__, i->opcode);

  //
  // B1: The operand is one of: an instruction, data to be read or data to be
  //     written
  //

  // Is OPCODE call6?
  if (i->info->flags & CALL6_INS)
    goto E;

  // Transfer
  if (i->info->flags & TRANSFER_INS)
    goto F;

  //
  // check read bracket for read access
  //

  // No
  // C(TPR.TRR) > C(SDW .R2)?
  if (cpu.TPR.TRR > cpu.SDW->R2) {
    //Set fault ACV3 = ORB
    cpu.acvFaults |= ACV3;
    PNL (L68_ (cpu.apu.state |= apu_FLT;))
    FMSG (acvFaultsMsg = "acvFaults(B) C(TPR.TRR) > C(SDW .R2)";)
  }

  if (cpu.SDW->R == 0) {
    // isolts 870
    cpu.TPR.TRR = cpu.PPR.PRR;

    //C(PPR.PSR) = C(TPR.TSR)?
    if (cpu.PPR.PSR != cpu.TPR.TSR) {
      //Set fault ACV4 = R-OFF
      cpu.acvFaults |= ACV4;
      PNL (L68_ (cpu.apu.state |= apu_FLT;))
      FMSG (acvFaultsMsg = "acvFaults(B) C(PPR.PSR) = C(TPR.TSR)";)
    //} else {
      // simWarn ("doAppendCycleOperandRead(B) SDW->R == 0 && cpu.PPR.PSR == cpu.TPR.TSR: %0#o\n", cpu.PPR.PSR);
    }
  }

  goto G;

////////////////////////////////////////
//
// Sheet 4: "C" "D"
//
////////////////////////////////////////

D:;

  // transfer or instruction fetch

  // check ring alarm to catch outbound transfers

  if (cpu.rRALR == 0)
    goto G;

  // C(PPR.PRR) < RALR?
  if (! (cpu.PPR.PRR < cpu.rRALR)) {
    cpu.acvFaults |= ACV13;
    PNL (L68_ (cpu.apu.state |= apu_FLT;))
    FMSG (acvFaultsMsg = "acvFaults(D) C(PPR.PRR) < RALR";)
  }

  goto G;

////////////////////////////////////////
//
// Sheet 5: "E"
//
////////////////////////////////////////

E:;

  //
  // check ring bracket for instruction fetch after call6 instruction
  //   (this is the call6 read operand)
  //

  //SDW.E set ON?
  if (! cpu.SDW->E) {
    // Set fault ACV2 = E-OFF
    cpu.acvFaults |= ACV2;
    PNL (L68_ (cpu.apu.state |= apu_FLT;))
    FMSG (acvFaultsMsg = "acvFaults(E) SDW .E set OFF";)
  }

  //SDW .G set ON?
  if (cpu.SDW->G)
    goto E1;

  // C(PPR.PSR) = C(TPR.TSR)?
  if (cpu.PPR.PSR == cpu.TPR.TSR && ! TST_I_ABS)
    goto E1;

  // XXX This doesn't seem right
  // EB is word 15; masking address makes no sense; rather 0-extend EB
  // Fixes ISOLTS 880-01
  if (cpu.TPR.CA >= (word18) cpu.SDW->EB) {
    // Set fault ACV7 = NO GA
    cpu.acvFaults |= ACV7;
    PNL (L68_ (cpu.apu.state |= apu_FLT;))
    FMSG (acvFaultsMsg = "acvFaults(E) TPR.CA4-17 >= SDW.CL";)
  }

E1:

  // C(TPR.TRR) > SDW.R3?
  if (cpu.TPR.TRR > cpu.SDW->R3) {
    //Set fault ACV8 = OCB
    cpu.acvFaults |= ACV8;
    PNL (L68_ (cpu.apu.state |= apu_FLT;))
    FMSG (acvFaultsMsg = "acvFaults(E1) C(TPR.TRR) > SDW.R3";)
  }

  // C(TPR.TRR) < SDW.R1?
  if (cpu.TPR.TRR < cpu.SDW->R1) {
    // Set fault ACV9 = OCALL
    cpu.acvFaults |= ACV9;
    PNL (L68_ (cpu.apu.state |= apu_FLT;))
    FMSG (acvFaultsMsg = "acvFaults(E1) C(TPR.TRR) < SDW.R1";)
  }

  // C(TPR.TRR) > C(PPR.PRR)?
  if (cpu.TPR.TRR > cpu.PPR.PRR) {
    // C(PPR.PRR) < SDW.R2?
    if (cpu.PPR.PRR < cpu.SDW->R2) {
      // Set fault ACV10 = BOC
      cpu.acvFaults |= ACV10;
      PNL (L68_ (cpu.apu.state |= apu_FLT;))
      FMSG (acvFaultsMsg = "acvFaults(E1) C(TPR.TRR) > C(PPR.PRR) && " "C(PPR.PRR) < SDW.R2";)
    }
  }

  // C(TPR.TRR) > SDW.R2?
  if (cpu.TPR.TRR > cpu.SDW->R2) {
    // SDW.R2 -> C(TPR.TRR)
    cpu.TPR.TRR = cpu.SDW->R2;
  }

  goto G;

////////////////////////////////////////
//
// Sheet 6: "F"
//
////////////////////////////////////////

F:;
  PNL (L68_ (cpu.apu.state |= apu_PIAU;))

  //
  // check ring bracket for instruction fetch
  //

  // C(TPR.TRR) < C(SDW .R1)?
  // C(TPR.TRR) > C(SDW .R2)?
  if (cpu.TPR.TRR < cpu.SDW->R1 || cpu.TPR.TRR > cpu.SDW->R2) {
    cpu.acvFaults |= ACV1;
    PNL (L68_ (cpu.apu.state |= apu_FLT;))
    FMSG (acvFaultsMsg = "acvFaults(F) C(TPR.TRR) < C(SDW .R1)";)
  }
  // SDW .E set ON?
  if (! cpu.SDW->E) {
    cpu.acvFaults |= ACV2;
    PNL (L68_ (cpu.apu.state |= apu_FLT;))
    FMSG (acvFaultsMsg = "acvFaults(F) SDW .E set OFF";)
  }

  // C(PPR.PRR) = C(TPR.TRR)?
  if (cpu.PPR.PRR != cpu.TPR.TRR) {
    //Set fault ACV12 = CRT
    cpu.acvFaults |= ACV12;
    PNL (L68_ (cpu.apu.state |= apu_FLT;))
    FMSG (acvFaultsMsg = "acvFaults(F) C(PPR.PRR) != C(TPR.TRR)";)
  }

  goto D;

////////////////////////////////////////
//
// Sheet 7: "G"
//
////////////////////////////////////////

G:;

  //C(TPR.CA)0,13 > SDW.BOUND?
  if (((cpu.TPR.CA >> 4) & 037777) > cpu.SDW->BOUND) {
    cpu.acvFaults |= ACV15;
    PNL (L68_ (cpu.apu.state |= apu_FLT;))
    FMSG (acvFaultsMsg = "acvFaults(G) C(TPR.CA)0,13 > SDW.BOUND";)
  }
  bound = cpu.SDW->BOUND;
  p = cpu.SDW->P;

  if (cpu.acvFaults) {
    PNL (L68_ (cpu.apu.state |= apu_FLT;))
    // Initiate an access violation fault
    doFault (cpup, FAULT_ACV, (_fault_subtype) {.fault_acv_subtype=cpu.acvFaults},
            "ACV fault");
  }

  // is segment C(TPR.TSR) paged?
  if (cpu.SDW->U)
    goto H; // Not paged

  // Yes. segment is paged ...
  // is PTW for C(TPR.CA) in PTWAM?

  if (nomatch ||
      ! fetch_ptw_from_ptwam (cpup, cpu.SDW->POINTER, cpu.TPR.CA)) {
    fetch_ptw (cpup, cpu.SDW, cpu.TPR.CA);
    if (! cpu.PTW0.DF) {
      // initiate a directed fault
      doFault (cpup, FAULT_DF0 + cpu.PTW0.FC, (_fault_subtype) {.bits=0},
              "PTW0.F == 0");
    }
    loadPTWAM (cpup, cpu.SDW->POINTER, cpu.TPR.CA, nomatch); // load PTW0 to PTWAM
  }

  // Prepage mode?
  // check for "uninterruptible" EIS instruction
  // ISOLTS-878 02: mvn,cmpn,mvne,ad3d; obviously also
  // ad2/3d,sb2/3d,mp2/3d,dv2/3d
  // DH03 p.8-13: probably also mve,btd,dtb
  // if (i->opcodeX && ((i->opcode & 0770)== 0200|| (i->opcode & 0770) == 0220
  //     || (i->opcode & 0770)== 020|| (i->opcode & 0770) == 0300)) {
  if ((i->opcode10 & 01750) == 01200 || // ad2d sb2d mp2d dv2d ad3d sb3d mp3d dv3d
      (i->opcode10 & 01770) == 01020 || // mve mvne
      (i->opcode10 & 01770) == 01300) { // mvn btd cmpn dtb
    do_ptw2 (cpup, cpu.SDW, cpu.TPR.CA);
  }
  goto I;

////////////////////////////////////////
//
// Sheet 8: "H", "I"
//
////////////////////////////////////////

H:;

  paged = false;

  PNL (L68_ (cpu.apu.state |= apu_FANP;))
#if 0
  // ISOLTS pa865 test-01a 101232
  if (get_bar_mode ()) {
    set_apu_status (apuStatus_FABS);
  } else
    ....
#endif
  set_apu_status (cpup, apuStatus_FANP);
  HDBGNote (cpup, "doAppendCycleOperandRead", "%s", "FANP");

  pageAddress = (cpu.SDW->ADDR & 077777760);
  finalAddress = (cpu.SDW->ADDR & 077777760) + cpu.TPR.CA;
  finalAddress &= 0xffffff;
  PNL (cpu.APUMemAddr = finalAddress;)

  goto HI;

I:;

// Set PTW.M

  paged = true;

  HDBGNote (cpup, "doAppendCycleOperandRead", "%s", "FAP");
  // final address paged
  set_apu_status (cpup, apuStatus_FAP);
  PNL (L68_ (cpu.apu.state |= apu_FAP;))

  word24 y2 = cpu.TPR.CA % 1024;

  pageAddress = (((word24)cpu.PTW->ADDR & 0777760) << 6);
  // AL39: The hardware ignores low order bits of the main memory page
  // address according to page size
  finalAddress = (((word24)cpu.PTW->ADDR & 0777760) << 6) + y2;
  finalAddress &= 0xffffff;
  PNL (cpu.APUMemAddr = finalAddress;)

#if defined(L68)
  if (cpu.MR_cache.emr && cpu.MR_cache.ihr)
    add_APU_history (APUH_FAP);
#endif /* if defined(L68) */
  //goto HI;

HI:

#if defined(TEST_UCACHE)
  if (cacheHit) {
    bool err = false;
    if (cachedAddress != pageAddress) {
     simPrintf ("cachedAddress %08o != pageAddress %08o\n",
             cachedAddress, pageAddress);
     err = true;
    }
    if (cachedR1 != RSDWH_R1) {
      simPrintf ("cachedR1 %01o != RSDWH_R1 %01o\n",
              cachedR1, RSDWH_R1);
      err = true;
    }
    if (cachedBound != bound) {
      simPrintf ("cachedBound %01o != bound %01o\n",
              cachedBound, bound);
      err = true;
    }
    if (cachedPaged != paged) {
      simPrintf ("cachedPaged %01o != paged %01o\n",
              cachedPaged, paged);
      err = true;
    }
    if (err) {
# if defined(HDBG)
      HDBGPrint ();
# endif /* if defined(HDBG) */
      simPrintf ("oprnd read err  %d %05o:%06o\n",
              evcnt, cpu.TPR.TSR, cpu.TPR.CA);
      exit (1);
    }
    //simPrintf ("hit  %d %05o:%06o\n", evcnt, cpu.TPR.TSR, cpu.TPR.CA);
    HDBGNote (cpup, "doAppendCycleOperandRead.h", "test hit %d %05o:%06o\n",
            evcnt, cpu.TPR.TSR, cpu.TPR.CA);
  } else {
    //simPrintf ("miss %d %05o:%06o\n", evcnt, cpu.TPR.TSR, cpu.TPR.CA);
    HDBGNote (cpup, "doAppendCycleOperandRead.h", "test miss %d %05o:%06o\n",
            evcnt, cpu.TPR.TSR, cpu.TPR.CA);
  }
#endif

  ucCacheSave (cpup, this, cpu.TPR.TSR, cpu.TPR.CA, bound, p, pageAddress, RSDWH_R1, paged);
#if defined(TEST_UCACHE)
  HDBGNote (cpup, "doAppendCycleOperandRead.h", "cache %d %u %05o:%06o %05o %o %08o %o %o",
          evcnt, this, cpu.TPR.TSR, cpu.TPR.CA, bound, p, pageAddress, RSDWH_R1, paged);
#endif /* if defined(TEST_UCACHE) */
evcnt ++;

  // isolts 870
  cpu.cu.XSF = 1;

  coreReadN (cpup, finalAddress, data, nWords, "OPERAND_READ");

  if (i->info->flags & CALL6_INS)
    goto N;

  // Transfer or instruction fetch?
  if (i->info->flags & TRANSFER_INS)
    goto L;

  // APU data movement?
  //  handled above
  goto Exit;

////////////////////////////////////////
//
// Sheet 10: "K", "L", "M", "N"
//
////////////////////////////////////////

L:; // Transfer or instruction fetch

  // Is OPCODE tspn?
  if (i->info->flags & TSPN_INS) {
    if (i->opcode <= 0273)
      n = (i->opcode & 3);
    else
      n = (i->opcode & 3) + 4;

    // C(PPR.PRR) -> C(PRn .RNR)
    // C(PPR.PSR) -> C(PRn .SNR)
    // C(PPR.IC) -> C(PRn .WORDNO)
    // 000000 -> C(PRn .BITNO)
    cpu.PR[n].RNR = cpu.PPR.PRR;
// According the AL39, the PSR is 'undefined' in absolute mode.
// ISOLTS thinks means don't change the operand
    if (get_addr_mode (cpup) == APPEND_mode)
      cpu.PR[n].SNR = cpu.PPR.PSR;
    cpu.PR[n].WORDNO = (cpu.PPR.IC + 1) & MASK18;
    SET_PR_BITNO (n, 0);
    HDBGRegPRW (cpup, n, "app tspn");
  }

// KL:

  // C(TPR.TSR) -> C(PPR.PSR)
  cpu.PPR.PSR = cpu.TPR.TSR;
  // C(TPR.CA) -> C(PPR.IC)
  cpu.PPR.IC = cpu.TPR.CA;

  //goto M;

M: // Set P

  // C(TPR.TRR) = 0?
  if (cpu.TPR.TRR == 0) {
    // C(SDW.P) -> C(PPR.P)
    cpu.PPR.P = p;
  } else {
    // 0 C(PPR.P)
    cpu.PPR.P = 0;
  }

  goto Exit;

N: // CALL6

  // C(TPR.TRR) = C(PPR.PRR)?
  if (cpu.TPR.TRR == cpu.PPR.PRR) {
    // C(PR6.SNR) -> C(PR7.SNR)
    cpu.PR[7].SNR = cpu.PR[6].SNR;
  } else {
    // C(DSBR.STACK) || C(TPR.TRR) -> C(PR7.SNR)
    cpu.PR[7].SNR = ((word15) (cpu.DSBR.STACK << 3)) | cpu.TPR.TRR;
  }

  // C(TPR.TRR) -> C(PR7.RNR)
  cpu.PR[7].RNR = cpu.TPR.TRR;
  // 00...0 -> C(PR7.WORDNO)
  cpu.PR[7].WORDNO = 0;
  // 000000 -> C(PR7.BITNO)
  SET_PR_BITNO (7, 0);
  HDBGRegPRW (cpup, 7, "app call6");
  // C(TPR.TRR) -> C(PPR.PRR)
  cpu.PPR.PRR = cpu.TPR.TRR;
  // C(TPR.TSR) -> C(PPR.PSR)
  cpu.PPR.PSR = cpu.TPR.TSR;
  // C(TPR.CA) -> C(PPR.IC)
  cpu.PPR.IC = cpu.TPR.CA;

  goto M;

Exit:;

  PNL (cpu.APUDataBusOffset = cpu.TPR.CA;)
  PNL (cpu.APUDataBusAddr = finalAddress;)

  PNL (L68_ (cpu.apu.state |= apu_FA;))


  return finalAddress;    // or 0 or -1???
}
#undef TEST_UCACHE
