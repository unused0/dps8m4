/*
 * vim: filetype=c:tabstop=4:ai:expandtab
 * SPDX-License-Identifier: ICU
 * scspell-id: dab0da0d-f62e-11ec-8f5d-80ee73e9b8e7
 *
 * ---------------------------------------------------------------------------
 *
 * Copyright (c) 2007-2013 Michael Mondy
 * Copyright (c) 2012-2016 Harry Reed
 * Copyright (c) 2013-2025 Charles Anthony
 * Copyright (c) 2021-2024 The DPS8M Development Team
 *
 * This software is made available under the terms of the ICU License.
 * See the LICENSE.md file at the top-level directory of this distribution.
 *
 * ---------------------------------------------------------------------------
 */

// AM81
// SCU memory has two store units A and B.
// Each unit has two halves A, A1, B, B1.
// The LWR STORE switch sets the memory order AB or BA.
// NB: If STORE A and STORE B are not equal in size, the larger store
//     must be assigned to the lower store address range.
// The INTERLACE toggle switch enables interleaving between the two
// stores A and B. The two stores must have the same size for interleaved
// operation.

// Devices connected to a SCU
enum activeDevice { ADEV_NONE, ADEV_CPU, ADEV_IOM };

enum scuMode { MODE_MANUAL = 0, MODE_PROGRAM = 1 };

typedef struct {

  // Panel switch MANUAL/PROGRAM
  enum scuMode mode;

  // Panel switches PORT ENABLE
  //   The 6000 SCU has ENABLE/PROG CONT/DIASBLED; AM-81 says Multics
  //   uses PROG_CONT
  //   The 4MW SCU has ENABLE ON/OFF; AM-81 says ON is program contol
  // The simulator only supports OFF or ON/PROG CONT.
  volatile atomic_uint portEnable [N_SCU_PORTS];  // enable/disable

  // Mask registers A and B, each with 32 interrupt bits.
  volatile _Atomic word32 execIntrMask [N_ASSIGNMENTS];

  // Mask assignment.
  // 2 mask registers, A and B, each 32 bits wide.
  // A CPU will be attached to port N.
  // Mask assignment assigns a mask register (A or B) to a CPU
  // on port N.
  // That is, when interrupt I is set:
  //   For reg = A, B
  //
  // Mask A, B is set to Off or 0-7.
  // mask_enable [A|B] says that mask A or B is not off
  // if (mask_enable) then mask_assignment is a port number
  volatile atomic_uint maskEnable [N_ASSIGNMENTS];     // enable/disable

  // Panel switches MASK/PORT ASSIGMENT
  volatile atomic_uint maskAssignment [N_ASSIGNMENTS]; // assigned port number

  // Interrupts set a cell; Multics clears it.
  volatile atomic_uint cells [N_CELL_INTERRUPTS];

  // Panel switches LWR STORE SIZE
  uint lowerStoreSize; // In K words, power of 2; 32 - 4096

  // Panel switches CYCLIC PRIORITY
  word7 cyclic;          // 7 bits

  // Panel switches NON EXISTENT ADDRESS
  word8 nea;             // 8 bits

  // Panel switchss STORE A A1 B B1 
  word4 online;          // 4 bits

  // Panel switch INTERLACE
  word1 interlace;       // 1 bit

  // Panel switch LWR STORE A/B
  word1 lwr;             // 1 bit

  // Note that SCUs had no switches to designate SCU 'A' or 'B', etc.
  // Instead, SCU "A" is the one with base address switches set for 01400,
  // SCU "B" is the SCU with base address switches set to 02000, etc.
  // uint mem_base; // zero on boot scu
  // mode reg: mostly not stored here; returned by scu_get_mode_register()
  // int mode; // program/manual; if 1, sscr instruction can set some fields

  struct ports {
    enum activeDevice type; // type of connected device
    int devIdx; // index of connected dev (cpu_unit_udx, iom_unit_idx
    bool isExpansionPort;
    // which port on the connected device?
    // if isExpansionPort is false, then only [0] is used.
    // if true, one connection for each sub-port; -1 if not connected
    volatile _Atomic int devicePort [N_SCU_SUBPORTS];
    volatile _Atomic bool subportEnables [N_SCU_SUBPORTS];
    volatile _Atomic bool xipmask [N_SCU_SUBPORTS];
    volatile _Atomic int xipmaskval;
  } ports [N_SCU_PORTS];

  // system controller mode register
  word4 id;
  word18 modeReg;

  uint elapsedDays;
// XXX make bool
  bool steadyClock;    // If non-zero the clock is tied to the cycle counter
// XXX make bool
  bool y2k;
  int64_t userCorrection;
  uint64_t lastTime;
} scu_t;

extern scu_t scu [N_SCUS];

extern DEVICE scuDev;


int scuSetInterrupt (uint scuUnitIdx, uint inum);
void scuInit (void);
simRc scuSSCR (cpuState_t * cpup, uint scuUnitIdx, UNUSED uint cpuUnitIdx, uint cpuPortNum, word18 addr, word36 rega, word36 regq);
simRc scuSMIC (cpuState_t * cpup, uint scuUnitIdx, uint UNUSED cpuUnitIdx, uint cpuPortNum, word36 rega);
simRc scuRSCR (cpuState_t * cpup, uint scuUnitIdx, uint cpuUnitIdx, word18 addr, word36 * rega, word36 * regq);
int scuCIOC (cpuState_t * cpup, uint cpuUnitIdx, uint scuUnitIdx, uint scuPortNum, uint expanderCommand, uint subMask);
simRc scuRMCM (uint scuUnitIdx, uint cpuUnitIdx, word36 * rega, word36 * regq);
simRc scuSMCM (uint scuUnitIdx, uint cpuUnitIdx, word36 rega, word36 regq);
uint scuGetHighestInterrupt (cpuState_t * cpup, uint scuUnitIdx);
simRc scuReset (DEVICE *dptr);
simRc scuSimResetUnit (UNIT * uptr, int32_t value, const char * cptr, void * desc);
void scuResetUnit (int scuUnitIdx);

int scuCmdShow (word36 scuIdx);
int scuCmdShowState (word36 scuIdx);
int scuCmdShowConfig (word36 scuIdx);
int scuCmdSetCyclic (word36 scuIdx, word36 cyclic);
int scuCmdSetElapseddays (word36 scuIdx, word36 elapseddays);
int scuCmdSetInterlace (word36 scuIdx, bool interlace);
int scuCmdSetLower (word36 scuIdx, bool lower);
int scuCmdSetLSS (word36 scuIdx, word36 lss);
int scuCmdSetMask (word36 scuIdx, int maskRegister, int setting);
int scuCmdSetMode (word36 scuIdx, int mode);
int scuCmdSetNEA (word36 scuIdx, word36 nea);
int scuCmdSetOnline (word36 scuIdx, uint banklist, bool online);
int scuCmdSetPort (word36 scuIdx, int portNumber, bool enable);
int scuCmdSetSteadyClock (word36 scuIdx, bool enable);
int scuCmdSetY2K (word36 scuIdx, bool enable);
