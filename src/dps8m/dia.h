/*
 * vim: filetype=c:tabstop=4:ai:expandtab
 * SPDX-License-Identifier: ICU
 * scspell-id: 1da4ad0a-f62e-11ec-8410-80ee73e9b8e7
 *
 * ---------------------------------------------------------------------------
 *
 * Copyright (c) 2007-2013 Michael Mondy
 * Copyright (c) 2012-2016 Harry Reed
 * Copyright (c) 2013-2024 Charles Anthony
 * Copyright (c) 2016 Michal Tomek
 * Copyright (c) 2021-2024 The DPS8M Development Team
 *
 * This software is made available under the terms of the ICU License.
 * See the LICENSE.md file at the top-level directory of this distribution.
 *
 * ---------------------------------------------------------------------------
 */

extern UNIT diaUnit [N_DIA_UNITS_MAX];
extern DEVICE diaDevice;

#define DIA_CLIENT_MAGIC 34393
struct diaClientDataStruct {
  uint magic;
  uint iomUnitIdx;
  uint chan;
  uint diaUnitIdx;
};
typedef struct diaClientDataStruct diaClientData;

// Indexed by sim unit number
typedef struct {
  // Has the thread been launched?
  bool launched;

  chnlState_t * chnlp;

  uint mailboxAddress;
  word24 l66Addr;

  uv_loop_t loop;
  uv_async_t async;

  char rhost[64];
  int32_t rport;
  uv_tcp_t tcpHandle;
  uv_connect_t reqConnect;

  //uv_udp_t socket;
#define ATTACH_ADDRESS_SZ 4096
  char attachAddress [ATTACH_ADDRESS_SZ];
  diaClientData clientData;
} diaUnitData_t;

typedef struct {
  diaUnitData_t diaUnitData [N_DIA_UNITS_MAX];
} diaData_t;

extern diaData_t diaData;

#if 0
// dn355_mailbox.incl.pl1
//   input_sub_mbx
//       pad1:8, line_number:10, n_free_buffers:18
//       n_chars:18, op_code:9, io_cmd:9
//       n_buffers
//       { abs_addr:24, tally:12 } [24]
//       command_data

struct input_sub_mbx
  {
    word36 word1; // dn355_no; is_hsla; la_no; slot_no    // 0      word0
    word36 word2; // cmd_data_len; op_code; io_cmd        // 1      word1
    word36 n_buffers;
    word36 dcws [24];
    word36 command_data;
  };
#endif

void diaInit(void);
void diaChnlInit (chnlState_t * chnlp);
void diaIomConnect (chnlState_t * chnlp);
iomCmdRc diaIomCmd (chnlState_t * chnlp);
void diaProcessEvents (void);
