%{

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include "xrayParse.tab.h"
#include "dps8m.h"

#define IDX(c) (tolower(c) - 'a')
%}

%option nounput
%option noinput

%%


[ \t]+  { /* ignore all whitespace */ }
"//".*  { /* ignore // comments */ }
"#".*   { /* ignore # comments */ }

";"                                 {                                                  return SEMICOLON; }
","                                 {                                                  return COMMA; }
[0-7]+                              { if (evalNumber (yytext, & yylval)) return ERROR; return NUMBER_OVALUE; }
[0-9]+\.                            { if (evalNumber (yytext, & yylval)) return ERROR; return NUMBER_DVALUE; }

(?i:dump)                           {                                                  return DUMP; }
(?i:d)                              {                                                  return DUMP; }
(?i:p)                              {                                                  return PRINT; }
(?i:print)                          {                                                  return PRINT; }
(?i:quit)                           {                                                  return QUIT; }
(?i:q)                              {                                                  return QUIT; }
(?i:slt)                            {                                                  return SLT; }

. return yytext[0];

%%

void set_input_string (const char * in) {
  yy_scan_string(in);
}

void end_lexical_scan (void) {
  yy_delete_buffer (YY_CURRENT_BUFFER);
}


