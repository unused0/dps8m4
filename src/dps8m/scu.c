/*
 * vim: filetype=c:tabstop=4:ai:expandtab
 * SPDX-License-Identifier: ICU
 * SPDX-License-Identifier: Multics
 * scspell-id: d49ab489-f62e-11ec-9ac1-80ee73e9b8e7
 *
 * ---------------------------------------------------------------------------
 *
 * Copyright (c) 2007-2013 Michael Mondy
 * Copyright (c) 2012-2016 Harry Reed
 * Copyright (c) 2013-2025 Charles Anthony
 * Copyright (c) 2021-2024 The DPS8M Development Team
 *
 * This software is made available under the terms of the ICU License.
 * See the LICENSE.md file at the top-level directory of this distribution.
 *
 * ---------------------------------------------------------------------------
 *
 * This source file may contain code comments that adapt, include, and/or
 * incorporate Multics program code and/or documentation distributed under
 * the Multics License.  In the event of any discrepancy between code
 * comments herein and the original Multics materials, the original Multics
 * materials should be considered authoritative unless otherwise noted.
 * For more details and historical background, see the LICENSE.md file at
 * the top-level directory of this distribution.
 *
 * ---------------------------------------------------------------------------
 */

/*
 * scu.c -- System Controller
 *
 * See AN70, section 8 and GB61.
 *
 * There were a few variations of SCs and SCUs:
 * SCU -- Series 60 Level 66 Controller
 * SC -- Level 68 System Controller
 * 4MW SCU -- A later version of the Level 68 SC
 *
 * SCUs control access to memory.
 * Each SCU owns a certain range of absolute memory.
 * This emulator allows the CPU to access memory directly however.
 * SCUs contain clocks.
 * SCUS also contain facilities which allow CPUS and IOMs to communicate.
 * CPUs or IOMS request access to memory via the SCU.
 * CPUs use the CIOC instr to talk to IOMs and other CPUs via a SCU.
 * IOMs use interrupts to ask a SCU to signal a CPU.
 * Other Interesting instructions:
 * read system controller reg and set system controller reg (RSCR & SSCR)
 *
 */

/*
 * Physical Details & Interconnection -- AN70, section 8.
 *
 * SCUs have 8 ports.
 * Active modules (CPUs and IOMs) have up to four of their ports
 * connected to SCU ports.
 *
 * The 4MW SCU has eight on/off switches to enable or disable
 * the ports.  However, the associated registers allow for
 * values of enabled, disabled, and program control.
 *
 * SCUs have stores (memory banks).
 *
 * SCUs have four sets of registers controlling interrupts.  Only two
 * of these sets, designated "A" and "B" are used.  Each set has:
 * Execute interrupt mask register -- 32 bits; enables/disables
 * the corresponding execute interrupt cell
 * Interrupt mask assignment register -- 9 bits total
 * two parts: assigned bit, set of assigned ports (8 bits)
 * In Multics, only one CPU will be assigned in either mask
 * and no CPU appears in both.   Earlier hardware versions had
 * four 10-position rotary switches.  Later hardware versions had
 * two 9-position (0..7 and off) rotary switches.
 *
 * Config panel -- Level 68 6000 SCU
 * -- from AM81
 * store A and store B
 * 3 position rotary switch: on line, maint, off line
 * size: 32K, 64K, 128K, 256K
 * exec interrupt mask assignment
 * four 10-position rotary switches (A through D): off, 0 .. 7, M
 * One switch for each program interrupt register
 * Assign mask registers to system ports
 * Normally assign one mask reg to each CPU
 *
 *   AM81:
 *     "        The EXECUTE INTERRUPT MASK ASSIGNMENT (EIMA) rotary switches
 *      determine where interrupts sent to memory are directed.  The four EIMA
 *      rotary switches, one for each program interrupt register, are used to
 *      assign mask registers to system ports. The normal settings assign one
 *      mask register to each CPU configured.
 *
 *      Each switch assigns mask registers as follows:
 *
 *          Position
 *            OFF     Unassigned
 *              0     Assigned to port 0
 *                ...
 *              7     Assigned to port 7
 *              M     Assigned to maintenance panel
 *
 *      Assignment of a mask register to a system port designates the
 *      port as a control port, and that port receives interrupt present
 *      signals. Up to four system ports can be designated as control
 *      ports. The normal settings assign one mask register to each CPU
 *      configured."
 *
 *
 *
 * Config panel -- Level 68 System Controller UNIT (4MW SCU)
 * -- from AM81
 * Store A, A1, B, B1 (online/offline)
 * LWR Store Size
 * PORT ENABLE
 * Eight on/off switches
 * Should be on for each port connected to a configured CPU
 * mask/port assignment
 * Two rotary switches (A & B); set to (off, 0..7)
 * See EXEC INTERRUPT on the 6000 SCU
 * When booting, one should be set to the port connected to
 * the bootload CPU.   The other should be off.
 *
 * If memory port B of CPU C goes to SCU D, then memory port B of all
 * other CPUs *and* IOMs must go to SCU D. -- AN70, 8-4.
 *
 * The base address of the SCU is the actual memory size * the port
 * assignment. -- AN70, 8-6.
 *
 *  43A239854 6000B Eng. Prod. Spec, 3.2.7 Interrupt Multiplex Word:
 *    "The IOM has the ability to set any of the 32 program interrupt
 *     cells located in the system controller containing the base address
 *     of the IOM. It should be noted that for any given IOM identity
 *     switch setting, the IOM can set only 8 of these program interrupt
 *     cells."
 *
 */

/*
 * === Initialization and Booting -- Part 1 -- Operator's view
 *
 * Booting Instructions (GB61)
 * First boot the BCE OS (Bootload command Environment).  See below.
 * A config deck is used
 * Bootload SCU is the one with a base addr of zero.
 * BCE is on a BCE/Multics System tape
 * Booted from tape into the system via bootload console

 */

/*
 * 58009906 (DPS8M)
 * When CPU needs to address the SCU (for a write/read data cycle,
 * for example), the ETMCM board int the CU of the CPU issues a $INT
 * to the SCU.  This signal is sent ... to the SCAMX active port
 * control board in the SCU
 */

// How?  If one of the 32 interrupt cells is set in one of the SCs,
// our processor will have the interrupt present (XIP) line active.
// Perhaps faults are flagged in the same way via the SXC system
// controller command.

// TEMPORARY
// Each SCU owns a certain range of absolute memory.
// CPUs use the cioc instr to talk to IOMs and other CPUs via a SCU.
// IOMs use interrupts to ask a SCU to signal a CPU.
// read system controller reg and set system controller reg (rscr & sscr)
// Bootload SCU is the one with a base addr of zero.
// 58009906
// When CPU needs to address the SCU (for a write/read data cycle,
// for example), the ETMCM board int the CU of the CPU issues a $INT
// to the SCU.  This signal is sent ... to the SCAMX active port
// control board in the
// -----------------------
// How?  If one of the 32 interrupt cells is set in one of the SCs,
// our processor will have the interrupt present (XIP) line active.
// Perhaps faults are flagged in the same way via the SXC system
// controller command.

/*
 * *** More (new) notes ***
 *
 * instr rmcm -- read mem controller mask register
 * ... for the selected controller, if the processor has a mask register
 * assigned ..
 * instr smcm -- set  mem controller mask register
 * ... for the selected controller, if the processor has a mask register
 * assigned, set it to C(AQ)
 * instr smic
 * turn on interrupt cells (any of 0..31)
 * instr cioc -- connect i/o channel, pg 173
 * SC addressed by Y sends a connect signal to the port specified
 * by C(Y)33,35
 * instr rscr & sscr -- Read/Store System Controller Register, pg 170
 *
 * 32 interrupt cells ... XIP
 * mask info
 * 8 mask registers
 * 58009906
 * =============
 *
 * AM81
 * Every active device (CPU, IOM) must be able to access all SCUs
 * Every SCU must have the same active device on the same SCU, so
 * all SCUs must have the same PORT ENABLE settings
 * Every active device must have the same SCU on the same port,
 * so all active devices will have the same config panel settings.
 * Ports must correspond -- port A on every CPU and IOM must either
 * be connected to the same SCU or not connected to any SCU.
 * IOMs should be on lower-numbered SCU ports than CPUs.
 * Multics can have 16MW words of memory.
 * CPUs have 8 ports, a..h.
 * SCUs have 8 ports, 0..7.
 *
 *
 * Level 68 6000 SCU Configuration Panel
 *   system control and monitor (cont&mon/mon/off)
 *   system boot control (on/off)
 *   alarm (disable/normal)
 *   maintenance panel mode (test/normal)
 *   store a
 *      mode (offline/maint/online)
 *      size (32k, 64k, 128k, 256k)
 *   store b
 *      mode (offline/maint/online)
 *      size (32k, 64k, 128k, 256k)
 *   execute interrupt mask assignment
 *      (A through D; off/0/1/2/3/4/5/6/7/m)
 *   [CAC] I interpret this as CPU [A..D] is connected to my port [0..7]
 *   address control
 *      lower store (a/b)
 *      offset (off, 16k, 32k, 64k)
 *      interlace (on/off)
 *   cycle port priority (on/off)
 *   port control (8 toggles) (enabled/prog cont/disable)
 *
 * The EXECUTE INTERRUPT MASK ASSIGNMENT (EIMA) rotary switches
 * determine where interrupts sent to memory are directed. The four EIMA
 * rotary switches, one for each program interrupt register, are used to
 * assign mask registers to system ports. The normal settings assign one
 * mask register to each CPU configured.
 *
 *  Assignment of a mask register to a system port designates the port as a
 *  control port, and that port receives interrupt present signals. Up to four
 *  system ports can be designated as control ports. The normal settings
 *  assign one mask register to each cpu configured.
 *
 *
 *
 * Configuration rules for Multics:
 *
 *   1. Each CPU in the system must be connected to each SCU in the system
 *
 *   2. Each IOM in the system must be connected to each SCU in the system
 *
 *   3. Each SCU in the system must be connected to every CPU and IOM in the
 *      system.
 *
 *   4. Corresponding ports on all CPUs and IOMs must be connected to the same
 *      SCU. For example, port A on every CPU and IOM must be connected to the
 *      same SCU or not connected to any SCU.
 *
 *   5. Corresponding ports on all SCUs must be connected to the same active
 *      device (CPU or IOM). For example, if port 0 on any SCU is connected to
 *      IOM A, then port 0 on all SCUs must be connected to IOM A.
 *
 *   6. IOMs should be connected to lower-number SCU ports the CPUs.
 *
 *   These rules are illustrated in Figure 3-5, where the port numbers for a
 *   small Multics system of 2 CPUS, 3 SCUs and 2 IOMs have been indicated
 *
 *
 *
 *
 *                    -----------------                      -----------------
 *                    |               |                      |               |
 *                    |     CPU A     |                      |     CPU B     |
 *                    |               |                      |               |
 *                    -----------------                      -----------------
 *                    | A | B | C | D |                      | A | B | C | D |
 *                    -----------------                      -----------------
 *                      |   |   |                              |   |   |
 *                      |   |   |                              |   |   -----------------
 *                      |   |   |                              |   |                   |
 *                      |   |   -------------------------------)---)----------------   |
 *                      |   |                                  |   |               |   |
 *   --------------------   -----------------                  |   |               |   |
 *   |                                      |                  |   |               |   |
 *   |   -----------------------------------)-------------------   |               |   |
 *   |   |                                  |                      |               |   |
 *   |   |                                  |   --------------------               |   |
 *   |   |                                  |   |                                  |   |
 * -----------------                      -----------------                      -----------------
 * | 7 | 6 | 5 | 4 |                      | 7 | 6 | 5 | 4 |                      | 7 | 6 | 5 | 4 |
 * -----------------                      -----------------                      -----------------
 * |               |                      |               |                      |               |
 * |     SCU C     |                      |     SCU B     |                      |     SCU A     |
 * |               |                      |               |                      |               |
 * -----------------                      -----------------                      -----------------
 * | 3 | 2 | 1 | 0 |                      | 3 | 2 | 1 | 0 |                      | 3 | 2 | 1 | 0 |
 * -----------------                      -----------------                      -----------------
 *           |   |                                  |   |                                  |   |
 *           |   |                                  |   -----------                        |   |
 *           |   |                                  |             |                        |   |
 *           |   -----------------------------------)---------    |                        |   |
 *           |                                      |        |    |                        |   |
 *           ----------    --------------------------        |    |                        |   |
 *                    |    |                                 |    |                        |   |
 *                    |    |   ------------------------------)----)-------------------------   |
 *                    |    |   |                             |    |                            |
 *                    |    |   |                             |    |  ---------------------------
 *                    |    |   |                             |    |  |
 *                   -----------------                      -----------------
 *                   | A | B | C | D |                      | A | B | C | D |
 *                   -----------------                      -----------------
 *                   |               |                      |               |
 *                   |     IOM A     |                      |     IOM B     |
 *                   |               |                      |               |
 *                   -----------------                      -----------------
 *
 *
 *
 *"During bootload, Multics requires a contiguous section of memory beginning at
 * absolute address 0 and sufficiently large to contain all routines and data
 * structures used during the first phase of Multics initialization (i.e.
 * collection 1).
 * The size of the section required varies among Multics release, and it also
 * depends on the size of the SST segment, which is dependent on the parameters
 * specified by the site on the SST config card. ... However
 * 512 KW is adequate for all circumstances. There can be no "holes" in memory
 * within this region. Beyond this region, "holes" can exist in memory."
 *
 *
 */

/*
 * From AN70-1 May84, pg 86 (8-6)
 *
 * RSCR SC_CFG bits 9-11 lower store size
 *
 * A DPS-8 SCU may have up to four store units attached to it. If this
 * is the case, two store units form a pair of units. The size of a
 * pair of units (or a single unit) is 32K * 2 ** (lower store size)
 * above.
 */

/*
 * From AN70-1 May84, pg 86 (8-6)
 *
 * SCU ADDRESSING
 *
 *       There are three ways in which an SCU is addressed.  In the
 * normal mode of operation (memory reading and writing), an active
 * unit (IOM or CPU) translates an absolute address into a memory
 * port (on it) and a relative memory address within the memory
 * described by the memory port. The active module sends the
 * address to the SCU on the proper memory port. If the active
 * module is enabled by the port enable mask in the referenced SCU,
 * the SCU will take the address given to it and provide the
 * necessary memory access.
 *
 *      The other two ways pertain to reading/setting control
 * registers in the SCU itself. For each of these, it is still
 * necessary to specify somehow the memory port on the CPU whose SCU
 * registers are desired. For the RMCM, SMCM and SMIC instructions,
 * this consists of providing a virtual address to the processor for
 * which bits 1 and 2 are the memory port desired.
 *
 *      The rscr and sscr instructions, though key off the final
 * absolute address to determine the SCI (or SCU store unit)
 * desired. Thus, software needs a way to translate a memory port
 * number into an absolute address to reach the SCU. This is done
 * with the paged segment scas, generated by int_scas (and
 * init_scu). scas has a page corresponding to each SCU and to each
 * store unit in each SCU. pmut$rscr and pmut$sscr use the memory
 * port number desired to generate a virtual address into scas whose
 * absolute address (courtesy of the ptws for sca) just happen to
 * describe memory within that SCU.
 *
 *       The cioc instruction (discussed below) also depends on the
 * final absolute address of the target operand to identify the SCU
 * to perform the operation. In the case of the cioc instruction,
 * though, the has no particular impact in Multics software. All
 * target operands for the cioc instruction when referencing IOMs
 * are in the low order SCU. When referencing CPUS, the SCU
 * performing the connecting has no real bearing.
 *
 * Inter-module communication
 *
 *       As mentioned earlier, communication between active modules
 * (CPUs and IOMs can only be performed through SCUs.
 *
 *       CPUs communicate to IOMs and other CPUs via the cioc
 * (connect i/o channel) instruction. The operand of the instruction
 * is a word in memory. The SCU containing this operand is the SCU
 * that performs the connect function. The word fetched from memory
 * contains in its low order bits the identity of a port on the SCU
 * to which this connection is to be sent. This only succeeds if the
 * target port is enabled (port enable mask) on the SCU. When the
 * target of the connection is an IOM; this generates a connect strobe
 * to the IOM. The IOM examines its mailbox in memory to determine
 * its course of action. When the target of the connect is another
 * CPU, this generates a connect fault in the target processor. The
 * target processor determines what course to follow on the basis
 * of information in memory analyzed by software. When a connect is
 * sent to a process (including the processor issuing the connect),
 * the connect is deferred until the processor stops
 * executing inhibited code (instructions with the inhibit bit set).
 *
 *       Signals sent from an IOM to a CPU are much more involved.
 * The basic flow is as follows. The IOM determines an interrupt
 * number. (The interrupt number is a five bit value, from 0 to 31.
 * The high order bits are the interrupt level.
 *
 * 0 - system fault
 * 1 - terminate
 * 2 - marker
 * 3 - special
 *
 * The low order three bits determines the IOM and IOM channel
 * group.
 *
 * 0 - IOM 0 channels 32-63
 * 1 - IOM 1 channels 32-63
 * 2 - IOM 2 channels 32-63
 * 3 - IOM 3 channels 32-63
 * 4 - IOM 0 channels 0-31
 * 5 - IOM 1 channels 0-31
 * 6 - IOM 2 channels 0-31
 * 7 - IOM 3 channels 0-31
 *
 * It also takes the channel number in the group (0-31 meaning
 * either channels 0-31 to 32-63) and sets the <channel number>th
 * bit in the <interrupt number>th memory location in the interrupt
 * mask word (IMW) array in memory. It then generates a word with
 * the <interrupt number>th bit set and sends this to the bootload
 * SCU with the SC (set execute cells) SCU command. This sets the
 * execute interrupt cell register in the SCU and sends an XIP
 * (execute interrupt present) signal to various processors
 * connected to the SCU. (The details of this are covered in the
 * next section.) One of the processors (the first to get to it)
 * sends an XEC (execute interrupt cells) SCU command to the SCU who
 * generated the XIP signal. The SCU provides the interrupt number
 * to the processor, who uses it to determine the address of a fault
 * pair in memory for the "fault" caused by this interrupt. The
 * processing of the XEC command acts upon the highest priority
 * (lowest number) bit in the execute interrupt cell register, and
 * also resets this bit in the register.
 *
 * Interrupts Masks and Assignment
 *
 *       The mechanism for determining which processors are candidates
 * for receiving an interrupt from an IOM is an involved
 * topic. First of all, a processor will not be interrupted as long
 * as it is executing inhibited instructions (instructions with the
 * inhibit bit set). Beyond this, though, lies the question of
 * interrupt masks and mask assignment.
 *
 *       Internal to the SCU are two sets of registers (A and B),
 * each set consisting of the execute interrupt mask register and
 * the interrupt mask assignment register. Each execute interrupt
 * mask register is 32 bits long, with each bit enabling the
 * corresponding bit in the execute interrupt cell register. Each
 * interrupt mask assignment register has two parts, an assigned bit
 * and a set of ports to which it is assigned (8 bits). When a bit
 * is set in the execute  interrupt sells register, the SCU ANDs this
 * bit with the corresponding bit in each of the execute interrupt
 * mask registers. If the corresponding bit of execute interrupt
 * mask register A, for example, is on, the SCU then looks at the A
 * interrupt mask assignment register. If this register is not
 * assigned (enable), no further action takes place in regards to
 * the A registers. (The B registers are still considered) (in
 * parallel, by the way).) If the register is assigned (enabled)
 * then interrupts will be send to all ports (processors) whose
 * corresponding bit is set in the interrupt mask assignment
 * register. This, only certain interrupts are allowed to be
 * signalled at any given time (base on the contents of the execute
 * interrupt mask registers) and only certain processors will
 * receive these interrupts (as controlled by the interrupt mask
 * assignment registers).
 *
 *       In Multics, only one processor is listed in each of the two
 * interrupt mask assignment registers, and no processor appears in
 * both. Thus there is a one for one correspondence between
 * interrupt masks that are assigned (interrupt mask registers whose
 * assigned (enabled) bit is on) and processors who have an
 * interrupt mask (SCU port number appears in an interrupt mask
 * register). So, at any one time only two processors
 * are eligible to receive interrupts. Other processors need not
 * worry about masking interrupts.
 *
 *       The contents of the interrupt mask registers may be
 * obtained with the SCU configuration information with the rscr
 * instruction and set with the sscr instruction.
 *
 *  bits   meaning
 *
 * 00-07   ports assigned to mask A (interrupt mask assignment A)
 * 08-08   mask A is unassigned (disabled)
 * 36-43   ports assigned to mask B (interrupt mask assignment B)
 * 44-44   mask B is unassigned (disabled)
 *
 *       The contents of a execute interrupt mask register are
 * obtained with the rmcm or the rscr instruction and set with the
 * smcm or the sscr instruction. The rmcm and smcm instruction only
 * work if the processor making the request has a mask register
 * assigned to it. If not, rmcm returns zero (no interrupt are
 * enabled to it) and a smcm is ignored (actually the port mask
 * setting is still done). The rscr and sscr instructions allow the
 * examining/setting of the execute interrupt mask register for any
 * port on a SCU; these have the same effect as smcm and rmcm if the
 * SCU port being referenced does not have a mask assigned to it.
 * The format of the data returned by these instructions is as
 * follows.
 *
 *  bits   meaning
 * 00-15   execute interrupt mask register 00-15
 * 32-35   SCU port mask 0-3
 * 36-51   execute interrupt mask register 16-31
 * 68-71   SCU port mask 4-7
 *
 */

// SCU numbering:
//
// AM81-04, pg 49: "... the ports are listed in order of increasing base
//    address, which corresponds to the order of mem config cards."
// pg 97: "mem port size state ... port as a value (a through h) that
//        corresponds to the number of the active module port to which the
//        system controller is connected.
//
// From this, I conclude;
//   The SCU connected to port A (0) is SCUA, 1 B, 2 C, etc.
//   SCUA starts at address 0, and the SCUs are sorted by increasing addresses.
//

// ============================================================================

#include <sys/time.h>

#include "dps8m.h"
#include "iom_channel.h"
#include "iom.h"
#include "faults.h"
#include "cable.h"
#include "utils.h"
# include "threadz.h"

scu_t scu [N_SCUS];

#define N_SCU_UNITS 1 // Default

static UNIT scuUnits [N_SCUS] = {
  [0 ... N_SCUS - 1] = {
    UDATA (NULL, 0, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL
  }
};

#define UNIT_NUM(uptr) ((uptr) - scuUnits)

// Hardware configuration switches

// sscr and other instructions override these settings

typedef struct {
  word1 mode;                           // program or manual
  uint portEnable[N_SCU_PORTS];         // enable/disable
  uint maskEnable[N_ASSIGNMENTS];       // enable/disable
  uint maskAssignment[N_ASSIGNMENTS];   // assigned port number
  uint lowerStoreSize;                  // In K words, power of 2; 32 - 4096
  word7 cyclic;                         // 7 bits
  word8 nea;                            // 8 bits
  word4 online;                         // 4 bits
  word1 interlace;                      // 1 bit
  word1 lwr;                            // 1 bit
} scuConfigSwitches_t;

static scuConfigSwitches_t scuConfigSwitches [N_SCUS];

static volatile atomic_int gtodWarnCnt = 0;

// ============================================================================

static bool scuCmdCheckIdx (int scuIdx) {
  if (scuIdx < 0 || scuIdx >= N_SCUS) {
    simPrintf ("Error: Invalid SCU tag; expected A-H\n");
    return true;
  }
  return false;
}


static bool scuCmdCheckMaskRegister (int maskRegister) {
  if (maskRegister < 0 || maskRegister > 1) {
    simPrintf ("Error: Invalid SCU mask register; expected A or B\n");
    return true;
  }
  return false;
}


static bool scuCmdCheckPortNumber (int portNumber) {
  if (portNumber < 0 || portNumber > N_SCU_PORTS - 1) {
    simPrintf ("Error: Invalid SCU port number; expected 0-%u\n", N_SCU_PORTS - 1);
    return true;
  }
  return false;
}


int scuCmdShow (word36 scuIdx) {
  if (scuCmdCheckIdx (scuIdx)) {
    return -1;
  }
  int rc = scuCmdShowConfig (scuIdx);
  if (rc)
    return rc;
  rc = scuCmdShowState (scuIdx);
  return rc;
}


int scuCmdShowConfig (word36 scuIdx) {
  if (scuCmdCheckIdx (scuIdx)) {
    return -1;
  }

  simPrintf ("SCU %c CONFIG:\n", AZ (scuIdx));

  static const char * map [N_SCU_PORTS] = {
    "0", "1", "2", "3", "4", "5", "6", "7"
  };

  scuConfigSwitches_t * sw = scuConfigSwitches + scuIdx;

  const char * mode = "<out of range>";
  switch (sw -> mode) {
    case MODE_PROGRAM:
      mode = "Program";
      break;
    case MODE_MANUAL:
      mode = "Manual";
      break;
  }

  simPrintf ("    Mode:                       %s\n", mode);
  simPrintf ("    Port:             ");
  for (int i = 0; i < N_SCU_PORTS; i ++)
    simPrintf ("        %d %s\n", i, sw->portEnable [i] ? "Enabled" : "Disabled");
  simPrintf ("    Mask:             ");
  for (int i = 0; i < N_ASSIGNMENTS; i ++) {
    simPrintf ("        %c:                     %s\n",
            AZ (i),
            sw->maskEnable[i] ? (map[sw->maskAssignment[i]]) : "Off");
  }
  simPrintf ("Lower Store Size:     %o\n",   sw->lowerStoreSize);
  simPrintf ("Cyclic:               %03o\n", sw->cyclic);
  simPrintf ("Non-existent address: %03o\n", sw->nea);
  simPrintf ("Online:               %02o\n", sw->online);
  simPrintf ("Interlace:            %s\n",   sw->interlace ? "Enabled" : "Disabled");
  simPrintf ("Lower:                %s\n",   sw->lwr ? "Enabled" : "Disabled");

  return 0;
}


int scuCmdShowState (word36 scuIdx) {
  if (scuCmdCheckIdx (scuIdx)) {
    return -1;
  }
  simPrintf ("SCU %c STATE:\n", AZ (scuIdx));
  scu_t * scup = scu + scuIdx;
  simPrintf ("    Mode %s\n", scup->mode == MODE_PROGRAM ? "PROGRAM" : "MANUAL");

  simPrintf ("    Port\n");
  for (int i = 0; i < N_SCU_PORTS; i ++) {
    struct ports * pp = scup -> ports + i;

    simPrintf ("        %d %s devIdx %d devicePort %2d type %s\n",
            i, scup->portEnable[i] ? "ENABLE " : "DISABLE",
            pp->devIdx, pp->devicePort[XXX_TEMP_SCU_SUBPORT],
            pp->type == ADEV_NONE ? "NONE" :
            pp->type == ADEV_CPU ? "CPU" :
            pp->type == ADEV_IOM ? "IOM" :
            "<enum broken>");
  }
  for (int i = 0; i < N_ASSIGNMENTS; i ++) {
    simPrintf ("    Cell %c\n", AZ (i));
    simPrintf ("        execIntrMask %012o\n", scup -> execIntrMask [i]);
    simPrintf ("        maskEnable     %s\n", scup -> maskEnable [i] ? "ENABLE" : "DISABLE");
    simPrintf ("        maskAssignment %d\n", scup -> maskAssignment [i]);
    simPrintf ("        cells          ");
    for (int j = 0; j < N_CELL_INTERRUPTS; j ++)
      simPrintf ("%d", scup -> cells [j]);
    simPrintf ("\n");
  }
  simPrintf ("Lower store size: %d\n",   scup->lowerStoreSize);
  simPrintf ("Cyclic:           %03o\n", scup->cyclic);
  simPrintf ("NEA:              %03o\n", scup->nea);
  simPrintf ("Online:           %02o\n", scup->online);
  simPrintf ("Interlace:        %o\n",   scup->interlace);
  simPrintf ("Interlace:        %s\n",   scup->interlace ? "Enabled" : "Disabled");
  simPrintf ("Lower:            %o\n",   scup->lwr);
  simPrintf ("ID:               %o\n",   scup->id);
  simPrintf ("modeReg:         %06o\n", scup->modeReg);
  simPrintf ("Elapsed days:     %d\n",   scup->elapsedDays);
  simPrintf ("Steady clock:     %s\n",   scup->steadyClock ? "Enabled" : "Disabled");
  simPrintf ("Y2K enabled:      %s\n",   scup->y2k ? "Enabled" : "Disabled");
  return 0;
}


int scuCmdSetCyclic (word36 scuIdx, word36 cyclic) {
  if (scuCmdCheckIdx (scuIdx)) {
    return -1;
  }
  if (cyclic > 0177) {
    simPrintf ("Error: Invalid cyclic value; expected 0-177\n");
    return -1;
  }
  scuConfigSwitches[scuIdx].cyclic = cyclic;
  return 0;
}


// NB: Sets scu state, not switches
int scuCmdSetElapseddays (word36 scuIdx, word36 elapsedDays) {
  if (scuCmdCheckIdx (scuIdx)) {
    return -1;
  }
  if (elapsedDays > 20000) {
    simPrintf ("Error: Invalid elapsed days value; expected 0-20000\n");
    return -1;
  }
  scu[scuIdx].elapsedDays = elapsedDays;
  return 0;
}


int scuCmdSetInterlace (word36 scuIdx, bool interlace)  {
  if (scuCmdCheckIdx (scuIdx)) {
    return -1;
  }
  scuConfigSwitches[scuIdx].interlace = interlace ? 1 : 0;
  return 0;
}


int scuCmdSetLower (word36 scuIdx, bool lower)  {
  if (scuCmdCheckIdx (scuIdx)) {
    return -1;
  }
  scuConfigSwitches[scuIdx].lwr = lower ? 1 : 0;
  return 0;
}


int scuCmdSetLSS (word36 scuIdx, word36 lss) {
  if (scuCmdCheckIdx (scuIdx)) {
    return -1;
  }
  if (lss > 7) {
    simPrintf ("Error: Invalid Lower store size setting; expected 0-7\n");
    return -1;
  }
  scuConfigSwitches[scuIdx].lowerStoreSize = lss;
  return 0;
}


int scuCmdSetMask (word36 scuIdx, int maskRegister, int setting) {
  if (scuCmdCheckIdx (scuIdx)) {
    return -1;
  }
  if (scuCmdCheckMaskRegister (maskRegister)) {
    return -1;
  }
  if (setting == -1) {
    scuConfigSwitches[scuIdx].maskEnable[maskRegister] = false;
  } else if (setting < N_SCU_PORTS) {
    scuConfigSwitches[scuIdx].maskEnable[maskRegister] = true;
    scuConfigSwitches[scuIdx].maskAssignment[maskRegister] = setting;
  } else {
    simPrintf ("Error: Invalid mask assignment value; expected 0-7\n");
  }
  return 0;
}


int scuCmdSetNEA (word36 scuIdx, word36 nea) {
  if (scuCmdCheckIdx (scuIdx)) {
    return -1;
  }
  if (nea > 0377) {
    simPrintf ("Error: Invalid NWA address; expected 0-377\n");
    return -1;
  }
  scuConfigSwitches[scuIdx].nea = (word8) nea;
  return 0;
}


// A  0 false
// A1 0 true
// B  1 false
// B1 1 true

int scuCmdSetOnline (word36 scuIdx, uint banklist, bool online) {
  if (scuCmdCheckIdx (scuIdx)) {
    return -1;
  }
  if (banklist > 017) {
    simPrintf ("Error: Invalid bank list; expected A, A1, B, B1\n");
    return -1;
  }
  if (online) {
    scuConfigSwitches[scuIdx].online |= banklist;
  } else {
    scuConfigSwitches[scuIdx].online &= ~banklist;
  }
  return 0;
}


int scuCmdSetPort (word36 scuIdx, int portNumber, bool enable) {
  if (scuCmdCheckIdx (scuIdx)) {
    return -1;
  }
  if (scuCmdCheckPortNumber (portNumber)) {
    return -1;
  }
  scuConfigSwitches[scuIdx].portEnable[portNumber] = enable;
  return 0;
}


int scuCmdSetMode (word36 scuIdx, int mode) {
  if (scuCmdCheckIdx (scuIdx)) {
    return -1;
  }
  scuConfigSwitches[scuIdx].mode = mode ? MODE_PROGRAM : MODE_MANUAL;  // 0: manual, 1: program
  return 0;
}


// NB sets scu, not switches
int scuCmdSetSteadyClock (word36 scuIdx, bool enable) {
  if (scuCmdCheckIdx (scuIdx)) {
    return -1;
  }
  scu[scuIdx].steadyClock = enable;
  return 0;
}


int scuCmdSetY2K (word36 scuIdx, bool enable) {
  if (scuCmdCheckIdx (scuIdx)) {
    return -1;
  }
  scu[scuIdx].y2k = enable;
  return 0;
}


static simRc scu_show_nunits (UNUSED FILE * st, UNUSED UNIT * uptr,
                               UNUSED int val, const UNUSED void * desc)
  {
    simPrintf ("Number of SCU units in system is %d\n", scuDev.numunits);
    return SCPE_OK;
  }

static simRc scu_set_nunits (UNUSED UNIT * uptr, UNUSED int32_t value,
                              const char * cptr, UNUSED void * desc)
  {
    if (! cptr)
      return SCPE_ARG;
    int n = atoi (cptr);
    if (n < 0 || n > N_SCUS)
      return SCPE_ARG;
    scuDev.numunits = (uint) n;
    return SCPE_OK;
  }

static simRc scu_show_state (UNUSED FILE * st, UNIT *uptr, UNUSED int val,
                              UNUSED const void * desc)
  {
    long scuUnitIdx = UNIT_NUM (uptr);
    if (scuUnitIdx < 0 || scuUnitIdx >= (int) scuDev.numunits)
      {
        simPrintf ("error: Invalid unit number %ld\n", (long) scuUnitIdx);
        return SCPE_ARG;
      }

    simPrintf ("SCU unit number %ld\n", (long) scuUnitIdx);
    scu_t * scup = scu + scuUnitIdx;
    simPrintf ("    Mode %s\n",
                scup->mode == MODE_PROGRAM ? "PROGRAM" : "MANUAL");

    for (int i = 0; i < N_SCU_PORTS; i ++)
      {
        struct ports * pp = scup -> ports + i;

        simPrintf ("    Port %d %s devIdx %d devicePort %d type %s\n",
                    i, scup->portEnable[i] ? "ENABLE " : "DISABLE",
                    pp->devIdx, pp->devicePort[XXX_TEMP_SCU_SUBPORT],
                    pp->type == ADEV_NONE ? "NONE" :
                    pp->type == ADEV_CPU ? "CPU" :
                    pp->type == ADEV_IOM ? "IOM" :
                    "<enum broken>");
      }
    for (int i = 0; i < N_ASSIGNMENTS; i ++)
      {
        //struct interrupts * ip = scup -> interrupts + i;

        simPrintf ("    Cell %c\n", 'A' + i);
        simPrintf ("        execIntrMask %012o\n",
                    scup -> execIntrMask [i]);
        simPrintf ("        maskEnable %s\n",
                    scup -> maskEnable [i] ? "ENABLE" : "DISABLE");
        simPrintf ("        maskAssignment %d\n",
                    scup -> maskAssignment [i]);
        simPrintf ("        cells ");
        for (int j = 0; j < N_CELL_INTERRUPTS; j ++)
          simPrintf ("%d", scup -> cells [j]);
        simPrintf ("\n");
      }
    simPrintf ("Lower store size: %d\n", scup -> lowerStoreSize);
    simPrintf ("Cyclic: %03o\n",         scup -> cyclic);
    simPrintf ("NEA: %03o\n",            scup -> nea);
    simPrintf ("Online: %02o\n",         scup -> online);
    simPrintf ("Interlace: %o\n",        scup -> interlace);
    simPrintf ("Lower: %o\n",            scup -> lwr);
    simPrintf ("ID: %o\n",               scup -> id);
    simPrintf ("modeReg: %06o\n",       scup -> modeReg);
    simPrintf ("Elapsed days: %d\n",     scup -> elapsedDays);
    simPrintf ("Steady clock: %s\n",     scup -> steadyClock ? "Enabled" : "Disabled");
    simPrintf ("Y2K enabled: %s\n",      scup -> y2k ? "Enabled" : "Disabled");
    return SCPE_OK;
  }

static simRc scu_show_config (UNUSED FILE * st, UNUSED UNIT * uptr,
                               UNUSED int val, UNUSED const void * desc)
{
    static const char * map [N_SCU_PORTS] =
      {
        "0", "1", "2", "3", "4", "5", "6", "7"
      };
    long scuUnitIdx = UNIT_NUM (uptr);
    if (scuUnitIdx < 0 || scuUnitIdx >= (int) scuDev.numunits)
      {
        simPrintf ("error: Invalid unit number %ld\n", (long) scuUnitIdx);
        return SCPE_ARG;
      }

    simPrintf ("SCU unit number %ld\n", (long) scuUnitIdx);

    scuConfigSwitches_t * sw = scuConfigSwitches + scuUnitIdx;

    const char * mode = "<out of range>";
    switch (sw -> mode)
      {
        case MODE_PROGRAM:
          mode = "Program";
          break;
        case MODE_MANUAL:
          mode = "Manual";
          break;
      }

    simPrintf ("Mode:                       %s\n", mode);
    simPrintf ("Port Enable:             ");
    for (int i = 0; i < N_SCU_PORTS; i ++)
      simPrintf (" %3o", sw -> portEnable [i]);
    simPrintf ("\n");
    for (int i = 0; i < N_ASSIGNMENTS; i ++)
      {
        simPrintf ("Mask %c:                     %s\n",
                    'A' + i,
                    sw->maskEnable[i] ? (map[sw->maskAssignment[i]]) : "Off");
      }
    simPrintf ("Lower Store Size:           %o\n",   sw -> lowerStoreSize);
    simPrintf ("Cyclic:                     %03o\n", sw -> cyclic);
    simPrintf ("Non-existent address:       %03o\n", sw -> nea);

    return SCPE_OK;
  }

//
// set scu0 config=<blah> [;<blah>]
//
//    blah =
//           mode=  manual | program
//           mask[A|B] = off | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7
//           portN = enable | disable
//           lwrstoresize = 32 | 64 | 128 | 256 | 512 | 1024 | 2048 | 4096
//           cyclic = n
//           nea = n
//
//      o  nea is not implemented; will read as "nea off"
//      o  Multics sets cyclic priority explicitly; config
//         switches are ignored.
//      o  STORE A, A1, B, B1 ONLINE/OFFLINE not implemented;
//         will always read online.
//      o  store size if not enforced; a full memory complement
//         is provided.
//      o  interlace not implemented; will read as 'off'
//      o  LOWER STORE A/B not implemented.
//      o  MASK is 'MASK/PORT ASSIGNMENT' analogous to the
//         'EXECUTE INTERRUPT MASK ASSIGNMENT of a 6000 SCU

static config_value_list_t cfg_mode_list [] =
  {
    { "manual",  0 },
    { "program", 1 },
    { NULL,      0 }
  };

static config_value_list_t cfg_mask_list [] =
  {
    { "off", -1 },
    { NULL,  0  }
  };

static config_value_list_t cfg_able_list [] =
  {
    { "disable", 0 },
    { "enable",  1 },
    { NULL,      0 }
  };

static config_value_list_t cfg_size_list [] =
  {
    { "32",    0 },
    { "64",    1 },
    { "128",   2 },
    { "256",   3 },
    { "512",   4 },
    { "1024",  5 },
    { "2048",  6 },
    { "4096",  7 },
    { "32K",   0 },
    { "64K",   1 },
    { "128K",  2 },
    { "256K",  3 },
    { "512K",  4 },
    { "1024K", 5 },
    { "2048K", 6 },
    { "4096K", 7 },
    { "1M",    5 },
    { "2M",    6 },
    { "4M",    7 },
    { NULL,    0 }
  };

static config_value_list_t cfg_on_off [] =
  {
    { "off",     0 },
    { "on",      1 },
    { "disable", 0 },
    { "enable",  1 },
    { NULL,      0 }
  };

static configList_t scu_config_list [] =
  {
    /*  0 */ { "mode",         1, 0,               cfg_mode_list },
    /*  1 */ { "maska",        0, N_SCU_PORTS - 1, cfg_mask_list },
    /*  2 */ { "maskb",        0, N_SCU_PORTS - 1, cfg_mask_list },
    /*  3 */ { "port0",        1, 0,               cfg_able_list },
    /*  4 */ { "port1",        1, 0,               cfg_able_list },
    /*  5 */ { "port2",        1, 0,               cfg_able_list },
    /*  6 */ { "port3",        1, 0,               cfg_able_list },
    /*  7 */ { "port4",        1, 0,               cfg_able_list },
    /*  8 */ { "port5",        1, 0,               cfg_able_list },
    /*  9 */ { "port6",        1, 0,               cfg_able_list },
    /* 10 */ { "port7",        1, 0,               cfg_able_list },
    /* 11 */ { "lwrstoresize", 0, 7,               cfg_size_list },
    /* 12 */ { "cyclic",       0, 0177,            NULL          },
    /* 13 */ { "nea",          0, 0377,            NULL          },
    // mask: 8 a_online, 4 a1_online, 2 b_online, 1, b1_online
    /* 14 */ { "onl",          0, 017,             NULL          },
    /* 15 */ { "int",          0, 1,               NULL          },
    /* 16 */ { "lwr",          0, 1,               NULL          },

    // Hacks

    /* 17 */ { "elapsed_days", 0, 20000,           NULL       },
    /* 18 */ { "steady_clock", 0, 1,               cfg_on_off },
    /* 20 */ { "y2k",          0, 1,               cfg_on_off },
             { NULL,           0, 0,               NULL       }
  };

static simRc scu_set_config (UNIT * uptr, UNUSED int32_t value,
                              const char * cptr, UNUSED void * desc)
  {
    long scuUnitIdx = UNIT_NUM (uptr);
    if (scuUnitIdx < 0 || scuUnitIdx >= (int) scuDev.numunits)
      {
        simPrintf ("error: scu_set_config: Invalid unit number %ld\n",
                    (long) scuUnitIdx);
        return SCPE_ARG;
      }

    scuConfigSwitches_t * sw = scuConfigSwitches + scuUnitIdx;

    configState_t cfg_state = { NULL, NULL };

    for (;;)
      {
        int64_t v;
        int rc = cfgParse ("scu_set_config", cptr, scu_config_list,
                           & cfg_state, & v);
        if (rc == CFG_DONE)
          break;

        if (rc == CFG_ERROR)
          {
            cfgParseDone (& cfg_state);
            return SCPE_ARG;
          }

        const char * p = scu_config_list [rc].name;
        if (strcmp (p, "mode") == 0)
          sw -> mode = (word1) v;
        else if (strcmp (p, "maska") == 0)
          {
            if (v == -1)
              sw -> maskEnable [0] = false;
            else
              {
                sw -> maskEnable [0] = true;
                sw -> maskAssignment [0] = (uint) v;
              }
          }
        else if (strcmp (p, "maskb") == 0)
          {
            if (v == -1)
              sw -> maskEnable [1] = false;
            else
              {
                sw -> maskEnable [1] = true;
                sw -> maskAssignment [1] = (uint) v;
              }
          }
        else if (strcmp (p, "port0") == 0)
          sw -> portEnable [0] = (uint) v;
        else if (strcmp (p, "port1") == 0)
          sw -> portEnable [1] = (uint) v;
        else if (strcmp (p, "port2") == 0)
          sw -> portEnable [2] = (uint) v;
        else if (strcmp (p, "port3") == 0)
          sw -> portEnable [3] = (uint) v;
        else if (strcmp (p, "port4") == 0)
          sw -> portEnable [4] = (uint) v;
        else if (strcmp (p, "port5") == 0)
          sw -> portEnable [5] = (uint) v;
        else if (strcmp (p, "port6") == 0)
          sw -> portEnable [6] = (uint) v;
        else if (strcmp (p, "port7") == 0)
          sw -> portEnable [7] = (uint) v;
        else if (strcmp (p, "lwrstoresize") == 0)
          sw -> lowerStoreSize = (uint) v;
        else if (strcmp (p, "cyclic") == 0)
          sw -> cyclic = (word7) v;
        else if (strcmp (p, "nea") == 0)
          sw -> nea = (word8) v;
        else if (strcmp (p, "onl") == 0)
          sw -> online = (uint) v;
        else if (strcmp (p, "int") == 0)
          sw -> interlace = (word1) v;
        else if (strcmp (p, "lwr") == 0)
          sw -> lwr = (word1) v;
        else if (strcmp (p, "elapsed_days") == 0)
          scu [scuUnitIdx].elapsedDays = (uint) v;
        else if (strcmp (p, "steady_clock") == 0)
          scu [scuUnitIdx].steadyClock = !! v;
        else if (strcmp (p, "y2k") == 0)
          scu [scuUnitIdx].y2k = !! v;
        else
          {
            simPrintf ("error: scu_set_config: invalid cfgParse rc <%d>\n",
                         rc);
            cfgParseDone (& cfg_state);
            return SCPE_ARG;
          }
      } // process statements
    cfgParseDone (& cfg_state);
    return SCPE_OK;
  }

static MTAB scu_mod [] =
  {
    {
      MTAB_XTD | MTAB_VUN | \
      MTAB_NMO | MTAB_VALR,                          /* Mask               */
      0,                                             /* Match              */
      (char *) "CONFIG",                             /* Print string       */
      (char *) "CONFIG",                             /* Match string       */
      scu_set_config,                                /* Validation routine */
      scu_show_config,                               /* Display routine    */
      NULL,                                          /* Value descriptor   */
      NULL                                           /* Help               */
    },
    {
      MTAB_XTD | MTAB_VDV | \
      MTAB_NMO | MTAB_VALR,                          /* Mask               */
      0,                                             /* Match              */
      (char *) "NUNITS",                             /* Print string       */
      (char *) "NUNITS",                             /* Match string       */
      scu_set_nunits,                                /* Validation routine */
      scu_show_nunits,                               /* Display routine    */
      (char *) "Number of SCU units in the system",  /* Value descriptor   */
      NULL                                           /* Help               */
    },
    {
      MTAB_XTD | MTAB_VUN | \
      MTAB_NMO | MTAB_VALR,                          /* Mask               */
      0,                                             /* Match              */
      (char *) "STATE",                              /* Print string       */
      (char *) "STATE",                              /* Match string       */
      NULL,                                          /* Validation routine */
      scu_show_state,                                /* Display routine    */
      (char *) "SCU unit internal state",            /* Value descriptor   */
      NULL                                           /* Help               */
    },
    {
      MTAB_XTD | MTAB_VUN | \
      MTAB_NMO | MTAB_VALR,                          /* Mask               */
      0,                                             /* Match              */
      (char *) "RESET",                              /* Print string       */
      (char *) "RESET",                              /* Match string       */
      scuSimResetUnit,                                /* Validation routine */
      NULL,                                          /* Display routine    */
      (char *) "reset SCU unit",                     /* Value descriptor   */
      NULL                                           /* Help               */
    },
    {
      0, 0, NULL, NULL, NULL, NULL, NULL, NULL
    }
  };

DEVICE scuDev =
  {
    (char *) "SCU",  /* Name                */
    scuUnits,        /* Units               */
    NULL,            /* Registers           */
    scu_mod,         /* Modifiers           */
    N_SCU_UNITS,     /* #Units              */
    10,              /* Address radix       */
    8,               /* Address width       */
    1,               /* Address increment   */
    8,               /* Data radix          */
    8,               /* Data width          */
    NULL,            /* Examine routine     */
    NULL,            /* Deposit routine     */
    & scuReset,     /* Reset routine       */
    NULL,            /* Boot routine        */
    NULL,            /* Attach routine      */
    NULL,            /* Detach routine      */
    NULL,            /* Context             */
    0,               /* Flags               */
    0,               /* Debug control flags */
    NULL,            /* Debug flag names    */
    NULL,            /* Memory size change  */
    NULL,            /* Logical name        */
    NULL,            /* Help                */
    NULL,            /* Attach_help         */
    NULL,            /* Help_ctx            */
    NULL,            /* Description         */
    NULL             /* End                 */
  };

void scuResetUnit (int scuUnitIdx)
  {
    scu_t * up = scu + scuUnitIdx;
    scuConfigSwitches_t * sw = scuConfigSwitches + scuUnitIdx;

    up->mode = sw->mode;
    for (int i = 0; i < N_SCU_PORTS; i ++)
      {
        up -> portEnable [i] = sw -> portEnable [i];
      }

    for (int i = 0; i < N_ASSIGNMENTS; i ++)
      {
        up -> maskEnable [i]     = sw -> maskEnable [i];
        up -> maskAssignment [i] = sw -> maskAssignment [i];
      }
    up -> lowerStoreSize = sw -> lowerStoreSize;
    up -> cyclic           = sw -> cyclic;
    up -> nea              = sw -> nea;
    up -> online              = sw -> online;
    up -> interlace        = sw -> interlace;
    up -> lwr              = sw -> lwr;

// This is to allow the CPU reset to update the memory map. IAC clears the
// attached SCUs; they clear the attached IOMs.

    for (uint port_num = 0; port_num < N_SCU_PORTS; port_num ++)
      {
        struct ports * portp = & scu [scuUnitIdx].ports [port_num];
        if (portp->type != ADEV_IOM)
          continue;
        //if (! scu [scuUnitIdx].portEnable [scuPortNum])
          //continue;
        iom_unit_reset_idx ((uint) portp->devIdx);
      }

// CAC - These settings were reversed engineer from the code instead
// of from the documentation. In case of issues, try fixing these, not the
// code.

    for (int i = 0; i < N_ASSIGNMENTS; i ++)
      {
        // XXX Hack for t4d
        up -> execIntrMask [i] = 037777777777;
      }
  }

simRc scuReset (UNUSED DEVICE * dptr)
  {
    // On reset, instantiate the config switch settings

    for (int scuUnitIdx = 0; scuUnitIdx < N_SCUS; scuUnitIdx ++)
      scuResetUnit (scuUnitIdx);
    return SCPE_OK;
  }

// ============================================================================

static pthread_mutex_t clock_lock = PTHREAD_MUTEX_INITIALIZER;

// The SCU clock is 52 bits long; fits in uint64_t
static uint64_t set_SCU_clock (cpuState_t * cpup, uint scuUnitIdx)
  {
    pthread_mutex_lock (& clock_lock);

// The emulator supports two clock models: steady and real
// In steady mode the time of day is coupled to the instruction clock,
// allowing reproducible behavior. In real, the clock is
// coupled to the actual time-of-day.

    if (scu [0].steadyClock)
      {
        // The is a bit of code that is waiting for 5000 ms; this
        // fools into going faster
#ifdef NEED_128
        uint128 big = construct_128 (0, cpu.instrCnt);
        // Sync up the clock and the TR; see wiki page "CAC 08-Oct-2014"
        //big *= 4u;
        big = lshift_128 (big, 2);
        //big += scu [0].elapsedDays * 1000000llu * 60llu * 60llu * 24llu;
        uint128 days = construct_128 (0, scu[0].elapsedDays);
        days         = multiply_128 (days, construct_128 (0, 1000000));
        days         = multiply_128 (days, construct_128 (0, 60 * 60 * 24));
        big          = add_128 (big, days);
#else
        __uint128_t big = cpu.instrCnt;
        // Sync up the clock and the TR; see wiki page "CAC 08-Oct-2014"
        big *= 4u;
        //big /= 100u;
        big += scu [0].elapsedDays * INT64_C (1000000u) * INT64_C (60u) * INT64_C (60u) * INT64_C (24u);
#endif

        // Boot time

// load_fnp is complaining that FNP core image is more than 5 years old; try
// moving the 'boot time' back to MR12.3 release date. (12/89 according to
// https://www.multicians.org/chrono.html

        // date -d "1990-01-01 00:00:00 -9" +%s
        // 631184400
        // For debugging MR12.3 and earlier with steadyClock, uncomment --
        // uint64_t UNIX_secs = 631184400;

        // Otherwise, we'll use the current time as the steadyClock starting point --
        uint64_t UNIX_secs = (uint64_t)time (NULL);

#ifdef NEED_128
        uint64_t UNIX_usecs = UNIX_secs * INT64_C (1000000u) + big.l;
#else
        uint64_t UNIX_usecs = UNIX_secs * INT64_C (1000000u) + (uint64_t) big;
#endif
        // now determine uSecs since Jan 1, 1901 ...
        uint64_t Multics_usecs = INT64_C (2177452800000000u) + UNIX_usecs;

        // The casting to uint show be okay; both are 64 bit, so if
        // userCorrection is <0, it will come out in the wash ok.
        Multics_usecs += (uint64_t) scu [scuUnitIdx].userCorrection;

        // The get calendar clock function is guaranteed to return
        // different values on successive calls.

        if (scu [scuUnitIdx].lastTime >= Multics_usecs)
          {
            Multics_usecs = scu [scuUnitIdx].lastTime + 1;
          }
        scu [scuUnitIdx].lastTime = Multics_usecs;
        goto done;
      }

    // The calendar clock consists of a 52-bit register which counts
    // microseconds and is readable as a double-precision integer by a
    // single instruction from any central processor. This rate is in
    // the same order of magnitude as the instruction processing rate of
    // the GE-645, so that timing of 10-instruction subroutines is
    // meaningful. The register is wide enough that overflow requires
    // several tens of years; thus it serves as a calendar containing
    // the number of microseconds since 0000 GMT, January 1, 1901
    ///  Secs from Jan 1, 1901 to Jan 1, 1970 - 2 177 452 800
    //   Seconds
    /// uSecs from Jan 1, 1901 to Jan 1, 1970 - 2 177 452 800 000 000
    //  uSeconds

    struct timeval now;
    gettimeofday (& now, NULL);

    if (scu [0].y2k) // Apply clock skew when Y2K mode enabled
      {
        // Back the clock up to just after the MR12.5 release
        // $ date --date='30 years ago' +%s ; date +%s
        // 1685451324
        // 7738766524
        now.tv_sec -= (1685451324 - 738766524); // XXX (jhj): make dynamic!
      }
    uint64_t UNIX_secs = (uint64_t) now.tv_sec;
    uint64_t UNIX_usecs = UNIX_secs * INT64_C (1000000u) + (uint64_t) now.tv_usec;

    static uint64_t last_UNIX_usecs = 0;
    if (UNIX_usecs < last_UNIX_usecs)
      {
        if (gtodWarnCnt < 11)
          {
            simWarn ("\nHost clock went backwards %llu uS!\n",
                      (unsigned long long) (last_UNIX_usecs - UNIX_usecs));
          }
        else if (gtodWarnCnt == 11)
          {
            simWarn ("\nHost clock went backwards %llu uS!  Suppressing further warnings.\n",
                      (unsigned long long) (last_UNIX_usecs - UNIX_usecs));
          }
        gtodWarnCnt ++;
      }
    last_UNIX_usecs = UNIX_usecs;

    // now determine uSecs since Jan 1, 1901 ...
    uint64_t Multics_usecs = INT64_C (2177452800000000u) + UNIX_usecs;

    // Correction factor from the set time command

    // The casting to uint show be okay; both are 64 bit, so if
    // userCorrection is <0, it will come out in the wash ok.
    Multics_usecs += (uint64_t) scu [scuUnitIdx].userCorrection;

    if (scu [scuUnitIdx].lastTime >= Multics_usecs)
        Multics_usecs = scu [scuUnitIdx].lastTime + 1;
    scu [scuUnitIdx].lastTime = Multics_usecs;

done:
    pthread_mutex_unlock (& clock_lock);

    return scu [scuUnitIdx].lastTime;

  }

// Either an interrupt has arrived on a port, or a mask register has
// been updated. Bring the CPU up date on the interrupts.

// threadz notes:
//
// deliver_interrupts is called either from a CPU instruction or from
// IOM set_general_interrupt on the IOM thread.
//
// potential race conditions:
//   CPU variables: XIP
//   SCU variables: cells, maskEnable, execIntrMask, mask assignment

// Always called with SCU lock set

static void deliver_interrupts (cpuState_t * cpup, uint scuUnitIdx)
  {
    for (uint cpun = 0; cpun < cpu_dev.numunits; cpun ++)
      {
        cpus[cpun].events.XIP[scuUnitIdx] = false;
      }

// If the CIOC generates marker and terminate interrupts, they will be posted simultaneously.
// Since the interrupts are recognized by priority and terminate has a higher priority then
// marker, if will be delivered first. The following code will deliver marker before terminate.

#ifdef REORDER
    for (uint jnum = 0; jnum < N_CELL_INTERRUPTS; jnum ++)
      {
        static const uint reorder[N_CELL_INTERRUPTS] = {
           0,  1,  2,  3,  4,  5,  6,  7,
          16, 17, 18, 29, 20, 21, 22, 23,
           8,  9, 10, 11, 12, 13, 14, 15,
          25, 25, 26, 27, 28, 29, 30, 31 };
        uint inum = reorder[jnum];
        if (! scu [scuUnitIdx].cells [inum])
          continue; //
        for (uint pima = 0; pima < N_ASSIGNMENTS; pima ++) // A, B
          {
            if (scu [scuUnitIdx].maskEnable [pima] == 0)
              continue;
            uint mask = scu [scuUnitIdx].execIntrMask [pima];
            uint port = scu [scuUnitIdx].maskAssignment [pima];
            if (scu [scuUnitIdx].ports [port].type != ADEV_CPU)
              continue;
            if ((mask & (1u << (31 - inum))) != 0)
              {
                uint sn = 0;
                if (scu[scuUnitIdx].ports[port].isExpansionPort)
                  {
                    sn = (uint) scu[scuUnitIdx].ports[port].xipmaskval;
                    if (sn >= N_SCU_SUBPORTS)
                      {
                        simWarn ("XIP mask not set; defaulting to subport 0\n");
                        sn = 0;
                      }
                  }
                if (! cables->scu_to_cpu[scuUnitIdx][port][sn].socket_in_use)
                  {
                    simWarn ("bad scuUnitIdx %u\n", scuUnitIdx);
                    continue;
                  }
                uint cpuUnitIdx = cables->scu_to_cpu[scuUnitIdx][port][sn].cpuUnitIdx;

                // Need to do this before setting XIP to avoid race condition
                // We are about to interrupt a CPU; this is done to either
                //   Multics signalling, such has CAM cache clear
                //   Adding a new CPU
                //   Readding a deleted CPU
                //   Starting an ISOLTS CPU
                // If it is a Multics signal, the target CPU will be marked as 'inMultics';
                // For the other cases, 'inMultics ' will not be set.
                // cpup != NULL means that this interrupt was generated by a CPU, rather
                // than an IOM; we need this to set the calling thread into slave mode.
                if ((! sys.sys_opts.nosync) && cpup && (! cpus[cpuUnitIdx].inMultics)) {
                  // The interrupt is to start or restart a CPU.
# ifdef SYNCTEST
                  simPrintf ("CPU %c becomes clock master\n", 'A' + cpuUnitIdx);
# endif
                  becomeClockMaster (& cpus[cpuUnitIdx]);
                  // Set this so that the calling thread will join the queue immediately.
                  cpu.syncClockModePoll = 0;
                  cpu.syncClockModeCache = true;
                }
                cpus[cpuUnitIdx].events.XIP[scuUnitIdx] = true;
                __asm volatile ("");
                atomic_thread_fence (memory_order_seq_cst);
                HDBGIntrSet (cpup, inum, cpuUnitIdx, scuUnitIdx, __func__);
                createCPUThread ((uint) cpuUnitIdx);
                wakeCPU ((uint) cpuUnitIdx);
              }
          }
      }
#else // !REORDER
    for (uint inum = 0; inum < N_CELL_INTERRUPTS; inum ++)
      {
        if (! scu [scuUnitIdx].cells [inum])
          continue; //
        for (uint pima = 0; pima < N_ASSIGNMENTS; pima ++) // A, B
          {
            if (scu [scuUnitIdx].maskEnable [pima] == 0)
              continue;
            uint mask = scu [scuUnitIdx].execIntrMask [pima];
            uint port = scu [scuUnitIdx].maskAssignment [pima];
            if (scu [scuUnitIdx].ports [port].type != ADEV_CPU)
              continue;
            if ((mask & (1u << (31 - inum))) != 0)
              {
                uint sn = 0;
                if (scu[scuUnitIdx].ports[port].isExpansionPort)
                  {
                    sn = (uint) scu[scuUnitIdx].ports[port].xipmaskval;
                    if (sn >= N_SCU_SUBPORTS)
                      {
                        simWarn ("XIP mask not set; defaulting to subport 0\n");
                        sn = 0;
                      }
                  }
                if (! cables->scu_to_cpu[scuUnitIdx][port][sn].socket_in_use)
                  {
                    simWarn ("bad scuUnitIdx %u\n", scuUnitIdx);
                    continue;
                  }
                uint cpuUnitIdx = cables->scu_to_cpu[scuUnitIdx][port][sn].cpuUnitIdx;
                // Need to do this before setting XIP to avoid race condition
                // We are about to interrupt a CPU; this is done to either
                //   Multics signalling, such has CAM cache clear
                //   Adding a new CPU
                //   Readding a deleted CPU
                //   Starting an ISOLTS CPU
                // If it is a Multics signal, the target CPU will be marked as 'inMultics';
                // For the other cases, 'inMultics ' will not be set.
                // cpup != NULL means that this interrupt was generated by a CPU, rather
                // than an IOM; we need this to set the calling thread into slave mode.
                if ((! sys.sys_opts.nosync) && cpup && (! cpus[cpuUnitIdx].inMultics)) {
                  // The interrupt is to start or restart a CPU.
# ifdef SYNCTEST
                  simPrintf ("CPU %c becomes clock master\n", 'A' + cpuUnitIdx);
# endif
                  becomeClockMaster (& cpus[cpuUnitIdx]);
                }
                cpus[cpuUnitIdx].events.XIP[scuUnitIdx] = true;
                HDBGIntrSet (&cpus[cpuUnitIdx], inum, cpuUnitIdx, scuUnitIdx, __func__);

                createCPUThread ((uint) cpuUnitIdx);
                wakeCPU ((uint) cpuUnitIdx);
              }
          }
      }
#endif // REORDER
    //atomic_thread_fence (memory_order_release);
  }

simRc scuSMIC (cpuState_t * cpup, uint scuUnitIdx, uint UNUSED cpuUnitIdx,
                 uint UNUSED cpuPortNum, word36 rega)
  {
    lock_scu ();
// smic can set cells but not reset them...
#if 1
    if (getbits36_1 (rega, 35))
      {
        for (uint i = 0; i < 16; i ++)
          {
            if (getbits36_1 (rega, i)) {
              scu [scuUnitIdx].cells [i + 16] = 1;
              HDBGIntrCellSet (cpup, scuUnitIdx, i + 16, "smic");
            }
          }
      }
    else
      {
        for (uint i = 0; i < 16; i ++)
          {
            if (getbits36_1 (rega, i)) {
              scu [scuUnitIdx].cells [i] = 1;
              HDBGIntrCellSet (cpup, scuUnitIdx, i, "smic");
            }
          }
      }
#else
    if (getbits36_1 (rega, 35))
      {
        for (uint i = 0; i < 16; i ++)
          {
            scu [scuUnitIdx].cells [i + 16] = getbits36_1 (rega, i) ? 1 : 0;
          }
      }
    else
      {
        for (uint i = 0; i < 16; i ++)
          {
            scu [scuUnitIdx].cells [i] =
              getbits36_1 (rega, i) ? 1 : 0;
          }
      }
#endif
    deliver_interrupts (cpup, scuUnitIdx);
    unlock_scu ();
    return SCPE_OK;
  }

// system controller and the function to be performed as follows:
//
//  Effective  Function
//  Address
//  y0000x     C(system controller mode register) -> C(AQ)
//  y0001x     C(system controller configuration switches) -> C(AQ)
//  y0002x     C(mask register assigned to port 0) -> C(AQ)
//  y0012x     C(mask register assigned to port 1) -> C(AQ)
//  y0022x     C(mask register assigned to port 2) -> C(AQ)
//  y0032x     C(mask register assigned to port 3) -> C(AQ)
//  y0042x     C(mask register assigned to port 4) -> C(AQ)
//  y0052x     C(mask register assigned to port 5) -> C(AQ)
//  y0062x     C(mask register assigned to port 6) -> C(AQ)
//  y0072x     C(mask register assigned to port 7) -> C(AQ)
//  y0003x     C(interrupt cells) -> C(AQ)
//
//  y0004x
//    or       C(calendar clock) -> C(AQ)
//  y0005x
//
//  y0006x
//    or C(store unit mode register) -> C(AQ)
//  y0007x
//
// where: y = value of C(TPR.CA)0,2 (C(TPR.CA)1,2 for the DPS 8M
// processor) used to select the system controller
// x = any octal digit
//

simRc scuSSCR (cpuState_t * cpup, uint scuUnitIdx, UNUSED uint cpuUnitIdx,
                 UNUSED uint cpuPortNum, word18 addr,
                 word36 rega, word36 regq)
  {
    // Only valid for a 4MW SCU

    if (scuUnitIdx >= scuDev.numunits)
      {
// XXX should this be a store fault?
        simWarn ("%s: scuUnitIdx out of range %d\n",
                   __func__, scuUnitIdx);
        return SCPE_OK;
      }

    // BCE uses clever addressing schemes to select SCUs; it appears we need
    // to be more selecting in picking out the function bits;
    //uint function = (addr >> 3) & 07777;
    uint function = (addr >> 3) & 07;

    // See scs.incl.pl1

    if (scu[scuUnitIdx].mode != MODE_PROGRAM)
      {
        simWarn ("%s: SCU mode is 'MANUAL', not 'PROGRAM' -- sscr "
                   "not allowed to set switches.\n",
                   __func__);
// XXX [CAC] Setting an unassigned register generates a STORE FAULT;
// this probably should as well
        return SCPE_OK;
      }

// Not used by 4MW

    switch (function)
      {
        case 00000: // Set system controller mode register
          {
            lock_scu ();
            scu [scuUnitIdx].id = (word4) getbits36_4 (regq, 50 - 36);
            scu [scuUnitIdx].modeReg = getbits36_18 (regq, 54 - 36);
            unlock_scu ();
          }
          break;

        case 00001: // Set system controller configuration register
                    // (4MW SCU only)
          {
            lock_scu ();
            scu_t * up = scu + scuUnitIdx;
            for (int maskab = 0; maskab < 2; maskab ++)
              {
                word9 mask = ((maskab ? regq : rega) >> 27) & 0777;
                if (mask & 01)
                  {
                    up -> maskEnable [maskab] = 0;
                  }
                else
                  {
                    up -> maskEnable [maskab] = 1;
                    for (int pn = 0; pn < N_SCU_PORTS; pn ++)
                      {
                        if ((2 << (N_SCU_PORTS - 1 - pn)) & mask)
                          {
                            up -> maskAssignment [maskab] = (uint) pn;
                            break;
                          }
                      }

                  }
              }
            // AN87-00A, pg 2-5, 2-6 specify which fields are and are not
            //  settable.

            //if (up -> lowerStoreSize != ((rega >> 24) & 07))
              //simPrintf ("??? The CPU tried to change the SCU store size\n");
            up -> lowerStoreSize = (rega >> 24) & 07;
            up -> cyclic           = (regq >>  8) & 0177;
            up -> nea              = (rega >>  6) & 0377;
            up -> online              = (rega >> 20) & 017;
            up -> interlace        = (rega >>  5) &  1;
            up -> lwr              = (rega >>  4) &  1;
            up -> portEnable [0]  = (rega >>  3) & 01;
            up -> portEnable [1]  = (rega >>  2) & 01;
            up -> portEnable [2]  = (rega >>  1) & 01;
            up -> portEnable [3]  = (rega >>  0) & 01;
            up -> portEnable [4]  = (regq >>  3) & 01;
            up -> portEnable [5]  = (regq >>  2) & 01;
            up -> portEnable [6]  = (regq >>  1) & 01;
            up -> portEnable [7]  = (regq >>  0) & 01;

            unlock_scu ();
            // XXX A, A1, B, B1, INT, LWR not implemented. (AG87-00A pgs 2-5,
            //  2-6)
            break;
          }

        case 00002: // Set mask register port 0
        //case 00012: // Set mask register port 1
        //case 00022: // Set mask register port 2
        //case 00032: // Set mask register port 3
        //case 00042: // Set mask register port 4
        //case 00052: // Set mask register port 5
        //case 00062: // Set mask register port 6
        //case 00072: // Set mask register port 7
          {
            lock_scu ();
            uint port_num = (addr >> 6) & 07;

            // Find mask reg assigned to specified port
            int mask_num = -1;
            uint n_masks_found = 0;
            for (int p = 0; p < N_ASSIGNMENTS; p ++)
              {
                //if (scup -> interrupts [p].mask_assign.unassigned)
                if (scu [scuUnitIdx].maskEnable [p] == 0)
                  continue;
                //if (scup -> interrupts [p].mask_assign.port == port_num)
                if (scu [scuUnitIdx ].maskAssignment [p] == port_num)
                  {
                    if (n_masks_found == 0)
                      mask_num = p;
                    n_masks_found ++;
                  }
              }

            if (! n_masks_found)
              {
// According to bootload_tape_label.alm, this condition is OK
                unlock_scu ();
                return SCPE_OK;
              }

            if (n_masks_found > 1)
              {
                // Not legal for Multics
                simWarn ("%s: Multiple masks assigned to cpu on port %u\n", __func__, port_num);
              }

            // See AN87
            //scup -> interrupts[mask_num].execIntrMask = 0;
            scu [scuUnitIdx].execIntrMask [mask_num] = 0;
            scu [scuUnitIdx].execIntrMask [mask_num] |=
              ((word32) getbits36_16 (rega, 0) << 16);
            scu [scuUnitIdx].execIntrMask [mask_num] |=
              getbits36_16 (regq, 0);
            scu [scuUnitIdx].maskEnable [mask_num] = 1;
            deliver_interrupts (cpup, scuUnitIdx);
            unlock_scu ();
          }
          break;

        case 00003: // Set interrupt cells
          {
            lock_scu ();
            for (uint i = 0; i < 16; i ++)
              {
                scu [scuUnitIdx].cells [i] =
                  getbits36_1 (rega, i) ? 1 : 0;
                scu [scuUnitIdx].cells [i + 16] =
                  getbits36_1 (regq, i) ? 1 : 0;
                if (getbits36_1 (rega, i))
                  HDBGIntrCellSet (cpup, scuUnitIdx, 16, "sscr");
                if (getbits36_1 (regq, i))
                  HDBGIntrCellSet (cpup, scuUnitIdx, i + 16, "sscr");
                  
              }
            deliver_interrupts (NULL, scuUnitIdx);
            unlock_scu ();
          }
          break;

        case 00004: // Set calendar clock (4MW SCU only)
        case 00005:
          {
            // AQ: 20-35 clock bits 0-15, 36-71 clock bits 16-51
            word16 b0_15   = (word16) getbits36_16 (cpu.rA, 20);
            word36 b16_51  = cpu.rQ;
            uint64_t new_clk = (((uint64_t) b0_15) << 36) | b16_51;
            lock_scu ();
            scu [scuUnitIdx].userCorrection =
              (int64_t) (new_clk - set_SCU_clock (cpup, scuUnitIdx));
            unlock_scu ();
            //simPrintf ("sscr %o\n", function);
          }
          break;

        case 00006: // Set unit mode register
        case 00007:
          // ticket 34
          // XXX See notes in AL39 sscr re: store unit selection
          // XXX compare rscr
          //simPrintf ("sscr %o\n", function);
          simWarn ("sscr set unit mode register\n");
          //return STOP_UNIMP;
          return SCPE_OK;

        default:
          simWarn ("sscr unhandled code\n");
          //return STOP_UNIMP;
          return SCPE_OK;
          //simPrintf ("sscr %o\n", function);
      }
    return SCPE_OK;
  }

simRc scuRSCR (cpuState_t * cpup, uint scuUnitIdx, uint cpuUnitIdx, word18 addr,
                 word36 * rega, word36 * regq)
  {
    // Only valid for a 4MW SCU

    if (scuUnitIdx >= scuDev.numunits)
      {
        simWarn ("%s: scuUnitIdx out of range %d\n",
                   __func__, scuUnitIdx);
        return SCPE_OK;
      }

    // BCE uses clever addressing schemes to select SCUs; it appears we need
    // to be more selecting in picking out the function bits;
    //uint function = (addr >> 3) & 07777;
    uint function = (addr >> 3) & 07;

    //simPrintf ("rscr %o\n", function);

    // See scs.incl.pl1

    switch (function)
      {
        case 00000: // Read system controller mode register
          {
            // AN-87
            // 0..0 -> A
            // 0..0 -> Q 36-49 (0-13)
            // ID -> Q 50-53 (14-17)
            // MODE REG -> Q 54-71 (18-35)
            //
            //  ID: 0000  8034, 8035
            //      0001  Level 68 SC
            //      0010  Level 66 SCU
            // CAC: According to scr.incl.pl1. 0010 is a 4MW SCU
            // MODE REG: these fields are only used by T&D
            * rega = 0;
            //* regq = 0000002000000; // ID = 0010
            * regq = 0;
            lock_scu ();
            putbits36_4 (regq, 50 - 36, scu [scuUnitIdx].id);
            putbits36_18 (regq, 54 - 36, scu [scuUnitIdx].modeReg);
            unlock_scu ();
            break;
          }

        case 00001: // Read system controller configuration register
          {
            // AN-87, scr.incl.pl1
            //
            // SCU:
            // reg A:
            //   MASK A | SIZE | A | A1 | B | B1 | PORT | 0 | MOD | NEA |
            //   INT | LWR | PMR 0-3
            // reg Q:
            //   MASK B | not used | CYCLIC PRIOR | not used | PMR 4-7
            //
            //   MASK A/B (9 bits): EIMA switch setting for mask A/B. The
            //    assigned port corresponds to the but position within the
            //    field. A bit in position 9 indicates that the mask is
            //    not assigned.
            // From scr.incl.pl1:
            // 400 => assigned to port 0
            //  .
            //  .
            // 002 => assigned to port 7
            // 001 => mask off */

            //
            //  SIZE (3 bits): Size of lower store
            //    000 = 32K ... 111 = 4M
            //
            //  A A1 B B1 (1 bit): store unit A/A1/B/B1 online
            //
            //  PORT (4 bits): Port number of the SCU port through which
            //    the RSCR instruction was received
            //
            //scuConfigSwitches_t * sw = scuConfigSwitches + scuUnitIdx;

            lock_scu ();
            scu_t * up = scu + scuUnitIdx;
            word9 maskab [2];
            for (int i = 0; i < 2; i ++)
              {
                if (up -> maskEnable [i])
                  {
                    maskab [i] = (2 << (N_SCU_PORTS - 1 -
                                        up -> maskAssignment [i])) & 0777;
                  }
                else
                  maskab [i] = 0001;
              }

            int scuPortNum = -1; // The port that the rscr instruction was
                                   // received on

            for (int pn = 0; pn < N_SCU_PORTS; pn ++)
              {
                for (int sn = 0; sn < N_SCU_SUBPORTS; sn ++)
                  {
                    if (cables->scu_to_cpu[scuUnitIdx][pn][sn].socket_in_use &&
                        cables->scu_to_cpu[scuUnitIdx][pn][sn].cpuUnitIdx ==
                          cpuUnitIdx)
                     {
                        scuPortNum = pn;
                        goto gotit;
                      }
                  }
              }
gotit:;
            if (scuPortNum < 0)
              {
                unlock_scu ();
                simWarn ("%s: can't find cpu port in the snarl of cables; "
                           "scu_unit_no %d, cpuUnitIdx %d\n",
                           __func__, scuUnitIdx, cpuUnitIdx);
                return SCPE_OK;
              }

            // AN87, pg 2-5
            word36 a, q;

            a = 0;
// (data, starting bit position, number of bits, value)
            putbits36_9 (& a,  0,  maskab [0]);
            putbits36_3 (& a,  9,  (word3) up -> lowerStoreSize);
            putbits36_4 (& a, 12,  (word4) up -> online); // A, A1, B, B1 online
            putbits36_4 (& a, 16,  (word4) scuPortNum);
            putbits36_1 (& a, 21,  (word1) scuConfigSwitches[scuUnitIdx].mode);
            putbits36_8 (& a, 22,  (word8) up -> nea);
            putbits36_1 (& a, 30,  (word1) up -> interlace);
            putbits36_1 (& a, 31,  (word1) up -> lwr);
            // XXX INT, LWR not implemented. (AG87-00A pgs 2-5. 2-6)
            // interlace <- 0
            // lower <- 0
            // Looking at scr_util.list, I *think* the port order
            // 0,1,2,3.
            putbits36_1 (& a, 32,  (word1) up -> portEnable [0]);
            putbits36_1 (& a, 33,  (word1) up -> portEnable [1]);
            putbits36_1 (& a, 34,  (word1) up -> portEnable [2]);
            putbits36_1 (& a, 35,  (word1) up -> portEnable [3]);
            * rega = a;

            q = 0;
            putbits36_9 (& q,  0,  maskab [1]);
            // cyclic prior <- 0
            putbits36_7 (& q, 57-36, (word7) up -> cyclic & MASK7);
            // Looking at scr_util.list, I *think* the port order
            // 0,1,2,3.
            putbits36_1 (& q, 32,  (word1) up -> portEnable [4]);
            putbits36_1 (& q, 33,  (word1) up -> portEnable [5]);
            putbits36_1 (& q, 34,  (word1) up -> portEnable [6]);
            putbits36_1 (& q, 35,  (word1) up -> portEnable [7]);
            * regq = q;

            unlock_scu ();
            break;
          }

        case 00002: // mask register
          {
            uint port_num = (addr >> 6) & MASK3;
            lock_scu ();
            scu_t * up = scu + scuUnitIdx;
            uint mask_contents = 0;
            if (up -> maskAssignment [0] == port_num)
              {
                mask_contents = up -> execIntrMask [0];
              }
            else if (up -> maskAssignment [1] == port_num)
              {
                mask_contents = up -> execIntrMask [1];
              }
            mask_contents &= MASK32;

            * rega = 0;
            putbits36 (rega,  0, 16, (mask_contents >> 16) & MASK16);
            putbits36 (rega, 32,  1, up -> portEnable [0]);
            putbits36 (rega, 33,  1, up -> portEnable [1]);
            putbits36 (rega, 34,  1, up -> portEnable [2]);
            putbits36 (rega, 35,  1, up -> portEnable [3]);

            * regq = 0;
            putbits36 (rega,  0, 16, (mask_contents >>  0) & MASK16);
            putbits36 (regq, 32,  1, up -> portEnable [4]);
            putbits36 (regq, 33,  1, up -> portEnable [5]);
            putbits36 (regq, 34,  1, up -> portEnable [6]);
            putbits36 (regq, 35,  1, up -> portEnable [7]);

            unlock_scu ();
          }
          break;

        case 00003: // Interrupt cells
          {
            lock_scu ();
            scu_t * up = scu + scuUnitIdx;
            // * rega = up -> execIntrMask [0];
            // * regq = up -> execIntrMask [1];
            for (uint i = 0; i < N_CELL_INTERRUPTS; i ++)
              {
                word1 cell = up -> cells [i] ? 1 : 0;
                if (i < 16)
                  putbits36_1 (rega, i, cell);
                else
                  putbits36_1 (regq, i - 16, cell);
              }
            unlock_scu ();
          }
          break;

        case 00004: // Get calendar clock (4MW SCU only)
        case 00005:
          {
            uint64_t clk = set_SCU_clock (cpup, scuUnitIdx);
            cpu.rQ =  clk  & 0777777777777;    // lower 36-bits of clock
            cpu.rA = (clk >> 36) & 0177777;    // upper 16-bits of clock
            HDBGRegAW (cpup, "rscr get clock");
            HDBGRegQW (cpup, "rscr get clock");
          }
        break;

        case 00006: // SU Mode register
        case 00007: // SU Mode register
          {
            //simPrintf ("rscr SU Mode Register%o\n", function);

// Completely undocumented...
//   scr.incl.alm
//"         Structure scr_su
//"
//          equ       scr_su_size,2
//
//
//          equ       scr_su.ZAC_line_word,1
//          equ       scr_su.ZAC_line_shift,30
//          bool      scr_su.ZAC_line_mask,000077
//          equ       scr_su.syndrome_word,1
//          equ       scr_su.syndrome_shift,22
//          bool      scr_su.syndrome_mask,000377
//          equ       scr_su.identification_word,1
//          equ       scr_su.identification_shift,18
//          bool      scr_su.identification_mask,000017
//          equ       scr_su.EDAC_disabled_word,1
//          bool      scr_su.EDAC_disabled,400000   " DL
//          equ       scr_su.MINUS_5_VOLT_margin_word,1
//"         equ       scr_su.MINUS_5_VOLT_margin_shift,11
//          bool      scr_su.MINUS_5_VOLT_margin_mask,000003
//          equ       scr_su.PLUS_5_VOLT_margin_word,1
//          equ       scr_su.PLUS_5_VOLT_margin_shift,9
//          bool      scr_su.PLUS_5_VOLT_margin_mask,000003
//          equ       scr_su.spare_margin_word,1
//          equ       scr_su.spare_margin_shift,7
//          bool      scr_su.spare_margin_mask,000003
//          equ       scr_su.PLUS_19_VOLT_margin_word,1
//"         equ       scr_su.PLUS_19_VOLT_margin_shift,5
//          bool      scr_su.PLUS_19_VOLT_margin_mask,000003
//          equ       scr_su.SENSE_strobe_margin_word,1
//"         equ       scr_su.SENSE_strobe_margin_shift,2
//          bool      scr_su.SENSE_strobe_margin_mask,000003
//"         equ       scr_su.maint_functions_enabled_word,1
//          bool      scr_su.maint_functions_enabled,000001 " DL

//                 1   1      1   2    2    2       2     3   3        3  3
//   0     6       4   8      9   3    5    7       9     1   2        4  5
//  ------------------------------------------------------------------------------
//  | ZAC | synd | id | EDAC | 0 | -5 | +5 | spare | +19 | 0 | sense | 0 | maint |
//  ------------------------------------------------------------------------------
//       6      8    4      1   4    2    2      2      2   1       2   1       1

// Okay, it looks safe to return 0.

            * rega = 0;
            * regq = 0;
          }
          break;

        default:
          simWarn ("rscr %o\n", function);
          return SCPE_OK;
      }
    return SCPE_OK;
  }

#if 0
struct timespec cioc_t0;
#endif

int scuCIOC (cpuState_t * cpup, uint cpuUnitIdx, uint scuUnitIdx, uint scuPortNum, uint expanderCommand, uint subMask) {
  lock_scu ();
  struct ports * portp = & scu [scuUnitIdx].ports [scuPortNum];

  int rc = 0;
  if (! scu [scuUnitIdx].portEnable [scuPortNum]) {
    rc = 1;
    goto done;
  }

  if (expanderCommand == 1) { // "set subport enables"
    for (uint i = 0; i < N_SCU_SUBPORTS; i++) {
      portp->subportEnables [i] = !! (subMask & (0200u >> i));
    }
    goto done;
  }

  if (expanderCommand == 2) { // "set xipmask"
    int cnt = 0;
    int val = -1;
    for (uint i = 0; i < N_SCU_SUBPORTS; i++) {
      portp->xipmask [i] = !! (subMask & (0200u >> i));
      if (portp->xipmask [i]) {
        val = (int) i;
        cnt ++;
      }
    }
    if (cnt > 1) {
      simWarn ("xip mask cnt > 1\n");
      val = -1;
    }
    portp->xipmaskval = val;
    goto done;
  }

  if (portp -> type == ADEV_IOM) {
    int iom_unit_idx = portp->devIdx;
    unlock_scu ();
# if !defined(IO_ASYNC_PAYLOAD_CHAN) && !defined(IO_ASYNC_PAYLOAD_CHAN_THREAD)
    lock_iom ();
    lock_libuv ();
# endif
    iom_interrupt (scuUnitIdx, (uint) iom_unit_idx);
# if !defined(IO_ASYNC_PAYLOAD_CHAN) && !defined(IO_ASYNC_PAYLOAD_CHAN_THREAD)
    unlock_libuv ();
    unlock_iom ();
# endif
    return 0;
  } else if (portp -> type == ADEV_CPU) {

#if 1
    // by subportEnables
    if (portp->isExpansionPort) {
      for (uint sn = 0; sn < N_SCU_SUBPORTS; sn ++) {
        if (portp->subportEnables[sn]) {
          if (! cables-> scu_to_cpu[scuUnitIdx][scuPortNum][sn].socket_in_use) {
            simWarn ("Can't find CPU to interrupt\n");
            continue;
          }
          uint cpuUnitIdx = cables-> scu_to_cpu[scuUnitIdx][scuPortNum][sn].cpuUnitIdx;
          setG7fault (cpup, (uint) cpuUnitIdx, FAULT_CON, fst_zero);
        }
      }
    } else {
      if (! cables->scu_to_cpu[scuUnitIdx][scuPortNum][0].socket_in_use) {
        simWarn ("Can't find CPU to interrupt\n");
        rc = 1;
        goto done;
      }
      uint cpuUnitIdx = cables->scu_to_cpu[scuUnitIdx][scuPortNum][0].cpuUnitIdx;
      setG7fault (cpup, (uint) cpuUnitIdx, FAULT_CON, fst_zero);
    }
#else
    // by xipmaskval
    int cpuUnitIdx = -1;
    if (portp->isExpansionPort) {
      cpuUnitIdx = cables->cablesFromCpus[scuUnitIdx][scuPortNum][portp->xipmaskval].cpuUnitIdx;
    } else {
      cpuUnitIdx = cables ->cablesFromCpus[scuUnitIdx][scuPortNum][0].cpu_unit_udx;
    }
    if (cpuUnitIdx < 0) {
      simWarn ("Can't find CPU to interrupt\n");
      rc = 1;
      goto done;
    }
    setG7fault (cpup, cpuUnitIdx, FAULT_CON, (_fault_subtype) {.bits=0});
  #endif
    goto done;
  } else {
    rc = 1;
    goto done;
  }
done:
  unlock_scu ();
  return rc;
}

// =============================================================================

// The SXC (set execute cells) SCU command.

// From AN70:
//  It then generates a word with
// the <interrupt number>th bit set and sends this to the bootload
// SCU with the SC (set execute cells) SCU command.
//

int scuSetInterrupt (uint scuUnitIdx, uint inum)
  {
    if (inum >= N_CELL_INTERRUPTS)
      {
        simWarn ("%s: Bad interrupt number %u\n", __func__, inum);
        return 1;
      }

    lock_scu ();
    scu [scuUnitIdx].cells [inum] = 1;
    HDBGIntrCellSet (NULL, scuUnitIdx, inum, __func__);
    deliver_interrupts (NULL, scuUnitIdx);
    unlock_scu ();
    return 0;
}

// Scan a SCU for interrupts from highest to lowest. If an interrupt is
// present, clear it, update the interrupt state bits and return the fault
// pair address for the interrupt (2 * interrupt number). If no interrupt
// is present, return 1.
//

uint scuGetHighestInterrupt (cpuState_t * cpup, uint scuUnitIdx)
  {
    lock_scu ();
    // lower numbered cells have higher priority
    for (int inum = 0; inum < N_CELL_INTERRUPTS; inum ++)
      {
        for (uint pima = 0; pima < N_ASSIGNMENTS; pima ++) // A, B
          {
            if (scu [scuUnitIdx].maskEnable [pima] == 0)
              continue;
            uint mask = scu [scuUnitIdx].execIntrMask [pima];
            uint port = scu [scuUnitIdx].maskAssignment [pima];
//            if (scu [scuUnitIdx].ports [port].type != ADEV_CPU ||
//              scu [scuUnitIdx].ports [port].devIdx != cpu.cpuIdx)
            if (scu[scuUnitIdx].ports[port].type != ADEV_CPU ||
                cpus[cpu.cpuIdx].scu_port[scuUnitIdx] != port)
              continue;
            if (scu [scuUnitIdx].cells [inum] &&
                (mask & (1u << (31 - inum))) != 0)
              {
                scu [scuUnitIdx].cells [inum] = false;
                deliver_interrupts (NULL, scuUnitIdx);
                unlock_scu ();
                return (uint) inum * 2;
              }
          }
      }
    unlock_scu ();
    return 1;
  }

simRc scuSimResetUnit (UNIT * uptr, UNUSED int32_t value,
                       UNUSED const char * cptr,
                       UNUSED void * desc)
  {
    uint scuUnitIdx = (uint) (uptr - scuUnits);
    scuResetUnit ((int) scuUnitIdx);
    return SCPE_OK;
  }

void scuInit (void)
  {
    // One time only initializations

    for (int u = 0; u < N_SCUS; u ++)
      {
        for (int p = 0; p < N_SCU_PORTS; p ++)
          {
            for (int s = 0; s < N_SCU_SUBPORTS; s ++)
              {
                scu[u].ports[p].devicePort[s]        = -1;
                scu[u].ports[p].subportEnables[s] = false;
                scu[u].ports[p].xipmask[s]         = false;
                // Invalid value for detecting uninitialized XIP mask.
                scu[u].ports[p].xipmaskval         = N_SCU_SUBPORTS;
              }
            scu[u].ports[p].type   = ADEV_NONE;
            scu[u].ports[p].isExpansionPort = false;
          }

        //  ID: 0000  8034, 8035
        //      0001  Level 68 SC
        //      0010  Level 66 SCU
        scu [u].id           = 02l; // 0b0010
        scu [u].modeReg     = 0;   // used by T&D
        scu [u].elapsedDays = 0;
      }

  }

simRc scuRMCM (uint scuUnitIdx, uint cpuUnitIdx, word36 * rega,
                 word36 * regq)
  {
    scu_t * up = scu + scuUnitIdx;

    // Assume no mask register assigned
    * rega = 0;
    * regq = 0;

    // Which port is cpuUnitIdx connected to? (i.e. which port did the
    // command come in on?
    int scuPortNum = -1; // The port that the rscr instruction was
                           // received on

    for (int pn = 0; pn < N_SCU_PORTS; pn ++)
      {
        for (int sn = 0; sn < N_SCU_SUBPORTS; sn ++)
          {
            if (cables->scu_to_cpu[scuUnitIdx][pn][sn].socket_in_use &&
                cables->scu_to_cpu[scuUnitIdx][pn][sn].cpuUnitIdx ==
                  cpuUnitIdx)
              {
                scuPortNum = pn;
                goto gotit;
              }
          }
      }

gotit:;

    //simPrintf ("rmcm scuPortNum %d\n", scuPortNum);

    if (scuPortNum < 0)
      {
        simWarn ("%s: can't find cpu port in the snarl of cables; "
                  "scu_unit_no %d, cpuUnitIdx %d\n",
                  __func__, scuUnitIdx, cpuUnitIdx);
        // Non 4MWs do a store fault
        return SCPE_OK;
      }

    // A reg:
    //  0          15  16           31  32       35
    //    IER 0-15        00000000        PER 0-3
    // Q reg:
    //  0          15  16           31  32       35
    //    IER 16-32       00000000        PER 4-7

    lock_scu ();
    uint mask_contents = 0;
    if (up -> maskAssignment [0] == (uint) scuPortNum)
      {
        mask_contents = up -> execIntrMask [0];
      }
    else if (up -> maskAssignment [1] == (uint) scuPortNum)
      {
        mask_contents = up -> execIntrMask [1];
      }
    mask_contents &= MASK32;

    * rega = 0;
    putbits36_16 (rega,  0, (mask_contents >> 16) & MASK16);
    putbits36_1  (rega, 32,  (word1) up -> portEnable [0]);
    putbits36_1  (rega, 33,  (word1) up -> portEnable [1]);
    putbits36_1  (rega, 34,  (word1) up -> portEnable [2]);
    putbits36_1  (rega, 35,  (word1) up -> portEnable [3]);

    * regq = 0;
    putbits36_16 (regq,  0, (mask_contents >>  0) & MASK16);
    putbits36_1  (regq, 32,  (word1) up -> portEnable [4]);
    putbits36_1  (regq, 33,  (word1) up -> portEnable [5]);
    putbits36_1  (regq, 34,  (word1) up -> portEnable [6]);
    putbits36_1  (regq, 35,  (word1) up -> portEnable [7]);

    unlock_scu ();
    return SCPE_OK;
  }

simRc scuSMCM (uint scuUnitIdx, uint cpuUnitIdx, word36 rega, word36 regq)
  {
    scu_t * up = scu + scuUnitIdx;

    // Which port is cpuUnitIdx connected to? (i.e. which port did the
    // command come in on?
    int scuPortNum = -1; // The port that the rscr instruction was
                           // received on

    for (int pn = 0; pn < N_SCU_PORTS; pn ++)
      {
        for (int sn = 0; sn < N_SCU_SUBPORTS; sn ++)
          {
            if (cables->scu_to_cpu[scuUnitIdx][pn][sn].socket_in_use &&
                cables->scu_to_cpu[scuUnitIdx][pn][sn].cpuUnitIdx ==
                  cpuUnitIdx)
              {
                scuPortNum = pn;
                goto gotit;
              }
          }
      }
gotit:;

    //simPrintf ("rmcm scuPortNum %d\n", scuPortNum);

    if (scuPortNum < 0)
      {
        simWarn ("%s: can't find cpu port in the snarl of cables; "
                   "scu_unit_no %d, cpuUnitIdx %d\n",
                   __func__, scuUnitIdx, cpuUnitIdx);
        return SCPE_OK;
      }

    // A reg:
    //  0          15  16           31  32       35
    //    IER 0-15        00000000        PER 0-3
    // Q reg:
    //  0          15  16           31  32       35
    //    IER 16-32       00000000        PER 4-7

    uint imask =
      ((uint) getbits36_16 (rega, 0) << 16) |
      ((uint) getbits36_16 (regq, 0) <<  0);
    lock_scu ();
    if (up -> maskAssignment [0] == (uint) scuPortNum)
      {
        up -> execIntrMask [0] = imask;
      }
    else if (up -> maskAssignment [1] == (uint) scuPortNum)
      {
        up -> execIntrMask [1] = imask;
      }

    scu [scuUnitIdx].portEnable [0] = (uint) getbits36_1 (rega, 32);
    scu [scuUnitIdx].portEnable [1] = (uint) getbits36_1 (rega, 33);
    scu [scuUnitIdx].portEnable [2] = (uint) getbits36_1 (rega, 34);
    scu [scuUnitIdx].portEnable [3] = (uint) getbits36_1 (rega, 35);
    scu [scuUnitIdx].portEnable [4] = (uint) getbits36_1 (regq, 32);
    scu [scuUnitIdx].portEnable [5] = (uint) getbits36_1 (regq, 33);
    scu [scuUnitIdx].portEnable [6] = (uint) getbits36_1 (regq, 34);
    scu [scuUnitIdx].portEnable [7] = (uint) getbits36_1 (regq, 35);

    deliver_interrupts (NULL, scuUnitIdx);
    unlock_scu ();

    return SCPE_OK;
  }
