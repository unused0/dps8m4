/*
 * vim: filetype=c:tabstop=4:ai:expandtab
 * SPDX-License-Identifier: ICU
 * scspell-id: 3ceeac54-f62e-11ec-b6e7-80ee73e9b8e7
 *
 * ---------------------------------------------------------------------------
 *
 * Copyright (c) 2007-2013 Michael Mondy
 * Copyright (c) 2012-2016 Harry Reed
 * Copyright (c) 2013-2024 Charles Anthony
 * Copyright (c) 2021-2024 The DPS8M Development Team
 *
 * This software is made available under the terms of the ICU License.
 * See the LICENSE.md file at the top-level directory of this distribution.
 *
 * ---------------------------------------------------------------------------
 */

#if !defined(EM_CONSTS_H)
# define EM_CONSTS_H

////////////////
//
// System components: SCU, IOM, CPU
//
////////////////

// SCU
enum { N_SCU_UNITS_MAX = 8 };

// IOM
enum { N_IOM_UNITS_MAX = 4 };

// CPU
enum { N_CPU_UNITS_MAX = 8 };

////////////////
//
// Controllers
//
////////////////

enum { N_CTLRS = 26 }; // A-Z

// URP (Unit record processor)
enum { N_URP_UNITS_MAX = 16 };

// ABSI
enum { N_ABSI_UNITS_MAX = 1 };

// MGP (Chaosnet)
enum { N_MGP_UNITS_MAX =  2 };

// FNP
enum { N_FNP_UNITS_MAX = 16 };

// OPC (Operator console)
enum { N_OPC_UNITS_MAX =  8 };

// MTP -- Number of MTP controllers: MTPA..MTPZ; also tapa_nn..tapz_nn
enum { N_MTP_UNITS_MAX = 26 }; // MTPA..MTPZ

// MSP
enum { N_MSP_UNITS_MAX = 16 };

// IPCD Disk
enum { N_IPCD_UNITS_MAX = 16 };

// IPCT Tape
enum { N_IPCT_UNITS_MAX = 16 };

// DIA
enum { N_DIA_UNITS_MAX = 16 };

////////////////
//
// Peripherals
//
////////////////

enum { N_PRPHS = 26 }; // A-Z

// Tape drive   number of tape drives that can be attached to an MTP controller
enum { N_MT_UNITS_MAX  = 64 };

// Printer
enum { N_PRT_UNITS_MAX = 34 };

// Socket controller
enum { N_SKC_UNITS_MAX = 64 };

// Card reader
enum { N_RDR_UNITS_MAX = 16 };

// Card punch
enum { N_PUN_UNITS_MAX = 16 };

// Disk
enum { N_DSK_UNITS_MAX = 64 };

//
// Memory
//

enum { MEMSIZE = MEM_SIZE_MAX };

//
// Controller ports
//

enum { MAX_CTLR_PORTS = 8 };

//
// CPU ports
//

enum { N_DPS8M_CPU_PORTS = 4 };
enum { N_L68_CPU_PORTS   = 8 };
enum { N_CPU_PORTS       = 8 }; // For data structures, allocate the worst case and enforce limit at run-time

#endif // EM_CONSTS_H
