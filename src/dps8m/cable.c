/*
 * vim: filetype=c:tabstop=4:ai:expandtab
 * SPDX-License-Identifier: ICU
 * scspell-id: 571b2a19-f62d-11ec-b8e7-80ee73e9b8e7
 *
 * ---------------------------------------------------------------------------
 *
 * Copyright (c) 2013-2024 Charles Anthony
 * Copyright (c) 2021-2024 The DPS8M Development Team
 *
 * This software is made available under the terms of the ICU License.
 * See the LICENSE.md file at the top-level directory of this distribution.
 *
 * ---------------------------------------------------------------------------
 */

//  The cable command
//
//  This command is responsible for managing the interconnection of
//  major subsystems: CPU, SCU, IOM; controllers: MTP, IPC, MSP, URP;
//  peripherals:
//
//  The unit numbers for CPUs, IOMs, and SCUs (eg IOM3 is IOM unit 3) are
//  scp unit numbers; these units can can individually configured to
//  desired Multics unit numbers. However, it is somewhat easier to
//  adopt an one-to-one practice; IOM0 == IOMA, etc.
//
//
//   CABLE RIPOUT
//      Remove all cables from the configuration.
//
//   CABLE SHOW
//      Show the current cabling configuration.
//
//   CABLE DUMP
//      Show the current cabling configuration in great detail.
//
//   CABLE SCUi j IOMk l
//
//      Connect SCU i port j to IOM k port l.
//      "cable SCU0 0 IOM0 2"
//
//   CABLE SCUi j CPUk l
//
//      Connect SCU i port j to CPU k port l.
//      "cable SCU0 7 CPU0 7"
//
//   CABLE IOMi j MTPk
//   CABLE IOMi j MTPk l
//
//      Connect IOM i channel j to MTP k port l (l defaults to 0).
//
//   CABLE IOMi j MSPk
//   CABLE IOMi j MSPk l
//
//      Connect IOM i channel j to MSP k port l (l defaults to 0).
//
//   CABLE IOMi j IPCDk
//   CABLE IOMi j IPCDk l
//
//      Connect IOM i channel j to IPCID k port l (l defaults to 0).
//
//   CABLE IOMi j IPCTk
//   CABLE IOMi j IPCTk l
//
//      Connect IOM i channel j to IPCT k port l (l defaults to 0).
//
//   CABLE IOMi j OPCk
//
//      Connect IOM i channel j to OPC k.
//
//   CABLE IOMi j FNPk
//
//      Connect IOM i channel j to FNP k.
//
//   CABLE IOMi j DIAk
//
//      Connect IOM i channel j to DIA k.
//
//   CABLE IOMi j ABSIk
//
//      Connect IOM i channel j to ABSI k.
//
//   CABLE IOMi j MGPk
//
//      Connect IOM i channel j to MGP k.
//
//   CABLE IOMi j SKCk
//
//      Connect IOM i channel j to SKC k.
//
//   CABLE MTPi j TAPEk
//
//      Connect MTP i device code j to tape unit k.
//
//   CABLE IPCDi j DISKk
//
//      Connect IPCD i device code j to disk unit k.
//
//   CABLE IPCTi j DISKk
//
//      Connect IPCT i device code j to disk unit k.
//
//   CABLE MSPi j DISKk
//
//      Connect MSP i device code j to disk unit k.
//
//   CABLE URPi j RDRk
//
//      Connect URP i device code j to card reader unit k.
//
//   CABLE URPi j PUNk
//
//      Connect URP i device code j to card punch unit k.
//
//   CABLE URPi j PRTk
//
//      Connect URP i device code j to printer unit k.
//

#include <ctype.h>

#include "dps8m.h"
#include "iom_channel.h"
#include "iom.h"
#include "cmd.h"
#include "tap.h"
#include "socket_dev.h"
#include "sys.h"
#include "cable.h"
#include "faults.h"
#include "state.h"
#include "opc.h"
#include "ipc.h"
#include "dsk.h"
#include "fnp.h"
#include "dia.h"
#include "urp.h"
#include "crdrdr.h"
#include "crdpun.h"
#include "prt.h"
#include "utils.h"
#if !defined(__MINGW64__) && !defined(__MINGW32__) && !defined (CROSS_MINGW64) && !defined (CROSS_MINGW32)
# include "absi.h"
# include "mgp.h"
#endif /* if !defined(__MINGW64__) && !defined(__MINGW32__) && !defined (CROSS_MINGW64) && !defined (CROSS_MINGW32) */

# include <unistd.h>
# include "shm.h"

struct cables_s * cables = NULL;

char * ctlr_type_strs [/* enum ctlr_type_e */] =
  {
    "none",
    "MTP", "MSP", "IPCD", "IPCT", "OPC",
    "URP", "FNP", "ABSI", "SKC", "DIA", "MGP"
  };

char * chan_type_strs [/* enum chan_type_e */] =
  {
    "CPI", "PSI", "Direct"
  };

static simRc sys_cable_graph (void);

static int parseval (char * value)
  {
    if (! value)
      return -1;
    if (strlen (value) == 1 && value[0] >= 'a' && value[0] <= 'z')
      return (int) (value[0] - 'a');
    if (strlen (value) == 1 && value[0] >= 'A' && value[0] <= 'Z')
      return (int) (value[0] - 'A');
    char * endptr;
    long l = strtol (value, & endptr, 0);
    if (* endptr || l < 0 || l > INT_MAX)
      {
        simPrintf ("error: CABLE: can't parse %s\n", value);
        return -1;
      }
    return (int) l;
  }

static int getval (char * * save, char * text)
  {
    char * value;
    value = strtok_r (NULL, ", ", save);
    if (! value)
      {
        simPrintf ("error: CABLE: can't parse %s\n", text);
        return -1;
      }
    return parseval (value);
  }

// Match "FOO" with "FOOxxx" where xx is a decimal number or [A-Za-z]
//  "IOM" : IOM0 IOMB iom15 IOMc
// On match return value of number of mapped character (a = 0, b = 1, ...)
// On fail, return -1;

static bool name_match (const char * str, const char * pattern, uint * val)
  {
    // Does str match pattern?
    size_t pat_len = strlen (pattern);
    if (strncasecmp (pattern, str, pat_len))
      return false;

    // Isolate the string past the pattern
    size_t rest = strlen (str) - pat_len;
    const char * p = str + pat_len;

    // Can't be empty
    if (! rest)
      return false; // no tag

    // [A-Za-z]? XXX Assume a-z contiguous; won't work in EBCDIC
    char * q;
    char * tags = "abcdefghijklmnopqrstuvwxyz";
    if (rest == 1 && (q = strchr (tags, tolower (*p))))
      {
        * val = (uint) (q - tags);
        return true;
      }

    // Starts with a digit?
    char * digits = "0123456789";
    q = strchr (digits, tolower (*p));
    if (! q)
      return false; // start not a digit

    long l = strtol (p, & q, 0);
    if (* q || l < 0 || l > INT_MAX)
      {
        simPrintf ("error: sys_cable: can't parse %s\n", str);
        return false;
      }
    * val =  (uint) l;
    return true;
  }

// back cable SCUx port# IOMx port#

static simRc back_cable_iom_to_scu (int uncable, uint iom_unit_idx, uint iom_port_num, uint scuUnitIdx, uint scuPortNum)
  {
    struct iom_to_scu_s * p = & cables->iom_to_scu[iom_unit_idx][iom_port_num];
    if (uncable)
      {
        p->socket_in_use = false;
      }
    else
      {
        if (p->socket_in_use)
          {
            simPrintf ("cable SCU: IOM%u port %u in use.\n", iom_unit_idx, iom_port_num);
             return SCPE_ARG;
          }
        p->socket_in_use = true;
        p->scuUnitIdx = scuUnitIdx;
        p->scuPortNum = scuPortNum;
      }
    return SCPE_OK;
  }

// cable SCUx IOMx

static simRc cable_scu_to_iom (int uncable, uint scuUnitIdx, uint scuPortNum, uint iom_unit_idx, uint iom_port_num)
  {
    struct scu_to_iom_s * p = & cables->scu_to_iom[scuUnitIdx][scuPortNum];
    if (uncable)
      {
        if (! p->socket_in_use)
          {
            simPrintf ("uncable SCU%u port %d: not cabled\n", scuUnitIdx, scuPortNum);
            return SCPE_ARG;
          }

        // Unplug the other end of the cable
        simRc rc = back_cable_iom_to_scu (uncable, iom_unit_idx, iom_port_num,
                                  scuUnitIdx, scuPortNum);
        if (rc)
          {
            return rc;
          }

        p->socket_in_use = false;
        scu[scuUnitIdx].ports[scuPortNum].type    = ADEV_NONE;
        scu[scuUnitIdx].ports[scuPortNum].devIdx = 0;
        // XXX Is this wrong? is isExpansionPort supposed to be an accumulation of bits?
        // XXX Doesn't matter; Multics misbehaves if more than 2 CPUs are on a
        // XXX expander port, so this doesn't matter
        scu[scuUnitIdx].ports[scuPortNum].isExpansionPort  = false;
        //scu[scuUnitIdx].ports[scuPortNum].devicePort[scu_subport_num] = 0;
      }
    else
      {
        if (p->socket_in_use)
          {
            simPrintf ("cable_scu: SCU %d port %d in use.\n", scuUnitIdx, scuPortNum);
            return SCPE_ARG;
          }

        // Plug the other end of the cable in
        simRc rc = back_cable_iom_to_scu (uncable, iom_unit_idx, iom_port_num,
                                  scuUnitIdx, scuPortNum);
        if (rc)
          {
            return rc;
          }

        p->socket_in_use = true;
        p->iom_unit_idx = iom_unit_idx;
        p->iom_port_num = (uint) iom_port_num;

        scu[scuUnitIdx].ports[scuPortNum].type        = ADEV_IOM;
        scu[scuUnitIdx].ports[scuPortNum].devIdx     = (int) iom_unit_idx;
        scu[scuUnitIdx].ports[scuPortNum].devicePort[0] = (int) iom_port_num;
        // XXX Is this wrong? is isExpansionPort supposed to be an accumulation of bits?
        // XXX Doesn't matter; Multics misbehaves if more than 2 CPUs are on a
        // XXX expander port, so this doesn't matter
        scu[scuUnitIdx].ports[scuPortNum].isExpansionPort      = 0;
        //scu[scuUnitIdx].ports[scuPortNum].devicePort[scu_subport_num] = 0;
      }
    return SCPE_OK;
  }

// back cable SCUx port# CPUx port#

static simRc back_cable_cpu_to_scu (int uncable, uint cpuUnitIdx, uint cpuPortNum,
        uint scuUnitIdx, uint scuPortNum, uint scu_subport_num)
  {
    struct cpu_to_scu_s * p = & cables->cpu_to_scu[cpuUnitIdx][cpuPortNum];
    if (uncable)
      {
        p->socket_in_use = false;
      }
    else
      {
        if (p->socket_in_use)
          {
            simPrintf ("cable SCU: CPU%u port %u in use.\n", cpuUnitIdx, cpuPortNum);
             return SCPE_ARG;
          }
        p->socket_in_use = true;
        p->scuUnitIdx    = scuUnitIdx;
        p->scuPortNum    = scuPortNum;
        p->scu_subport_num = scu_subport_num;
      }
    return SCPE_OK;
  }

// cable SCUx CPUx

static simRc cable_scu_to_cpu (int uncable, uint scuUnitIdx, uint scuPortNum,
        uint scu_subport_num, uint cpuUnitIdx, uint cpuPortNum, bool isExpansionPort)
  {
    struct scu_to_cpu_s * p = & cables->scu_to_cpu[scuUnitIdx][scuPortNum][scu_subport_num];
    if (uncable)
      {
        if (! p->socket_in_use)
          {
            simPrintf ("uncable SCU%u port %u subport %u: not cabled\n",
                    scuUnitIdx, scuPortNum, scu_subport_num);
            return SCPE_ARG;
          }

        // Unplug the other end of the cable
        simRc rc = back_cable_cpu_to_scu (uncable, cpuUnitIdx, cpuPortNum,
                                  scuUnitIdx, scuPortNum, scu_subport_num);
        if (rc)
          {
            return rc;
          }

        p->socket_in_use = false;
        scu[scuUnitIdx].ports[scuPortNum].type                      = ADEV_NONE;
        scu[scuUnitIdx].ports[scuPortNum].devIdx                   = 0;
        // XXX Is this wrong? is isExpansionPort supposed to be an accumulation of bits?
        // XXX Doesn't matter; Multics misbehaves if more than 2 CPUs are on a
        // XXX expander port, so this doesn't matter
        scu[scuUnitIdx].ports[scuPortNum].isExpansionPort                    = false;
        scu[scuUnitIdx].ports[scuPortNum].devicePort[scu_subport_num] = 0;
      }
    else
      {
        if (p->socket_in_use)
          {
            simPrintf ("cable_scu: SCU %u port %u subport %u in use.\n",
                    scuUnitIdx, scuPortNum, scu_subport_num);
            return SCPE_ARG;
          }

        // Plug the other end of the cable in
        simRc rc = back_cable_cpu_to_scu (uncable, cpuUnitIdx, cpuPortNum,
                                  scuUnitIdx, scuPortNum, scu_subport_num);
        if (rc)
          {
            return rc;
          }

        p->socket_in_use = true;
        p->cpuUnitIdx = cpuUnitIdx;
        p->cpuPortNum = (uint) cpuPortNum;

        scu[scuUnitIdx].ports[scuPortNum].type                      = ADEV_CPU;
        scu[scuUnitIdx].ports[scuPortNum].devIdx                   = (int) cpuUnitIdx;
        scu[scuUnitIdx].ports[scuPortNum].devicePort[0]               = (int) cpuPortNum;
        // XXX Is this wrong? is isExpansionPort supposed to be an accumulation of bits?
        // XXX Doesn't matter; Multics misbehaves if more than 2 CPUs are on a
        // XXX expander port, so this doesn't matter
        scu[scuUnitIdx].ports[scuPortNum].isExpansionPort                    = isExpansionPort;
        scu[scuUnitIdx].ports[scuPortNum].devicePort[scu_subport_num] = (int) cpuPortNum;

        cpus[cpuUnitIdx].scu_port[scuUnitIdx]                       = scuPortNum;
      }
    // Taking this out breaks the unit test segment loader.
    setup_scbank_map (& cpus[cpuUnitIdx]);
    return SCPE_OK;
  }

// cable SCUx port# IOMx port#
// cable SCUx port# CPUx port#

static simRc cable_scu (int uncable, uint scuUnitIdx, char * * name_save)
  {
    if (scuUnitIdx >= scuDev.numunits)
      {
        simPrintf ("cable_scu: SCU unit number out of range <%d>\n",
                    scuUnitIdx);
        return SCPE_ARG;
      }

    int scuPortNum = getval (name_save, "SCU port number");

    // The scu port number may have subport data encoded; check range
    // after we have decided if the is a connection to an IOM or a CPU.

    //    if (scuPortNum < 0 || scuPortNum >= N_SCU_PORTS)
    //      {
    //        simPrintf ("cable_scu: SCU port number out of range <%d>\n",
    //                    scuPortNum);
    //        return SCPE_ARG;
    //      }

    // XXX combine into parse_match ()
    // extract 'IOMx' or 'CPUx'
    char * param = strtok_r (NULL, ", ", name_save);
    if (! param)
      {
        simPrintf ("cable_scu: can't parse IOM\n");
        return SCPE_ARG;
      }
    uint unit_idx;

    // SCUx IOMx
    if (name_match (param, "IOM", & unit_idx))
      {
        if (unit_idx >= N_IOM_UNITS_MAX)
          {
            simPrintf ("cable SCU: IOM unit number out of range <%d>\n", unit_idx);
            return SCPE_ARG;
          }

        if (scuPortNum < 0 || scuPortNum >= N_SCU_PORTS)
          {
            simPrintf ("cable_scu: SCU port number out of range <%d>\n",
                        scuPortNum);
            return SCPE_ARG;
          }

        // extract iom port number
        param = strtok_r (NULL, ", ", name_save);
        if (! param)
          {
            simPrintf ("cable SCU: can't parse IOM port number\n");
            return SCPE_ARG;
          }
        int iom_port_num = parseval (param);

        if (iom_port_num < 0 || iom_port_num >= N_IOM_PORTS)
          {
            simPrintf ("cable SCU: IOM port number out of range <%d>\n", iom_port_num);
            return SCPE_ARG;
          }
        return cable_scu_to_iom (uncable, scuUnitIdx, (uint) scuPortNum,
                                 unit_idx, (uint) iom_port_num);
      }

    // SCUx CPUx
    else if (name_match (param, "CPU", & unit_idx))
      {
        if (unit_idx >= N_CPU_UNITS_MAX)
          {
            simPrintf ("cable SCU: IOM unit number out of range <%d>\n", unit_idx);
            return SCPE_ARG;
          }

        // Encoding expansion port info into port number:
        //   [0-7]: port number
        //   [1-7][0-3]:  port number, sub port number.
        // This won't let me put an expansion port on port 0, but documents
        // say to put the CPUs on the high ports and the IOMs on the low, so
        // there is no reason to put an expander on port 0.
        //

        int scu_subport_num = 0;
        bool isExpansionPort = false;
        int exp_port = scuPortNum / 10;
        if (exp_port)
          {
            scu_subport_num = scuPortNum % 10;
            if (scu_subport_num < 0 || scu_subport_num >= N_SCU_SUBPORTS)
              {
                simPrintf ("cable SCU: subport number out of range <%d>\n",
                            scu_subport_num);
                return SCPE_ARG;
              }
            scuPortNum /= 10;
            isExpansionPort = true;
          }
        if (scuPortNum < 0 || scuPortNum >= N_SCU_PORTS)
          {
            simPrintf ("cable SCU: port number out of range <%d>\n",
                        scuPortNum);
            return SCPE_ARG;
          }

        // extract cpu port number
        param = strtok_r (NULL, ", ", name_save);
        if (! param)
          {
            simPrintf ("cable SCU: can't parse CPU port number\n");
            return SCPE_ARG;
          }
        int cpuPortNum = parseval (param);

        if (cpuPortNum < 0 || cpuPortNum >= N_CPU_PORTS)
          {
            simPrintf ("cable SCU: CPU port number out of range <%d>\n", cpuPortNum);
            return SCPE_ARG;
          }
        return cable_scu_to_cpu (uncable, scuUnitIdx, (uint) scuPortNum,
                                 (uint) scu_subport_num, unit_idx, (uint) cpuPortNum, isExpansionPort);
      }
    else
      {
        simPrintf ("cable SCU: can't parse IOM or CPU\n");
        return SCPE_ARG;
      }
  }

static simRc cable_ctlr_to_iom (int uncable, struct ctlr_to_iom_s * there, uint iom_unit_idx, uint chan_num)
  {
    if (uncable)
      {
        if (! there->socket_in_use)
          {
            simPrintf ("error: UNCABLE: controller not cabled\n");
            return SCPE_ARG;
          }
        if (there->iom_unit_idx != iom_unit_idx ||
            there->chan_num != chan_num)
          {
            simPrintf ("error: UNCABLE: wrong controller\n");
            return SCPE_ARG;
          }
        there->socket_in_use = false;
      }
   else
      {
        if (there->socket_in_use)
          {
            simPrintf ("error: CABLE: controller in use\n");
            return SCPE_ARG;
          }
        there->socket_in_use = true;
        there->iom_unit_idx = iom_unit_idx;
        there->chan_num     = chan_num;
      }
    return SCPE_OK;
  }

static simRc cable_ctlr (int uncable,
                         uint iom_unit_idx, uint chan_num,
                         uint ctlr_unit_idx, uint port_num,
                         char * service,
                         DEVICE * devp,
                         struct ctlr_to_iom_s * there,
                         enum ctlr_type_e ctlr_type, enum chan_type_e chan_type,
                         bool isConsole,
                         UNIT * unitp,
                         iom_connect_t * iom_connect,
                         iom_cmd_t * iom_cmd)
  {
    if (ctlr_unit_idx >= devp->numunits)
      {
        simPrintf ("%s: unit index out of range <%d>\n",
                    service, ctlr_unit_idx);
        return SCPE_ARG;
      }

    struct iom_to_ctlr_s * p = & cables->iom_to_ctlr[iom_unit_idx][chan_num];

    if (uncable)
      {
        if (! p->socket_in_use)
          {
            simPrintf ("%s: not cabled\n", service);
            return SCPE_ARG;
          }

        if (p->ctlr_unit_idx != ctlr_unit_idx)
          {
            simPrintf ("%s: Wrong IOM expected %d, found %d\n",
                        service, ctlr_unit_idx, p->ctlr_unit_idx);
            return SCPE_ARG;
          }

        // Unplug the other end of the cable
        simRc rc = cable_ctlr_to_iom (uncable, there,
                                       iom_unit_idx, chan_num);
        if (rc)
          {
            return rc;
          }
        p->socket_in_use  = false;
        p->iom_connect = NULL;
        p->iom_cmd = NULL;
      }
    else
      {
        if (p->socket_in_use)
          {
            simPrintf ("%s: socket in use; unit number %d. (%o); "
                        "not cabling.\n", service, iom_unit_idx, iom_unit_idx);
            return SCPE_ARG;
          }

        // Plug the other end of the cable in
        simRc rc = cable_ctlr_to_iom (uncable, there, iom_unit_idx, chan_num);
        if (rc)
          {
            return rc;
          }
        p->socket_in_use        = true;
        p->ctlr_unit_idx = ctlr_unit_idx;
        p->port_num      = port_num;
        p->ctlr_type     = ctlr_type;
        p->chan_type     = chan_type;
        p->dev           = devp;
        p->board         = unitp;
        p->isConsole     = isConsole;
        p->iom_connect   = iom_connect;
        p->iom_cmd       = iom_cmd;
      }

    return SCPE_OK;
  }

//    cable IOMx chan# MTPx [port#]  // tape controller
//    cable IOMx chan# MSPx [port#]  // disk controller
//    cable IOMx chah# IPCDx [port#]  // FIPS disk controller
//    cable IOMx chah# IPCTx [port#]  // FIPS tape controller
//    cable IOMx chan# OPCx          // Operator console
//    cable IOMx chan# FNPx          // FNP
//    cable IOMx chan# DIAx       // DIA
//    cable IOMx chan# ABSIx         // ABSI
//    cable IOMx chan# MGPx          // MGP
//    cable IOMx chan# SKCx          // Socket controller

static simRc cable_iom (int uncable, uint iom_unit_idx, char * * name_save)
  {
    if (iom_unit_idx >= iom_dev.numunits)
      {
        simPrintf ("error: CABLE IOM: unit number out of range <%d>\n",
                    iom_unit_idx);
        return SCPE_ARG;
      }

    int chan_num = getval (name_save, "IOM channel number");

    if (chan_num < 0 || chan_num >= MAX_CHANNELS)
      {
        simPrintf ("error: CABLE IOM channel number out of range <%d>\n",
                    chan_num);
        return SCPE_ARG;
      }

    // extract controller type
    char * param = strtok_r (NULL, ", ", name_save);
    if (! param)
      {
        simPrintf ("error: CABLE IOM can't parse controller type\n");
        return SCPE_ARG;
      }
    uint unit_idx;

    // IOMx IPCDx
    if (name_match (param, "IPCD", & unit_idx))
      {
        if (unit_idx >= N_IPCD_UNITS_MAX)
          {
            simPrintf ("error: CABLE IOM: IPCD unit number out of range <%d>\n", unit_idx);
            return SCPE_ARG;
          }

        // extract IPCD port number
        int ipcd_port_num = 0;
        param = strtok_r (NULL, ", ", name_save);
        if (param)
          ipcd_port_num = parseval (param);

        if (ipcd_port_num < 0 || ipcd_port_num >= MAX_CTLR_PORTS)
          {
            simPrintf ("error: CABLE IOM: IPCD port number out of range <%d>\n", ipcd_port_num);
            return SCPE_ARG;
          }
        return cable_ctlr (uncable,
                           iom_unit_idx, (uint) chan_num,
                           unit_idx, (uint) ipcd_port_num,
                           "CABLE IOMx IPCDx",
                           & ipcd_dev,
                           & cables->ipcd_to_iom[unit_idx][ipcd_port_num],
                           CTLR_T_IPCD, chan_type_PSI, false,
                           & ipcd_unit [unit_idx], NULL, dsk_iom_cmd);
      }

    // IOMx IPCTx
    if (name_match (param, "IPCT", & unit_idx))
      {
        if (unit_idx >= N_IPCT_UNITS_MAX)
          {
            simPrintf ("error: CABLE IOM: IPCT unit number out of range <%d>\n", unit_idx);
            return SCPE_ARG;
          }

        // extract IPCT port number
        int ipct_port_num = 0;
        param = strtok_r (NULL, ", ", name_save);
        if (param)
          ipct_port_num = parseval (param);

        if (ipct_port_num < 0 || ipct_port_num >= MAX_CTLR_PORTS)
          {
            simPrintf ("error: CABLE IOM: IPCT port number out of range <%d>\n", ipct_port_num);
            return SCPE_ARG;
          }
        return cable_ctlr (uncable,
                           iom_unit_idx, (uint) chan_num,
                           unit_idx, (uint) ipct_port_num,
                           "CABLE IOMx IPCTx",
                           & ipct_dev,
                           & cables->ipct_to_iom[unit_idx][ipct_port_num],
                           CTLR_T_IPCT, chan_type_PSI, false,
                           & ipct_unit [unit_idx], NULL, mt_iom_cmd);
      }

    // IOMx MSPx
    if (name_match (param, "MSP", & unit_idx))
      {
        if (unit_idx >= N_MSP_UNITS_MAX)
          {
            simPrintf ("error: CABLE IOM: MSP unit number out of range <%d>\n", unit_idx);
            return SCPE_ARG;
          }

        // extract MSP port number
        int msp_port_num = 0;
        param = strtok_r (NULL, ", ", name_save);
        if (param)
          msp_port_num = parseval (param);

        if (msp_port_num < 0 || msp_port_num >= MAX_CTLR_PORTS)
          {
            simPrintf ("error: CABLE IOM: MSP port number out of range <%d>\n", msp_port_num);
            return SCPE_ARG;
          }
        return cable_ctlr (uncable,
                           iom_unit_idx, (uint) chan_num,
                           unit_idx, (uint) msp_port_num,
                           "CABLE IOMx MSPx",
                           & msp_dev,
                           & cables->msp_to_iom[unit_idx][msp_port_num],
                           CTLR_T_MSP, chan_type_PSI, false,
                           & msp_unit [unit_idx], NULL, dsk_iom_cmd); // XXX mtp_iom_cmd?
      }

    // IOMx MTPx
    if (name_match (param, "MTP", & unit_idx))
      {
        if (unit_idx >= N_MTP_UNITS_MAX)
          {
            simPrintf ("error: CABLE IOM: MTP unit number out of range <%d>\n", unit_idx);
            return SCPE_ARG;
          }

        // extract MTP port number
        int mtp_port_num = 0;
        param = strtok_r (NULL, ", ", name_save);
        if (param)
          mtp_port_num = parseval (param);

        if (mtp_port_num < 0 || mtp_port_num >= MAX_CTLR_PORTS)
          {
            simPrintf ("error: CABLE IOM: MTP port number out of range <%d>\n", mtp_port_num);
            return SCPE_ARG;
          }
        return cable_ctlr (uncable,
                           iom_unit_idx, (uint) chan_num,
                           unit_idx, (uint) mtp_port_num,
                           "CABLE IOMx MTPx",
                           & mtp_dev,
                           & cables->mtp_to_iom[unit_idx][mtp_port_num],
                           CTLR_T_MTP, chan_type_PSI, false,
                           & mtp_unit [unit_idx], NULL, mt_iom_cmd); // XXX mtp_iom_cmd?
      }

    // IOMx URPx
    if (name_match (param, "URP", & unit_idx))
      {
        if (unit_idx >= N_URP_UNITS_MAX)
          {
            simPrintf ("error: CABLE IOM: URP unit number out of range <%d>\n", unit_idx);
            return SCPE_ARG;
          }

        // extract URP port number
        int urp_port_num = 0;
        param = strtok_r (NULL, ", ", name_save);
        if (param)
          urp_port_num = parseval (param);

        if (urp_port_num < 0 || urp_port_num >= MAX_CTLR_PORTS)
          {
            simPrintf ("error: CABLE IOM: URP port number out of range <%d>\n", urp_port_num);
            return SCPE_ARG;
          }

        return cable_ctlr (uncable,
                           iom_unit_idx, (uint) chan_num,
                           unit_idx, (uint) urp_port_num,
                           "CABLE IOMx URPx",
                           & urp_dev,
                           & cables->urp_to_iom[unit_idx][urp_port_num],
                           CTLR_T_URP, chan_type_PSI, false,
                           & urp_unit [unit_idx], NULL, urp_iom_cmd);
      }

    // IOMx OPCx
    if (name_match (param, "OPC", & unit_idx))
      {
        if (unit_idx >= N_OPC_UNITS_MAX)
          {
            simPrintf ("error: CABLE IOM: OPC unit number out of range <%d>\n", unit_idx);
            return SCPE_ARG;
          }

        uint opc_port_num = 0;
        return cable_ctlr (uncable,
                           iom_unit_idx, (uint) chan_num,
                           unit_idx, opc_port_num,
                           "CABLE IOMx OPCx",
                           & opc_dev,
                           & cables->opc_to_iom[unit_idx][opc_port_num],
                           CTLR_T_OPC, chan_type_CPI, true,
                           & opc_unit [unit_idx], NULL, opc_iom_cmd);
      }

    // IOMx FNPx
    if (name_match (param, "FNP", & unit_idx))
      {
        if (unit_idx >= N_FNP_UNITS_MAX)
          {
            simPrintf ("error: CABLE IOM: FNP unit number out of range <%d>\n", unit_idx);
            return SCPE_ARG;
          }

        uint fnp_port_num = 0;
        return cable_ctlr (uncable,
                           iom_unit_idx, (uint) chan_num,
                           unit_idx, fnp_port_num,
                           "CABLE IOMx FNPx",
                           & fnp_dev,
                           & cables->fnp_to_iom[unit_idx][fnp_port_num],
                           CTLR_T_FNP, chan_type_direct, false,
                           & fnp_unit [unit_idx], NULL, fnp_iom_cmd);
      }

    // IOMx DIAx
    if (name_match (param, "DIA", & unit_idx))
      {
        if (unit_idx >= N_DIA_UNITS_MAX)
          {
            simPrintf ("error: CABLE IOM: DIA unit number out of range <%d>\n", unit_idx);
            return SCPE_ARG;
          }

        uint dia_port_num = 0;
simPrintf ("cable dia chan_num %d unit_idx %u\n", chan_num, unit_idx);
        return cable_ctlr (uncable,
                           iom_unit_idx, (uint) chan_num,
                           unit_idx, dia_port_num,
                           "CABLE IOMx diax",
                           & diaDevice,
                           & cables->dia_to_iom[unit_idx][dia_port_num],
                           CTLR_T_DIA, chan_type_direct, false,
                           & diaUnit [unit_idx],
                           diaIomConnect,
                           NULL);
      }

#if defined(WITH_ABSI_DEV)
# if !defined(__MINGW64__) && !defined(__MINGW32__) && !defined (CROSS_MINGW64) && !defined(CROSS_MINGW32)
    // IOMx ABSIx
    if (name_match (param, "ABSI", & unit_idx))
      {
        if (unit_idx >= N_ABSI_UNITS_MAX)
          {
            simPrintf ("error: CABLE IOM: ABSI unit number out of range <%d>\n", unit_idx);
            return SCPE_ARG;
          }

        uint absi_port_num = 0;
        return cable_ctlr (uncable,
                           iom_unit_idx, (uint) chan_num,
                           unit_idx, absi_port_num,
                           "CABLE IOMx ABSIx",
                           & absi_dev,
                           & cables->absi_to_iom[unit_idx][absi_port_num],
                           CTLR_T_ABSI, chan_type_direct, false,
                           & absi_unit [unit_idx], NULL, absi_iom_cmd);
      }
# endif /* if !defined(__MINGW64__) && !defined(__MINGW32__) && !defined (CROSS_MINGW64) && !defined(CROSS_MINGW32) */
#endif /* if defined(WITH_ABSI_DEV) */

#if defined(WITH_MGP_DEV)
# if !defined(__MINGW64__) && !defined(__MINGW32__) && !defined(CROSS_MINGW64) && !defined(CROSS_MINGW32)
    // IOMx MGPx
    if (name_match (param, "MGP", & unit_idx))
      {
        if (unit_idx >= N_MGP_UNITS_MAX)
          {
            simPrintf ("error: CABLE IOM: MGP unit number out of range <%d>\n", unit_idx);
            return SCPE_ARG;
          }

        uint mgp_port_num = 0;
        return cable_ctlr (uncable,
                           iom_unit_idx, (uint) chan_num,
                           unit_idx, mgp_port_num,
                           "CABLE IOMx MGPx",
                           & mgp_dev,
                           & cables->mgp_to_iom[unit_idx][mgp_port_num],
                           CTLR_T_MGP, chan_type_direct, false,
                           & mgp_unit [unit_idx], NULL, mgp_iom_cmd);
      }
# endif /* if !defined(__MINGW64__) && !defined(__MINGW32__) && !defined(CROSS_MINGW64) && !defined(CROSS_MINGW32) */
#endif /* if defined(WITH_MGP_DEV) */

#if defined(WITH_SOCKET_DEV)
# if !defined(__MINGW64__) && !defined(__MINGW32__) && !defined(CROSS_MINGW64) && !defined(CROSS_MINGW32)
    // IOMx SKCx
    if (name_match (param, "SKC", & unit_idx))
      {
        if (unit_idx >= N_SKC_UNITS_MAX)
          {
            simPrintf ("error: CABLE IOM: SKC unit number out of range <%d>\n", unit_idx);
            return SCPE_ARG;
          }

        uint skc_port_num = 0;
        return cable_ctlr (uncable,
                           iom_unit_idx, (uint) chan_num,
                           unit_idx, skc_port_num,
                           "CABLE IOMx SKCx",
                           & skc_dev,
                           & cables->sk_to_iom[unit_idx][skc_port_num],
                           CTLR_T_SKC, chan_type_direct, false,
                           & sk_unit [unit_idx], NULL, skc_iom_cmd);
      }
# endif /* if !defined(__MINGW64__) && !defined(__MINGW32__) && !defined(CROSS_MINGW64) && !defined(CROSS_MINGW32) */
#endif /* if defined(WITH_SOCKET_DEV) */

    simPrintf ("cable IOM: can't parse controller type\n");
    return SCPE_ARG;
  }

static simRc cable_periph_to_ctlr (int uncable,
                                    uint ctlr_unit_idx, uint dev_code,
                                    enum ctlr_type_e ctlr_type,
                                    struct dev_to_ctlr_s * there,
                                    iom_connect_t * iom_connect,
                                    iom_cmd_t * iom_cmd)
  {
    if (uncable)
      {
        if (! there->socket_in_use)
          {
            simPrintf ("error: UNCABLE: device not cabled\n");
            return SCPE_ARG;
          }
        if (there->ctlr_unit_idx != ctlr_unit_idx ||
            there->dev_code != dev_code)
          {
            simPrintf ("error: UNCABLE: wrong controller\n");
            return SCPE_ARG;
          }
        there->socket_in_use = false;
      }
   else
      {
        if (there->socket_in_use)
          {
            simPrintf ("error: CABLE: device in use\n");
            return SCPE_ARG;
          }
        there->socket_in_use        = true;
        there->ctlr_unit_idx = ctlr_unit_idx;
        there->dev_code      = dev_code;
        there->ctlr_type     = ctlr_type;
      }
    return SCPE_OK;
  }

static simRc cable_periph (int uncable,
                            uint ctlr_unit_idx,
                            uint dev_code,
                            enum ctlr_type_e ctlr_type,
                            struct ctlr_to_dev_s * here,
                            uint unit_idx,
                            iom_connect_t * iom_connect,
                            iom_cmd_t * iom_cmd,
                            struct dev_to_ctlr_s * there,
                            char * service)
  {
    if (uncable)
      {
        if (! here->socket_in_use)
          {
            simPrintf ("%s: socket not in use\n", service);
            return SCPE_ARG;
          }
        // Unplug the other end of the cable
        simRc rc = cable_periph_to_ctlr (uncable,
                                          ctlr_unit_idx, dev_code, ctlr_type,
                                          there,
                                          iom_connect,
                                          iom_cmd);
        if (rc)
          {
            return rc;
          }

        here->socket_in_use  = false;
        here->iom_connect = NULL;
        here->iom_cmd = NULL;
      }
    else
      {
        if (here->socket_in_use)
          {
            simPrintf ("%s: controller socket in use; unit number %u. dev_code %oo\n",
                        service, ctlr_unit_idx, dev_code);
            return SCPE_ARG;
          }

        // Plug the other end of the cable in
        simRc rc = cable_periph_to_ctlr (uncable,
                                          ctlr_unit_idx, dev_code, ctlr_type,
                                          there,
                                          iom_connect,
                                          iom_cmd);
        if (rc)
          {
            return rc;
          }

        here->socket_in_use   = true;
        here->unit_idx = unit_idx;
        here->iom_connect  = iom_connect;
        here->iom_cmd  = iom_cmd;
      }

    return SCPE_OK;
  }

// cable MTPx dev_code TAPEx

static simRc cable_mtp (int uncable, uint ctlr_unit_idx, char * * name_save)
  {
    if (ctlr_unit_idx >= mtp_dev.numunits)
      {
        simPrintf ("error: CABLE MTP: controller unit number out of range <%d>\n",
                    ctlr_unit_idx);
        return SCPE_ARG;
      }

    int dev_code = getval (name_save, "MTP device code");

    if (dev_code < 0 || dev_code >= MAX_CHANNELS)
      {
        simPrintf ("error: CABLE MTP device code out of range <%d>\n",
                    dev_code);
        return SCPE_ARG;
      }

    // extract tape index
    char * param = strtok_r (NULL, ", ", name_save);
    if (! param)
      {
        simPrintf ("error: CABLE IOM can't parse device name\n");
        return SCPE_ARG;
      }
    uint mt_unit_idx;

    // MPCx TAPEx
    if (name_match (param, "TAPE", & mt_unit_idx))
      {
        if (mt_unit_idx >= N_MT_UNITS_MAX)
          {
            simPrintf ("error: CABLE IOM: TAPE unit number out of range <%d>\n", mt_unit_idx);
            return SCPE_ARG;
          }

        return cable_periph (uncable,
                             ctlr_unit_idx,
                             (uint) dev_code,
                             CTLR_T_MTP,
                             & cables->mtp_to_tap[ctlr_unit_idx][dev_code],
                             mt_unit_idx,
                             NULL, mt_iom_cmd,
                             & cables->tap_to_ctlr[mt_unit_idx],
                             "CABLE MTPx TAPEx");
      }

    simPrintf ("cable MTP: can't parse device name\n");
    return SCPE_ARG;
  }

// cable IPCDx dev_code DISKx

static simRc cable_ipcd (int uncable, uint ctlr_unit_idx, char * * name_save)
  {
    if (ctlr_unit_idx >= ipcd_dev.numunits)
      {
        simPrintf ("error: CABLE IPCD: controller unit number out of range <%d>\n",
                    ctlr_unit_idx);
        return SCPE_ARG;
      }

    int dev_code = getval (name_save, "IPCD device code");

    if (dev_code < 0 || dev_code >= MAX_CHANNELS)
      {
        simPrintf ("error: CABLE IPCD device code out of range <%d>\n",
                    dev_code);
        return SCPE_ARG;
      }

    // parse DISK
    char * param = strtok_r (NULL, ", ", name_save);
    if (! param)
      {
        simPrintf ("error: CABLE IOM can't parse device name\n");
        return SCPE_ARG;
      }
    uint dsk_unit_idx;

    // IPCDx DISKx
    if (name_match (param, "DISK", & dsk_unit_idx))
      {
        if (dsk_unit_idx >= N_DSK_UNITS_MAX)
          {
            simPrintf ("error: CABLE IOM: DISK unit number out of range <%d>\n", dsk_unit_idx);
            return SCPE_ARG;
          }

        return cable_periph (uncable,
                             ctlr_unit_idx,
                             (uint) dev_code,
                             CTLR_T_IPCD,
                             & cables->ipcd_to_dsk[ctlr_unit_idx][dev_code],
                             dsk_unit_idx,
                             NULL, dsk_iom_cmd, // XXX
                             & cables->dsk_to_ctlr[dsk_unit_idx],
                             "CABLE IPCDx DISKx");
      }

    simPrintf ("cable IPCD: can't parse device name\n");
    return SCPE_ARG;
  }

// cable IPCTx dev_code TAPEx

static simRc cable_ipct (int uncable, uint ctlr_unit_idx, char * * name_save)
  {
    if (ctlr_unit_idx >= ipct_dev.numunits)
      {
        simPrintf ("error: CABLE IPCD: controller unit number out of range <%d>\n",
                    ctlr_unit_idx);
        return SCPE_ARG;
      }

    int dev_code = getval (name_save, "IPCT device code");

    if (dev_code < 0 || dev_code >= MAX_CHANNELS)
      {
        simPrintf ("error: CABLE IPCT device code out of range <%d>\n",
                    dev_code);
        return SCPE_ARG;
      }

    // parse TAPE
    char * param = strtok_r (NULL, ", ", name_save);
    if (! param)
      {
        simPrintf ("error: CABLE IOM can't parse device name\n");
        return SCPE_ARG;
      }
    uint tap_unit_idx;

    // IPCTx TAPEx
    if (name_match (param, "TAPE", & tap_unit_idx))
      {
        if (tap_unit_idx >= N_MT_UNITS_MAX)
          {
            simPrintf ("error: CABLE IOM: TAPE unit number out of range <%d>\n", tap_unit_idx);
            return SCPE_ARG;
          }

        return cable_periph (uncable,
                             ctlr_unit_idx,
                             (uint) dev_code,
                             CTLR_T_IPCT,
                             & cables->ipct_to_tap[ctlr_unit_idx][dev_code],
                             tap_unit_idx,
                             NULL, mt_iom_cmd, // XXX
                             & cables->tap_to_ctlr[tap_unit_idx],
                             "CABLE IPCTx TAPEx");
      }

    simPrintf ("cable IPCT: can't parse device name\n");
    return SCPE_ARG;
  }

// cable MSPx dev_code DISKx

static simRc cable_msp (int uncable, uint ctlr_unit_idx, char * * name_save)
  {
    if (ctlr_unit_idx >= msp_dev.numunits)
      {
        simPrintf ("error: CABLE MSP: controller unit number out of range <%d>\n",
                    ctlr_unit_idx);
        return SCPE_ARG;
      }

    int dev_code = getval (name_save, "MSP device code");

    if (dev_code < 0 || dev_code >= MAX_CHANNELS)
      {
        simPrintf ("error: CABLE MSP device code out of range <%d>\n",
                    dev_code);
        return SCPE_ARG;
      }

    // extract tape index
    char * param = strtok_r (NULL, ", ", name_save);
    if (! param)
      {
        simPrintf ("error: CABLE IOM can't parse device name\n");
        return SCPE_ARG;
      }
    uint dsk_unit_idx;

    // MPCx DISKx
    if (name_match (param, "DISK", & dsk_unit_idx))
      {
        if (dsk_unit_idx >= N_DSK_UNITS_MAX)
          {
            simPrintf ("error: CABLE IOM: DISK unit number out of range <%d>\n", dsk_unit_idx);
            return SCPE_ARG;
          }

        return cable_periph (uncable,
                             ctlr_unit_idx,
                             (uint) dev_code,
                             CTLR_T_MSP,
                             & cables->msp_to_dsk[ctlr_unit_idx][dev_code],
                             dsk_unit_idx,
                             NULL, dsk_iom_cmd, // XXX
                             & cables->dsk_to_ctlr[dsk_unit_idx],
                             "CABLE MSPx DISKx");
      }

    simPrintf ("cable MSP: can't parse device name\n");
    return SCPE_ARG;
  }

// cable URPx dev_code [RDRx PUNx PRTx]

static simRc cable_urp (int uncable, uint ctlr_unit_idx, char * * name_save)
  {
    if (ctlr_unit_idx >= urp_dev.numunits)
      {
        simPrintf ("error: CABLE URP: controller unit number out of range <%d>\n",
                    ctlr_unit_idx);
        return SCPE_ARG;
      }

    int dev_code = getval (name_save, "URP device code");

    if (dev_code < 0 || dev_code >= MAX_CHANNELS)
      {
        simPrintf ("error: CABLE URP device code out of range <%d>\n",
                    dev_code);
        return SCPE_ARG;
      }

    // extract tape index
    char * param = strtok_r (NULL, ", ", name_save);
    if (! param)
      {
        simPrintf ("error: CABLE IOM can't parse device name\n");
        return SCPE_ARG;
      }
    uint unit_idx;

    // URPx RDRx
    if (name_match (param, "RDR", & unit_idx))
      {
        if (unit_idx >= N_RDR_UNITS_MAX)
          {
            simPrintf ("error: CABLE IOM: DISK unit number out of range <%d>\n", unit_idx);
            return SCPE_ARG;
          }

        return cable_periph (uncable,
                             ctlr_unit_idx,
                             (uint) dev_code,
                             CTLR_T_URP,
                             & cables->urp_to_urd[ctlr_unit_idx][dev_code],
                             unit_idx,
                             NULL, rdr_iom_cmd, // XXX
                             & cables->rdr_to_urp[unit_idx],
                             "CABLE URPx RDRx");
      }

    // URPx PUNx
    if (name_match (param, "PUN", & unit_idx))
      {
        if (unit_idx >= N_PUN_UNITS_MAX)
          {
            simPrintf ("error: CABLE IOM: DISK unit number out of range <%d>\n", unit_idx);
            return SCPE_ARG;
          }

        return cable_periph (uncable,
                             ctlr_unit_idx,
                             (uint) dev_code,
                             CTLR_T_URP,
                             & cables->urp_to_urd[ctlr_unit_idx][dev_code],
                             unit_idx,
                             NULL, pun_iom_cmd, // XXX
                             & cables->pun_to_urp[unit_idx],
                             "CABLE URPx PUNx");
      }

    // URPx PRTx
    if (name_match (param, "PRT", & unit_idx))
      {
        if (unit_idx >= N_PRT_UNITS_MAX)
          {
            simPrintf ("error: CABLE IOM: DISK unit number out of range <%d>\n", unit_idx);
            return SCPE_ARG;
          }

        return cable_periph (uncable,
                             ctlr_unit_idx,
                             (uint) dev_code,
                             CTLR_T_URP,
                             & cables->urp_to_urd[ctlr_unit_idx][dev_code],
                             unit_idx,
                             NULL, prt_iom_cmd, // XXX
                             & cables->prt_to_urp[unit_idx],
                             "CABLE URPx PRTx");
      }

    simPrintf ("cable URP: can't parse device name\n");
    return SCPE_ARG;
  }

simRc sys_cable (int32_t arg, const char * buf)
  {
    char * copy = strdup (buf);
    MALLOC_CHECK (copy);
    simRc rc = SCPE_ARG;

    // process statement

    // extract first word
    char * name_save = NULL;
    char * name;
    name = strtok_r (copy, ", \t", & name_save);
    if (! name)
      {
        simPrintf ("error: CABLE: sys_cable could not parse name\n");
        goto exit;
      }

    uint unit_num;
    if (strcasecmp (name, "RIPOUT") == 0)
      rc = sys_cable_ripout (0, NULL);
    else if (strcasecmp (name, "SHOW") == 0)
      rc = sys_cable_show (0, NULL);
    else if (strcasecmp (name, "DUMP") == 0)
      rc = sys_cable_show (1, NULL);
    else if (strcasecmp (name, "GRAPH") == 0)
      rc = sys_cable_graph ();
    else if (name_match (name, "SCU", & unit_num))
      rc = cable_scu (arg, unit_num, & name_save);
    else if (name_match (name, "IOM", & unit_num))
      rc = cable_iom (arg, unit_num, & name_save);
    else if (name_match (name, "MTP", & unit_num))
      rc = cable_mtp (arg, unit_num, & name_save);
    else if (name_match (name, "IPCD", & unit_num))
      rc = cable_ipcd (arg, unit_num, & name_save);
    else if (name_match (name, "IPCT", & unit_num))
      rc = cable_ipct (arg, unit_num, & name_save);
    else if (name_match (name, "MSP", & unit_num))
      rc = cable_msp (arg, unit_num, & name_save);
    else if (name_match (name, "URP", & unit_num))
      rc = cable_urp (arg, unit_num, & name_save);
    else
      {
        simPrintf ("error: CABLE: Invalid name <%s>\n", name);
        goto exit;
      }
    if (name_save && strlen (name_save))
      {
        simPrintf ("CABLE ignored '%s'\n", name_save);
      }
exit:
    FREE (copy);
    return rc;
  }

static void cable_init (void)
  {
    // sets cablesFromIomToDev[iomUnitIdx].devices[chanNum][dev_code].type
    //  to DEVT_NONE and socket_in_use to false

    (void) memset (cables, 0, sizeof (struct cables_s));
  }

#define all(i,n) \
  for (uint i = 0; i < n; i ++)

static simRc sys_cable_graph (void)
  {

    // Find all used CPUs, IOMs, SCUs
    bool cpus_used [N_CPU_UNITS_MAX];
    memset (cpus_used, 0, sizeof (cpus_used));

    all (u, N_CPU_UNITS_MAX)
      all (prt, N_CPU_PORTS)
        {
          struct cpu_to_scu_s * p = & cables->cpu_to_scu[u][prt];
          if (p->socket_in_use)
            cpus_used[u] = true;
        }

    bool scus_used [N_SCU_UNITS_MAX];
    memset (scus_used, 0, sizeof (scus_used));

    all (u, N_SCU_UNITS_MAX)
      all (prt, N_SCU_PORTS)
        {
          struct scu_to_iom_s * p = & cables->scu_to_iom[u][prt];
          if (p->socket_in_use)
            scus_used[u] = true;
        }
    all (u, N_CPU_UNITS_MAX)
      all (prt, N_CPU_PORTS)
        {
          struct cpu_to_scu_s * p = & cables->cpu_to_scu[u][prt];
          if (p->socket_in_use)
            scus_used[p->scuUnitIdx] = true;
        }

    bool ioms_used [N_IOM_UNITS_MAX];
    memset (ioms_used, 0, sizeof (ioms_used));

    all (u, N_SCU_UNITS_MAX)
      all (prt, N_SCU_PORTS)
        {
          struct scu_to_iom_s * p = & cables->scu_to_iom[u][prt];
          if (p->socket_in_use)
            ioms_used[p->iom_unit_idx] = true;
        }


    // Set up ranking
    //     CPUs
    //     SCUs
    //     IOMs

    simPrintf ("graph {\n");
    simPrintf ("    rankdir=TB;\n");

    simPrintf ("    { rank=same; ");
    for (int i = 0; i < N_CPU_UNITS_MAX; i ++)
      if (cpus_used[i])
        simPrintf (" CPU%c;", i + 'A');
    simPrintf ("}\n");
    
    simPrintf ("    { rank=same; ");
    for (int i = 0; i < N_SCU_UNITS_MAX; i ++)
      if (scus_used[i])
        simPrintf (" SCU%c;", i + 'A');
    simPrintf ("}\n");
    
    simPrintf ("    { rank=same; ");
    for (int i = 0; i < N_IOM_UNITS_MAX; i ++)
      if (ioms_used[i])
        simPrintf (" IOM%c;", i + 'A');
    simPrintf ("}\n");
    
#if 0
    // Rank controllers.
    // It is harder to generate a used list becuase controllers 
    // are indexed by type and idx instead of just idx like CPU/SCU/IOMs.
    // But dot appears to be okay with duplicates in the rank list.

    simPrintf ("    { rank=same; ");
    all (u, N_IOM_UNITS_MAX)
      all (c, MAX_CHANNELS)
        {
          struct iom_to_ctlr_s * p = & cables->iom_to_ctlr[u][c];
          if (p->socket_in_use)
            simPrintf (" %s%d;", ctlr_type_strs[p->ctlr_type], p->ctlr_unit_idx);
        }
    simPrintf ("}\n");
#endif
 
#define R_CTLR_IOM(big,small) \
    simPrintf ("    { rank=same; "); \
    all (u, N_ ## big ## _UNITS_MAX) \
      all (prt, MAX_CTLR_PORTS) \
        { \
          struct ctlr_to_iom_s * p = & cables->small ## _to_iom[u][prt]; \
          if (p->socket_in_use) \
            simPrintf (" %s%d;", #big, u); \
        } \
    simPrintf ("}\n");

    R_CTLR_IOM (MTP, mtp)
    R_CTLR_IOM (MSP, msp)
    R_CTLR_IOM (IPCD, ipcd)
    R_CTLR_IOM (IPCT, ipct)
    R_CTLR_IOM (URP, urp)
    R_CTLR_IOM (DIA, dia)
#ifndef __MINGW64__
    R_CTLR_IOM (ABSI, absi)
#endif
    R_CTLR_IOM (OPC, opc)

#define R_DEV_CTLR(from_big,from_small, to_label, to_big, to_small) \
    simPrintf ("    { rank=same; "); \
    all (u, N_ ## to_big ## _UNITS_MAX) \
      { \
        struct dev_to_ctlr_s * p = & cables->to_small ## _to_ ## from_small[u]; \
        if (p->socket_in_use) \
          simPrintf (" %s%d;", #to_label, u); \
      } \
    simPrintf ("}\n");

    R_DEV_CTLR (MTP, ctlr, TAPE, MT, tap);
    R_DEV_CTLR (CTLR, ctlr, DISK, DSK, dsk);
    R_DEV_CTLR (URP, urp, RDR, RDR, rdr);
    R_DEV_CTLR (URP, urp, PUN, PUN, pun);
    R_DEV_CTLR (URP, urp, PRT, PRT, prt);


    // Generate CPU/SCU/IOM cables
    
    all (u, N_CPU_UNITS_MAX)
      all (prt, N_CPU_PORTS)
        {
          struct cpu_to_scu_s * p = & cables->cpu_to_scu[u][prt];
          if (p->socket_in_use)
            simPrintf ("    CPU%c -- SCU%c;\n", u + 'A', p->scuUnitIdx + 'A');
        }

    all (u, N_SCU_UNITS_MAX)
      all (prt, N_SCU_PORTS)
        {
          struct scu_to_iom_s * p = & cables->scu_to_iom[u][prt];
          if (p->socket_in_use)
            simPrintf ("    SCU%c -- IOM%c;\n", u + 'A', p->iom_unit_idx + 'A');
        }
    // Generate IOM to controller cables

    all (u, N_IOM_UNITS_MAX)
      all (c, MAX_CHANNELS)
        {
          struct iom_to_ctlr_s * p = & cables->iom_to_ctlr[u][c];
          if (p->socket_in_use)
            simPrintf ("    IOM%c -- %s%d;\n", u + 'A', ctlr_type_strs[p->ctlr_type], p->ctlr_unit_idx);
        }

    // Generate controller to device cables

#if 0
#define G_CTLR_DEV(from_big,from_small, to_label, to_big, to_small) \
    simPrintf ("  %-4s dev_code --> %-4s   command\n", #from_big, #to_label); \
    all (u, N_ ## from_big ## _UNITS_MAX) \
      all (prt, N_DEV_CODES) \
        { \
          struct ctlr_to_dev_s * p = & cables->from_small ## _to_ ## to_small[u][prt]; \
          if (p->socket_in_use) \
            simPrintf ("    %s%d -- %s%d;\n", #from_big, u, #to_label, p->unit_idx); \
        } 
    G_CTLR_DEV (MTP, mtp, TAPE, MT, tap);
    G_CTLR_DEV (IPCD, ipcd, DISK, DSK, dsk);
    G_CTLR_DEV (IPCT, ipct, TAPE, TAP, tap);
    G_CTLR_DEV (MSP, msp, DISK, DSK, dsk);
    G_CTLR_DEV (URP, urp, URP, URP, urd);
#endif


//          simPrintf ("    %s%d -- %s%d;\n", #to_label, p->dev_code, ctlr_type_strs[p->ctlr_type], p->ctlr_unit_idx); 
#define G_DEV_CTLR(from_big,from_small, to_label, to_big, to_small) \
    all (u, N_ ## to_big ## _UNITS_MAX) \
      { \
        struct dev_to_ctlr_s * p = & cables->to_small ## _to_ ## from_small[u]; \
        if (p->socket_in_use) \
          simPrintf ("    %s%d -- %s%d;\n", ctlr_type_strs[p->ctlr_type], p->ctlr_unit_idx, #to_label, u); \
      } 
    G_DEV_CTLR (MTP, ctlr, TAPE, MT, tap);
    G_DEV_CTLR (CTLR, ctlr, DISK, DSK, dsk);
    G_DEV_CTLR (URP, urp, RDR, RDR, rdr);
    G_DEV_CTLR (URP, urp, PUN, PUN, pun);
    G_DEV_CTLR (URP, urp, PRT, PRT, prt);

    simPrintf ("}\n");
    return SCPE_OK;
  }


simRc sys_cable_show (int32_t dump, UNUSED const char * buf)
  {
    simPrintf ("SCU <--> IOM\n");
    simPrintf ("   SCU port --> IOM port\n");
    all (u, N_SCU_UNITS_MAX)
      all (prt, N_SCU_PORTS)
        {
          struct scu_to_iom_s * p = & cables->scu_to_iom[u][prt];
          if (p->socket_in_use)
            simPrintf (" %4u %4u    %4u %4u\n", u, prt, p->iom_unit_idx, p->iom_port_num);
        }

    if (dump)
      {
        simPrintf ("   IOM port --> SCU port\n");
        all (u, N_IOM_UNITS_MAX)
          all (prt, N_IOM_PORTS)
            {
              struct iom_to_scu_s * p = & cables->iom_to_scu[u][prt];
              if (p->socket_in_use)
                simPrintf (" %4u %4u    %4u %4u\n", u, prt, p->scuUnitIdx, p->scuPortNum);
            }
      }
    simPrintf ("\n");

    simPrintf ("SCU <--> CPU\n");
    simPrintf ("   SCU port --> CPU port\n");
    all (u, N_SCU_UNITS_MAX)
      all (prt, N_SCU_PORTS)
        all (sp, N_SCU_SUBPORTS)
          {
            struct scu_to_cpu_s * p = & cables->scu_to_cpu[u][prt][sp];
            if (p->socket_in_use)
              simPrintf (" %4u %4u    %4u %4u\n", u, prt, p->cpuUnitIdx, p->cpuPortNum);
          }

    if (dump)
      {
        simPrintf ("   CPU port --> SCU port subport\n");
        all (u, N_CPU_UNITS_MAX)
          all (prt, N_CPU_PORTS)
            {
              struct cpu_to_scu_s * p = & cables->cpu_to_scu[u][prt];
              if (p->socket_in_use)
                simPrintf (" %4u %4u    %4u %4u  %4u\n",
                        u, prt, p->scuUnitIdx, p->scuPortNum, p->scu_subport_num);
            }
        }
    simPrintf ("\n");

    simPrintf ("IOM <--> controller\n");
    simPrintf ("                 ctlr       ctlr  chan\n");
    simPrintf ("   IOM chan -->  idx  port  type  type      device      board    command\n");
    all (u, N_IOM_UNITS_MAX)
      all (c, MAX_CHANNELS)
        {
          struct iom_to_ctlr_s * p = & cables->iom_to_ctlr[u][c];
          if (p->socket_in_use)
            simPrintf (" %4u %4u     %4u  %4u %-6s  %-6s %10p %10p %10p %10p\n",
                    u, c, p->ctlr_unit_idx, p->port_num, ctlr_type_strs[p->ctlr_type],
                    chan_type_strs[p->chan_type], (void *) p->dev,
                    (void *) p->board,
                    (void *) p->iom_connect,
                    (void *) p->iom_cmd);
        }

    if (dump)
      {
#define CTLR_IOM(big,small)                                                              \
    simPrintf ("  %-4s port --> IOM channel\n", #big);                                  \
    all (u, N_ ## big ## _UNITS_MAX)                                                     \
      all (prt, MAX_CTLR_PORTS)                                                          \
        {                                                                                \
          struct ctlr_to_iom_s * p = & cables->small ## _to_iom[u][prt];                 \
          if (p->socket_in_use)                                                                 \
            simPrintf (" %4u %4u    %4u %4u\n", u, prt, p->iom_unit_idx, p->chan_num);  \
        }
        CTLR_IOM (MTP, mtp)
        CTLR_IOM (MSP, msp)
        CTLR_IOM (IPCD, ipcd)
        CTLR_IOM (IPCT, ipct)
        CTLR_IOM (URP, urp)
        CTLR_IOM (FNP, fnp)
        CTLR_IOM (DIA, dia)
#if defined(WITH_ABSI_DEV)
# if !defined(__MINGW64__) && !defined(__MINGW32__) && !defined(CROSS_MINGW32) && !defined(CROSS_MINGW64)
        CTLR_IOM (ABSI, absi)
# endif /* if !defined(__MINGW64__) && !defined(__MINGW32__) && !defined(CROSS_MINGW32) && !defined(CROSS_MINGW64) */
#endif /* if defined(WITH_ABSI_DEV) */
#if defined(WITH_MGP_DEV)
# if !defined(__MINGW64__) && !defined(__MINGW32__) && !defined(CROSS_MINGW32) && !defined(CROSS_MINGW64)
        CTLR_IOM (MGP, mgp)
# endif /* if !defined(__MINGW64__) && !defined(__MINGW32__) && !defined(CROSS_MINGW32) && !defined(CROSS_MINGW64) */
#endif /* if defined(WITH_MGP_DEV) */
#if defined(WITH_SOCKET_DEV)
# if !defined(__MINGW32__) && !defined(__MINGW64__) && !defined(CROSS_MINGW32) && !defined(CROSS_MINGW64)
        CTLR_IOM (SKC, sk)
# endif /* if !defined(__MINGW32__) && !defined(__MINGW64__) && !defined(CROSS_MINGW32) && !defined(CROSS_MINGW64) */
#endif /* if defined(WITH_SOCKET_DEV) */
        CTLR_IOM (OPC, opc)
      }
    simPrintf ("\n");

    simPrintf ("controller <--> device\n");

#define CTLR_DEV(from_big,from_small, to_label, to_big, to_small)                                 \
    simPrintf ("  %-4s dev_code --> %-4s   command\n", #from_big, #to_label);                    \
    all (u, N_ ## from_big ## _UNITS_MAX)                                                         \
      all (prt, N_DEV_CODES)                                                                      \
        {                                                                                         \
          struct ctlr_to_dev_s * p = & cables->from_small ## _to_ ## to_small[u][prt];            \
          if (p->socket_in_use)                                                                          \
            simPrintf (" %4u  %4u        %4u %10p %10p\n", u, prt, p->unit_idx, (void *) p->iom_connect, (void *) p->iom_cmd); \
        }
#define DEV_CTLR(from_big,from_small, to_label, to_big, to_small)               \
    simPrintf ("  %-4s --> %-4s dev_code type\n", #to_label, #from_big);       \
    all (u, N_ ## to_big ## _UNITS_MAX)                                         \
      {                                                                         \
        struct dev_to_ctlr_s * p = & cables->to_small ## _to_ ## from_small[u]; \
        if (p->socket_in_use)                                                          \
          simPrintf (" %4u    %4u   %4u    %5s\n", u, p->ctlr_unit_idx,        \
                  p->dev_code, ctlr_type_strs[p->ctlr_type]);                   \
      }
    CTLR_DEV (MTP, mtp, TAPE, MT, tap);
    if (dump)
      {
        DEV_CTLR (MTP, ctlr, TAPE, MT, tap);
      }
    CTLR_DEV (IPCD, ipcd, DISK, DSK, dsk);
    CTLR_DEV (IPCT, ipct, TAPE, TAP, tap);
    CTLR_DEV (MSP, msp, DISK, DSK, dsk);
    if (dump)
      {
        DEV_CTLR (CTLR, ctlr, DISK, DSK, dsk);
      }
    CTLR_DEV (URP, urp, URP, URP, urd);
    if (dump)
      {
        DEV_CTLR (URP, urp, RDR, RDR, rdr);
      }
    if (dump)
      {
        DEV_CTLR (URP, urp, PUN, PUN, pun);
      }
    if (dump)
      {
        DEV_CTLR (URP, urp, PRT, PRT, prt);
      }

    return SCPE_OK;
  }

simRc sys_cable_ripout (UNUSED int32_t arg, UNUSED const char * buf)
  {
    cable_init ();
    scuInit ();
    return SCPE_OK;
  }

void sysCableInit (void)
  {
#if 0
   if (! cables)
      {
        cables = (struct cables_s *) create_shm ("cables",
                                                 sizeof (struct cables_s));
        if (cables == NULL)
          {
            simPrintf ("create_shm cables failed\n");
            simWarn ("create_shm cables failed\n");
          }
      }
#endif
    cables = & systemState->cables;

    // Initialize data structures
    cable_init ();
  }
