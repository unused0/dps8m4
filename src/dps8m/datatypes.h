/*
 * vim: filetype=c:tabstop=4:ai:expandtab
 * SPDX-License-Identifier: ICU
 * scspell-id: 6965b612-f62e-11ec-a432-80ee73e9b8e7
 *
 * ---------------------------------------------------------------------------
 *
 * Copyright (c) 2012-2016 Harry Reed
 * Copyright (c) 2012 Dave Jordan
 * Copyright (c) 2013-2024 Charles Anthony
 * Copyright (c) 2016 Jean-Michel Merliot
 * Copyright (c) 2021-2024 The DPS8M Development Team
 *
 * This software is made available under the terms of the ICU License.
 * See the LICENSE.md file at the top-level directory of this distribution.
 *
 * ---------------------------------------------------------------------------
 */

# include <stdint.h>


typedef int64_t __int64_t;

# if defined(NEED_128)
typedef struct { uint64_t h; uint64_t l; } x__uint128_t;
typedef struct { int64_t h;  uint64_t l; } x__int128_t;
#  define construct_128(h, l) ((uint128) { (h), (l) })
#  define construct_s128(h, l) ((int128) { (h), (l) })
# endif /* if defined(NEED_128) */

# if defined(__MINGW32__)
#  include <stdint.h>
typedef t_uint64    u_int64_t;
# endif /* if defined(__MINGW32__) */
# if defined(__HAIKU__)
#  include <stdint.h>
# endif /* if defined(__HAIKU__) */

/* Data types */

typedef uint8_t     word1;
typedef uint8_t     word2;
typedef uint8_t     word3;
typedef uint8_t     word4;
typedef uint8_t     word5;
typedef uint8_t     word6;
typedef uint8_t     word7;
typedef uint8_t     word8;
typedef int8_t      word8s; // signed 8-bit quantity
typedef uint16_t    word9;
typedef uint16_t    word10;
typedef uint16_t    word11;
typedef uint16_t    word12;
typedef int16_t     word12s;
typedef uint16_t    word13;
typedef uint16_t    word14;
typedef uint16_t    word15;
typedef uint16_t    word16;
typedef uint32_t    word17;
typedef uint32_t    word18;
typedef uint32_t    word19;
typedef int32_t     word18s;
typedef uint32_t    word20;
typedef int32_t     word20s;
typedef uint32_t    word21;
typedef uint32_t    word22;
typedef uint32_t    word23;
typedef uint32_t    word24;
typedef uint32_t    word27;
typedef int32_t     word27s;
typedef uint32_t    word28;
typedef uint32_t    word32;
typedef uint64_t    word34;
typedef uint64_t    word36;
typedef uint64_t    word37;
typedef uint64_t    word38;
typedef int64_t     word38s;
typedef int64_t     word36s;
# if !defined(NEED_128)
typedef __uint128_t word72;
typedef __int128_t  word72s;
typedef __uint128_t word73;
typedef __uint128_t word74;
typedef __uint128_t uint128;
typedef __int128_t  int128;
# else
typedef x__uint128_t word72;
typedef x__int128_t  word72s;
typedef x__uint128_t word73;
typedef x__uint128_t word74;
typedef x__uint128_t uint128;
typedef x__int128_t  int128;
# endif /* if !defined(NEED_128) */

typedef word36      float36;    // single precision float
typedef word72      float72;    // double precision float

typedef unsigned int uint;   // efficient unsigned int, at least 32 bits

