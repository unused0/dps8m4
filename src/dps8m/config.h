// Enable speed over debugging if not TESTING
# if !defined(TESTING)
#  define SPEED
# endif /* if !defined(TESTING) */

// Quiet compiler unused warnings
# define QUIET_UNUSED

// Experimental dial_out line disconnect delay
// FNP polled ~100Hz; 2 secs. is 200 polls
# define DISC_DELAY 200

// Micro-cache
#  if !defined(UCACHE_STATS)
#   define UCACHE_STATS
#  endif /* if !defined(UCACHE_STATS) */

// Shift/rotate instruction barrel shifter
# define BARREL_SHIFTER 1

# if defined(PANEL68)
#  define PNL(x) x
# else
#  define PNL(x)
# endif /* if defined(PANEL68) */

# define L68_(x) if (cpu.tweaks.l68_mode) { x }
# define DPS8M_(x) if (! cpu.tweaks.l68_mode) { x }

// Debugging tool
# if defined(TESTING)
#  define IF1 if (cpu.tweaks.isolts_mode)
# else
#  define IF1 if (0)
# endif /* if defined(TESTING) */

// Instruction profiler
// #define MATRIX

// Run TR on work done, not wall clock.
// Define one of these; tied to memory access (MEM) or to instruction
// execution (EXEC)

// XXX TR_WORK_MEM break synchronous clocks
//# define TR_WORK_MEM
# define TR_WORK_EXEC

# define MAX_DEV_NAME_LEN 64

// Enable clock sync testing
//#define SYNCTEST
//#define FIX1 // Nope, didn't help

//#define OSCAR 1

