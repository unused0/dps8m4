/*
 * vim: filetype=c:tabstop=4:ai:expandtab
 * SPDX-License-Identifier: ICU
 * SPDX-License-Identifier: Multics
 * scspell-id: 6e07fe19-f62d-11ec-86f2-80ee73e9b8e7
 *
 * ---------------------------------------------------------------------------
 *
 * Copyright (c) 2007-2013 Michael Mondy
 * Copyright (c) 2012-2016 Harry Reed
 * Copyright (c) 2013-2024 Charles Anthony
 * Copyright (c) 2017 Michal Tomek
 * Copyright (c) 2021-2023 Jeffrey H. Johnson
 * Copyright (c) 2021-2024 The DPS8M Development Team
 *
 * This software is made available under the terms of the ICU License.
 * See the LICENSE.md file at the top-level directory of this distribution.
 *
 * ---------------------------------------------------------------------------
 *
 * This source file may contain code comments that adapt, include, and/or
 * incorporate Multics program code and/or documentation distributed under
 * the Multics License.  In the event of any discrepancy between code
 * comments herein and the original Multics materials, the original Multics
 * materials should be considered authoritative unless otherwise noted.
 * For more details and historical background, see the LICENSE.md file at
 * the top-level directory of this distribution.
 *
 * ---------------------------------------------------------------------------
 */

#include <stdio.h>
#include <unistd.h>
#include <ctype.h>

#include "dps8m.h"
#include "iom_channel.h"
#include "iom.h"
#include "cable.h"
#include "addrmods.h"
#include "faults.h"
#include "append.h"
#include "ins.h"
#include "cmd.h"
#include "tap.h"
#include "state.h"
#include "math.h"
#include "iefp.h"
#include "opc.h"
#include "fnp.h"
#include "socket_dev.h"
#include "crdrdr.h"
#include "dia.h"
#include "absi.h"
#include "mgp.h"
#include "utils.h"
# include "shm.h"
#include "opcodetable.h"
#include "sim_defs.h"
# include "threadz.h"

//#include "ver.h"

#define ASSUME0 0

// CPU data structures

static UNIT cpuUnits [N_CPU_UNITS_MAX] = {
#if defined(NO_C_ELLIPSIS)
  { UDATA (NULL, UNIT_FIX|UNIT_BINK, MEMSIZE), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL },
  { UDATA (NULL, UNIT_FIX|UNIT_BINK, MEMSIZE), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL },
  { UDATA (NULL, UNIT_FIX|UNIT_BINK, MEMSIZE), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL },
  { UDATA (NULL, UNIT_FIX|UNIT_BINK, MEMSIZE), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL },
  { UDATA (NULL, UNIT_FIX|UNIT_BINK, MEMSIZE), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL },
  { UDATA (NULL, UNIT_FIX|UNIT_BINK, MEMSIZE), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL },
  { UDATA (NULL, UNIT_FIX|UNIT_BINK, MEMSIZE), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL },
  { UDATA (NULL, UNIT_FIX|UNIT_BINK, MEMSIZE), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL }
#else
  [0 ... N_CPU_UNITS_MAX - 1] = {
    UDATA (NULL, UNIT_FIX|UNIT_BINK, MEMSIZE), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL
  }
#endif
};

#define UNIT_IDX(uptr) ((uptr) - cpuUnits)

struct stall_point_s stall_points [N_STALL_POINTS];
bool stall_point_active = false;

#if defined(PANEL68)
static void panel_process_event (cpuState_t * cpup);
#endif /* if defined(PANEL68) */

#if !defined(SPEED)
static bool watch_bits [MEMSIZE];
#endif /* if !defined(SPEED) */

static uv_loop_t * ev_poll_loop;
static uv_timer_t ev_poll_handle;

// Forward declarations to make simh support code more organizzed.

static simRc simh_cpu_reset_and_clear_unit (UNIT * uptr,
                                             UNUSED int32_t value,
                                             UNUSED const char * cptr,
                                             UNUSED void * desc);


////////////////////////////////////////////////////////////////////////////////
//
// SIMH support code
//
//

static simRc cpu_show_config (UNUSED FILE * st, UNIT * uptr,
                               UNUSED int val, UNUSED const void * desc) {
  long cpuUnitIdx = UNIT_IDX (uptr);
  if (cpuUnitIdx < 0 || cpuUnitIdx >= N_CPU_UNITS_MAX) {
    simWarn ("error: Invalid unit number %ld\n", (long) cpuUnitIdx);
    return SCPE_ARG;
  }

#define PFC_INT8 "%c%c%c%c%c%c%c%c"

#define PBI_8(i)                     \
    ( ((i) & 0x80ll) ? '1' : '0' ),  \
    ( ((i) & 0x40ll) ? '1' : '0' ),  \
    ( ((i) & 0x20ll) ? '1' : '0' ),  \
    ( ((i) & 0x10ll) ? '1' : '0' ),  \
    ( ((i) & 0x08ll) ? '1' : '0' ),  \
    ( ((i) & 0x04ll) ? '1' : '0' ),  \
    ( ((i) & 0x02ll) ? '1' : '0' ),  \
    ( ((i) & 0x01ll) ? '1' : '0' )

#define PFC_INT16 PFC_INT8  PFC_INT8
#define PFC_INT32 PFC_INT16 PFC_INT16
#define PFC_INT64 PFC_INT32 PFC_INT32

#define PBI_16(i) PBI_8((i)  >>  8), PBI_8(i)
#define PBI_32(i) PBI_16((i) >> 16), PBI_16(i)
#define PBI_64(i) PBI_32((i) >> 32), PBI_32(i)

  char dsbin[66], adbin[34];

  sim_msg ("CPU unit number %ld\n", (long) cpuUnitIdx);

  sim_msg ("Fault base:                   %03o(8)\n",      cpus[cpuUnitIdx].switches.FLT_BASE);
  sim_msg ("CPU number:                   %01o(8)\n",      cpus[cpuUnitIdx].switches.cpu_num);
  sim_msg ("Data switches:                %"PRIo36"(8)\n", cpus[cpuUnitIdx].switches.data_switches);
  (void) snprintf (dsbin, 65, PFC_INT64, PBI_64 (cpus[cpuUnitIdx].switches.data_switches));
  sim_msg ("                              %36s(2)\n",      dsbin + strlen(dsbin) - 36);
  sim_msg ("                              ------------------------------------\n");
  sim_msg ("               Bit            000000000011111111112222222222333333\n");
  sim_msg ("                 Positions    012345678901234567890123456789012345\n");
  sim_msg ("\n");
  sim_msg ("Address switches:             %06o(8)\n",      cpus[cpuUnitIdx].switches.addr_switches);
  (void) snprintf (adbin, 33, PFC_INT32, PBI_32 (cpus[cpuUnitIdx].switches.addr_switches));
  sim_msg ("                              %18s(2)\n",      adbin + strlen (adbin) - 18);
  for (int i = 0; i < (cpus[cpuUnitIdx].tweaks.l68_mode ? N_L68_CPU_PORTS : N_DPS8M_CPU_PORTS); i ++) {
    sim_msg ("Port%c enable:                 %01o(8)\n",   'A' + i, cpus[cpuUnitIdx].switches.enable [i]);
    sim_msg ("Port%c init enable:            %01o(8)\n",   'A' + i, cpus[cpuUnitIdx].switches.init_enable [i]);
    sim_msg ("Port%c assignment:             %01o(8)\n",   'A' + i, cpus[cpuUnitIdx].switches.assignment [i]);
    sim_msg ("Port%c interlace:              %01o(8)\n",   'A' + i, cpus[cpuUnitIdx].switches.interlace [i]);
    sim_msg ("Port%c store size:             %01o(8)\n",   'A' + i, cpus[cpuUnitIdx].switches.store_size [i]);
  }
  sim_msg ("Processor mode:               %s [%o]\n",      cpus[cpuUnitIdx].switches.procMode == procModeMultics ?
                                                             "Multics" : cpus[cpuUnitIdx].switches.procMode == procModeGCOS ?
                                                             "GCOS" : "???",
                                                           cpus[cpuUnitIdx].switches.procMode);
  sim_msg ("8K Cache:                     %s\n",           cpus[cpuUnitIdx].switches.enable_cache ? "Enabled" : "Disabled");
  sim_msg ("SDWAM:                        %s\n",           cpus[cpuUnitIdx].switches.sdwam_enable ? "Enabled" : "Disabled");
  sim_msg ("PTWAM:                        %s\n",           cpus[cpuUnitIdx].switches.ptwam_enable ? "Enabled" : "Disabled");

  sim_msg ("Processor speed:              %02o(8)\n",      cpus[cpuUnitIdx].options.proc_speed);
  sim_msg ("Enable simulated SDWAM/PTWAM: %01o(8)\n",      cpus[cpuUnitIdx].tweaks.enable_wam);
  sim_msg ("TRO faults enabled:           %01o(8)\n",      cpus[cpuUnitIdx].tweaks.tro_enable);
  sim_msg ("useMap:                       %d\n",           cpus[cpuUnitIdx].tweaks.useMap);
  sim_msg ("PROM installed:               %01o(8)\n",      cpus[cpuUnitIdx].options.prom_installed);
  sim_msg ("Hex mode installed:           %01o(8)\n",      cpus[cpuUnitIdx].options.hex_mode_installed);
  sim_msg ("8K cache installed:           %01o(8)\n",      cpus[cpuUnitIdx].options.cache_installed);
  sim_msg ("Clock slave installed:        %01o(8)\n",      cpus[cpuUnitIdx].options.clock_slave_installed);
#if defined(AFFINITY)
  if (cpus[cpuUnitIdx].tweaks.set_affinity)
    sim_msg ("CPU affinity:                 %d\n",         cpus[cpuUnitIdx].tweaks.affinity);
  else
    sim_msg ("CPU affinity:                 not set\n");
#endif
  sim_msg ("ISOLTS mode:                  %01o(8)\n",      cpus[cpuUnitIdx].tweaks.isolts_mode);
  sim_msg ("NODIS mode:                   %01o(8)\n",      cpus[cpuUnitIdx].tweaks.runStart);
  sim_msg ("6180 mode:                    %01o(8) [%s]\n", cpus[cpuUnitIdx].tweaks.l68_mode, cpus[cpuUnitIdx].tweaks.l68_mode ? "6180" : "DPS8/M");
  return SCPE_OK;
}

//
// set cpu0 config=<blah> [;<blah>]
//
//    blah =
//           faultbase = n
//           num = n
//           data = n
//           portenable = n
//           portconfig = n
//           portinterlace = n
//           mode = n
//           speed = n
//    Hacks:
//           steadyclock = on|off
//           enable_wam = n
//           tro_enable = n

static config_value_list_t cfg_multics_fault_base [] = {
  { "multics", 2 },
  { NULL,      0 }
};

static config_value_list_t cfg_on_off [] = {
  { "off",     0 },
  { "on",      1 },
  { "disable", 0 },
  { "enable",  1 },
  { NULL,      0 }
};

static config_value_list_t cfg_l68_mode [] = {
  { "dps8/m", 0 },
  { "dps8m",  0 },
  { "dps8",   0 },
  { "l68",    1 },
  { "l6180",  1 },
  { "6180",   1 },
};

static config_value_list_t cfg_cpu_mode [] = {
  { "gcos",    0 },
  { "multics", 1 },
  { NULL,      0 }
};

static config_value_list_t cfg_port_letter [] = {
  { "a",  0 },
  { "b",  1 },
  { "c",  2 },
  { "d",  3 },
  { "e",  4 },
  { "f",  5 },
  { "g",  6 },
  { "h",  7 },
  { NULL, 0 }
};

static config_value_list_t cfg_interlace [] = {
  { "off", 0 },
  { "2",   2 },
  { "4",   4 },
  { NULL,  0 }
};

#if defined(AFFINITY)
static config_value_list_t cfg_affinity [] = {
  { "off", -1 },
  { NULL,  0  }
};
#endif

static config_value_list_t cfg_size_list [] = {
#if 0
// For Level-68:
// rsw.incl.pl1
//
//  /* DPS and L68 memory sizes */
//  dcl  dps_mem_size_table (0:7) fixed bin (24) static options (constant) init
//      (32768, 65536, 4194304, 131072, 524288, 1048576, 2097152, 262144);
//
//  Note that the third array element above, is changed incompatibly in MR10.0.
//  In previous releases, this array element was used to decode a port size of
//  98304 (96K). With MR10.0 it is now possible to address 4MW per CPU port, by
//  installing  FCO # PHAF183 and using a group 10 patch plug, on L68 and DPS
//  CPUs.

  { "32",    0 }, //   32768
  { "64",    1 }, //   65536
  { "4096",  2 }, // 4194304
  { "128",   3 }, //  131072
  { "512",   4 }, //  524288
  { "1024",  5 }, // 1048576
  { "2048",  6 }, // 2097152
  { "256",   7 }, //  262144

  { "32K",   0 },
  { "64K",   1 },
  { "4096K", 2 },
  { "128K",  3 },
  { "512K",  4 },
  { "1024K", 5 },
  { "2048K", 6 },
  { "256K",  7 },

  { "1M", 5 },
  { "2M", 6 },
  { "4M", 2 },

// For DPS8/M:
// These values are taken from the dps8_mem_size_table loaded by the boot tape.

  {    "32", 0 },
  {    "64", 1 },
  {   "128", 2 },
  {   "256", 3 },
  {   "512", 4 },
  {  "1024", 5 },
  {  "2048", 6 },
  {  "4096", 7 },

  {   "32K", 0 },
  {   "64K", 1 },
  {  "128K", 2 },
  {  "256K", 3 },
  {  "512K", 4 },
  { "1024K", 5 },
  { "2048K", 6 },
  { "4096K", 7 },

  { "1M", 5 },
  { "2M", 6 },
  { "4M", 7 },
  { NULL, 0 }
#endif
  { "32",     8 },    //   32768
  { "32K",    8 },    //   32768
  { "64",     9 },    //   65536
  { "64K",    9 },    //   65536
  { "128",   10 },    //  131072
  { "128K",  10 },    //  131072
  { "256",   11 },    //  262144
  { "256K",  11 },    //  262144
  { "512",   12 },    //  524288
  { "512K",  12 },    //  524288
  { "1024",  13 },    // 1048576
  { "1024K", 13 },    // 1048576
  { "1M",    13 },
  { "2048",  14 },    // 2097152
  { "2048K", 14 },    // 2097152
  { "2M",    14 },
  { "4096",  15 },    // 4194304
  { "4096K", 15 },    // 4194304
  { "4M",    15 },
  { NULL,    0  }
};

static configList_t cpu_config_list [] = {
  { "faultbase",             0,  0177,            cfg_multics_fault_base },
  { "num",                   0,  07,              NULL                   },
  { "data",                  0,  W36_C (0777777777777),   NULL                   },
  { "stopnum",               0,  999999,          NULL                   },
  { "mode",                  0,  01,              cfg_cpu_mode           },
  { "speed",                 0,  017,             NULL                   },  // XXX use keywords
  { "port",                  0,  N_CPU_PORTS - 1, cfg_port_letter        },
  { "assignment",            0,  7,               NULL                   },
  { "interlace",             0,  1,               cfg_interlace          },
  { "enable",                0,  1,               cfg_on_off             },
  { "init_enable",           0,  1,               cfg_on_off             },
  { "store_size",            0,  7,               cfg_size_list          },
  { "enable_cache",          0,  1,               cfg_on_off             },
  { "sdwam",                 0,  1,               cfg_on_off             },
  { "ptwam",                 0,  1,               cfg_on_off             },

  // Hacks
  { "enable_wam",            0,  1,               cfg_on_off             },
  { "tro_enable",            0,  1,               cfg_on_off             },
  { "useMap",                0,  1,               cfg_on_off             },
  { "address",               0,  0777777,         NULL                   },
  { "prom_installed",        0,  1,               cfg_on_off             },
  { "hex_mode_installed",    0,  1,               cfg_on_off             },
  { "cache_installed",       0,  1,               cfg_on_off             },
  { "clock_slave_installed", 0,  1,               cfg_on_off             },
  { "enable_emcall",         0,  1,               cfg_on_off             },

  // Tuning
#if defined(AFFINITY)
  { "affinity",              -1, 32767,           cfg_affinity           },
#endif
  { "isolts_mode",           0,  1,               cfg_on_off             },
  { "nodis",                 0,  1,               cfg_on_off             },
  { "l68_mode",              0,  1,               cfg_l68_mode           },
  { "enableluf",             0,  1,               cfg_on_off             },
  { "kips",                  0,  1000000,         NULL                   },
  { NULL,                    0,  0,               NULL                   }
};

static void setLUFLimits (uint cpuUnitIdx, uint kips) {
  cpus[cpuUnitIdx].tweaks.kips = kips;
  uint64_t kips_ = (uint64_t) kips;
  cpus[cpuUnitIdx].tweaks.luf_limits[0] =  2000 * kips_ / 1000;
  cpus[cpuUnitIdx].tweaks.luf_limits[1] =  4000 * kips_ / 1000;
  cpus[cpuUnitIdx].tweaks.luf_limits[2] =  8000 * kips_ / 1000;
  cpus[cpuUnitIdx].tweaks.luf_limits[3] = 16000 * kips_ / 1000;
  cpus[cpuUnitIdx].tweaks.luf_limits[4] = 32000 * kips_ / 1000;
}

static simRc cpu_set_config (UNIT * uptr, UNUSED int32_t value,
                              const char * cptr, UNUSED void * desc) {
  long cpuUnitIdx = UNIT_IDX (uptr);
  if (cpuUnitIdx < 0 || cpuUnitIdx >= N_CPU_UNITS_MAX) {
    simWarn ("error: cpu_set_config: Invalid unit number %ld\n", (long) cpuUnitIdx);
    return SCPE_ARG;
  }

  static int port_num = 0;

  configState_t cfg_state = { NULL, NULL };

  for (;;) {
    int64_t v;
    int rc = cfgParse (__func__, cptr, cpu_config_list, & cfg_state, & v);
    if (rc == CFG_DONE) {
      break;
    }
    if (rc == CFG_ERROR) {
      cfgParseDone (& cfg_state);
      return SCPE_ARG;
    }

    const char * p = cpu_config_list [rc] . name;
    if (strcmp (p, "faultbase") == 0)
      cpus[cpuUnitIdx].switches.FLT_BASE = (uint) v;
    else if (strcmp (p, "num") == 0)
      cpus[cpuUnitIdx].switches.cpu_num = (uint) v;
    else if (strcmp (p, "data") == 0)
      cpus[cpuUnitIdx].switches.data_switches = (word36) v;
    else if (strcmp (p, "stopnum") == 0) {
      // set up for check stop
      // convert stopnum to bcd
      int64_t d1 = (v / 1000) % 10;
      int64_t d2 = (v /  100) % 10;
      int64_t d3 = (v /   10) % 10;
      int64_t d4 = (v /    1) % 10;
      word36 d = W36_C (0123000000000);
      putbits36_6 (& d,  9, (word4) d1);
      putbits36_6 (& d, 15, (word4) d2);
      putbits36_6 (& d, 21, (word4) d3);
      putbits36_6 (& d, 27, (word4) d4);
      cpus[cpuUnitIdx].switches.data_switches = d;
    }
    else if (strcmp (p, "address") == 0)
      cpus[cpuUnitIdx].switches.addr_switches = (word18) v;
    else if (strcmp (p, "mode") == 0)
      cpus[cpuUnitIdx].switches.procMode = v ? procModeMultics : procModeGCOS;
    else if (strcmp (p, "speed") == 0)
      cpus[cpuUnitIdx].options.proc_speed = (uint) v;
    else if (strcmp (p, "port") == 0) {
      // DPS8 has 4 ports; 6180 has 8.
      if ((! cpus[cpuUnitIdx].tweaks.l68_mode) && (int) v > 4) {
        cfgParseDone (& cfg_state);
        return SCPE_ARG;
      }
      port_num = (int) v;
    }
    else if (strcmp (p, "assignment") == 0)
      cpus[cpuUnitIdx].switches.assignment [port_num] = (uint) v;
    else if (strcmp (p, "interlace") == 0)
      cpus[cpuUnitIdx].switches.interlace [port_num] = (uint) v;
    else if (strcmp (p, "enable") == 0)
      cpus[cpuUnitIdx].switches.enable [port_num] = (uint) v;
    else if (strcmp (p, "init_enable") == 0)
      cpus[cpuUnitIdx].switches.init_enable [port_num] = (uint) v;
    else if (strcmp (p, "store_size") == 0) {
      // If the store size is specified as a switch postion, the parser
      // will return 0-7; if it is specified as a word count, the parser
      // will return 8-15 (32K, 64K, ... 4096K). The following code
      // determines the switch setting based on the CPU model.
      // See "store_size encoding" in cpu.h
      if (v > 7) {
        // DPS8 and 6160 have different mapppings from switch position
        // to memory size.
        if (cpus[cpuUnitIdx].tweaks.l68_mode) {
          switch (v) {
            case  8:  v = 0;   break; // 32K
            case  9:  v = 1;   break; // 64K
            case 10:  v = 3;   break; // 128K
            case 11:  v = 7;   break; // 256K
            case 12:  v = 4;   break; // 512K
            case 13:  v = 5;   break; // 1024K
            case 14:  v = 6;   break; // 2048K
            case 15:  v = 2;   break; // 4096K
          }
        } else {
          switch (v) {
            case  8:  v = 0;   break; // 32K
            case  9:  v = 1;   break; // 64K
            case 10:  v = 2;   break; // 128K
            case 11:  v = 3;   break; // 256K
            case 12:  v = 4;   break; // 512K
            case 13:  v = 5;   break; // 1024K
            case 14:  v = 6;   break; // 2048K
            case 15:  v = 7;   break; // 4096K
          }
        }
      }
      cpus[cpuUnitIdx].switches.store_size [port_num] = (uint) v;
    }
    else if (strcmp (p, "enable_cache") == 0)
      cpus[cpuUnitIdx].switches.enable_cache = (uint) v ? true : false;
    else if (strcmp (p, "sdwam") == 0)
      cpus[cpuUnitIdx].switches.sdwam_enable = (uint) v ? true : false;
    else if (strcmp (p, "ptwam") == 0)
      cpus[cpuUnitIdx].switches.ptwam_enable = (uint) v ? true : false;
    else if (strcmp (p, "enable_wam") == 0)
      cpus[cpuUnitIdx].tweaks.enable_wam = (uint) v;
    else if (strcmp (p, "tro_enable") == 0)
      cpus[cpuUnitIdx].tweaks.tro_enable = (uint) v;
    else if (strcmp (p, "useMap") == 0)
      cpus[cpuUnitIdx].tweaks.useMap = v;
    else if (strcmp (p, "prom_installed") == 0)
      cpus[cpuUnitIdx].options.prom_installed = v;
    else if (strcmp (p, "hex_mode_installed") == 0)
      cpus[cpuUnitIdx].options.hex_mode_installed = v;
    else if (strcmp (p, "cache_installed") == 0)
      cpus[cpuUnitIdx].options.cache_installed = v;
    else if (strcmp (p, "clock_slave_installed") == 0)
      cpus[cpuUnitIdx].options.clock_slave_installed = v;
    else if (strcmp (p, "enable_emcall") == 0)
      cpus[cpuUnitIdx].tweaks.enable_emcall = v;
#if defined(AFFINITY)
    else if (strcmp (p, "affinity") == 0)
      if (v < 0) {
        cpus[cpuUnitIdx].tweaks.set_affinity = false;
      }
      else {
        cpus[cpuUnitIdx].tweaks.set_affinity = true;
        cpus[cpuUnitIdx].tweaks.affinity = (uint) v;
      }
#endif
      else if (strcmp (p, "isolts_mode") == 0) {
        bool was = cpus[cpuUnitIdx].tweaks.isolts_mode;
        cpus[cpuUnitIdx].tweaks.isolts_mode = v;
        if (v && ! was) {
          uint store_sz;
          if (cpus[cpuUnitIdx].tweaks.l68_mode) // L68
            store_sz = 3;
          else // DPS8M
            store_sz = 2;
          cpus[cpuUnitIdx].isolts_switches_save     = cpus[cpuUnitIdx].switches;

          cpus[cpuUnitIdx].switches.data_switches   = W36_C (00000030714000);
          cpus[cpuUnitIdx].switches.addr_switches   = 0100150;
          cpus[cpuUnitIdx].tweaks.useMap            = true;
          cpus[cpuUnitIdx].tweaks.enable_wam        = true;
          cpus[cpuUnitIdx].switches.assignment  [0] = 0;
          cpus[cpuUnitIdx].switches.interlace   [0] = false;
          cpus[cpuUnitIdx].switches.enable      [0] = false;
          cpus[cpuUnitIdx].switches.init_enable [0] = false;
          cpus[cpuUnitIdx].switches.store_size  [0] = store_sz;

          cpus[cpuUnitIdx].switches.assignment  [1] = 0;
          cpus[cpuUnitIdx].switches.interlace   [1] = false;
          cpus[cpuUnitIdx].switches.enable      [1] = true;
          cpus[cpuUnitIdx].switches.init_enable [1] = false;
          cpus[cpuUnitIdx].switches.store_size  [1] = store_sz;

          cpus[cpuUnitIdx].switches.assignment  [2] = 0;
          cpus[cpuUnitIdx].switches.interlace   [2] = false;
          cpus[cpuUnitIdx].switches.enable      [2] = false;
          cpus[cpuUnitIdx].switches.init_enable [2] = false;
          cpus[cpuUnitIdx].switches.store_size  [2] = store_sz;

          cpus[cpuUnitIdx].switches.assignment  [3] = 0;
          cpus[cpuUnitIdx].switches.interlace   [3] = false;
          cpus[cpuUnitIdx].switches.enable      [3] = false;
          cpus[cpuUnitIdx].switches.init_enable [3] = false;
          cpus[cpuUnitIdx].switches.store_size  [3] = store_sz;

          if (cpus[cpuUnitIdx].tweaks.l68_mode) { // L68
            cpus[cpuUnitIdx].switches.assignment  [4] = 0;
            cpus[cpuUnitIdx].switches.interlace   [4] = false;
            cpus[cpuUnitIdx].switches.enable      [4] = false;
            cpus[cpuUnitIdx].switches.init_enable [4] = false;
            cpus[cpuUnitIdx].switches.store_size  [4] = 3;

            cpus[cpuUnitIdx].switches.assignment  [5] = 0;
            cpus[cpuUnitIdx].switches.interlace   [5] = false;
            cpus[cpuUnitIdx].switches.enable      [5] = false;
            cpus[cpuUnitIdx].switches.init_enable [5] = false;
            cpus[cpuUnitIdx].switches.store_size  [5] = 3;

            cpus[cpuUnitIdx].switches.assignment  [6] = 0;
            cpus[cpuUnitIdx].switches.interlace   [6] = false;
            cpus[cpuUnitIdx].switches.enable      [6] = false;
            cpus[cpuUnitIdx].switches.init_enable [6] = false;
            cpus[cpuUnitIdx].switches.store_size  [6] = 3;

            cpus[cpuUnitIdx].switches.assignment  [7] = 0;
            cpus[cpuUnitIdx].switches.interlace   [7] = false;
            cpus[cpuUnitIdx].switches.enable      [7] = false;
            cpus[cpuUnitIdx].switches.init_enable [7] = false;
            cpus[cpuUnitIdx].switches.store_size  [7] = 3;
          }
          cpus[cpuUnitIdx].switches.enable      [1] = true;

          if (cpus[cpuUnitIdx].executing) {
            cpus[cpuUnitIdx].forceRestart = true;
            wakeCPU (cpuUnitIdx);
          } else {
            cpuResetUnitIdx ((uint) cpuUnitIdx, false);
            //simh_cpu_reset_and_clear_unit (cpuUnits + cpuUnitIdx, 0, NULL, NULL);
          }

        } else if (was && !v) {
          cpus[cpuUnitIdx].switches = cpus[cpuUnitIdx].isolts_switches_save;

          if (cpus[cpuUnitIdx].executing) {
            cpus[cpuUnitIdx].forceRestart = true;
            wakeCPU (cpuUnitIdx);
          } else {
            cpuResetUnitIdx ((uint) cpuUnitIdx, false);
            //simh_cpu_reset_and_clear_unit (cpuUnits + cpuUnitIdx, 0, NULL, NULL);
          }

        }
      }
      else if (strcmp (p, "nodis") == 0)
        cpus[cpuUnitIdx].tweaks.runStart = v;
      else if (strcmp (p, "l68_mode") == 0)
        cpus[cpuUnitIdx].tweaks.l68_mode = v;
      else if (strcmp (p, "enableluf") == 0)
        cpus[cpuUnitIdx].tweaks.enableLUF = v;
      else if (strcmp (p, "kips") == 0) {
        cpus[cpuUnitIdx].tweaks.kips = v;
        setLUFLimits (cpuUnitIdx, (uint) v);
      }
      else {
        simWarn ("error: cpu_set_config: Invalid cfgParse rc <%ld>\n", (long) rc);
        cfgParseDone (& cfg_state);
        return SCPE_ARG;
      }
    } // process statements
  cfgParseDone (& cfg_state);

  return SCPE_OK;
}

static simRc cpu_show_nunits (UNUSED FILE * st, UNUSED UNIT * uptr, UNUSED int val, UNUSED const void * desc) {
  sim_msg ("Number of CPUs in system is %d\n", cpu_dev.numunits);
  return SCPE_OK;
}

static simRc cpu_set_nunits (UNUSED UNIT * uptr, UNUSED int32_t value, const char * cptr, UNUSED void * desc) {
  if (! cptr)
    return SCPE_ARG;
  int n = atoi (cptr);
  if (n < 0 || n > N_CPU_UNITS_MAX)
    return SCPE_ARG;
  cpu_dev.numunits = (uint32_t) n;
  return SCPE_OK;
}

static simRc cpu_show_kips (UNUSED FILE * st, UNUSED UNIT * uptr, UNUSED int val, UNUSED const void * desc) {
    for (uint i = 0; i < N_CPU_UNITS_MAX; i ++)
      sim_msg ("CPU %c KIPS %u\n", i, cpus[i].tweaks.kips);
    return SCPE_OK;
  }

static simRc cpu_set_kips (UNUSED UNIT * uptr, UNUSED int32_t value, const char * cptr, UNUSED void * desc) {
  if (! cptr)
    return SCPE_ARG;
  int n = atoi (cptr);
  if (n < 1 || n > 100000)
    return SCPE_ARG;
   for (uint i = 0; i < N_CPU_UNITS_MAX; i ++)
    setLUFLimits (i, (uint) n);
  return SCPE_OK;
}

static simRc cpu_show_stall (UNUSED FILE * st, UNUSED UNIT * uptr, UNUSED int val, UNUSED const void * desc) {
  if (! stall_point_active) {
    simPrintf ("No stall points\n");
    return SCPE_OK;
  }

  simPrintf ("Stall points\n");
  for (int i = 0; i < N_STALL_POINTS; i ++)
    if (stall_points[i].segno || stall_points[i].offset) {
#if defined(WIN_STDIO)
      simPrintf ("%2ld %05o:%06o %10lu\n",
                  (long) i, stall_points[i].segno, stall_points[i].offset,
                  (unsigned long) stall_points[i].time);
#else
      simPrintf ("%2ld %05o:%06o %'10lu\n",
                  (long) i, stall_points[i].segno, stall_points[i].offset,
                  (unsigned long) stall_points[i].time);
#endif
        }
    return SCPE_OK;
  }

// set cpu stall=n=s:o=t
//   n stall point number
//   s segment number (octal)
//   o offset (octal)
//   t time in microseconds (decimal)

static simRc cpu_set_stall (UNUSED UNIT * uptr, UNUSED int32_t value, const char * cptr, UNUSED void * desc) {
  long n, s, o, t;
  char * end;

  if (! cptr)
    return SCPE_ARG;

  n = strtol (cptr, & end, 0);
  if (* end != '=')
    return SCPE_ARG;
  if (n < 0 || n >= N_STALL_POINTS)
    return SCPE_ARG;

  s = strtol (end + 1, & end, 8);
  if (* end != ':')
    return SCPE_ARG;
  if (s < 0 || s > MASK15)
    return SCPE_ARG;

  o = strtol (end + 1, & end, 8);
  if (* end != '=')
    return SCPE_ARG;
  if (o < 0 || o > MASK18)
    return SCPE_ARG;

  t = strtol (end + 1, & end, 0);
  if (* end != 0)
    return SCPE_ARG;
  if (t < 0 || t >= 100000000)
    return SCPE_ARG;

  stall_points[n].segno  = (word15) s;
  stall_points[n].offset = (word18) o;
  stall_points[n].time   = (useconds_t) t;
  stall_point_active     = false;

  for (int i = 0; i < N_STALL_POINTS; i ++)
    if (stall_points[n].segno && stall_points[n].offset)
      stall_point_active = true;

  return SCPE_OK;
}

// Set a CPU to be a Level 68 (6180), with 16MW on the low four PORTS.
static simRc setCPUConfigL68 (UNIT * uptr, UNUSED int32_t value, UNUSED const char * cptr, UNUSED void * desc) {
  long cpuUnitIdx = UNIT_IDX (uptr);
  if (cpuUnitIdx < 0 || cpuUnitIdx >= N_CPU_UNITS_MAX)
    return SCPE_ARG;
  cpuState_t * cpun = cpus + cpuUnitIdx;

  cpun->tweaks.l68_mode = 1;
  cpun->options.hex_mode_installed = 0;
  for (uint port_num = 0; port_num < N_DPS8M_CPU_PORTS; port_num ++) {
    cpun->switches.assignment[port_num] = port_num;
    cpun->switches.interlace[port_num] = 0;
    cpun->switches.store_size[port_num] = 2;
    cpun->switches.enable[port_num] = 1;
    cpun->switches.init_enable[port_num] = 1;
  }
  for (uint port_num = N_DPS8M_CPU_PORTS; port_num < N_L68_CPU_PORTS; port_num ++) {
    cpun->switches.assignment[port_num] = 0;
    cpun->switches.interlace[port_num] = 0;
    cpun->switches.store_size[port_num] = 0;
    cpun->switches.enable[port_num] = 0;
    cpun->switches.init_enable[port_num] = 0;
  }
  return SCPE_OK;
}

// Set a CPU to be a DPS8/M with 16MW on the four PORTS.
static simRc setCPUConfigDPS8M (UNIT * uptr, UNUSED int32_t value, UNUSED const char * cptr, UNUSED void * desc) {
  long cpuUnitIdx = UNIT_IDX (uptr);
  if (cpuUnitIdx < 0 || cpuUnitIdx >= N_CPU_UNITS_MAX)
    return SCPE_ARG;
  cpuState_t * cpun = cpus + cpuUnitIdx;

  cpun->tweaks.l68_mode = 0;
  cpun->options.hex_mode_installed = 0;
  for (uint port_num = 0; port_num < N_DPS8M_CPU_PORTS; port_num ++) {
    cpun->switches.assignment[port_num] = port_num;
    cpun->switches.interlace[port_num] = 0;
    cpun->switches.store_size[port_num] = 7;
    cpun->switches.enable[port_num] = 1;
    cpun->switches.init_enable[port_num] = 1;
  }
  for (uint port_num = N_DPS8M_CPU_PORTS; port_num < N_L68_CPU_PORTS; port_num ++) {
    cpun->switches.assignment[port_num] = 0;
    cpun->switches.interlace[port_num] = 0;
    cpun->switches.store_size[port_num] = 0;
    cpun->switches.enable[port_num] = 0;
    cpun->switches.init_enable[port_num] = 0;
  }
  return SCPE_OK;
}

char * cycleString (cycles_e cycle) {
  switch (cycle) {
    case FAULT_cycle:
      return "FAULT_cycle";
    case EXEC_cycle:
      return "EXEC_cycle";
    case FAULT_EXEC_cycle:
      return "FAULT_EXEC_cycle";
    case INTERRUPT_cycle:
      return "INTERRUPT_cycle";
    case INTERRUPT_EXEC_cycle:
      return "INTERRUPT_EXEC_cycle";
    case FETCH_cycle:
      return "FETCH_cycle";
    case PSEUDO_FETCH_cycle:
      return "PSEUDO_FETCH_cycle";
    case SYNC_FAULT_RTN_cycle:
      return "SYNC_FAULT_RTN_cycle";
    default:
      return "unknown cycle";
  }
}


static void inline setCpuCycle (cpuState_t * cpup, cycles_e cycle) {
  cpu.cycle = cycle;
}

// DPS8M Memory of 36 bit words is implemented as an array of 64 bit words.
// Put state information into the unused high order bits.
#define MEM_UNINITIALIZED (1LLU<<62)

static void cpuClearMemory (uint cpuUnitIdx) {
  cpuState_t * cpup = cpus + cpuUnitIdx;
  if (cpup->tweaks.isolts_mode) {
    // Currently isolts_mode requires useMap, so this is redundant
    if (cpup->tweaks.useMap) {
      for (uint pgnum = 0; pgnum < N_SCBANKS; pgnum ++) {
        int base = cpup->sc_addr_map [pgnum];
        if (base < 0)
          continue;
        for (uint addr = 0; addr < SCBANK_SZ; addr ++) {
          coreWriteUnmasked (cpup, pgnum * SCBANK_SZ + addr, W36_C (0), __func__);
        }
      }
    }
  } else {
    for (uint addr = 0; addr < MEMSIZE; addr ++) {
      coreWriteUnmasked (cpup, addr, W36_C (0), __func__);
    }
  }
}

void cpuResetUnitIdx (uint cpun, bool clear_mem) {
  cpuState_t * cpup = & cpus [cpun];

  setup_scbank_map (cpup);

  if (clear_mem) {
    cpuClearMemory (cpun);
  }
  cpu.rA = 0;
  cpu.rQ = 0;

  cpu.PPR.IC   = 0;
  cpu.PPR.PRR  = 0;
  cpu.PPR.PSR  = 0;
  cpu.PPR.P    = 1;
  cpu.RSDWH_R1 = 0;
  cpu.rTR      = MASK27;
  // clock_gettime (CLOCK_MONOTONIC, & cpu.rTRTime);
  if (cpu.tweaks.isolts_mode) {
    cpu.shadowTR = 0;
    cpu.rTRlsb   = 0;
  }
  cpu.rTRticks = 0;

  set_addr_mode (cpup, ABSOLUTE_mode);
  SET_I_NBAR;

  cpu.CMR.LUF  = 3;    // default of 16 mS
  cpu.cu.SD_ON = cpu.switches.sdwam_enable ? 1 : 0;
  cpu.cu.PT_ON = cpu.switches.ptwam_enable ? 1 : 0;
  cpu.lufOccurred = false;

  if (cpu.tweaks.runStart) {
    setCpuCycle (cpup, FETCH_cycle);
  } else {
    setCpuCycle (cpup, EXEC_cycle);
    cpu.cu.IWB = W36_C (0000000616200); // Stuff DIS w/interrupt inhibit in instruction buffer
  }
  cpu.wasXfer        = false;
  cpu.wasInhibited   = false;

  cpu.interrupt_flag = false;
  cpu.g7_flag        = false;

  cpu.faultRegister [0] = 0;
  cpu.faultRegister [1] = 0;

#if defined(RAPRx)
  cpu.apu.lastCycle = UNKNOWN_CYCLE;
#endif

  //cpu.rcfDelete = false;

  (void) memset (& cpu.PPR, 0, sizeof (ppr_s));

  tidy_cu (cpup);
}

static simRc simh_cpu_reset_and_clear_unit (UNIT * uptr, UNUSED int32_t value, UNUSED const char * cptr, UNUSED void * desc) {
  long cpuUnitIdx = UNIT_IDX (uptr);
  cpuClearMemory ((uint) cpuUnitIdx);
  // Crashes console?
  cpuResetUnitIdx ((uint) cpuUnitIdx, false);
  return SCPE_OK;
}

static simRc simh_cpu_reset_unit (UNIT * uptr, UNUSED int32_t value, UNUSED const char * cptr, UNUSED void * desc) {
  long cpuUnitIdx = UNIT_IDX (uptr);
  cpuResetUnitIdx ((uint) cpuUnitIdx, false); // no clear memory
  return SCPE_OK;
}

static MTAB cpuMods[] = {
  { MTAB_unit_value,                /* Mask               */
    0,                              /* Match              */
    "CONFIG",                       /* Print string       */
    "CONFIG",                       /* Match string       */
    cpu_set_config,                 /* Validation routine */
    cpu_show_config,                /* Display routine    */
    NULL,                           /* Value descriptor   */
    NULL                            /* Help               */
  },

// RESET  -- reset CPU
// INITIALIZE -- reset CPU

  { MTAB_unit_value,                /* Mask               */
    0,                              /* Match              */
    "RESET",                        /* Print string       */
    "RESET",                        /* Match string       */
    simh_cpu_reset_unit,            /* Validation routine */
    NULL,                           /* Display routine    */
    NULL,                           /* Value descriptor   */
    NULL                            /* Help               */
  },

  { MTAB_unit_value,                /* Mask               */
    0,                              /* Match              */
    "INITIALIZE",                   /* Print string       */
    "INITIALIZE",                   /* Match string       */
    simh_cpu_reset_unit,            /* Validation routine */
    NULL,                           /* Display routine    */
    NULL,                           /* Value descriptor   */
    NULL                            /* Help               */
  },

// INITAILIZEANDCLEAR -- reset CPU, clear Memory
// IAC -- reset CPU, clear Memory

  { MTAB_unit_value,                /* Mask               */
    0,                              /* Match              */
    "INITIALIZEANDCLEAR",           /* Print string       */
    "INITIALIZEANDCLEAR",           /* Match string       */
    simh_cpu_reset_and_clear_unit,  /* Validation routine */
    NULL,                           /* Display routine    */
    NULL,                           /* Value descriptor   */
    NULL                            /* Help               */
  },

  { MTAB_unit_value,                /* Mask               */
    0,                              /* Match              */
    "IAC",                          /* Print string       */
    "IAC",                          /* Match string       */
    simh_cpu_reset_and_clear_unit,  /* Validation routine */
    NULL,                           /* Display routine    */
    NULL,                           /* Value descriptor   */
    NULL                            /* Help               */
  },

  { MTAB_dev_value,                 /* Mask               */
    0,                              /* Match              */
    "NUNITS",                       /* Print string       */
    "NUNITS",                       /* Match string       */
    cpu_set_nunits,                 /* Validation routine */
    cpu_show_nunits,                /* Display routine    */
    NULL,                           /* Value descriptor   */
    NULL                            /* Help               */
  },

  { MTAB_dev_value,                 /* Mask               */
    0,                              /* Match              */
    "KIPS",                         /* Print string       */
    "KIPS",                         /* Match string       */
    cpu_set_kips,                   /* Validation routine */
    cpu_show_kips,                  /* Display routine    */
    NULL,                           /* Value descriptor   */
    NULL                            /* Help               */
  },

  { MTAB_dev_value,                 /* Mask               */
    0,                              /* Match              */
    "STALL",                        /* Print string       */
    "STALL",                        /* Match string       */
    cpu_set_stall,                  /* Validation routine */
    cpu_show_stall,                 /* Display routine    */
    NULL,                           /* Value descriptor   */
    NULL                            /* Help               */
  },

  { MTAB_unit_value,                /* Mask               */
    0,                              /* Match              */
    "DPS8M",                        /* Print string       */
    "DPS8M",                        /* Match string       */
    setCPUConfigDPS8M,              /* Validation routine */
    NULL,                           /* Display routine    */
    NULL,                           /* Value descriptor   */
    NULL                            /* Help               */
  },

  { MTAB_unit_value,                /* Mask               */
    0,                              /* Match              */
    "L68",                          /* Print string       */
    "L68",                          /* Match string       */
    setCPUConfigL68,                /* Validation routine */
    NULL,                           /* Display routine    */
    NULL,                           /* Value descriptor   */
    NULL                            /* Help               */
  },

  { 0, 0, NULL, NULL, NULL, NULL, NULL, NULL }
};

// This is part of the scp interface
const char *sim_stop_messages[] = {
  "Unknown error",           // SCPE_OK
  "Simulation stop",         // STOP_STOP
  "Breakpoint",              // STOP_BKPT
};

/* End of scp interface */

/* Processor configuration switches
 *
 * From AM81-04 Multics System Maintenance Procedures
 *
 * "A Level 68 IOM system may contain a maximum of 7 CPUs, 4 IOMs, 8 SCUs,
 * and 16MW of memory ...
 * [CAC]: ... but AN87 says Multics only supports two IOMs
 *
 * ASSIGNMENT: 3 toggle switches determine the base address of the SCU
 * connected to the port. The base address (in KW) is the product of this
 * number and the value defined by the STORE SIZE patch plug for the port.
 *
 * ADDRESS RANGE: toggle FULL/HALF. Determines the size of the SCU as full or
 * half of the STORE SIZE patch.
 *
 * PORT ENABLE: (4? toggles)
 *
 * INITIALIZE ENABLE: (4? toggles) These switches enable the receipt of an
 * initialize signal from the SCU connected to the ports. This signal is used
 * during the first part of bootload to set all CPUs to a known (idle) state.
 * The switch for each port connected to an SCU should be ON, otherwise off.
 *
 * INTERLACE: ... All INTERLACE switches should be OFF for Multics operation.
 *
 */

// XXX PPR.IC oddly incremented. ticket #6

char * str_SDW0 (char * buf, sdw0_s * SDW) {
  (void) sprintf (buf, "ADDR=%06o R1=%o R2=%o R3=%o F=%o FC=%o BOUND=%o R=%o "
                        "E=%o W=%o P=%o U=%o G=%o C=%o EB=%o",
                  SDW->ADDR, SDW->R1,    SDW->R2, SDW->R3, SDW->DF,
                  SDW->FC,   SDW->BOUND, SDW->R,  SDW->E,  SDW->W,
                  SDW->P,    SDW->U,     SDW->G,  SDW->C,  SDW->EB);
  return buf;
}

static simRc cpu_boot (UNUSED int32_t cpuUnitIdx, UNUSED DEVICE * dptr) {
  simWarn ("Try 'BOOT IOMn'\n");
  return SCPE_ARG;
}

// The original h/w had one to four (DPS8/M) or eight (Level 68) SCUs;
// each SCU held memory.
// Memory accesses were sent to the SCU that held the region of memory
// being addressed.
//
// eg, SCU 0 has 1 MW of memory and SCU 1 has 2 MW
// Address
//      0M +------------------+
//         |                  |  SCU 0
//      1M +------------------+
//         |                  |  SCU 1
//         |                  |
//      3M +------------------+
//
// So SCU 0 has the first MW of addresses, and SCU1 has the second and third
// MWs.
//
// The simulator has a single 16MW array of memory. This code walks the SCUs
// allocates memory regions out of that array to the SCUs based on their
// individual configurations. The 16MW is divided into 4 zones, one for each
// SCU. (SCU0 uses the first 4MW, SCU1 the second 4MW, etc.
//

#define ZONE_SZ (MEM_SIZE_MAX / 4)

//
// The minimum SCU memory size increment is 64KW, which I will refer to as
// a 'bank'. To map a CPU address to the simulated array, the CPU address is
// divided into a bank number and an offset into that bank
//
//    bank_num = addr / SCBANK_SZ
//    bank_offset = addr % SCBANK_SZ
//
// sc_addr_map[] maps bank numbers to offset in the simulated memory array
//
//    real_addr = sc_addr_map[bank_num] + bank_offset
//

void setup_scbank_map (cpuState_t * cpup) {
  // Initialize to unmapped
  for (uint pg = 0; pg < N_SCBANKS; pg ++) {
    cpu.sc_addr_map [pg] = -1;
    cpu.sc_scu_map  [pg] = -1;
  }
  for (uint u = 0; u < N_SCU_UNITS_MAX; u ++)
    cpu.sc_num_banks[u] = 0;

  // For each port
  for (int port_num = 0; port_num < (cpu.tweaks.l68_mode ? N_L68_CPU_PORTS : N_DPS8M_CPU_PORTS); port_num ++) {
    // Ignore disabled ports
    if (! cpu.switches.enable [port_num])
      continue;

    // Ignore disconnected ports
    // This will happen during early initialization,
    // before any cables are run.
    if (! cables->cpu_to_scu[cpu.cpuIdx][port_num].socket_in_use) {
      continue;
    }

    // Calculate the amount of memory in the SCU in words
    uint store_size = cpu.switches.store_size [port_num];
    uint dps8m_store_table [8] = { 32768, 65536, 131072, 262144, 524288, 1048576, 2097152, 4194304 };
// ISOLTS sez:
// for DPS88:
//   3. set store size switches to 2222.
// for L68:
//   3. remove the right free-edge connector on the 645pq wwb at slot ab28.
//
// During ISOLTS initialization, it requires that the memory switch be set to
// '3' for all eight ports; this corresponds to '2' for the DPS8M (131072)
// Then:
// isolts: a "lda 65536" (64k) failed to produce a store fault
//
// So it seems that the memory size is expected to be 64K, not 128K as per
// the switches; presumably step 3 causes this. Fake it by tweaking store table:
//
    uint l68_store_table [8] = { 32768, 65536, 4194304, 131072, 524288, 1048576, 2097152, 262144 };
    uint l68_isolts_store_table [8] = { 32768, 65536, 4194304, 65536, 524288, 1048576, 2097152, 262144 };

    uint sz_wds =
      cpu.tweaks.l68_mode ?
        cpu.tweaks.isolts_mode ?
          l68_isolts_store_table [store_size] :
          l68_store_table [store_size] :
        dps8m_store_table [store_size];

    // Calculate the base address that will be assigned to the SCU
    uint base_addr_wds = sz_wds * cpu.switches.assignment[port_num];

    // Now convert to SCBANK_SZ (number of banks)
    uint num_banks             = sz_wds / SCBANK_SZ;
    cpu.sc_num_banks[port_num] = num_banks;
    uint base_addr_bks         = base_addr_wds / SCBANK_SZ;

    // For each page handled by the SCU
    for (uint pg = 0; pg < num_banks; pg ++) {

      // What is the address of this bank?
      uint addr_bks = base_addr_bks + pg;
      // Past the end of memory?
      if (addr_bks < N_SCBANKS) {
        // Has this address been already assigned?
        if (cpu.sc_addr_map [addr_bks] != -1) {
          simWarn ("scbank overlap addr_bks %d (%o) old port %d newport %d\n",
                    addr_bks, addr_bks, cpu.sc_addr_map [addr_bks], port_num);
        } else {
          // Assign it
          cpu.sc_addr_map[addr_bks] = (int) ((int) port_num * (int) ZONE_SZ + (int) pg * (int) SCBANK_SZ);
          cpu.sc_scu_map[addr_bks]  = port_num;
        }
      } else {
        simWarn ("addr_bks too big port %d addr_bks %d (%o), "
                  "limit %d (%o)\n",
                  port_num, addr_bks, addr_bks, N_SCBANKS, N_SCBANKS);
      }
    }
  } // for port_num
} // sc_bank_map

int lookup_cpu_mem_map (cpuState_t * cpup, word24 addr) {
  uint scpg = addr / SCBANK_SZ;
  if (scpg < N_SCBANKS)
    return cpu.sc_scu_map[scpg];
  return -1;
}

//
// serial.txt format
//
//      sn:  number[,number]
//
//  Additional numbers will be for multi-cpu systems.
//  XXX: Other fields to be added.

static void get_serial_number (cpuState_t * cpup) {
  bool havesn = false;
  FILE * fp = fopen ("./serial.txt", "r");
  while (fp && ! feof (fp)) {
    char buffer [81] = "";
# if !defined(__clang_analyzer__)
    char * checksn = fgets (buffer, sizeof (buffer), fp);
    (void) checksn;
#endif
    uint cpun, sn;
    if (sscanf (buffer, "sn: %u", & cpu.switches.serno) == 1) {
      if (!sim_quiet) {
        sim_msg ("%s CPU serial number: %u\n", sim_name, cpu.switches.serno);
      }
      havesn = true;
    } else if (sscanf (buffer, "sn%u: %u", & cpun, & sn) == 2) {
      if (cpun < N_CPU_UNITS_MAX) {
        cpus[cpun].switches.serno = sn;
        if (!sim_quiet) {
          sim_msg ("%s CPU %u serial number: %u\n",
          sim_name, cpun, cpus[cpun].switches.serno);
        }
        havesn = true;
      }
    }
  }
  if (!havesn) {
    if (!sim_quiet) {
      sim_msg ("\nPlease register your system at "
               "https://ringzero.wikidot.com/wiki:register\n");
      sim_msg ("or create the file 'serial.txt' containing the line "
               "'sn: 0'.\n\n");
    }
  }
  if (fp)
    fclose (fp);
}

#if defined(STATS)
static void do_stats (void) {
  static struct timespec stats_time;
  static bool first = true;
  if (first) {
    first = false;
    clock_gettime (CLOCK_BOOTTIME, & stats_time);
    sim_msg ("stats started\n");
  } else {
    struct timespec now, delta;
    clock_gettime (CLOCK_BOOTTIME, & now);
    timespec_diff (& stats_time, & now, & delta);
    stats_time = now;
    sim_msg ("stats %6ld.%02ld\n", delta.tv_sec, delta.tv_nsec / 10000000);

    sim_msg ("Instruction counts\n");
    for (uint i = 0; i < 8; i ++) {
      sim_msg (" %'9"PRIu64"\n", cpus[i].instrCnt);
      cpus[i].instrCnt = 0;
    }
    sim_msg ("\n");
  }
}
#endif

// The 100Hz timer has expired; poll I/O

static void ev_poll_cb (UNUSED uv_timer_t * handle) {
  // Call the one hertz stuff every 100 loops
  static uint oneHz = 0;
  if (oneHz ++ >= sys.sys_opts.sys_slow_poll_interval) { // ~ 1Hz
    oneHz = 0;
    rdrProcessEvent ();
# if defined(STATS)
    do_stats ();
# endif
    cpus[0].instrCntT0 = cpus[0].instrCntT1;
    cpus[0].instrCntT1 = cpus[0].instrCnt;
  }
  //diaProcessEvents ();
  fnpProcessEvent ();
# if defined(WITH_SOCKET_DEV)
#  if !defined(__MINGW64__) && !defined(__MINGW32__) && !defined(CROSS_MINGW32) && !defined(CROSS_MINGW64)
  sk_process_event ();
#  endif /* if !defined(__MINGW64__) && !defined(__MINGW32__) && !defined(CROSS_MINGW32) && !defined(CROSS_MINGW64) */
# endif /* if defined(WITH_SOCKET_DEV) */
  consoleProcess ();
# if defined(IO_ASYNC_PAYLOAD_CHAN)
  iomProcess ();
# endif
# if defined(WITH_ABSI_DEV)
#  if !defined(__MINGW32__) && !defined(__MINGW64__) && !defined(CROSS_MINGW32) && !defined(CROSS_MINGW64)
  absi_process_event ();
#  endif /* if !defined(__MINGW32__) && !defined(__MINGW64__) && !defined(CROSS_MINGW32) && !defined(CROSS_MINGW64) */
# endif /* if defined(WITH_ABSI_DEV) */
# if defined(WITH_MGP_DEV)
#  if !defined(__MINGW32__) && !defined(__MINGW64__) && !defined(CROSS_MINGW32) && !defined(CROSS_MINGW64)
  mgp_process_event ();
#  endif /*  if !defined(__MINGW32__) && !defined(__MINGW64__) && !defined(CROSS_MINGW32) && !defined(CROSS_MINGW64) */
# endif /* if defined(WITH_MGP_DEV) */
  PNL (panel_process_event (& cpus[0]));
}

// called once initialization

void cpu_init (void) {

// !!!! Do not use 'cpu' in this routine; usage of 'cpus' violates 'restrict'
// !!!! attribute

  M = systemState->M;
  cpus = systemState->cpus;

#if !defined(SPEED)
  (void) memset (& watch_bits, 0, sizeof (watch_bits));
#endif /* if !defined(SPEED) */

  (void) memset (cpus, 0, sizeof (cpuState_t) * N_CPU_UNITS_MAX);

  for (int i = 0; i < N_CPU_UNITS_MAX; i ++) {
    cpus[i].cpuIdx = i;
    cpus[i].switches.FLT_BASE = 2; // Some of the UnitTests assume this
  }

  get_serial_number (& cpus[0]);

  ev_poll_loop = uv_default_loop ();
  uv_timer_init (ev_poll_loop, & ev_poll_handle);
  // 10 ms == 100Hz
  uv_timer_start (& ev_poll_handle, ev_poll_cb, sys.sys_opts.sys_poll_interval, sys.sys_opts.sys_poll_interval);
  // TODO: reset *all* other structures to zero

#if defined(MATRIX)
  initializeTheMatrix ();
#endif /* if defined(MATRIX) */
}

static void cpu_reset (void) {
  for (uint i = 0; i < N_CPU_UNITS_MAX; i ++)
    cpuResetUnitIdx (i, false);
}

static simRc simCpuReset (UNUSED DEVICE *dptr) {
  // (void) memset (M, -1, MEMSIZE * sizeof (word36));

  // Fill DPS8M memory with zeros, plus a flag only visible to the emulator
  // marking the memory as uninitialized.

  cpu_reset ();
  return SCPE_OK;
}

/* Memory examine */
//  simRc examine_routine (t_val *eval_array, t_addr addr, UNIT *uptr, int32_t
//  switches)
//  Copy  sim_emax consecutive addresses for unit uptr, starting
//  at addr, into eval_array. The switch variable has bit<n> set if the n'th
//  letter was specified as a switch to the examine command.
// Not true...

static simRc cpuExamine (t_value *vptr, t_addr addr, UNUSED UNIT * uptr, UNUSED int32_t sw) {
  if (addr>= MEMSIZE)
      return SCPE_NXM;
  if (vptr != NULL) {
    word36 w;
    coreRead (& cpus[0], addr, & w, __func__);
    *vptr = w;
  }
  return SCPE_OK;
}

/* Memory deposit */

static simRc cpuDeposit (t_value val, t_addr addr, UNUSED UNIT * uptr, UNUSED int32_t sw) {
  if (addr >= MEMSIZE)
    return SCPE_NXM;
  coreWrite (& cpus[0], addr, val, __func__);
  return SCPE_OK;
}

/*
 * register stuff ...
 */

// scp has to have a statically allocated IC to refer to.
static word18 dummy_IC;

static REG cpuRegs[] = {
  // IC must be the first; see sim_PC.
  { ORDATA (IC, dummy_IC,       VASIZE), 0, 0, 0 },
  { NULL, NULL, 0, 0, 0, 0,  NULL, NULL, 0, 0, 0 }
};

/*
 * scp interface
 */

REG *sim_PC = & cpuRegs[0];

/* CPU device descriptor */

DEVICE cpu_dev = {
  "CPU",          // name
  cpuUnits,       // units
  cpuRegs,        // registers
  cpuMods,        // modifiers
  N_CPU_UNITS,    // #units
  8,              // address radix
  PASIZE,         // address width
  1,              // addr increment
  8,              // data radix
  36,             // data width
  & cpuExamine,   // examine routine
  & cpuDeposit,   // deposit routine
  & simCpuReset,  // reset routine
  & cpu_boot,     // boot routine
  NULL,           // attach routine
  NULL,           // detach routine
  NULL,           // context
  0,              // device flags
  0,              // debug control flags
  NULL,           // debug flag names
  NULL,           // memory size change
  NULL,           // logical name
  NULL,           // help
  NULL,           // attach help
  NULL,           // help context
  NULL,           // description
  NULL
};

void printPtid (pthread_t pt) {
  unsigned char *ptc = (unsigned char *) (void *) (& pt);
  sim_msg ("  Thread ID: 0x");
  for (size_t i = 0; i < sizeof (pt); i++) {
    sim_msg ("%02x", (unsigned) (ptc[i]));
  }
  sim_msg ("\n");
#if defined(__APPLE__)
  sim_msg ("   Mach TID: 0x%x\n", pthread_mach_thread_np (pt));
#endif /* if defined(__APPLE__) */
}

cpuState_t * cpus = NULL;

// Scan the SCUs; it one has an interrupt present, return the fault pair
// address for the highest numbered interrupt on that SCU. If no interrupts
// are found, return 1.

// Called with SCU lock set

static uint get_highest_intr (cpuState_t * cpup) {
  uint fp = 1;
  for (uint scuUnitIdx = 0; scuUnitIdx < N_SCU_UNITS_MAX; scuUnitIdx ++) {
    //atomic_thread_fence (memory_order_acquire);
    if (cpu.events.XIP [scuUnitIdx]) {
      fp = scuGetHighestInterrupt (cpup, scuUnitIdx); // CALLED WITH SCU LOCK
      if (fp != 1)
        break;
    }
  }
  return fp;
}

// During long running instructions, sample the interrupts to allow
// mid-instruction faults.
//
// Checking the IWB interrupt inhibit suffices; the only time we execute
// out of IRODD, EIS instructions would be illegal.

bool mifSampleInterrupts (cpuState_t * cpup) {
  // If interrupts inhibited for this instrction, don't sample.
  if (GET_I (cpu.cu.IWB))
    return false;
  // sampleInterrupts has the side-effect of resetting the LUF counter
  return sampleInterrupts (cpup);
}

simRc simh_hooks (cpuState_t * cpup) {
  int reason = 0;

  if (breakEnable && stop_cpu)
    return STOP_STOP;

  if (cpu.tweaks.isolts_mode == 0) {
    // check clock queue
    if (sim_interval <= 0) {
      reason = sim_process_event ();
      if ((! breakEnable) && reason == SCPE_STOP)
        reason = SCPE_OK;
      if (reason)
        return reason;
    }
  }

  sim_interval --;

  return reason;
}

#if defined(PANEL68)
static void panel_process_event (cpuState_t * cpup) {
  // INITIALIZE pressed; treat at as a BOOT.
  if (cpu.panelInitialize && cpu.DATA_panel_s_trig_sw == 0) {
     // Wait for release
     while (cpu.panelInitialize)
       ;
     if (cpu.DATA_panel_init_sw)
       cpuResetUnitIdx (ASSUME0, true); // INITIALIZE & CLEAR
     else
       cpuResetUnitIdx (ASSUME0, false); // INITIALIZE
     // XXX Until a boot switch is wired up
     do_boot ();
  }
  // EXECUTE pressed; EXECUTE PB set, EXECUTE FAULT set
  if (cpu.DATA_panel_s_trig_sw == 0 &&
      cpu.DATA_panel_execute_sw &&    // EXECUTE button
      cpu.DATA_panel_scope_sw &&      // 'EXECUTE PB/SCOPE REPEAT' set to PB
      cpu.DATA_panel_exec_sw == 0)  { // 'EXECUTE SWITCH/EXECUTE FAULT'
    //  set to FAULT
    // Wait for release
    while (cpu.DATA_panel_execute_sw)
      ;

    if (cpu.DATA_panel_exec_sw) { // EXECUTE SWITCH
      cpuResetUnitIdx (ASSUME0, false);
      cpu.cu.IWB = cpu.switches.data_switches;
      setCpuCycle (cpup, EXEC_cycle);
    } else { // EXECUTE FAULT
// XXX Assuming Panel is CPU0
      setG7fault (& cpus[0], 0, FAULT_EXF, fst_zero);
    }
  }
}
#endif

bool bce_dis_called = false;

// The hypervisor CPU for the threadz model
simRc sim_instr (cpuState_t * cpup) {
  simRc reason = 0;

# if 0
  static bool inited = false;
  if (! inited) {
    inited = true;

#  if defined(IO_THREADZ)
// Create channel threads

  for (uint iom_unit_idx = 0; iom_unit_idx < N_IOM_UNITS_MAX; iom_unit_idx ++) {
    for (uint chan_num = 0; chan_num < MAX_CHANNELS; chan_num ++) {
      if (get_ctlr_in_use (iom_unit_idx, chan_num)) {
        enum ctlr_type_e ctlr_type = cables->iom_to_ctlr[iom_unit_idx][chan_num].ctlr_type;
        createChnThread (iom_unit_idx, chan_num, ctlr_type_strs [ctlr_type]);
        chnRdyWait (iom_unit_idx, chan_num);
      }
    }
  }

// Create IOM threads

  for (uint iom_unit_idx = 0; iom_unit_idx < N_IOM_UNITS_MAX; iom_unit_idx ++) {
    createIOMThread (iom_unit_idx);
    iomRdyWait (iom_unit_idx);
  }
#  endif

// Create CPU threads

  for (uint cpu_idx = 0; cpu_idx < cpu_dev.numunits; cpu_idx ++)
    createCPUThread (cpu_idx);
# endif
  if (cpuThreadz[0].run == false)
    createCPUThread (0);
  do {
    // Process deferred events and breakpoints
    reason = simh_hooks (cpup);
    if (reason)
      break;

    if (bce_dis_called) {
      reason = STOP_STOP;
      break;
    }

// Loop runs at 1000 Hz

    lock_iom ();
    lock_libuv ();
    uv_run (ev_poll_loop, UV_RUN_NOWAIT);
    unlock_libuv ();
    unlock_iom ();
    PNL (panel_process_event (cpup));

    int con_unit_idx = check_attn_key ();
    if (con_unit_idx != -1)
      console_attn_idx (con_unit_idx);

# if defined(IO_ASYNC_PAYLOAD_CHAN_THREAD)
    struct timespec next_time;
    clock_gettime (CLOCK_REALTIME, & next_time);
    next_time.tv_nsec += 1000l * 1000l;
    if (next_time.tv_nsec >= 1000l * 1000l *1000l) {
      next_time.tv_nsec -= 1000l * 1000l *1000l;
      next_time.tv_sec  += (time_t) 1;
    }
    struct timespec new_time;
    do {
      pthread_mutex_lock (& iom_start_lock);
      pthread_cond_timedwait (& iomCond, & iom_start_lock, & next_time);
      pthread_mutex_unlock (& iom_start_lock);
      lock_iom ();
        lock_libuv ();

          iomProcess ();

        unlock_libuv ();
      unlock_iom ();

      clock_gettime (CLOCK_REALTIME, & new_time);
    }
    while ((next_time.tv_sec == new_time.tv_sec) ?
             (next_time.tv_nsec > new_time.tv_nsec) :
             (next_time.tv_sec  > new_time.tv_sec));
# else
    sim_usleep (1000); // 1000 us == 1 ms == 1/1000 sec.
# endif
  } while (reason == 0);

#ifdef STATS
  for (uint cpuNo = 0; cpuNo < N_CPU_UNITS_MAX; cpuNo ++)
    cpuStats (cpuNo);

  for (uint iomNo = 0; iomNo < N_IOM_UNITS_MAX; iomNo ++)
    iomStats (iomNo);
#endif

  //HDBGPrint ();

  return reason;
}

simRc sim_instr0 (void) {
  return sim_instr (& cpus[0]);
}

//
// Okay, let's treat this as a state machine
//
//  INTERRUPT_cycle
//     clear interrupt, load interrupt pair into instruction buffer
//     set INTERRUPT_EXEC_cycle
//  INTERRUPT_EXEC_cycle
//     execute instruction in instruction buffer
//     if (! transfer) set INTERUPT_EXEC2_cycle
//     else set FETCH_cycle
//  INTERRUPT_EXEC2_cycle
//     execute odd instruction in instruction buffer
//     set INTERUPT_EXEC2_cycle
//
//  FAULT_cycle
//     fetch fault pair into instruction buffer
//     set FAULT_EXEC_cycle
//  FAULT_EXEC_cycle
//     execute instructions in instruction buffer
//     if (! transfer) set FAULT_EXE2_cycle
//     else set FETCH_cycle
//  FAULT_EXEC2_cycle
//     execute odd instruction in instruction buffer
//     set FETCH_cycle
//
//  FETCH_cycle
//     fetch instruction into instruction buffer
//     set EXEC_cycle
//
//  EXEC_cycle
//     execute instruction in instruction buffer
//     if (repeat conditions) keep cycling
//     if (pair) set EXEC2_cycle
//     else set FETCH_cycle
//  EXEC2_cycle
//     execute odd instruction in instruction buffer
//
//  XEC_cycle
//     load instruction into instruction buffer
//     set EXEC_cycle
//
//  XED_cycle
//     load instruction pair into instruction buffer
//     set EXEC_cycle
//
// other extant cycles:
//  ABORT_cycle

void * cpuThreadMain (void * arg) {
  int myid = * (int *) arg;
  cpuState_t * cpup = & cpus[myid];
  unsigned char umyid = (unsigned char) toupper ('a' + (int) myid);

  sim_msg ("CPU %c thread created.\n", (unsigned int) umyid);
# if defined(TESTING)
  printPtid (pthread_self ());
# endif /* if defined(TESTING) */

  sim_os_set_thread_priority (PRIORITY_ABOVE_NORMAL);
  setSignals ();
  threadz_sim_instr (cpup);
  return NULL;
}

static void do_LUF_fault (cpuState_t * cpup) {
  CPT (cpt1U, 16); // LUF
  cpu.lufCounter  = 0;
  cpu.lufOccurred = false;
// This is a hack to fix ISOLTS 776. ISOLTS checks that the TR has
// decremented by the LUF timeout value. To implement this, we set
// the TR to the expected value.

// LUF  time
//  0    2ms
//  1    4ms
//  2    8ms
//  3   16ms
// units
// you have: 2ms
// units
// You have: 512000Hz
// You want: 1/2ms
//    * 1024
//    / 0.0009765625
//
//  TR = 1024 << LUF
  if (cpu.tweaks.isolts_mode)
    cpu.shadowTR = (word27) cpu.TR0 - (1024u << (is_priv_mode (cpup) ? 4 : cpu.CMR.LUF));

// That logic fails for test 785.
//
// set slave mode, LUF time 16ms.
// loop for 15.9 ms.
// set master mode.
// loop for 15.9 ms. The LUF should be noticed, and lufOccurred set.
// return to slave mode. The LUF should fire, with the timer register
// being set for 31.1 ms.
// XXX: With out accurate cycle timing or simply fudging the results, I don't
// see how to fix this one.

  doFault (cpup, FAULT_LUF, fst_zero, "instruction cycle lockup");
}

/*
 * addrModes_e get_addr_mode()
 *
 * Report what mode the CPU is in.
 * This is determined by examining a couple of IR flags.
 *
 * TODO: get_addr_mode() probably belongs in the CPU source file.
 *
 */

static void set_temporary_absolute_mode (cpuState_t * cpup) {
  CPT (cpt1L, 20); // set temp. abs. mode
  cpu.secret_addressing_mode = true;
  cpu.cu.XSF = false;
}

static bool clear_temporary_absolute_mode (cpuState_t * cpup) {
  CPT (cpt1L, 21); // clear temp. abs. mode
  cpu.secret_addressing_mode = false;
  return cpu.cu.XSF;
}

#ifdef SYNCTEST
static const int workAllocationQuantum = 64;
static const int syncClockModePollRate = 128;
static int allocCount;
#else
static const int workAllocationQuantum = 32;
static const int syncClockModePollRate = 64;
#endif
static const int masterCycleCntlimit = 2048;

// cpu points to the thread that is to become master

void becomeClockMaster (cpuState_t * newMaster) {
#ifdef SYNCTEST
  simPrintf ("CPU%c %s entry\n", newMaster->cpuIdx + 'A', __func__);
  allocCount = 0;
#endif

  // Set syncClockMode
  bool expected = false;
  atomic_compare_exchange_strong (& sys.syncClockMode, & expected, true);
  // Was someone else already clock master?
  if (expected) {
    // Yes; let them rule
    simPrintf ("%s: someone else beat us here.\n", __func__);
    HDBGNote (newMaster, __func__, "someone else beat us here.%s", "");
    return;
  }


  // newMaster CPU is now master
  //   Remember its index
  sys.syncClockModeMasterIdx = newMaster->cpuIdx;
  //   Tell it is master
  newMaster->syncClockModeMaster = true; // That CPU is the clock master
  //   Tell it that syncMode is on
  newMaster->syncClockModeCache = true;
  
  //   Initialze its cycle counter
  newMaster->masterCycleCnt = 0;

  //   Initialize the work allocations of all other CPUs
  for (int i = 0; i < N_CPU_UNITS_MAX; i ++) {
    if (i != newMaster->cpuIdx) { // not the new master
      cpus[i].workAllocation = 0;
      __asm volatile ("");
      atomic_thread_fence (memory_order_acquire);
      if (cpus[i].inMultics && ! cpus[i].isSlave) { // it is up but not yet a slave
        __asm volatile ("");
        cpus[i].syncClockModePoll = 0;
        __asm volatile ("");
        cpus[i].becomeSlave = true;
        __asm volatile ("");
        atomic_thread_fence (memory_order_release);
      } // candidate
    } // not target CPU
  } // every CPU

  __asm volatile ("");
  sys.syncClockMode = true;
  atomic_thread_fence (memory_order_release);
  HDBGNote (newMaster, __func__, "becomes clock master%s", "");
} // becomeClockMaster

void giveupClockMaster (cpuState_t * cpup) {
  HDBGNote (cpup, __func__, "entry%s", "");
#ifdef SYNCTEST
  HDBGNote (cpup, __func__, "alloc count %d", allocCount);
  simPrintf ("CPU%c %s entry\n", cpu.cpuIdx + 'A', __func__);
  simPrintf ("CPU%c Alloc count %d\n", cpu.cpuIdx + 'A', allocCount);
#endif
  __asm volatile ("");
  cpu.syncClockModeMaster = false;
  __asm volatile ("");
  sys.syncClockMode = false; // Free the other processors
  __asm volatile ("");
  for (int i = 0; i < N_CPU_UNITS_MAX; i ++) {
    cpus[i].syncClockModeCache = false;
    atomic_thread_fence (memory_order_release);
  }
}

simRc threadz_sim_instr (cpuState_t * cpup) {

#ifdef STATS
#if !defined(SCHED_NEVER_YIELD)
  uint64_t lockYieldAll     = 0;
#endif /* if !defined(SCHED_NEVER_YIELD) */
  uint64_t lockWaitMaxAll   = 0;
  uint64_t lockWaitAll      = 0;
  uint64_t lockImmediateAll = 0;
  uint64_t lockCntAll       = 0;
  uint64_t instrCntAll      = 0;
  uint64_t cycleCntAll      = 0;
#endif

  simRc reason = 0;

  // This allows long jumping to the top of the state machine
  int val = setjmp (cpu.jmpMain);

  switch (val) {
    case JMP_ENTRY:
    case JMP_REENTRY:
      reason = 0;
      break;
    case JMP_SYNC_FAULT_RETURN:
      setCpuCycle (cpup, SYNC_FAULT_RTN_cycle);
      break;
    case JMP_STOP:
      reason = STOP_STOP;
      goto leave;
    case JMP_REFETCH:

      // Not necessarily so, but the only times
      // this path is taken is after an RCU returning
      // from an interrupt, which could only happen if
      // was xfer was false; or in a DIS cycle, in
      // which case we want it false so interrupts
      // can happen.
      cpu.wasXfer = false;

      setCpuCycle (cpup, FETCH_cycle);
      break;
    case JMP_RESTART:
      setCpuCycle (cpup, EXEC_cycle);
      break;

    case JMP_FORCE_RESTART:
      // The configuration has been changed on a CPU that
      // has been started and in DIS idle. DIS sees the
      // forceRestart flag and longjmps here.
      // cpuResetUnitIdx will update the CPU state to
      // match the new configiguration and set the
      // state to enter DIS.
      cpuResetUnitIdx (cpu.cpuIdx, false);
      // Were we a clock master?
      if (sys.syncClockMode && sys.syncClockModeMasterIdx == cpu.cpuIdx)
        giveupClockMaster (cpup);
      break;
     
    default:
      simWarn ("longjmp value of %d unhandled\n", val);
      goto leave;
  }

  // Main instruction fetch/decode loop

  DCDstruct * ci = & cpu.currentInstruction;

  if (cpu.restart)
    setCpuCycle (cpup, FAULT_cycle);

  // These are used to signal createCPUThread that the "round
  // up the slaves" code above has run; they must be set
  // after that. This line will prevent the compiler from
  // hoisting them.
  __asm volatile ("");
  cpu.executing = true;
  if (cpu.tweaks.isolts_mode) {
    ;
  } else {
    cpu.inMultics = true;
  }

  do {

    reason = 0;

    cpu.cycleCnt ++;

    coreUnlockAll (cpup);

// Check for TR underflow. The TR is stored in a uint32_t, but is 27 bits wide.
// The TR update code decrements the TR; if it passes through 0, the high bits
// will be set.

// If we assume a 1 MIPS reference platform, the TR would be decremented every
// two instructions (1/2 MHz)

#if 0
        cpu.rTR     -= cpu.rTRticks * 100;
        //cpu.rTR   -= cpu.rTRticks * 50;
        cpu.rTRticks = 0;
#else
# define TR_RATE 2

        //cpu.rTR      -= cpu.rTRticks / TR_RATE;
        // ubsan
    cpu.rTR = (word27) (((word27s) cpu.rTR) - (word27s) (cpu.rTRticks / TR_RATE));
    cpu.rTRticks %= TR_RATE;

#endif

    if (cpu.rTR & ~MASK27) {
      cpu.rTR &= MASK27;
      if (cpu.tweaks.tro_enable)
        setG7fault (cpup, cpu.cpuIdx, FAULT_TRO, fst_zero);
    }

    switch (cpu.cycle) {

      case INTERRUPT_cycle: {
        CPT (cpt1U, 0); // Interrupt cycle
        // In the INTERRUPT CYCLE, the processor safe-stores
        // the Control Unit Data (see Section 3) into
        // program-invisible holding registers in preparation
        // for a Store Control Unit (scu) instruction, enters
        // temporary absolute mode, and forces the current
        // ring of execution C(PPR.PRR) to
        // 0. It then issues an XEC system controller command
        // to the system controller on the highest priority
        // port for which there is a bit set in the interrupt
        // present register.

        uint intr_pair_addr = get_highest_intr (cpup);
        HDBGIntr (cpup, intr_pair_addr, "");
        cpu.cu.FI_ADDR = (word5) (intr_pair_addr / 2);
        cu_safe_store (cpup);
        // XXX the whole interrupt cycle should be rewritten as an xed
        // instruction pushed to IWB and executed

        CPT (cpt1U, 1); // safe store complete
        // Temporary absolute mode
        set_temporary_absolute_mode (cpup);

        // Set to ring 0
        cpu.PPR.PRR = 0;
        cpu.TPR.TRR = 0;

        // Check that an interrupt is actually pending
        if (cpu.interrupt_flag) {
          CPT (cpt1U, 2); // interrupt pending
          // clear interrupt, load interrupt pair into instruction
          // buffer; set INTERRUPT_EXEC_cycle.

          // In the h/w this is done later, but doing it now allows
          // us to avoid clean up for no interrupt pending.

          if (intr_pair_addr != 1) { // no interrupts

            CPT (cpt1U, 3); // interrupt identified

            // get interrupt pair
            coreRead2 (cpup, intr_pair_addr,
                       & cpu.cu.IWB, & cpu.cu.IRODD, __func__);
            HDBGMRead (cpup, intr_pair_addr, cpu.cu.IWB, "intr even");
            HDBGMRead (cpup, intr_pair_addr + 1, cpu.cu.IRODD, "intr odd");
            cpu.cu.xde = 1;
            cpu.cu.xdo = 1;
            cpu.isExec = true;
            cpu.isXED  = true;

            CPT (cpt1U, 4); // interrupt pair fetched
            cpu.interrupt_flag = false;
            setCpuCycle (cpup, INTERRUPT_EXEC_cycle);
            break;
          } // int_pair != 1
        } // interrupt_flag

        // If we get here, there was no interrupt

        CPT (cpt1U, 5); // interrupt pair spurious
        cpu.interrupt_flag = false;
        clear_temporary_absolute_mode (cpup);
        // Restores addressing mode
        cu_safe_restore (cpup);
        // We can only get here if wasXfer was
        // false, so we can assume it still is.
        cpu.wasXfer = false;
// The only place cycle is set to INTERRUPT_cycle in FETCH_cycle; therefore
// we can safely assume that is the state that should be restored.
        setCpuCycle (cpup, FETCH_cycle);
        break;
      }

      case FETCH_cycle:
#if defined(PANEL68)
        (void) memset (cpu.cpt, 0, sizeof (cpu.cpt));
#endif
        CPT (cpt1U, 13); // fetch cycle

        PNL (L68_ (cpu.INS_FETCH = false;))

// "If the interrupt inhibit bit is not set in the current instruction
// word at the point of the next sequential instruction pair virtual
// address formation, the processor samples the [group 7 and interrupts]."

// Since XEx/RPx may overwrite IWB, we must remember
// the inhibit bits (cpu.wasInhibited).

// If the instruction pair virtual address being formed is the result of a
// transfer of control condition or if the current instruction is
// Execute (xec), Execute Double (xed), Repeat (rpt), Repeat Double (rpd),
// or Repeat Link (rpl), the group 7 faults and interrupt present lines are
// not sampled.

// Group 7 Faults
//
// Shutdown
//
// An external power shutdown condition has been detected. DC POWER shutdown
// will occur in approximately one millisecond.
//
// Timer Runout
//
// The timer register has decremented to or through the value zero. If the
// processor is in privileged mode or absolute mode, recognition of this fault
// is delayed until a return to normal mode or BAR mode. Counting in the timer
// register continues.
//
// Connect
//
// A connect signal ($CON strobe) has been received from a system controller.
// This event is to be distinguished from a Connect Input/Output Channel (cioc)
// instruction encountered in the program sequence.

        // check BAR bound and raise store fault if above
        // pft 04d 10070, ISOLTS-776 06ad
        if (get_bar_mode (cpup))
          getBARAddress (cpup, cpu.PPR.IC);

        // Don't check timer runout if privileged
        // ISOLTS-776 04bcf, 785 02c
        // (but do if in a DIS instruction with bit28 clear)
        bool tmp_priv_mode = is_priv_mode (cpup);
        bool is_dis        = cpu.currentInstruction.opcode  == 0616 &&
                             cpu.currentInstruction.opcodeX == 0;
        bool noCheckTR     = tmp_priv_mode &&
                             ! (is_dis && GET_I (cpu.cu.IWB) == 0);

        if (is_dis) {
          // take interrupts and g7 faults as long as
          // last instruction is DIS (??)
          cpu.interrupt_flag = sampleInterrupts (cpup);
          cpu.g7_flag = noCheckTR ? bG7PendingNoTRO (cpup) : bG7Pending (cpup);
        } else if (! (cpu.cu.xde | cpu.cu.xdo | cpu.cu.rpt | cpu.cu.rd | cpu.cu.rl)) {
          if ((! cpu.wasInhibited) && (cpu.PPR.IC & 1) == 0 && (! cpu.wasXfer)) {
            CPT (cpt1U, 14); // sampling interrupts
            cpu.interrupt_flag = sampleInterrupts (cpup);
            cpu.g7_flag =
            noCheckTR ? bG7PendingNoTRO (cpup) : bG7Pending (cpup);
          }
        cpu.wasInhibited = false;
      } else {
        // XEx at an odd location disables interrupt sampling
        // also for the next instruction pair. ISOLTS-785 02g,
        // 776 04g
        // Set the inhibit flag
        // (I assume RPx behaves in the same way)
        if ((cpu.PPR.IC & 1) == 1)
          cpu.wasInhibited = true;
      }

// Multics executes a CPU connect instruction (which should eventually cause a
// connect fault) while interrupts are inhibited and an IOM interrupt is
// pending. Multics then executes a DIS instruction (Delay Until Interrupt
// Set). This should cause the processor to "sleep" until an interrupt is
// signaled. The DIS instruction sees that an interrupt is pending, sets
// cpu.interrupt_flag to signal that the CPU to service the interrupt and
// resumes the CPU.
//
// The CPU state machine sets up to fetch the next instruction. If checks to
// see if this instruction should check for interrupts or faults according to
// the complex rules (interrupts inhibited, even address, not RPT or XEC,
// etc.); it this case, the test fails as the next instruction is at an odd
// address. If the test had passed, the cpu.interrupt_flag would be set or
// cleared depending on the pending interrupt state data, AND the cpu.g7_flag
// would be set or cleared depending on the faults pending data (in this case,
// the connect fault).
//
// Because the flags were not updated, after the test, cpu.interrupt_flag is
// set (since the DIS instruction set it) and cpu.g7_flag is not set.
//
// Next, the CPU sees the that cpu.interrupt flag is set, and starts the
// interrupt cycle despite the fact that a higher priority g7 fault is pending.

// To fix this, check (or recheck) g7 if an interrupt is going to be faulted.
// Either DIS set interrupt_flag and FETCH_cycle didn't so g7 needs to be
// checked, or FETCH_cycle did check it when it set interrupt_flag in which
// case it is being rechecked here. It is [locally] idempotent and light
// weight, so this should be okay.

      if (cpu.g7_flag) {
        cpu.g7_flag        = false;
        cpu.interrupt_flag = false;
        doG7Fault (cpup, !noCheckTR);
      }
      if (cpu.interrupt_flag) {
// This is the only place cycle is set to INTERRUPT_cycle; therefore
// return from interrupt can safely assume the it should set the cycle
// to FETCH_cycle.
        CPT (cpt1U, 15); // interrupt
        setCpuCycle (cpup, INTERRUPT_cycle);
        break;
      }

// "While in absolute mode or privileged mode the lockup fault is signalled at
// the end of the time limit set in the lockup timer but is not recognized
// until the 32 millisecond limit. If the processor returns to normal mode or
// BAR mode after the fault has been signalled but before the 32 millisecond
// limit, the fault is recognized before any instruction in the new mode is
// executed."

      /*FALLTHRU*/ /* fall through */
      case PSEUDO_FETCH_cycle:

#if !defined(LUF_BY_MEMORY)
        cpu.lufCounter ++;
#endif
#if 1
        if (cpu.tweaks.enableLUF) {
          tmp_priv_mode = is_priv_mode (cpup);
          if (cpu.lufCounter > cpu.tweaks.luf_limits[cpu.CMR.LUF]) {
//simPrintf ("cnt %lu luf %lu\n", cpu.lufCounter, cpu.tweaks.luf_limits[cpu.CMR.LUF]);
//simPrintf ("%o:%05o:%06o\n", cpu.cpuIdx, cpu.PPR.PSR, cpu.PPR.IC);
//hdbgPrint();
//exit (0);
            cpu.lufCounter = 0;
            if (tmp_priv_mode) {
              // In priv. mode the LUF is noted but not executed
              cpu.lufOccurred = true;
            } else {
//simPrintf ("LUF 1\n");
              do_LUF_fault (cpup);
            }
          } // lufCounter > luf_limit

          // After 32ms, the LUF fires regardless of priv.
          if (cpu.lufCounter > cpu.tweaks.luf_limits[4])
//{simPrintf ("LUF 2\n");
            do_LUF_fault (cpup);
//}

          // If the LUF occurred in priv. mode and we left priv. mode,
          // fault.
          if (! tmp_priv_mode && cpu.lufOccurred)
//{simPrintf ("LUF 3\n");
            do_LUF_fault (cpup);
//}
        } // enableLUF
#else
        if ((tmp_priv_mode && cpu.lufCounter > cpu.tweaks.luf_limits[4]) ||
                (! tmp_priv_mode && cpu.lufCounter > cpu.tweaks.luf_limits[cpu.CMR.LUF])) {
          CPT (cpt1U, 16); // LUF
          cpu.lufCounter = 0;

// This is a hack to fix ISOLTS 776. ISOLTS checks that the TR has
// decremented by the LUF timeout value. To implement this, we set
// the TR to the expected value.

// LUF  time
//  0    2ms
//  1    4ms
//  2    8ms
//  3   16ms
// units
// you have: 2ms
// units
// You have: 512000Hz
// You want: 1/2ms
//    * 1024
//    / 0.0009765625
//
//  TR = 1024 << LUF
          if (cpu.tweaks.isolts_mode)
            cpu.shadowTR = (word27) cpu.TR0 - (1024u << (is_priv_mode (cpup) ? 4 : cpu.CMR.LUF));

          doFault (cpup, FAULT_LUF, fst_zero, "instruction cycle lockup");
        }
#endif
        if (cpu.cycle == PSEUDO_FETCH_cycle) {
          cpu.apu.lastCycle    = INSTRUCTION_FETCH;
          cpu.cu.XSF           = 0;
          cpu.cu.TSN_VALID [0] = 0;
          cpu.TPR.TSR          = cpu.PPR.PSR;
          cpu.TPR.TRR          = cpu.PPR.PRR;
          cpu.wasInhibited     = false;
        } else {
          CPT (cpt1U, 20); // not XEC or RPx
          cpu.isExec               = false;
          cpu.isXED                = false;
          // fetch next instruction into current instruction struct
          cpu.cu.XSF               = 0;
          cpu.cu.TSN_VALID [0]     = 0;
          cpu.TPR.TSR              = cpu.PPR.PSR;
          cpu.TPR.TRR              = cpu.PPR.PRR;
          PNL (cpu.prepare_state   = ps_PIA);
          PNL (L68_ (cpu.INS_FETCH = true;))
          fetchInstruction (cpup, cpu.PPR.IC);
        }

        CPT (cpt1U, 21); // go to exec cycle
        advanceG7Faults (cpup);
        setCpuCycle (cpup, EXEC_cycle);
        break;

      case EXEC_cycle:
      case FAULT_EXEC_cycle:
      case INTERRUPT_EXEC_cycle: {
        CPT (cpt1U, 22); // exec cycle

        // Have we been told to becomes a slave?
        if (UNLIKELY (cpu.becomeSlave)) {
          cpu.becomeSlave = false;
          // Wait for the master to wake up
          while (! sys.syncClockMode) {
            usleep (1);
          }
          // Force the poll below
          cpu.syncClockModePoll = 0;
        }

        // Ready to check sync clock mode?
        if (cpu.syncClockModeCache || --cpu.syncClockModePoll <= 0) {
          cpu.syncClockModePoll = cpu.tweaks.isolts_mode ? 1 : syncClockModePollRate;

          // Are the clocks synchronized?
          if (sys.syncClockMode) {

          // Remember that this thread is synchronized
          cpu.syncClockModeCache = true;

          // Are we the master?
          if (sys.syncClockModeMasterIdx == cpu.cpuIdx) {

            // Master
            cpu.masterCycleCnt ++;
            if (cpu.masterCycleCnt > masterCycleCntlimit) {
#ifdef SYNCTEST
              simPrintf ("too many cycles\n");
#endif
              giveupClockMaster (cpup);
              goto bail;
            }

          // Have we used up our allocation?
          if (cpu.workAllocation <= 0) {
#ifdef SYNCTEST
            allocCount ++;
#endif

            // If usleep(1) actually takes only 1 us, then this
            // will be at least 2 seconds.
            //int64_t waitTimeout = 2000000;
            // Quick testing shows it is closer to 2 minutes...
            int64_t waitTimeout = 100000;


            // Has everyone used up their allocation?
            while (1) {  // while others still have work to do
              bool alldone = true;
              for (int i = 0; i < N_CPU_UNITS_MAX; i ++) {
                if (cpus[i].inMultics && cpus[i].workAllocation > 0) {
                  wakeCPU (i);
                  alldone = false;
                  //break;
                } // up and working
              } // cpus
              if (alldone) {
                // Everyone has used up there allocations; dole out some more work
                for (int i = 0; i < N_CPU_UNITS_MAX; i ++) {
                  if (cpus[i].inMultics) {
                    cpus[i].workAllocation += cpu.tweaks.isolts_mode ? 1 : workAllocationQuantum;
                    wakeCPU (i);
                  }
                } 
                break; // while (1)
              } // alldone
              if (waitTimeout-- < 0) {
                // timed out waiting for everyone to finish their
                // work allocation; assume something is fouled.
                simPrintf ("Clock master CPU %c timed out\n", "ABCDEFGH"[cpu.cpuIdx]);
                for (int i = 0; i < N_CPU_UNITS_MAX; i ++) {
                  if (cpus[i].inMultics && cpus[i].workAllocation > 0) {
                    simPrintf ("CPU %c remaining allocation: %ld\n", "ABCDEFGH"[i], cpus[i].workAllocation);
                  }
                }
                simPrintf ("Conceding clock mastery...\n");
                cpu.syncClockModeCache = false;
                giveupClockMaster (cpup);
                goto bail;
              }
              usleep (1);
            } // while (1) -- while others still have work to do
          } // have we used up our allocation
          // We are master and have work allocated; fall through
          // and do some work

        } else { // Master/slave?

          // We are not the master; must be a slave

          // Have we just become a slave?
          if (! cpu.isSlave) {
            HDBGNote (NULL, __func__, "CPU%c Becoming slave", cpu.cpuIdx + 'A');
#ifdef SYNCTEST
           simPrintf ("CPU%c becoming slave\n", cpu.cpuIdx + 'A');
#endif
          }
          cpu.isSlave = true;

          // Wait for allocation
          while (sys.syncClockMode && cpu.workAllocation <= 0)
            usleep (1);

          // We are slave and have work allocated; fall through
          // and do some work

        } // master/slave

      } else { // ! sys.syncClockMode
        // Forget that this thread is synchronized
        cpu.syncClockModeCache = false;
        if (cpu.isSlave) {
          HDBGNote (cpup, __func__, "Free; free at last%s", "");
#ifdef SYNCTEST
          simPrintf ("CPU%c free; free at last\n", cpu.cpuIdx + 'A');
#endif
          cpu.isSlave = false;
        }
      } // ! sys.syncClockMode
    } // polling
bail:

    
    if (LIKELY (! cpu.tweaks.isolts_mode) &&
       UNLIKELY (cpu.inMultics)) {
      cpu.inMultics = true;
    }

        if (stall_point_active) {
          for (int i = 0; i < N_STALL_POINTS; i ++) {
            if (stall_points[i].segno  && stall_points[i].segno  == cpu.PPR.PSR &&
                stall_points[i].offset && stall_points[i].offset == cpu.PPR.IC) {
# ifdef CTRACE
              (void) fprintf (stderr, "%10lu %s stall %d\n", seqno (), cpunstr[cpu.cpuIdx], i);
# endif
              sim_usleep (stall_points[i].time);
              break;
            }
          }
        }

        // The only time we are going to execute out of IRODD is
        // during RPD, at which time interrupts are automatically
        // inhibited; so the following can ignore RPD harmlessly
        if (GET_I (cpu.cu.IWB))
          cpu.wasInhibited = true;

        simRc ret = executeInstruction (cpup);
        DO_WORK_EXEC;
        CPT (cpt1U, 23); // execution complete

        if (cpu.tweaks.l68_mode)
          addL68CuHistory (cpup);
        else
          addDps8mCuHistory (cpup);

        if (ret > 0) {
          reason = ret;
          break;
        }

        if (ret == CONT_XEC) {
          CPT (cpt1U, 27); // XEx instruction
          cpu.wasXfer = false;
          cpu.isExec  = true;
          if (cpu.cu.xdo)
            cpu.isXED = true;

          cpu.cu.XSF           = 0;
          cpu.cu.TSN_VALID [0] = 0;
          cpu.TPR.TSR          = cpu.PPR.PSR;
          cpu.TPR.TRR          = cpu.PPR.PRR;
          break;
        }

        if (ret == CONT_TRA || ret == CONT_RET) {
          CPT (cpt1U, 24); // transfer instruction
          cpu.cu.xde  = cpu.cu.xdo = 0;
          cpu.isExec  = false;
          cpu.isXED   = false;
          // even for CONT_RET else isolts 886 fails
          cpu.wasXfer = true;

          if (cpu.cycle != EXEC_cycle) { // fault or interrupt
            clearFaultCycle (cpup);

// BAR mode:  [NBAR] is set ON (taking the processor
// out of BAR mode) by the execution of any transfer instruction
// other than tss during a fault or interrupt trap.

            if (! (cpu.currentInstruction.opcode == 0715 && cpu.currentInstruction.opcodeX == 0)) {
              CPT (cpt1U, 9); // nbar set
              SET_I_NBAR;
            }

            if (!clear_temporary_absolute_mode (cpup)) {
              // didn't go appending
              CPT (cpt1U, 10); // temporary absolute mode
              set_addr_mode (cpup, ABSOLUTE_mode);
            } else {
              // went appending
            }

          } // fault or interrupt

          if (TST_I_ABS && cpu.cu.XSF)
            set_addr_mode (cpup, APPEND_mode);

          if (ret == CONT_TRA) {
            // PSEUDO_FETCH_cycle does not check interrupts/g7faults
            cpu.wasXfer = false;
            setCpuCycle (cpup, PSEUDO_FETCH_cycle);
          } else {
            setCpuCycle (cpup, FETCH_cycle);
          }
          break;   // Don't bump PPR.IC, instruction already did it
        } /* if (ret == CONT_TRA || ret == CONT_RET) */

        if (ret == CONT_DIS) {
          CPT (cpt1U, 25); // DIS instruction

          // If we do a DIS, skip the going to sleep
          if (cpu.syncClockModeCache) {
            break;
          }

// If we get here, we have encountered a DIS instruction in EXEC_cycle.
//
// We need to idle the CPU until one of the following conditions:
//
//  An external interrupt occurs.
//  The Timer Register underflows.
//  The emulator polled devices need polling.
//
// The external interrupt will only be posted to the CPU engine if the
// device poll posts an interrupt. This means that we do not need to
// detect the interrupts here; if we wake up and poll the devices, the
// interrupt will be detected by the DIS instruction when it is re-executed.
//
// The Timer Register is a fast, high-precision timer but Multics uses it
// in only two ways: detecting I/O lockup during early boot, and process
// quantum scheduling (1/4 second for Multics).
//
// Neither of these require high resolution or high accuracy.
//
// The goal of the polling code is sample at about 100Hz; updating the timer
// register at that rate should suffice.
//
//    sleep for 1/100 of a second
//    update the polling state to trigger a poll
//    update the timer register by 1/100 of a second
//    force the scp queues to process
//    continue processing
//

// The usleep logic is not smart enough w.r.t. ROUND_ROBIN/ISOLTS.
// The sleep should only happen if all running processors are in
// DIS mode.
                  // 1/100 is .01 secs.
                  // *1000 is 10  milliseconds
                  // *1000 is 10000 microseconds
                  // in uSec;

// XXX If interrupt inhibit set, then sleep forever instead of TRO
                  // rTR is 512KHz; sleepCPU is in 1Mhz
                  //   rTR * 1,000,000 / 512,000
                  //   rTR * 1000 / 512
                  //   rTR * 500 / 256
                  //   rTR * 250 / 128
                  //   rTR * 125 / 64

          // unsigned long left = cpu.rTR * 125u / 64u;
          // ubsan
          unsigned long left = (unsigned long) ((uint64_t) (cpu.rTR) * 125u / 64u);
          unsigned long nowLeft = left;
          if (!sampleInterrupts (cpup))
            nowLeft = sleepCPU (cpup, left);
          if (nowLeft) {
            // sleepCPU uses a clock that is not guaranteed to be monotonic, and occasionally returns nowLeft > left.
            // Don't run rTR backwards if that happens
            if (nowLeft <= left)
              cpu.rTR = (word27) (left * 64 / 125);
          } else {
            // We slept until timer runout
            if (cpu.tweaks.tro_enable)
              setG7fault (cpup, cpu.cpuIdx, FAULT_TRO, fst_zero);
            cpu.rTR = MASK27;
          }
          cpu.rTRticks = 0;
          break;
        } /* if (ret == CONT_DIS) */

        cpu.wasXfer = false;

        if (ret < 0) {
          simWarn ("executeInstruction returned %d?\n", ret);
          break;
        }

        if ((! cpu.cu.repeat_first) &&
            (cpu.cu.rpt ||
            (cpu.cu.rd && (cpu.PPR.IC & 1)) ||
            cpu.cu.rl)) {
          CPT (cpt1U, 26); // RPx instruction
          if (cpu.cu.rd)
          -- cpu.PPR.IC;
          cpu.wasXfer = false;
          setCpuCycle (cpup, FETCH_cycle);
          break;
        }

        // If we just did the odd word of a fault pair
        if (cpu.cycle == FAULT_EXEC_cycle && !cpu.cu.xde && cpu.cu.xdo) {
          clear_temporary_absolute_mode (cpup);
          cu_safe_restore (cpup);
          CPT (cpt1U, 12); // cu restored
          clearFaultCycle (cpup);
          // cu_safe_restore calls decode_instruction ()
          // we can determine the instruction length.
          // decode_instruction() restores ci->info->ndes
          cpu.wasXfer  = false;
          cpu.isExec   = false;
          cpu.isXED    = false;

          cpu.PPR.IC  += ci->info->ndes;
          cpu.PPR.IC ++;

          setCpuCycle (cpup, FETCH_cycle);
          break;
        }

        // If we just did the odd word of a interrupt pair
        if (cpu.cycle == INTERRUPT_EXEC_cycle && !cpu.cu.xde && cpu.cu.xdo) {
          clear_temporary_absolute_mode (cpup);
          cu_safe_restore (cpup);
          // cpu.cu.xdo = 0;
// The only place cycle is set to INTERRUPT_cycle in FETCH_cycle; therefore
// we can safely assume that is the state that should be restored.
          CPT (cpt1U, 12); // cu restored
          cpu.wasXfer = false;
          cpu.isExec  = false;
          cpu.isXED   = false;

          setCpuCycle (cpup, FETCH_cycle);
          break;
        }

        // Even word of fault or interrupt pair or xed
        if (cpu.cu.xde && cpu.cu.xdo) {
          // Get the odd
          cpu.cu.IWB           = cpu.cu.IRODD;
          cpu.cu.xde           = 0;
          cpu.isExec           = true;
          cpu.isXED            = true;
          cpu.cu.XSF           = 0;
          cpu.cu.TSN_VALID [0] = 0;
          cpu.TPR.TSR          = cpu.PPR.PSR;
          cpu.TPR.TRR          = cpu.PPR.PRR;
          break; // go do the odd word
        }

        if (cpu.cu.xde || cpu.cu.xdo) {  // we are in an XEC/XED
          cpu.cu.xde        = cpu.cu.xdo = 0;
          cpu.isExec        = false;
          cpu.isXED         = false;
          CPT (cpt1U, 27);           // XEx instruction
          cpu.wasXfer       = false;
          cpu.PPR.IC ++;
          if (ci->info->ndes > 0)
            cpu.PPR.IC     += ci->info->ndes;
          cpu.wasInhibited  = true;
          setCpuCycle (cpup, FETCH_cycle);
          break;
        }

        //ASSURE (cpu.cycle == EXEC_cycle);
        if (cpu.cycle != EXEC_cycle)
          simWarn ("expected EXEC_cycle (%d)\n", cpu.cycle);

        cpu.cu.xde = cpu.cu.xdo = 0;
        cpu.isExec = false;
        cpu.isXED  = false;

        // use prefetched instruction from cpu.cu.IRODD
        // we must have finished an instruction at an even location
        // skip multiword EIS instructions
        // skip repeat instructions for now
        // skip dis - we may need to take interrupts/g7faults
        // skip if (last instruction) wrote to current instruction range
        //  the hardware really does this and isolts tests it
        //  Multics Differences Manual DPS8 70/M
        //  should take segment number into account?
        if ((cpu.PPR.IC & 1) == 0 &&
            ci->info->ndes == 0 &&
            !cpu.cu.repeat_first && !cpu.cu.rpt && !cpu.cu.rd && !cpu.cu.rl &&
            !(cpu.currentInstruction.opcode == 0616 && cpu.currentInstruction.opcodeX == 0) &&
            (cpu.PPR.IC & ~3u) != (cpu.last_write  & ~3u)) {
          cpu.PPR.IC ++;
          cpu.wasXfer = false;
          cpu.cu.IWB  = cpu.cu.IRODD;
          setCpuCycle (cpup, PSEUDO_FETCH_cycle);
          break;
        }

        cpu.PPR.IC ++;
        if (ci->info->ndes > 0)
          cpu.PPR.IC += ci->info->ndes;

        CPT (cpt1U, 28); // enter fetch cycle
        cpu.wasXfer = false;
        setCpuCycle (cpup, FETCH_cycle);
        break;
      }

      case SYNC_FAULT_RTN_cycle: {
        CPT (cpt1U, 29); // sync. fault return
        cpu.wasXfer = false;
        // cu_safe_restore should have restored CU.IWB, so
        // we can determine the instruction length.
        // decode_instruction() restores ci->info->ndes

        cpu.PPR.IC += ci->info->ndes;
        cpu.PPR.IC ++;
        cpu.wasXfer = false;
        setCpuCycle (cpup, FETCH_cycle);
        break;
      }

      case FAULT_cycle: {
        CPT (cpt1U, 30); // fault cycle
        // In the FAULT CYCLE, the processor safe-stores the Control
        // Unit Data (see Section 3) into program-invisible holding
        // registers in preparation for a Store Control Unit ( scu)
        // instruction, then enters temporary absolute mode, forces the
        // current ring of execution C(PPR.PRR) to 0, and generates a
        // computed address for the fault trap pair by concatenating
        // the setting of the FAULT BASE switches on the processor
        // configuration panel with twice the fault number (see Table
        // 7-1).  This computed address and the operation code for the
        // Execute Double (xed) instruction are forced into the
        // instruction register and executed as an instruction. Note
        // that the execution of the instruction is not done in a
        // normal EXECUTE CYCLE but in the FAULT CYCLE with the
        // processor in temporary absolute mode.

        // F(A)NP should never be stored when faulting.
        // ISOLTS-865 01a,870 02d
        // Unconditional reset of APU status to FABS breaks boot.
        // Checking for F(A)NP here is equivalent to checking that the
        // last append cycle has made it as far as H/I without a fault.
        // Also reset it on TRB fault. ISOLTS-870 05a
        if ((cpu.cu.APUCycleBits & 060) || cpu.secret_addressing_mode)
            set_apu_status (cpup, apuStatus_FABS);

        // XXX the whole fault cycle should be rewritten as an xed
        // instruction pushed to IWB and executed

        // AL39: TRB fault doesn't safestore CUD - the original fault
        // CUD should be stored

        // ISOLTS-870 05a: CUD[5] and IWB are safe stored, possibly
        //  due to CU overlap

        // keep IRODD untouched if TRB occurred in an even location
        if (cpu.faultNumber != FAULT_TRB || cpu.cu.xde == 0) {
          cu_safe_store (cpup);
        } else {
          word36 tmpIRODD = cpu.scu_data[7];
          cu_safe_store (cpup);
          cpu.scu_data[7] = tmpIRODD;
        }
        CPT (cpt1U, 31); // safe store complete

        // Temporary absolute mode
        set_temporary_absolute_mode (cpup);

        // Set to ring 0
        cpu.PPR.PRR = 0;
        cpu.TPR.TRR = 0;

        // (12-bits of which the top-most 7-bits are used)
        uint fltAddress = (cpu.switches.FLT_BASE << 5) & 07740;
        L68_ (
          if (cpu.is_FFV) {
            cpu.is_FFV = false;
            CPTUR (cptUseMR);
            // The high 15 bits
            fltAddress = (cpu.MR.FFV & MASK15) << 3;
          }
        )

        // absolute address of fault YPair
        word24 addr = fltAddress + 2 * cpu.faultNumber;

        if (cpu.restart) {
          cpu.restart = false;
          addr = cpu.restart_address;
        }

        coreRead2 (cpup, addr, & cpu.cu.IWB, & cpu.cu.IRODD, __func__);
        HDBGMRead (cpup, addr, cpu.cu.IWB, "fault even");
        HDBGMRead (cpup, addr + 1, cpu.cu.IRODD, "fault odd");
        cpu.cu.xde = 1;
        cpu.cu.xdo = 1;
        cpu.isExec = true;
        cpu.isXED  = true;

        CPT (cpt1U, 33); // set fault exec cycle
        setCpuCycle (cpup, FAULT_EXEC_cycle);

        break;
      }

    }  // switch (cpu.cycle)
  } while (reason == 0);

leave:
  cpu.executing = false;
  cpu.inMultics = false;
  //HDBGPrint ();
#ifdef STATS
  for (unsigned short n = 0; n < N_CPU_UNITS_MAX; n++) {
#if !defined(SCHED_NEVER_YIELD)
    lockYieldAll     = lockYieldAll     + cpus[n].coreLockState.lockYield;
#endif /* if !defined(SCHED_NEVER_YIELD) */
    lockWaitMaxAll   = lockWaitMaxAll   + cpus[n].coreLockState.lockWaitMax;
    lockWaitAll      = lockWaitAll      + cpus[n].coreLockState.lockWait;
    lockImmediateAll = lockImmediateAll + cpus[n].coreLockState.lockImmediate;
    lockCntAll       = lockCntAll       + cpus[n].coreLockState.lockCnt;
    instrCntAll      = instrCntAll      + cpus[n].instrCnt;
    cycleCntAll      = cycleCntAll      + cpus[n].cycleCnt;
  }

  (void) fflush (stderr);
  (void) fflush (stdout);
  if (cycleCntAll > cpu.cycleCnt) {
    sim_msg ("\n");
    sim_msg ("+---------------------------------+\n");
    sim_msg ("|     Aggregate CPU Statistics    |\n");
    sim_msg ("+---------------------------------+\n");
    (void) fflush (stderr);
    (void) fflush (stdout);
# if defined(WIN_STDIO)
    sim_msg ("|  cycles        %15"PRIu64"  |\n", cycleCntAll);
    sim_msg ("|  instructions  %15"PRIu64"  |\n", instrCntAll);
    (void) fflush (stderr);
    (void) fflush (stdout);
    sim_msg ("+---------------------------------+\n");
    sim_msg ("|  lockCnt       %15"PRIu64"  |\n", lockCntAll);
    sim_msg ("|  lockImmediate %15"PRIu64"  |\n", lockImmediateAll);
    (void) fflush (stderr);
    (void) fflush (stdout);
    sim_msg ("+---------------------------------+\n");
    sim_msg ("|  lockWait      %15"PRIu64"  |\n", lockWaitAll);
    sim_msg ("|  lockWaitMax   %15"PRIu64"  |\n", lockWaitMaxAll);
    (void) fflush (stderr);
    (void) fflush (stdout);
#  if !defined(SCHED_NEVER_YIELD)
    sim_msg ("|  lockYield     %15"PRIu64"  |\n", lockYieldAll);
#  else
    sim_msg ("|  lockYield                ----  |\n");
#  endif /* if !defined(SCHED_NEVER_YIELD) */
    sim_msg ("+---------------------------------+\n");
    (void) fflush (stderr);
    (void) fflush (stdout);
# else
    sim_msg ("|  cycles        %'15"PRIu64"  |\n", cycleCntAll);
    sim_msg ("|  instructions  %'15"PRIu64"  |\n", instrCntAll);
    (void) fflush (stderr);
    (void) fflush (stdout);
    sim_msg ("+---------------------------------+\n");
    sim_msg ("|  lockCnt       %'15"PRIu64"  |\n", lockCntAll);
    sim_msg ("|  lockImmediate %'15"PRIu64"  |\n", lockImmediateAll);
    (void) fflush (stderr);
    (void) fflush (stdout);
    sim_msg ("+---------------------------------+\n");
    sim_msg ("|  lockWait      %'15"PRIu64"  |\n", lockWaitAll);
    sim_msg ("|  lockWaitMax   %'15"PRIu64"  |\n", lockWaitMaxAll);
    (void) fflush (stderr);
    (void) fflush (stdout);
#  if !defined(SCHED_NEVER_YIELD)
    sim_msg ("|  lockYield     %'15"PRIu64"  |\n", lockYieldAll);
#  else
    sim_msg ("|  lockYield                ----  |\n");
#  endif /* if !defined(SCHED_NEVER_YIELD) */
    sim_msg ("+---------------------------------+\n");
    (void) fflush (stderr);
    (void) fflush (stdout);
# endif /* if defined(WIN_STDIO) */
  }
#endif

// scp needs to have the IC statically allocated, so a placeholder
// was created. Update the placeholder so the IC can be seen via scp
// and restarting sim_instr won't lose the place.

  dummy_IC = cpu.PPR.IC;

  stopCPUThread (cpup);
}

/*
 * cd@libertyhaven.com - sez ...
 *  If the instruction addresses a block of four words, the target of the
 * instruction is supposed to be an address that is aligned on a four-word
 * boundary (0 mod 4). If not, the processor will grab the four-word block
 * containing that address that begins on a four-word boundary, even if it
 * has to go back 1 to 3 words. Analogous explanation for 8, 16, and 32 cases.
 *
 * olin@olinsibert.com - sez ...
 *  It means that the appropriate low bits of the address are forced to zero.
 * So it's the previous words, not the succeeding words, that are used to
 * satisfy the request. -- Olin
 */

int operand_size (cpuState_t * cpup) {
  DCDstruct * i = & cpu.currentInstruction;
  if (i->info->flags & (READ_OPERAND | STORE_OPERAND))
    return 1;
  else if (i->info->flags & (READ_YPAIR | STORE_YPAIR))
    return 2;
  else if (i->info->flags & (READ_YBLOCK8 | STORE_YBLOCK8))
    return 8;
  else if (i->info->flags & (READ_YBLOCK16 | STORE_YBLOCK16))
    return 16;
  else if (i->info->flags & (READ_YBLOCK32 | STORE_YBLOCK32))
    return 32;
  return 0;
}

// read instruction operands

void readOperandRead (cpuState_t * cpup, word18 addr) {
  CPT (cpt1L, 6); // read_operand

  switch (operand_size (cpup)) {
    case 1:
      CPT (cpt1L, 7); // word
      ReadOperandRead (cpup, addr, & cpu.CY);
      break;
    case 2:
      CPT (cpt1L, 8); // double word
      addr &= 0777776;   // make even
      Read2OperandRead (cpup, addr, cpu.Ypair);
      break;
    case 8:
      CPT (cpt1L, 9); // oct word
      addr &= 0777770;   // make on 8-word boundary
      Read8 (cpup, addr, cpu.Yblock8, cpu.currentInstruction.b29);
      break;
    case 16:
      CPT (cpt1L, 10); // 16 words
      addr &= 0777770;   // make on 8-word boundary
      Read16 (cpup, addr, cpu.Yblock16);
      break;
    case 32:
      CPT (cpt1L, 11); // 32 words
      addr &= 0777740;   // make on 32-word boundary
      for (uint j = 0 ; j < 32 ; j += 1)
        ReadOperandRead (cpup, addr + j, cpu.Yblock32 + j);
      break;
  }
}

void readOperandRMW (cpuState_t * cpup, word18 addr) {
  CPT (cpt1L, 6); // read_operand
  switch (operand_size (cpup)) {
    case 1:
      CPT (cpt1L, 7); // word
      ReadOperandRMW (cpup, addr, & cpu.CY);
      break;
    case 2:
      CPT (cpt1L, 8); // double word
      addr &= 0777776;   // make even
      Read2OperandRead (cpup, addr, cpu.Ypair);
      break;
    case 8:
      CPT (cpt1L, 9); // oct word
      addr &= 0777770;   // make on 8-word boundary
      Read8 (cpup, addr, cpu.Yblock8, cpu.currentInstruction.b29);
      break;
    case 16:
      CPT (cpt1L, 10); // 16 words
      addr &= 0777770;   // make on 8-word boundary
      Read16 (cpup, addr, cpu.Yblock16);
      break;
    case 32:
      CPT (cpt1L, 11); // 32 words
      addr &= 0777740;   // make on 32-word boundary
      for (uint j = 0 ; j < 32 ; j += 1)
        ReadOperandRMW (cpup, addr + j, cpu.Yblock32 + j);
      break;
  }
}

// write instruction operands

simRc write_operand (cpuState_t * cpup, word18 addr, UNUSED processor_cycle_type cyctyp) {
  switch (operand_size (cpup)) {
    case 1:
      CPT (cpt1L, 12); // word
      WriteOperandStore (cpup, addr, cpu.CY);
      break;
    case 2:
      CPT (cpt1L, 13); // double word
      addr &= 0777776;   // make even
      Write2OperandStore (cpup, addr + 0, cpu.Ypair);
      break;
    case 8:
      CPT (cpt1L, 14); // 8 words
      addr &= 0777770;   // make on 8-word boundary
      Write8 (cpup, addr, cpu.Yblock8, cpu.currentInstruction.b29);
      break;
    case 16:
      CPT (cpt1L, 15); // 16 words
      addr &= 0777770;   // make on 8-word boundary
      Write16 (cpup, addr, cpu.Yblock16);
      break;
    case 32:
      CPT (cpt1L, 16); // 32 words
      addr &= 0777740;   // make on 32-word boundary
      Write32 (cpup, addr, cpu.Yblock32);
      break;
  }

  return SCPE_OK;
}

#if !defined(SPEED)
simRc set_mem_watch (int32_t arg, const char * buf) {
  if (strlen (buf) == 0) {
    if (arg) {
      simWarn ("no argument to watch?\n");
      return SCPE_ARG;
    }
    sim_msg ("Clearing all watch points\n");
    (void) memset (& watch_bits, 0, sizeof (watch_bits));
    return SCPE_OK;
  }
  char * end;
  long int n = strtol (buf, & end, 0);
  if (* end || n < 0 || n >= MEMSIZE) {
    simWarn ("Invalid argument to watch? %ld\n", n);
    return SCPE_ARG;
  }
  watch_bits [n] = arg != 0;
  return SCPE_OK;
}
#endif /* if !defined(SPEED) */

/*!
 * "Raw" core interface ....
 */

#if defined(SPEED)
#define nem_check(cpup, addr, context)
#else
static void nem_check (cpuState_t * cpup, word24 addr, const char * context) {
  if (lookup_cpu_mem_map (cpup, addr) < 0) {
    doFault (cpup, FAULT_STR, fst_str_nea,  context);
  }
}
#endif /* if !defined(SPEED) */

void coreRead (cpuState_t * cpup, word24 addr, word36 *data, const char * ctx) {
  PNL (cpu.portBusy = true;)
  SC_MAP_ADDR (addr, addr);
#  if !defined(SUNLINT)
  word36 w;
  LOAD_ACQ_CORE_WORD (w, addr);
# if !defined(SPEED)
  if (watch_bits [addr]) {
    sim_msg ("WATCH [%"PRIu64"] %05o:%06o read   %08o %012"PRIo36" (%s)\n", cpu.cycleCnt, cpu.PPR.PSR, cpu.PPR.IC, addr, w, ctx);
    traceInstruction (cpup);
  }
#ifdef SYNCTEST
  if (addr == 00320420 || addr == 0 || addr == 1) {
    HDBGNote (cpup, __func__, "%05o:%06o %08o %012lo", cpu.PPR.PSR, cpu.PPR.IC, addr, *data);
    simPrintf ("CPU%c %s %05o:%06o %08o %012lo\n", cpu.cpuIdx + 'A', __func__, cpu.PPR.PSR, cpu.PPR.IC, addr, * data);
  }
#endif
#endif /* if !defined(SPEED) */
  *data = w & DMASK;
# endif /* ifndef SUNLINT */

  DO_WORK_MEM;
# ifdef LUF_BY_MEMORY
  cpu.lufCounter ++;
# endif
  PNL (trackport (cpup, addr, * data));
}

void coreReadLock (cpuState_t * cpup, word24 addr, word36 *data, UNUSED const char * ctx) {
  SC_MAP_ADDR (addr, addr);
  LOCK_CORE_WORD (addr, & cpu.coreLockState);
  if (cpu.coreLockState.locked_addr != 0) {
    simWarn ("coreReadLock: locked %08o locked_addr %08o %c %05o:%06o\n", addr, cpu.coreLockState.locked_addr, cpu.cpuIdx + 'A', cpu.PPR.PSR, cpu.PPR.IC);
    coreUnlockAll (cpup);
  }
  cpu.coreLockState.locked_addr = addr;
#  if !defined(SUNLINT)
  word36 v;
  LOAD_ACQ_CORE_WORD (v, addr);
  * data = v & DMASK;
#  endif /* if !defined(SUNLINT) */
# ifdef LUF_BY_MEMORY
  cpu.lufCounter ++;
# endif
#ifdef SYNCTEST
  if (addr == 00320420 || addr == 0 || addr == 1) {
    HDBGNote (cpup, __func__, "%05o:%06o %08o %012lo", cpu.PPR.PSR, cpu.PPR.IC, addr, *data);
    simPrintf ("CPU%c %s %05o:%06o %08o %012lo\n", cpu.cpuIdx + 'A', __func__, cpu.PPR.PSR, cpu.PPR.IC, addr, * data);
  }
#endif
}

void coreWrite (cpuState_t * cpup, word24 addr, word36 data, const char * ctx) {
  PNL (cpu.portBusy = true;)
  SC_MAP_ADDR (addr, addr);
  if (cpu.tweaks.isolts_mode) {
    if (cpu.MR.sdpap) {
      simWarn ("failing to implement sdpap\n");
      cpu.MR.sdpap = 0;
    }
    if (cpu.MR.separ) {
      simWarn ("failing to implement separ\n");
      cpu.MR.separ = 0;
    }
  }
#ifdef SYNCTEST
  if (addr == 00320420 || addr == 0 || addr == 1) {
    HDBGNote (NULL, __func__, "CPU%c %05o:%06o %08o %012lo", cpu.cpuIdx + 'A', cpu.PPR.PSR, cpu.PPR.IC, addr, data);
    simPrintf ("CPU%c %s %05o:%06o %08o %012lo\n", cpu.cpuIdx + 'A', __func__, cpu.PPR.PSR, cpu.PPR.IC, addr, data);
  }
#endif
  LOCK_CORE_WORD (addr, & cpu.coreLockState);
#  if !defined(SUNLINT)
  STORE_REL_CORE_WORD (addr, data);
#  endif /* if !defined(SUNLINT) */
#if !defined(SPEED)
  if (watch_bits [addr]) {
    sim_msg ("WATCH [%"PRIu64"] %05o:%06o write  %08o %012"PRIo36" (%s)\n", cpu.cycleCnt, cpu.PPR.PSR, cpu.PPR.IC, addr, data, ctx);
    traceInstruction (cpup);
  }
#endif /* if !defined(SPEED) */
  DO_WORK_MEM;
# ifdef LUF_BY_MEMORY
  cpu.lufCounter ++;
# endif
  PNL (trackport (cpup, addr, data));
}

void coreWriteUnmasked (cpuState_t * cpup, word24 addr, word36 data, const char * ctx) {
  PNL (cpu.portBusy = true;)
  SC_MAP_ADDR (addr, addr);
  if (cpu.tweaks.isolts_mode) {
    if (cpu.MR.sdpap) {
      simWarn ("failing to implement sdpap\n");
      cpu.MR.sdpap = 0;
    }
    if (cpu.MR.separ) {
      simWarn ("failing to implement separ\n");
      cpu.MR.separ = 0;
    }
  }
  LOCK_CORE_WORD (addr, & cpu.coreLockState);
#  ifndef SUNLINT
  STORE_REL_CORE_WORD_UNMASKED (addr, data);
#  endif /* ifndef SUNLINT */
# ifndef SPEED
  if (watch_bits [addr]) {
    sim_msg ("WATCH [%"PRIu64"] %05o:%06o write  %08o %012"PRIo36" (%s)\n", cpu.cycleCnt, cpu.PPR.PSR, cpu.PPR.IC, addr, data, ctx);
    traceInstruction (cpup);
  }
#ifdef SYNCTEST
  if (addr == 00320420 || addr == 0 || addr == 1) {
    HDBGNote (NULL, __func__, "%05o:%06o %08o %012lo", cpu.PPR.PSR, cpu.PPR.IC, addr, data);
    simPrintf ("CPU%c %s %05o:%06o %08o %012lo\n", cpu.cpuIdx + 'A', __func__, cpu.PPR.PSR, cpu.PPR.IC, addr, data);
  }
#endif
# endif
  DO_WORK_MEM;
# ifdef LUF_BY_MEMORY
  cpu.lufCounter ++;
# endif
  PNL (trackport (cpup, addr, data));
}

void coreWriteUnlock (cpuState_t * cpup, word24 addr, word36 data, UNUSED const char * ctx) {
  SC_MAP_ADDR (addr, addr);
  if (cpu.coreLockState.locked_addr != addr) {
    simWarn ("coreWriteUnlock: locked %08o locked_addr %08o %c %05o:%06o\n",
              addr,        cpu.coreLockState.locked_addr, cpu.cpuIdx + 'A', cpu.PPR.PSR, cpu.PPR.IC);
   coreUnlockAll (cpup);
  }

# ifndef SUNLINT
  STORE_REL_CORE_WORD (addr, data);
# endif /* ifndef SUNLINT */
  cpu.coreLockState.locked_addr = 0;
  DO_WORK_MEM;
# ifdef LUF_BY_MEMORY
  cpu.lufCounter ++;
# endif
}

void coreUnlockAll (cpuState_t * cpup) {
  if (cpu.coreLockState.locked_addr != 0) {
    simWarn ("coreUnlockAll: locked %08o %c %05o:%06o\n",
              cpu.coreLockState.locked_addr, cpu.cpuIdx + 'A', cpu.PPR.PSR,     cpu.PPR.IC);
# ifndef SUNLINT
      // We know that the word was:
      //     locked by this thread
      //     the word was acquired as part of the locking process
      //     another thread writing to a locked word is undefined
      //  therefore we read M directly without acquire.
    //STORE_REL_CORE_WORD (cpu.coreLockState.locked_addr, M[cpu.coreLockState.locked_addr]);
    word36 data;
    coreRead (cpup, cpu.coreLockState.locked_addr, & data, __func__);
    STORE_REL_CORE_WORD (cpu.coreLockState.locked_addr, data);
# endif /* ifndef SUNLINT */
    cpu.coreLockState.locked_addr = 0;
    DO_WORK_MEM;
# ifdef LUF_BY_MEMORY
    cpu.lufCounter ++;
# endif
  }
}

void coreWriteZone (cpuState_t * cpup, word24 addr, word36 data, const char * ctx) {
  PNL (cpu.portBusy = true;)
  if (cpu.tweaks.isolts_mode) {
    if (cpu.MR.sdpap) {
      simWarn ("failing to implement sdpap\n");
      cpu.MR.sdpap = 0;
    }
    if (cpu.MR.separ) {
      simWarn ("failing to implement separ\n");
      cpu.MR.separ = 0;
    }
  }
  // coreReadLock, coreWriteUnlock do SC_MAP_ADDR internally
  word36 v;
  coreReadLock (cpup, addr,  &v, ctx);
  v = (v & ~cpu.zone) | (data & cpu.zone);
  coreWriteUnlock (cpup, addr, v, ctx);
  cpu.useZone = false; // Safety
# ifndef SPEED
  if (watch_bits [addr]) {
    sim_msg ("WATCH [%"PRIu64"] %05o:%06o writez %08o %012"PRIo36" (%s)\n", cpu.cycleCnt, cpu.PPR.PSR, cpu.PPR.IC, addr, data, ctx);
    traceInstruction (cpup);
  }
#ifdef SYNCTEST
  if (addr == 00320420 || addr == 0 || addr == 1) {
    HDBGNote (NULL, __func__, "CPU%c %05o:%06o %08o %012lo", cpuIdx + 'A', cpu.PPR.PSR, cpu.PPR.IC, addr, data);
    simPrintf ("CPU%c %s %05o:%06o %08o %012lo\n", cpu.cpuIdx + 'A', __func__, cpu.PPR.PSR, cpu.PPR.IC, addr, data);
  }
#endif
# endif
  DO_WORK_MEM;
# ifdef LUF_BY_MEMORY
  cpu.lufCounter ++;
# endif
  PNL (trackport (cpup, addr, data));
}

void coreRead2 (cpuState_t * cpup, word24 addr, word36 *even, word36 *odd, const char * ctx) {
  PNL (cpu.portBusy = true;)
  /*LINTED E_FUNC_VAR_UNUSED*/ /* Appease SUNLINT */
  word36 v;
  if (addr & 1) {
    addr &= (word24)~1; /* make it an even address */
  }
  SC_MAP_ADDR (addr, addr);
#  ifndef SUNLINT
  LOAD_ACQ_CORE_WORD (v, addr);
  if (v & MEM_LOCKED)
    simWarn ("coreRead2: even locked %08o locked_addr %08o %c %05o:%06o\n",
              addr,        cpu.coreLockState.locked_addr, cpu.cpuIdx + 'A',
              cpu.PPR.PSR, cpu.PPR.IC);
  *even = v & DMASK;
# ifndef SPEED
  if (watch_bits [addr]) {
    sim_msg ("WATCH [%"PRIu64"] %05o:%06o read2  %08o %012"PRIo36" (%s)\n", cpu.cycleCnt, cpu.PPR.PSR, cpu.PPR.IC, addr, v, ctx);
    traceInstruction (cpup);
  }
# endif
#ifdef SYNCTEST
  if (addr == 00320420 || addr == 0 || addr == 1) {
    HDBGNote (NULL, __func__, "CPU%c %05o:%06o %08o %012lo", cpu.cpuIdx + 'A', cpu.PPR.PSR, cpu.PPR.IC, addr, *even);
    simPrintf ("CPU%c %s %05o:%06o %08o %012lo\n", cpu.cpuIdx + 'A', __func__, cpu.PPR.PSR, cpu.PPR.IC, addr, *even);
  }
#endif
  addr++;
#  endif /* ifndef SUNLINT */
#  ifndef SUNLINT
  LOAD_ACQ_CORE_WORD (v, addr);
  if (v & MEM_LOCKED)
    simWarn ("coreRead2: odd locked %08o locked_addr %08o %c %05o:%06o\n",
              addr,        cpu.coreLockState.locked_addr, cpu.cpuIdx + 'A',
              cpu.PPR.PSR, cpu.PPR.IC);
  *odd = v & DMASK;
#  endif /* ifndef SUNLINT */
# ifndef SPEED
  if (watch_bits [addr]) {
    sim_msg ("WATCH [%"PRIu64"] %05o:%06o read2  %08o %012"PRIo36" (%s)\n", cpu.cycleCnt, cpu.PPR.PSR, cpu.PPR.IC, addr, v, ctx);
    traceInstruction (cpup);
  }
# endif
#ifdef SYNCTEST
  if (addr == 00320420 || addr == 0 || addr == 1) {
    HDBGNote (NULL, __func__, "CPU%c %05o:%06o %08o %012lo", cpu.cpuIdx + 'A', cpu.PPR.PSR, cpu.PPR.IC, addr, *odd);
    simPrintf ("CPU%c %s %05o:%06o %08o %012lo\n", cpu.cpuIdx + 'A', __func__, cpu.PPR.PSR, cpu.PPR.IC, addr, *odd);
  }
#endif
  DO_WORK_MEM;
# ifdef LUF_BY_MEMORY
  cpu.lufCounter ++;
# endif
  PNL (trackport (cpup, addr - 1, * even));
}

void coreWrite2 (cpuState_t * cpup, word24 addr, word36 even, word36 odd, const char * ctx) {
  PNL (cpu.portBusy = true;)
  if (addr & 1) {
    simWarn ("warning: subtracting 1 from pair at %o in coreWrite2 (%s)\n", addr, ctx);
    addr &= (word24) ~1; /* make it even a dress, or iron a skirt ;) */
  }
  SC_MAP_ADDR (addr, addr);
  if (cpu.tweaks.isolts_mode) {
    if (cpu.MR.sdpap) {
      simWarn ("failing to implement sdpap\n");
      cpu.MR.sdpap = 0;
    }
    if (cpu.MR.separ) {
      simWarn ("failing to implement separ\n");
      cpu.MR.separ = 0;
    }
  }

# ifndef SPEED
  if (watch_bits [addr]) {
    sim_msg ("WATCH [%"PRIu64"] %05o:%06o write2 %08o %012"PRIo36" (%s)\n", cpu.cycleCnt, cpu.PPR.PSR, cpu.PPR.IC, addr, even, ctx);
    traceInstruction (cpup);
  }
# endif
#ifdef SYNCTEST
  if (addr == 00320420 || addr == 0 || addr == 1) {
    HDBGNote (NULL, __func__, "CPU%c %05o:%06o %08o %012lo", cpu.cpuIdx + 'A', cpu.PPR.PSR, cpu.PPR.IC, addr, even);
    simPrintf ("CPU%c %s %05o:%06o %08o %012lo\n", cpu.cpuIdx + 'A', __func__, cpu.PPR.PSR, cpu.PPR.IC, addr, even);
  }
#endif
  LOCK_CORE_WORD (addr, & cpu.coreLockState);
#  ifndef SUNLINT
  STORE_REL_CORE_WORD (addr, even);
#  endif /* ifndef SUNLINT */
  addr++;

  // If the even address is OK, the odd will be
  //mem_check (addr,  "coreWrite2 nem");

# ifndef SPEED
  if (watch_bits [addr]) {
    sim_msg ("WATCH [%"PRIu64"] %05o:%06o write2 %08o %012"PRIo36" (%s)\n", cpu.cycleCnt, cpu.PPR.PSR, cpu.PPR.IC, addr, odd, ctx);
    traceInstruction (cpup);
  }
# endif
#ifdef SYNCTEST
  if (addr == 00320420 || addr == 0 || addr == 1) {
    HDBGNote (NULL, __func__, "CPU%c %05o:%06o %08o %012lo", cpu.cpuIdx + 'A', cpu.PPR.PSR, cpu.PPR.IC, addr, odd);
    simPrintf ("CPU%c %s %05o:%06o %08o %012lo\n", cpu.cpuIdx + 'A', __func__, cpu.PPR.PSR, cpu.PPR.IC, addr, odd);
  }
#endif
  LOCK_CORE_WORD (addr, & cpu.coreLockState);
#  ifndef SUNLINT
  STORE_REL_CORE_WORD (addr, odd);
#  endif /* ifndef SUNLINT */
  DO_WORK_MEM;
# ifdef LUF_BY_MEMORY
  cpu.lufCounter ++;
# endif
  PNL (trackport (cpup, addr - 1, even));
}

/*
 * Instruction fetcher ...
 * Fetch + decode instruction at 18-bit address 'addr'
 */

/*
 * Instruction decoder .....
 */

void decode_instruction (cpuState_t * cpup, word36 inst, DCDstruct * p) {
  CPT (cpt1L, 17); // instruction decoder
  (void) memset (p, 0, sizeof (DCDstruct));

  p->opcode   = GET_OP (inst);   // get opcode
  p->opcodeX  = GET_OPX (inst);   // opcode extension
  p->opcode10 = p->opcode | (p->opcodeX ? 01000 : 0);
  p->address  = GET_ADDR (inst); // address field from instruction
  p->b29      = GET_A (inst);    // "A" the indirect via pointer register flag
  p->i        = GET_I (inst);    // "I" inhibit interrupt flag
  p->tag      = GET_TAG (inst);  // instruction tag

  p->info     = get_iwb_info  (p);     // get info for IWB instruction

  if (p->info->flags & IGN_B29)
    p->b29 = 0;   // make certain 'a' bit is valid always

  if (p->info->ndes > 0) {
    p->b29 = 0;
    p->tag = 0;
    if (p->info->ndes > 1)
      (void) memset (& cpu.currentEISinstruction, 0, sizeof (cpu.currentEISinstruction));
  }
}

// MM stuff ...

//
// is_priv_mode()
//
// Report whether or or not the CPU is in privileged mode.
// True if in absolute mode or if priv bit is on in segment TPR.TSR
// The processor executes instructions in privileged mode when forming
// addresses in absolute mode or when forming addresses in append mode and the
// segment descriptor word (SDW) for the segment in execution specifies a
// privileged procedure and the execution ring is equal to zero.
//
// PPR.P A flag controlling execution of privileged instructions.
//
// Its value is 1 (permitting execution of privileged instructions) if PPR.PRR
// is 0 and the privileged bit in the segment descriptor word (SDW.P) for the
// procedure is 1; otherwise, its value is 0.
//

int is_priv_mode (cpuState_t * cpup) {
// Back when it was ABS/APP/BAR, this test was right; now that
// it is ABS/APP,BAR/NBAR, check bar mode.
// Fixes ISOLTS 890 05a.
  if (get_bar_mode (cpup))
    return 0;

// PPR.P is only relevant if we're in APPEND mode. ABSOLUTE mode ignores it.
  if (get_addr_mode (cpup) == ABSOLUTE_mode)
    return 1;
  else if (cpu.PPR.P)
    return 1;

  return 0;
}

/*
 * get_bar_mode: During fault processing, we do not want to fetch and execute
 * the fault vector instructions in BAR mode. We leverage the
 * secret_addressing_mode flag that is set in set_TEMPORARY_ABSOLUTE_MODE to
 * direct us to ignore the I_NBAR indicator register.
 */

bool get_bar_mode (cpuState_t * cpup) {
  return ! (cpu.secret_addressing_mode || TST_I_NBAR);
}

addrModes_e get_addr_mode (cpuState_t * cpup) {
  if (cpu.secret_addressing_mode)
    return ABSOLUTE_mode; // This is not the mode you are looking for

  if (TST_I_ABS)
    return ABSOLUTE_mode;
  else
    return APPEND_mode;
}

/*
 * set_addr_mode()
 *
 * Put the CPU into the specified addressing mode.   This involves
 * setting a couple of IR flags and the PPR priv flag.
 */

void set_addr_mode (cpuState_t * cpup, addrModes_e mode) {
//    cpu.cu.XSF = false;
// Temporary hack to fix fault/intr pair address mode state tracking
//   1. secret_addressing_mode is only set in fault/intr pair processing.
//   2. Assume that the only set_addr_mode that will occur is the b29 special
//   case or ITx.
    //if (secret_addressing_mode && mode == APPEND_mode)
      //set_went_appending ();

  cpu.secret_addressing_mode = false;
  if (mode == ABSOLUTE_mode) {
    CPT (cpt1L, 22); // set abs mode
    SET_I_ABS;
    cpu.PPR.P = 1;
  } else if (mode == APPEND_mode) {
    CPT (cpt1L, 23); // set append mode
    CLR_I_ABS;
  } else {
    simWarn ("APU: Unable to determine address mode. Can't happen!\n");
  }
}

/*
 * stuff to handle BAR mode ...
 */

/*
 * The Base Address Register provides automatic hardware Address relocation and
 * Address range limitation when the processor is in BAR mode.
 *
 * BAR.BASE: Contains the 9 high-order bits of an 18-bit address relocation
 * constant. The low-order bits are generated as zeros.
 *
 * BAR.BOUND: Contains the 9 high-order bits of the unrelocated address limit.
 * The low- order bits are generated as zeros. An attempt to access main memory
 * beyond this limit causes a store fault, out of bounds. A value of 0 is truly
 * 0, indicating a null memory range.
 *
 * In BAR mode, the base address register (BAR) is used. The BAR contains an
 * address bound and a base address. All computed addresses are relocated by
 * adding the base address. The relocated address is combined with the
 * procedure pointer register to form the virtual memory address. A program is
 * kept within certain limits by subtracting the unrelocated computed address
 * from the address bound. If the result is zero or negative, the relocated
 * address is out of range, and a store fault occurs.
 */

// CANFAULT
word18 getBARAddress (cpuState_t * cpup, word18 addr) {
  if (cpu . BAR.BOUND == 0)
    // store fault, out of bounds.
    doFault (cpup, FAULT_STR, fst_str_oob, "BAR store fault; out of bounds");

  // A program is kept within certain limits by subtracting the
  // unrelocated computed address from the address bound. If the result
  // is zero or negative, the relocated address is out of range, and a
  // store fault occurs.
  //
  // BAR.BOUND - CA <= 0
  // BAR.BOUND <= CA
  // CA >= BAR.BOUND
  //
  if (addr >= (((word18) cpu . BAR.BOUND) << 9))
    // store fault, out of bounds.
    doFault (cpup, FAULT_STR, fst_str_oob, "BAR store fault; out of bounds");

  word18 barAddr = (addr + (((word18) cpu . BAR.BASE) << 9)) & 0777777;
  return barAddr;
}

//=============================================================================

static void add_history (cpuState_t * cpup, uint hset, word36 w0, word36 w1) {
  //if (cpu.MR.emr) {
    cpu.history [hset] [cpu.history_cyclic[hset]] [0] = w0;
    cpu.history [hset] [cpu.history_cyclic[hset]] [1] = w1;
    cpu.history_cyclic[hset] = (cpu.history_cyclic[hset] + 1) % N_MODEL_HIST_SIZE;
  // }
}

void addHistoryForce (cpuState_t * cpup, uint hset, word36 w0, word36 w1) {
  cpu.history [hset] [cpu.history_cyclic[hset]] [0] = w0;
  cpu.history [hset] [cpu.history_cyclic[hset]] [1] = w1;
  cpu.history_cyclic[hset] = (cpu.history_cyclic[hset] + 1) % N_MODEL_HIST_SIZE;
}

void addDps8mCuHistory (cpuState_t * cpup) {
  if (cpu.skip_cu_hist)
    return;
  if (! cpu.MR_cache.emr)
    return;
  if (! cpu.MR_cache.ihr)
    return;
  if (cpu.MR_cache.hrxfr && ! cpu.wasXfer)
    return;

  word36 flags   = 0; // XXX fill out
  word5 proccmd  = 0; // XXX fill out
  word7 flags2   = 0; // XXX fill out
  word36 w0      = 0, w1 = 0;
  w0            |= flags & W36_C (0777777000000);
  w0            |= IWB_IRODD & MASK18;
  w1            |= (cpu.iefpFinalAddress & MASK24) << 12;
  w1            |= (proccmd & MASK5) << 7;
  w1            |= flags2 & 0176;
  add_history (cpup, CU_HIST_REG, w0, w1);
}

#if !defined(QUIET_UNUSED)
void add_dps8m_DU_OU_history (cpuState_t * cpup, word36 flags, word18 ICT, word9 RS_REG, word9 flags2) {
  word36 w0  = flags, w1 = 0;
  w1        |= (ICT & MASK18) << 18;
  w1        |= (RS_REG & MASK9) << 9;
  w1        |= flags2 & MASK9;
  add_history (cpup, DPS8M_DU_OU_HIST_REG, w0, w1);
}

void addDps8mApuHistory (cpuState_t * cpup, word15 ESN, word21 flags, word24 RMA, word3 RTRR, word9 flags2) {
  word36 w0  = 0, w1 = 0;
  w0        |= (ESN & MASK15) << 21;
  w0        |= flags & MASK21;
  w1        |= (RMA & MASK24) << 12;
  w1        |= (RTRR & MASK3) << 9;
  w1        |= flags2 & MASK9;
  add_history (cpu.tweaks.l68_mode ? L68_APU_HIST_REG : DPS8M_APU_HIST_REG, w0, w1);
}

void addDps8mEapuHistory (word18 ZCA, word18 opcode) {
  word36 w0  = 0;
  w0        |= (ZCA & MASK18) << 18;
  w0        |= opcode & MASK18;
  add_history (DPS8M_EAPU_HIST_REG, w0, 0);
  //cpu.eapu_hist[cpu.eapu_cyclic].ZCA = ZCA;
  //cpu.eapu_hist[cpu.eapu_cyclic].opcode = opcode;
  //cpu.history_cyclic[DPS8M_EAPU_HIST_REG] =
    //(cpu.history_cyclic[DPS8M_EAPU_HIST_REG] + 1) % N_DPS8M_HIST_SIZE;
}
#endif

// According to ISOLTS
//
//   0 PIA
//   1 POA
//   2 RIW
//   3 SIW
//   4 POT
//   5 PON
//   6 RAW
//   7 SAW
//   8 TRGO
//   9 XDE
//  10 XDO
//  11 IC
//  12 RPTS
//  13 WI
//  14 AR F/E
//  15 XIP
//  16 FLT
//  17 COMPL. ADD BASE
//  18:23 OPCODE/TAG
//  24:29 ADDREG
//  30:34 COMMAND A/B/C/D/E
//  35:38 PORT A/B/C/D
//  39 FB XEC
//  40 INS FETCH
//  41 CU STORE
//  42 OU STORE
//  43 CU LOAD
//  44 OU LOAD
//  45 RB DIRECT
//  46 -PC BUSY
//  47 PORT BUSY

void addL68CuHistory (cpuState_t * cpup) {
  CPT (cpt1L, 24); // add cu hist
// XXX strobe on opcode match
  if (cpu.skip_cu_hist)
    return;
  if (! cpu.MR_cache.emr)
    return;
  if (! cpu.MR_cache.ihr)
    return;

  word36 w0 = 0, w1 = 0;

  // 0 PIA
  // 1 POA
  // 2 RIW
  // 3 SIW
  // 4 POT
  // 5 PON
  // 6 RAW
  // 7 SAW
  PNL (putbits36_8 (& w0, 0, cpu.prepare_state);)
  // 8 TRG
  putbits36_1  (& w0, 8, cpu.wasXfer);
  // 9 XDE
  putbits36_1  (& w0, 9, cpu.cu.xde);
  // 10 XDO
  putbits36_1  (& w0, 10, cpu.cu.xdo);
  // 11 IC
  putbits36_1  (& w0, 11, USE_IRODD ? 1 : 0);
  // 12 RPT
  putbits36_1  (& w0, 12, cpu.cu.rpt);
  // 13 WI Wait for instruction fetch XXX Not tracked
  // 14 ARF "AR F/E" Address register Full/Empty Address has valid data
  PNL (putbits36_1 (& w0, 14, cpu.AR_F_E);)
  // 15 !XA/Z "-XIP NOT prepare interrupt address"
  putbits36_1  (& w0, 15, cpu.cycle != INTERRUPT_cycle ? 1 : 0);
  // 16 !FA/Z Not tracked. (cu.-FL?)
  putbits36_1  (& w0, 16, cpu.cycle != FAULT_cycle ? 1 : 0);
  // 17 M/S  (master/slave, cu.-BASE?, NOT BAR MODE)
  putbits36_1  (& w0, 17, TSTF (cpu.cu.IR, I_NBAR) ? 1 : 0);
  // 18:35 IWR (lower half of IWB)
  putbits36_18 (& w0, 18, (word18) (IWB_IRODD & MASK18));

  // 36:53 CA
  putbits36_18 (& w1, 0, cpu.TPR.CA);
  // 54:58 CMD system controller command XXX
  // 59:62 SEL port select (XXX ignoring "only valid if port A-D is selected")
  PNL (putbits36_1 (& w1, 59-36, (cpu.portSelect == 0) ? 1 : 0);)
  PNL (putbits36_1 (& w1, 60-36, (cpu.portSelect == 1) ? 1 : 0);)
  PNL (putbits36_1 (& w1, 61-36, (cpu.portSelect == 2) ? 1 : 0);)
  PNL (putbits36_1 (& w1, 62-36, (cpu.portSelect == 3) ? 1 : 0);)
  // 63 XEC-INT An interrupt is present
  putbits36_1 (& w1, 63-36, cpu.interrupt_flag ? 1 : 0);
  // 64 INS-FETCH Perform an instruction fetch
  PNL (putbits36_1 (& w1, 64-36, cpu.INS_FETCH ? 1 : 0);)
  // 65 CU-STORE Control unit store cycle XXX
  // 66 OU-STORE Operations unit store cycle XXX
  // 67 CU-LOAD Control unit load cycle XXX
  // 68 OU-LOAD Operations unit load cycle XXX
  // 69 DIRECT Direct cycle XXX
  // 70 -PC-BUSY Port control logic not busy XXX
  // 71 BUSY Port interface busy XXX

  add_history (cpup, CU_HIST_REG, w0, w1);

  // Check for overflow
  CPTUR (cptUseMR);
  if (cpu.MR.hrhlt && cpu.history_cyclic[CU_HIST_REG] == 0) {
    //cpu.history_cyclic[CU_HIST_REG] = 15;
    if (cpu.MR.ihrrs)
      cpu.MR.ihr = 0;
    set_FFV_fault (cpup, 4);
    return;
  }
}

// du history register inputs (actual names)
// bit 00= fpol-cx;010       bit 36= fdud-dg;112
// bit 01= fpop-cx;010       bit 37= fgdlda-dc;010
// bit 02= need-desc-bd;000  bit 38= fgdldb-dc;010
// bit 03= sel-adr-bd;000    bit 39= fgdldc-dc;010
// bit 04= dlen=direct-bd;000bit 40= fnld1-dp;110
// bit 05= dfrst-bd;021      bit 41= fgldp1-dc;110
// bit 06= fexr-bd;010       bit 42= fnld2-dp;110
// bit 07= dlast-frst-bd;010 bit 43= fgldp2-dc;110
// bit 08= ddu-ldea-bd;000   bit 44= fanld1-dp;110
// bit 09= ddu-stea-bd;000   bit 45= fanld2-dp;110
// bit 10= dredo-bd;030      bit 46= fldwrt1-dp;110
// bit 11= dlvl<wd-sz-bg;000 bit 47= fldwrt2-dp;110
// bit 12= exh-bg;000        bit 48= data-avldu-cm;000
// bit 13= dend-seg-bd;111   bit 49= fwrt1-dp;110
// bit 14= dend-bd;000       bit 50= fgstr-dc;110
// bit 15= du=rd+wrt-bd;010  bit 51= fanstr-dp;110
// bit 16= ptra00-bd;000     bit 52= fstr-op-av-dg;010
// bit 17= ptra01-bd;000     bit 53= fend-seg-dg;010
// bit 18= fa/i1-bd;110      bit 54= flen<128-dg;010
// bit 19= fa/i2-bd;110      bit 55= fgch-dp;110
// bit 20= fa/i3-bd;110      bit 56= fanpk-dp;110
// bit 21= wrd-bd;000        bit 57= fexmop-dl;110
// bit 22= nine-bd;000       bit 58= fblnk-dp;100
// bit 23= six-bd;000        bit 59= unused
// bit 24= four-bd;000       bit 60= dgbd-dc;100
// bit 25= bit-bd;000        bit 61= dgdb-dc;100
// bit 26= unused            bit 62= dgsp-dc;100
// bit 27= unused            bit 63= ffltg-dc;110
// bit 28= unused            bit 64= frnd-dg;120
// bit 29= unused            bit 65= dadd-gate-dc;100
// bit 30= fsampl-bd;111     bit 66= dmp+dv-gate-db;100
// bit 31= dfrst-ct-bd;010   bit 67= dxpn-gate-dg;100
// bit 32= adj-lenint-cx;000 bit 68= unused
// bit 33= fintrptd-cx;010   bit 69= unused
// bit 34= finhib-stc1-cx;010bit 70= unused
// bit 35= unused            bit 71= unused

void addL68DuHistory (cpuState_t * cpup) {
  CPT (cpt1L, 25); // add du hist
  PNL (add_history (cpup, L68_DU_HIST_REG, cpu.du.cycle1, cpu.du.cycle2);)
}

void addL68OuHistory (cpuState_t * cpup) {
  CPT (cpt1L, 26); // add ou hist
  word36 w0 = 0, w1 = 0;

  // 0-16 RP
  //   0-8 OP CODE
  PNL (putbits36_9 (& w0,  0,       cpu.ou.RS);)

  //   9 CHAR
  putbits36_1 (& w0,       9,       cpu.ou.characterOperandSize ? 1 : 0);

  //   10-12 TAG 1/2/3
  putbits36_3 (& w0,       10,      cpu.ou.characterOperandOffset);

  //   13 CRFLAG
  putbits36_1 (& w0,       13,      cpu.ou.crflag);

  //   14 DRFLAG
  putbits36_1 (& w0,       14,      cpu.ou.directOperandFlag ? 1 : 0);

  //   15-16 EAC
  putbits36_2 (& w0,       15,      cpu.ou.eac);

  // 17 0
  // 18-26 RS REG
  PNL (putbits36_9 (& w0,  18,      cpu.ou.RS);)

  // 27 RB1 FULL
  putbits36_1 (& w0,       27,      cpu.ou.RB1_FULL);

  // 28 RP FULL
  putbits36_1 (& w0,       28,      cpu.ou.RP_FULL);

  // 29 RS FULL
  putbits36_1 (& w0,       29,      cpu.ou.RS_FULL);

  // 30-35 GIN/GOS/GD1/GD2/GOE/GOA
  putbits36_6 (& w0,       30,      (word6) (cpu.ou.cycle >> 3));

  // 36-38 GOM/GON/GOF
  putbits36_3 (& w1,       36-36,   (word3) cpu.ou.cycle);

  // 39 STR OP
  putbits36_1 (& w1,       39-36,   cpu.ou.STR_OP);

  // 40 -DA-AV XXX

  // 41-50 stuvwyyzAB -A-REG -Q-REG -X0-REG .. -X7-REG
  PNL (putbits36_10 (& w1, 41-36, (word10) ~opcodes10 [cpu.ou.RS].regUse);)

  // 51-53 0

  // 54-71 ICT TRACKER
  putbits36_18 (& w1,      54 - 36, cpu.PPR.IC);

  add_history (cpup, L68_OU_HIST_REG, w0, w1);
}

// According to ISOLTS
//  0:2 OPCODE RP
//  3 9 BIT CHAR
//  4:6 TAG 3/4/5
//  7 CR FLAG
//  8 DIR FLAG
//  9 RP15
// 10 RP16
// 11 SPARE
// 12:14 OPCODE RS
// 15 RB1 FULL
// 16 RP FULL
// 17 RS FULL
// 18 GIN
// 19 GOS
// 20 GD1
// 21 GD2
// 22 GOE
// 23 GOA
// 24 GOM
// 25 GON
// 26 GOF
// 27 STORE OP
// 28 DA NOT
// 29:38 COMPLEMENTED REGISTER IN USE FLAG A/Q/0/1/2/3/4/5/6/7
// 39 ?
// 40 ?
// 41 ?
// 42:47 ICT TRACT

// XXX add_APU_history

//  0:5 SEGMENT NUMBER
//  6 SNR/ESN
//  7 TSR/ESN
//  8 FSDPTW
//  9 FPTW2
// 10 MPTW
// 11 FANP
// 12 FAP
// 13 AMSDW
// 14:15 AMSDW #
// 16 AMPTW
// 17:18 AMPW #
// 19 ACV/DF
// 20:27 ABSOLUTE MEMORY ADDRESS
// 28 TRR #
// 29 FLT HLD

void addL68ApuHistory (cpuState_t * cpup, enum APUH_e op) {
  CPT (cpt1L, 28); // add apu hist
  word36 w0 = 0, w1 = 0;

  w0 = op; // set 17-24 FDSPTW/.../FAP bits

  // 0-14 ESN
  putbits36_15 (& w0,      0,  cpu.TPR.TSR);
  // 15-16 BSY
  PNL (putbits36_1 (& w0,  15, (cpu.apu.state & apu_ESN_SNR) ? 1 : 0);)
  PNL (putbits36_1 (& w0,  16, (cpu.apu.state & apu_ESN_TSR) ? 1 : 0);)
  // 25 SDWAMM
  putbits36_1 (& w0,       25, cpu.cu.SDWAMM);
  // 26-29 SDWAMR
  putbits36_4 (& w0,       26, (word4) cpu.SDWAMR);
  // 30 PTWAMM
  putbits36_1 (& w0,       30, cpu.cu.PTWAMM);
  // 31-34 PTWAMR
  putbits36_4 (& w0,       31, (word4) cpu.PTWAMR);
  // 35 FLT
  PNL (putbits36_1 (& w0,  35, (cpu.apu.state & apu_FLT) ? 1 : 0);)

  // 36-59 ADD
  PNL (putbits36_24 (& w1, 0,  cpu.APUMemAddr);)
  // 60-62 TRR
  putbits36_3 (& w1,       24, cpu.TPR.TRR);
  // 66 XXX Multiple match error in SDWAM
  // 70 Segment is encachable
  putbits36_1 (& w1,       34, cpu.SDW0.C);
  // 71 XXX Multiple match error in PTWAM

  add_history (cpup, L68_APU_HIST_REG, w0, w1);
}

void setupPROM (uint cpuNo, unsigned char * PROM) {

// 58009997-040 MULTICS Differences Manual DPS 8-70M Aug83
//
// THESE OFFSETS ARE IN OCTAL
//
//  0-13 CPU Model Number
// 13-25 CPU Serial Number
// 26-33 Date-Ship code (YYMMDD)
// 34-40 CPU ID Field (reference RSW 2)
//  Byte 40: Bits 03 (Bits 32-35 of RSW 2 Field
//           Bit 4=1 Hex Option included
//           Bit 5=1 RSCR (Clock) is Slave Mode included
//           Bits 6-7 Reserved for later use.
//       50: Operating System Use
// 51-1777(8) To be defined.
// NOTE: There is the possibility of disagreement between the
//       ID bits of RSW 2 and the ID bits of PROM locations
//       35-40. This condition could result when alterable
//       configuration condition is contained in the PROM.
//       The user is advised to ignore the PROM fields which
//       contain the processor fault vector base (GCOS III)
//       and the processor number and rely on the RSW 2 bits
//       for this purpose. Bits 14-16 of the RSW 2 should be
//       ignored and the bits representing this information in
//       the PROM should be treated as valid.

// "0-13" disagrees with Multics source (start_pl1); it interprets
// it as "0-12"; most likely a typo in 58009997-040.

// CAC notes: I interpret the fields as
//  0-12 CPU Model Number                                          //  0-10  11 chars
// 13-25 CPU Serial Number // 13 chars                             // 11-21  11 chars
// 26-33 Date-Ship code (YYMMDD) // 8 chars (enough for YYYYMMDD). // 22-27   6 chars
// 34-40 CPU ID Field (reference RSW 2)                            // 28-32   5 chars
//  Byte 40: Bits 03 (Bits 32-35 of RSW 2 Field                    //    32
//           Bit 4=1 Hex Option included
//           Bit 5=1 RSCR (Clock) is Slave Mode included
//           Bits 6-7 Reserved for later use.
//       50: Operating System Use                                  //    40

  word36 rsw2 = 0;

  // The PROM copy of RSW 2 contains a canonical RSW 2 rather than the actual RSW 2.
  //   The port interlace is set to 0
  //   The fault base is set to 2 (Multics)
  //   Processor mode is set to 0 (Multics)

  //  0 -   3   4   Port interlace = 0000
  putbits36_4 (& rsw2,  0,   0);
  //  4 -   5   2   CPU type  01 = DPS8
  putbits36_2 (& rsw2,  4,  001);
  //  6 - 12    7   Fault Base  = 2
  putbits36_7 (& rsw2,  6,   2);
  // 13 - 13    1   PROM Present = 1
  putbits36_1 (& rsw2,  13,  1);
  // 14 - 18    5   Pad 00000
  putbits36_5 (& rsw2,  14,  0);
  // 19 - 19    1   CPU  1 = DPS8
  putbits36_1 (& rsw2,  19,  1);
  // 20 - 20    1   8K Cache  1 = Present
  putbits36_1 (& rsw2,  20,  cpus[cpuNo].options.cache_installed ? 1 : 0);
  // 21 - 22    2   Pad
  putbits36_2 (& rsw2,  21,  0);
  // 23 - 23    1   Always 1 for Multics CPU
  putbits36_1 (& rsw2,  23,  1);
  // 24 - 24    1   Proc Mode Bit
  putbits36_1 (& rsw2,  24,  0);
  // 25 - 28    4   Pad
  putbits36_4 (& rsw2,  25,  0);
  // 29 - 32    4   CPU speed options
  putbits36_4 (& rsw2,  29,  cpus[cpuNo].options.proc_speed & 017LL);
  // 33 - 35    3   CPU number
  putbits36_3 (& rsw2,  33,  cpus[cpuNo].switches.cpu_num & 07LL);

  word4 rsw2Ext = 0;
  if (cpus[cpuNo].options.hex_mode_installed)
    rsw2Ext |= 010;  // bit 4
  if (cpus[cpuNo].options.clock_slave_installed)
    rsw2Ext |= 004;  // bit 5
  // bits 6,7 reserved for future use

  char serial[12];
  (void) sprintf (serial, "%-11u", cpus[cpuNo].switches.serno);

#if defined(VER_H_PROM_SHIP)
  char * ship = VER_H_PROM_SHIP;
#else
  char * ship = "200101";
#endif /* VER_H_PROM_SHIP */

#if !defined(VER_H_PROM_MAJOR_VER)
# define VER_H_PROM_MAJOR_VER "999"
#endif /* VER_H_PROM_MAJOR_VER */

#if !defined(VER_H_PROM_MINOR_VER)
# define VER_H_PROM_MINOR_VER "999"
#endif /* VER_H_PROM_MINOR_VER */

#if !defined(VER_H_PROM_PATCH_VER)
# define VER_H_PROM_PATCH_VER "999"
#endif /* VER_H_PROM_PATCH_VER */

#if !defined(VER_H_PROM_OTHER_VER)
# define VER_H_PROM_OTHER_VER "999"
#endif /* VER_H_PROM_OTHER_VER */

#if !defined(VER_H_GIT_RELT)
# define VER_H_GIT_RELT "X"
#endif /* VER_H_GIT_RELT */

#if !defined(VER_H_PROM_VER_TEXT)
# define VER_H_PROM_VER_TEXT "Unknown                      "
#endif /* VER_H_PROM_VER_TEXT */

#if defined(BUILD_PROM_OSA_TEXT)
# define BURN_PROM_OSA_TEXT BUILD_PROM_OSA_TEXT
#else
# if !defined(VER_H_PROM_OSA_TEXT)
#  define BURN_PROM_OSA_TEXT "Unknown Build Op Sys"
# else
#  define BURN_PROM_OSA_TEXT VER_H_PROM_OSA_TEXT
# endif /* VER_H_PROM_OSA_TEXT */
#endif /* BUILD_PROM_OSA_TEXT */

#if defined(BUILD_PROM_OSV_TEXT)
# define BURN_PROM_OSV_TEXT BUILD_PROM_OSV_TEXT
#else
# if !defined(VER_H_PROM_OSV_TEXT)
#  define BURN_PROM_OSV_TEXT "Unknown Build Arch. "
# else
#  define BURN_PROM_OSV_TEXT VER_H_PROM_OSV_TEXT
# endif /* VER_H_PROM_OSV_TEXT */
#endif /* BUILD_PROM_OSV_TEXT */

#if defined(BUILD_PROM_TSA_TEXT)
# define BURN_PROM_TSA_TEXT BUILD_PROM_TSA_TEXT
#else
# if defined(_M_X64) || defined(_M_AMD64) || defined(__amd64__) || defined(__x86_64__) || defined(__AMD64)
#  define VER_H_PROM_TSA_TEXT "Intel x86_64 (AMD64)"
# elif defined(_M_IX86) || defined(__i386) || defined(__i486) || defined(__i586) || defined(__i686) || defined(__ix86)
#  define VER_H_PROM_TSA_TEXT "Intel ix86 (32-bit) "
# elif defined(_M_ARM64) || defined(__aarch64__) || defined(__arm64__)
#  define VER_H_PROM_TSA_TEXT "AArch64/ARM64/64-bit"
# elif defined(_M_ARM) || defined(__arm__)
#  define VER_H_PROM_TSA_TEXT "AArch32/ARM32/32-bit"
# elif defined(__ia64__) || defined(_M_IA64) || defined(__itanium__)
#  define VER_H_PROM_TSA_TEXT "Intel Itanium (IA64)"
# elif defined(__ppc64__) || defined(__PPC64__) || defined(__ppc64le__) || defined(__PPC64LE__) || defined(__powerpc64__) || \
  defined(__POWERPC64__) || \
  defined(_M_PPC64) || \
  defined(__PPC64) || \
  defined(_ARCH_PPC64)
#  define VER_H_PROM_TSA_TEXT "Power ISA (64-bit)  "
# elif defined(__ppc__) || defined(__PPC__) || defined(__powerpc__) || defined(__POWERPC__) || defined(_M_PPC) || \
  defined(__PPC) || \
  defined(__ppc32__) || \
  defined(__PPC32__) || \
  defined(__powerpc32__) || \
  defined(__POWERPC32__) || \
  defined(_M_PPC32) || \
  defined(__PPC32)
#  define VER_H_PROM_TSA_TEXT "PowerPC ISA (32-bit)"
# elif defined(__s390x__)
#  define VER_H_PROM_TSA_TEXT "IBM z/Architecture  "
# elif defined(__s390__)
#  define VER_H_PROM_TSA_TEXT "IBM ESA System/390  "
# elif defined(__J2__) || defined(__J2P__) || defined(__j2__) || defined(__j2p__)
#  define VER_H_PROM_TSA_TEXT "J-Core J2 Open CPU  "
# elif defined(__SH4__) || defined(__sh4__) || defined(__SH4) || defined(__sh4)
#  define VER_H_PROM_TSA_TEXT "Hitachi/Renesas SH-4"
# elif defined(__SH2__) || defined(__sh2__) || defined(__SH2) || defined(__sh2)
#  define VER_H_PROM_TSA_TEXT "Hitachi/Renesas SH-2"
# elif defined(__alpha__)
#  define VER_H_PROM_TSA_TEXT "Alpha AXP           "
# elif defined(__hppa__) || defined(__HPPA__) || defined(__PARISC__) || defined(__parisc__)
#  define VER_H_PROM_TSA_TEXT "HP PA-RISC          "
# elif defined(__ICE9__) || defined(__ice9__) || defined(__ICE9) || defined(__ice9)
#  define VER_H_PROM_TSA_TEXT "SiCortex ICE-9      "
# elif defined(mips64) || defined(__mips64__) || defined(MIPS64) || defined(_MIPS64_) || defined(__mips64)
#  define VER_H_PROM_TSA_TEXT "MIPS64              "
# elif defined(mips) || defined(__mips__) || defined(MIPS) || defined(_MIPS_) || defined(__mips)
#  define VER_H_PROM_TSA_TEXT "MIPS                "
# elif defined(__OpenRISC__) || defined(__OPENRISC__) || defined(__openrisc__) || defined(__OR1K__) || defined(__OPENRISC1K__)
#  define VER_H_PROM_TSA_TEXT "OpenRISC            "
# elif defined(__sparc64) || defined(__SPARC64) || defined(__SPARC64__) || defined(__sparc64__)
#  define VER_H_PROM_TSA_TEXT "SPARC64             "
# elif defined(__sparc) || defined(__SPARC) || defined(__SPARC__) || defined(__sparc__)
#  define VER_H_PROM_TSA_TEXT "SPARC               "
# elif defined(__riscv) || defined(__riscv__)
#  define VER_H_PROM_TSA_TEXT "RISC-V              "
# elif defined(__myriad2__)
#  define VER_H_PROM_TSA_TEXT "Myriad2             "
# elif defined(__loongarch64) || defined(__loongarch__)
#  define VER_H_PROM_TSA_TEXT "LoongArch           "
# elif defined(_m68851) || defined(__m68k__) || defined(__m68000__) || defined(__M68K)
#  define VER_H_PROM_TSA_TEXT "Motorola m68k       "
# elif defined(__m88k__) || defined(__m88000__) || defined(__M88K)
#  define VER_H_PROM_TSA_TEXT "Motorola m88k       "
# elif defined(__VAX__) || defined(__vax__)
#  define VER_H_PROM_TSA_TEXT "VAX                 "
# elif defined(__NIOS2__) || defined(__nios2__)
#  define VER_H_PROM_TSA_TEXT "Altera Nios II      "
# elif defined(__MICROBLAZE__) || defined(__microblaze__)
#  define VER_H_PROM_TSA_TEXT "Xilinx MicroBlaze   "
# endif
# if !defined(VER_H_PROM_TSA_TEXT)
#  define BURN_PROM_TSA_TEXT "Unknown Target Arch."
# else
#  define BURN_PROM_TSA_TEXT VER_H_PROM_TSA_TEXT
# endif /* VER_H_PROM_TSA_TEXT */
#endif /* BUILD_PROM_TSA_TEXT */

#if (defined(__WIN__) || defined(_WIN32) || defined(IS_WINDOWS) || defined(_MSC_VER) || defined(__MINGW32__) || \
        defined(__MINGW64__) || defined(CROSS_MINGW32) || defined(CROSS_MINGW64)) && !defined(__CYGWIN__)
# define DC_IS_WINDOWS 1
#else
# define DC_IS_WINDOWS 0
#endif

#if defined(BUILD_PROM_TSV_TEXT)
# define BURN_PROM_TSV_TEXT BUILD_PROM_TSV_TEXT
#else
# if DC_IS_WINDOWS
#  define VER_H_PROM_TSV_TEXT "Microsoft Windows   "
# elif defined(__CYGWIN__)
#  define VER_H_PROM_TSV_TEXT "Windows/Cygwin      "
# elif (defined(__sunos) || defined(__sun) || defined(__sun__)) && (defined(SYSV) || defined(__SVR4) || defined(__SVR4__) || \
        defined(__svr4__))
#  if defined(__illumos__)
#   define VER_H_PROM_TSV_TEXT "illumos             "
#  else
#   define VER_H_PROM_TSV_TEXT "Solaris             "
#  endif
# elif defined(__APPLE__) && defined(__MACH__)
#  define VER_H_PROM_TSV_TEXT "Apple macOS         "
# elif defined(__GNU__) && !defined(__linux__)
#  define VER_H_PROM_TSV_TEXT "GNU/Hurd            "
# elif defined(__ANDROID__) && defined(__ANDROID_API__)
#  if defined(__linux__)
#   define VER_H_PROM_TSV_TEXT "Android/Linux       "
#  else
#   define VER_H_PROM_TSV_TEXT "Android             "
#  endif
# elif defined(__lynxOS__) || defined(__LYNXOS__) || defined(LynxOS) || defined(LYNXOS)
#  define VER_H_PROM_TSV_TEXT "LynxOS              "
# elif defined(__HELENOS__)
#  define VER_H_PROM_TSV_TEXT "HelenOS             "
# elif defined(__linux__)
#  if defined(__BIONIC__)
#   define VER_H_PROM_TSV_TEXT "Linux/Bionic-libc   "
#  elif defined(__UCLIBC__) || defined(UCLIBC)
#   define VER_H_PROM_TSV_TEXT "Linux/uClibc        "
#  elif defined(__NEWLIB__)
#   define VER_H_PROM_TSV_TEXT "Linux/Newlib        "
#  elif defined(__dietlibc__)
#   define VER_H_PROM_TSV_TEXT "Linux/Diet-libc     "
#  elif defined(__GLIBC__)
#   define VER_H_PROM_TSV_TEXT "GNU/Linux           "
#  else
#   define VER_H_PROM_TSV_TEXT "Linux               "
#  endif
# elif defined(__HAIKU__)
#  define VER_H_PROM_TSV_TEXT "Haiku               "
# elif defined(__serenity__)
#  define VER_H_PROM_TSV_TEXT "SerenityOS          "
# elif defined(__FreeBSD__)
#  define VER_H_PROM_TSV_TEXT "FreeBSD             "
# elif defined(__NetBSD__)
#  define VER_H_PROM_TSV_TEXT "NetBSD              "
# elif defined(__OpenBSD__)
#  define VER_H_PROM_TSV_TEXT "OpenBSD             "
# elif defined(__DragonFly__)
#  define VER_H_PROM_TSV_TEXT "DragonFly BSD       "
# elif defined(_AIX)
#  if !defined(__PASE__)
#   define VER_H_PROM_TSV_TEXT "IBM AIX             "
#  else
#   define VER_H_PROM_TSV_TEXT "IBM OS/400 (PASE)   "
#  endif
# elif defined(__VXWORKS__) || defined(__VXWORKS) || defined(__vxworks) || defined(__vxworks__) || defined(_VxWorks)
#  if !defined(__RTP__)
#   define VER_H_PROM_TSV_TEXT "VxWorks             "
#  else
#   define VER_H_PROM_TSV_TEXT "VxWorks RTP         "
#  endif
# elif defined(__rtems__)
#  if defined(__FreeBSD_version)
#   define VER_H_PROM_TSV_TEXT "RTEMS/LibBSD        "
#  else
#   define VER_H_PROM_TSV_TEXT "RTEMS               "
#  endif
# elif defined(__ZEPHYR__)
#  define VER_H_PROM_TSV_TEXT "Zephyr              "
# elif defined(ti_sysbios_BIOS___VERS) || defined(ti_sysbios_BIOS__top__)
#  define VER_H_PROM_TSV_TEXT "TI-RTOS (SYS/BIOS)  "
# elif defined(__OSV__) // -V1040
#  define VER_H_PROM_TSV_TEXT "OSv                 "
# elif defined(MINIX) || defined(MINIX3) || defined(MINIX315) || defined(__minix__) || defined(__minix3__) || defined(__minix315__)
#  define VER_H_PROM_TSV_TEXT "Minix               "
# elif defined(__QNX__)
#  if defined(__QNXNTO__)
#   define VER_H_PROM_TSV_TEXT "QNX Neutrino        "
#  else
#   define VER_H_PROM_TSV_TEXT "QNX                 "
#  endif
# endif
# if !defined(VER_H_PROM_TSV_TEXT)
#  define BURN_PROM_TSV_TEXT "Unknown Target OpSys"
# else
#  define BURN_PROM_TSV_TEXT VER_H_PROM_TSV_TEXT
# endif /* VER_H_PROM_TSV_TEXT */
#endif /* BUILD_PROM_TSV_TEXT */

#if !defined(VER_H_GIT_DATE_SHORT)
# define VER_H_GIT_DATE_SHORT "2021-01-01"
#endif /* if !defined(VER_H_GIT_DATE_SHORT) */

#if !defined(BURN_PROM_BUILD_NUM)
# define BURN_PROM_BUILD_NUM "        "
#endif /* if !defined(BURN_PROM_BUILD_NUM) */

#define BURN(offset, length, string) memcpy ((char *) PROM + (offset), string, length)
#define BURN1(offset, byte) PROM[offset] = (char) (byte)

  (void) memset (PROM, 255, 1024);

  //   Offset Length  Data
  //              12345678901
  BURN  ( 00,  11,  "DPS 8/SIM M");                //    0-10  CPU model ("XXXXXXXXXXX")
  BURN  (013,  11,  serial);                       //   11-21  CPU serial ("DDDDDDDDDDD")
  BURN  (026,   6,  ship);                         //   22-27  CPU ship date ("YYMMDD")
  BURN1 (034,       getbits36_8 (rsw2,  0));       //   34     RSW 2 bits  0- 7
  BURN1 (035,       getbits36_8 (rsw2,  8));       //   35     RSW 2 bits  8-15
  BURN1 (036,       getbits36_8 (rsw2, 16));       //   36     RSW 2 bits 16-23
  BURN1 (037,       getbits36_8 (rsw2, 24));       //   37     RSW 2 bits 24-31
  BURN1 (040,     ((getbits36_4 (rsw2, 32) << 4) \
                               | rsw2Ext));        //   40     RSW 2 bits 32-35, options bits

  /* Begin extended PROM data */
  BURN  ( 60,   1,  "2");                          //   60     PROM Layout Version Number
  BURN  ( 70,  10,  VER_H_GIT_DATE_SHORT);         //   70     Release Git Commit Date
  BURN  ( 80,   3,  VER_H_PROM_MAJOR_VER);         //   80     Major Release Number
  BURN  ( 83,   3,  VER_H_PROM_MINOR_VER);         //   83     Minor Release Number
  BURN  ( 86,   3,  VER_H_PROM_PATCH_VER);         //   86     Patch Release Number
  BURN  ( 89,   3,  VER_H_PROM_OTHER_VER);         //   89     Iteration Release Number
  BURN  ( 92,   8,  BURN_PROM_BUILD_NUM);          //   92     Reserved for Build Number
  BURN  (100,   1,  VER_H_GIT_RELT);               //  100     Release Type
  BURN  (101,  29,  VER_H_PROM_VER_TEXT);          //  101     Release Text
  BURN  (130,  20,  BURN_PROM_OSA_TEXT);           //  130     Build System Architecture
  BURN  (150,  20,  BURN_PROM_OSV_TEXT);           //  150     Build System Operating System
  BURN  (170,  20,  BURN_PROM_TSA_TEXT);           //  170     Target System Architecture
  BURN  (190,  20,  BURN_PROM_TSV_TEXT);           //  190     Target System Architecture
}

void cpuStats (uint cpuNo) {
  if (! cpus[cpuNo].cycleCnt)
    return;

  (void) fflush (stderr);
  (void) fflush (stdout);
  sim_msg ("\n");
  (void) fflush (stdout);
  (void) fflush (stderr);
  sim_msg ("+---------------------------------+\n");
  sim_msg ("|         CPU %c Statistics        |\n", 'A' + cpuNo);
  sim_msg ("+---------------------------------+\n");
  (void) fflush (stdout);
  (void) fflush (stderr);
#if defined(WIN_STDIO)
  sim_msg ("|  cycles        %15"PRIu64"  |\n", cpus[cpuNo].cycleCnt);
  sim_msg ("|  instructions  %15"PRIu64"  |\n", cpus[cpuNo].instrCnt);
  (void) fflush (stdout);
  (void) fflush (stderr);
  sim_msg ("+---------------------------------+\n");
  sim_msg ("|  lockCnt       %15"PRIu64"  |\n",  cpus[cpuNo].coreLockState.lockCnt);
  sim_msg ("|  lockImmediate %15"PRIu64"  |\n",  cpus[cpuNo].coreLockState.lockImmediate);
  (void) fflush (stdout);
  (void) fflush (stderr);
  sim_msg ("+---------------------------------+\n");
  sim_msg ("|  lockWait      %15"PRIu64"  |\n",  cpus[cpuNo].coreLockState.lockWait);
  sim_msg ("|  lockWaitMax   %15"PRIu64"  |\n",  cpus[cpuNo].coreLockState.lockWaitMax);
  (void) fflush (stdout);
  (void) fflush (stderr);
# if !defined(SCHED_NEVER_YIELD)
  sim_msg ("|  lockYield     %15"PRIu64"  |\n",  cpus[cpuNo].coreLockState.lockYield);
  (void) fflush (stdout);
  (void) fflush (stderr);
# else
  sim_msg ("|  lockYield                ----  |\n");
  (void) fflush (stdout);
  (void) fflush (stderr);
# endif /* if !defined(SCHED_NEVER_YIELD) */
  sim_msg ("+---------------------------------+");
  (void) fflush (stdout);
  (void) fflush (stderr);
# if !defined(UCACHE)
#  if !defined(UCACHE_STATS)
  sim_msg ("\n");
#  endif
# endif
  (void) fflush (stdout);
  (void) fflush (stderr);
#else
  sim_msg ("|  cycles        %'15"PRIu64"  |\n", cpus[cpuNo].cycleCnt);
  sim_msg ("|  instructions  %'15"PRIu64"  |\n", cpus[cpuNo].instrCnt);
  (void) fflush (stdout);
  (void) fflush (stderr);
  sim_msg ("+---------------------------------+\n");
  sim_msg ("|  lockCnt       %'15"PRIu64"  |\n", cpus[cpuNo].coreLockState.lockCnt);
  sim_msg ("|  lockImmediate %'15"PRIu64"  |\n", cpus[cpuNo].coreLockState.lockImmediate);
  (void) fflush (stdout);
  (void) fflush (stderr);
  sim_msg ("+---------------------------------+\n");
  sim_msg ("|  lockWait      %'15"PRIu64"  |\n", cpus[cpuNo].coreLockState.lockWait);
  sim_msg ("|  lockWaitMax   %'15"PRIu64"  |\n", cpus[cpuNo].coreLockState.lockWaitMax);
  (void) fflush (stdout);
  (void) fflush (stderr);
# if !defined(SCHED_NEVER_YIELD)
  sim_msg ("|  lockYield     %'15"PRIu64"  |\n", cpus[cpuNo].coreLockState.lockYield);
  (void) fflush (stdout);
  (void) fflush (stderr);
# else
  sim_msg ("|  lockYield                ----  |\n");
  (void) fflush (stdout);
  (void) fflush (stderr);
# endif /* if !defined(SCHED_NEVER_YIELD) */
  sim_msg ("+---------------------------------+");
  (void) fflush (stdout);
  (void) fflush (stderr);
# if !defined(UCACHE)
#  if !defined(UCACHE_STATS)
  sim_msg ("\n");
#  endif
# endif
  (void) fflush (stderr);
  (void) fflush (stdout);
#endif

#if defined(UCACHE_STATS)
  ucacheStats (cpuNo);
#endif
}

#include <locale.h>
#include "segldr.h"

void perfTest (char * testName) {
  if (testName == NULL)
    testName = "strip.mem";

  (void) setlocale(LC_NUMERIC, "");

  // dps8m_init_strip
  systemState = malloc (sizeof (struct system_state_s));
  MALLOC_CHECK (systemState);
  M = systemState->M;
  cpus = systemState->cpus;
  (void) memset (cpus, 0, sizeof (cpuState_t) * N_CPU_UNITS_MAX);
  for (int i = 0; i < N_CPU_UNITS_MAX; i ++) {
    cpus[i].switches.FLT_BASE = 2; // Some of the UnitTests assume this
    cpus[i].instrCnt = 0;
    cpus[i].cycleCnt = 0;
    for (int j = 0; j < N_FAULTS; j ++)
      cpus[i].faultCnt [j] = 0;
  }
  initThreadz ();

  cpus[0].tweaks.enable_emcall = 1;
  opc_dev.numunits = 1;
  cpuResetUnitIdx (0, false);
  setCpuCycle (& cpus[0], FETCH_cycle);
  mrestore (testName);
  threadz_sim_instr (& cpus[0]);
}

static bool cpuCmdCheckIdx (int cpuIdx) {
  if (cpuIdx < 0 || cpuIdx >= N_CPUS) {
    simPrintf ("Error: Invalid CPU tag; expected A-H\n");
    return true;
  }
  return false;
}


static bool cpuCmdCheckPort (int cpuIdx, word36 portNumber) {
  if (cpus[cpuIdx].tweaks.l68_mode) {
    // L68
    if (portNumber >= N_L68_CPU_PORTS) {
      simPrintf ("Error: Invalid CPU port number; must be 0-7\n");
      return true;
    }
  } else {
    // DPS8M
    if (portNumber >= N_DPS8M_CPU_PORTS) {
      simPrintf ("Error: Invalid CPU port number; must be 0-3\n");
      return true;
    }
  }
  return false;
}


int cpuCmdSetL68 (word36 cpuIdx, uint l68) {
  if (cpuCmdCheckIdx (cpuIdx)) {
    return -1;
  }
  if (l68 > 1) {
    simPrintf ("cpuCmdSetL68 internal error; l68 > 1\n");
    return -1;
  }
#ifdef CMD_VERBOSE
  simPrintf ("SET CPU %c %s\n", AZ (cpuIdx), l68 ? "L68" : "DPS8M");
#endif
  cpus[cpuIdx].tweaks.l68_mode = l68 ? 1 : 0;
  return 0;
}


int cpuCmdSetAffinity (word36 cpuIdx, bool enable, word36 affinity) {
  if (cpuCmdCheckIdx (cpuIdx)) {
    return -1;
  }
#ifdef CMD_VERBOSE
  if (enable) {
    simPrintf ("SET CPU %c AFFINITY %d\n", AZ (cpuIdx), (uint) affinity);
  } else {
    simPrintf ("SET CPU %c AFFINITY OFF\n", AZ (cpuIdx));
  }
#endif
  cpus[cpuIdx].tweaks.set_affinity = enable;
  if (enable) {
    cpus[cpuIdx].tweaks.affinity = (uint) affinity;
  }
  return 0; 
}


int cpuCmdSetFaultbase (word36 cpuIdx, word36 address) {
  if (cpuCmdCheckIdx (cpuIdx)) {
    return -1;
  }
  if (address > 0177) {
    simPrintf ("Error: Invalid fault base; must be 0-177\n");
  }
#ifdef CMD_VERBOSE
  simPrintf ("SET CPU %c FAULTBASE %03"PRIo36"\n", AZ (cpuIdx), address);
#endif
  cpus[cpuIdx].switches.FLT_BASE = (uint) address;
  return 0;
}


int cpuCmdSetMode (word36 cpuIdx, enum procModeSettings mode) {
  if (cpuCmdCheckIdx (cpuIdx)) {
    return -1;
  }
  if (mode != procModeGCOS && mode != procModeMultics) {
    simPrintf ("Error: Invalid CPU mode; must be GCOS or Multics\n");
    return -1;
  }
#ifdef CMD_VERBOSE
  simPrintf ("SET CPU %c MODE %s\n", AZ (cpuIdx), mode == procModeMultics ? "Multics" : "GCOS");
#endif
  cpus[cpuIdx].switches.procMode = mode;
  return 0;
}


int cpuCmdSetNumber (word36 cpuIdx, word36 number) {
  if (cpuCmdCheckIdx (cpuIdx)) {
    return -1;
  }
  if (number > 07) {
    simPrintf ("Error: Invalid CPU number; must be 0-7\n");
  }
#ifdef CMD_VERBOSE
  simPrintf ("SET CPU %c NUMBER %"PRIo36"\n", AZ (cpuIdx), number);
#endif
  cpus[cpuIdx].switches.cpu_num = (uint) number;
  return 0;
}


int cpuCmdSetData (word36 cpuIdx, word36 data) {
  if (cpuCmdCheckIdx (cpuIdx)) {
    return -1;
  }
  if (data > MASK36) {
    simPrintf ("Error: Invalid CPU data; must be 0-777777777777\n");
  }
#ifdef CMD_VERBOSE
  simPrintf ("SET CPU %c DATA %012"PRIo36"\n", AZ (cpuIdx), data);
#endif
  cpus[cpuIdx].switches.data_switches = data;
  return 0;
}


int cpuCmdSetDataOne (word36 cpuIdx, word36 position, int value) {
  if (cpuCmdCheckIdx (cpuIdx)) {
    return -1;
  }
  if (position > 35) {
    simPrintf ("Error: Invalid CPU swtich number; must be 0-35.\n");
  }
  if (value < 0 || value > 1) {
    simPrintf ("Error: Invalid CPU swtich setting; must be 0 or 1.\n");
  }
#ifdef CMD_VERBOSE
  simPrintf ("SET CPU %c DATA %012"PRIo36" %d\n", AZ (cpuIdx), position, value);
#endif
  putbits36 (& cpus[cpuIdx].switches.data_switches, (uint) position, 1, (uint) value);
  return 0;
}


int cpuCmdSetAddress (word36 cpuIdx, word36 address) {
  if (cpuCmdCheckIdx (cpuIdx)) {
    return -1;
  }
  if (address > MASK18) {
    simPrintf ("Error: Invalid CPU data; must be 0-777777\n");
  }
#ifdef CMD_VERBOSE
  simPrintf ("SET CPU %c ADDRESS %012"PRIo36"\n", AZ (cpuIdx), address);
#endif
  cpus[cpuIdx].switches.addr_switches = address;
  return 0;
}


int cpuCmdSetCache (word36 cpuIdx, bool enable) {
  if (cpuCmdCheckIdx (cpuIdx)) {
    return -1;
  }
#ifdef CMD_VERBOSE
  simPrintf ("SET CPU %c CACHE %s\n", AZ (cpuIdx), enable ? "ENABLE" : "DISABLE");
#endif
  cpus[cpuIdx].switches.enable_cache = enable;
  return 0;
}


int cpuCmdSetCacheInstalled (word36 cpuIdx, bool enable) {
  if (cpuCmdCheckIdx (cpuIdx)) {
    return -1;
  }
#ifdef CMD_VERBOSE
  simPrintf ("SET CPU %c CACHE %s\n", AZ (cpuIdx), enable ? "INSTALLED" : "NOT INSTALLED");
#endif
  cpus[cpuIdx].options.cache_installed = enable;
  return 0;
}


int cpuCmdSetClockslaveInstalled (word36 cpuIdx, bool enable) {
  if (cpuCmdCheckIdx (cpuIdx)) {
    return -1;
  }
#ifdef CMD_VERBOSE
  simPrintf ("SET CPU %c CLOCKSLAVE %s\n", AZ (cpuIdx), enable ? "INSTALLED" : "NOT INSTALLED");
#endif
  cpus[cpuIdx].options.clock_slave_installed = enable;
  return 0;
}


int cpuCmdSetEmcall (word36 cpuIdx, bool enable) {
  if (cpuCmdCheckIdx (cpuIdx)) {
    return -1;
  }
#ifdef CMD_VERBOSE
  simPrintf ("SET CPU %c EMCALL %s\n", AZ (cpuIdx), enable ? "ENABLE" : "DISABLE");
#endif
  cpus[cpuIdx].tweaks.enable_emcall = enable;
  return 0;
}


int cpuCmdSetHexMode (word36 cpuIdx, bool enable) {
  if (cpuCmdCheckIdx (cpuIdx)) {
    return -1;
  }
#ifdef CMD_VERBOSE
  simPrintf ("SET CPU %c HEXMODE %s\n", AZ (cpuIdx), enable ? "INSTALLED" : "NOT INSTALLED");
#endif
  cpus[cpuIdx].options.hex_mode_installed = enable;
  return 0;
}


int cpuCmdSetIsoltsMode (word36 cpuIdx, bool enable) {
  if (cpuCmdCheckIdx (cpuIdx)) {
    return -1;
  }
#ifdef CMD_VERBOSE
  simPrintf ("SET CPU %c ISOLTS_MODE %s\n", AZ (cpuIdx), enable ? "ENABLED" : "NOT ENABLED");
#endif
  bool was = cpus[cpuIdx].tweaks.isolts_mode;
  cpus[cpuIdx].tweaks.isolts_mode = enable;
  if (enable && ! was) {
    uint store_sz;
    if (cpus[cpuIdx].tweaks.l68_mode) // L68
      store_sz = 3;
    else // DPS8M
      store_sz = 2;
    cpus[cpuIdx].isolts_switches_save     = cpus[cpuIdx].switches;

    cpus[cpuIdx].switches.data_switches   = W36_C (00000030714000);
    cpus[cpuIdx].switches.addr_switches   = 0100150;
    cpus[cpuIdx].tweaks.useMap            = true;
    cpus[cpuIdx].tweaks.enable_wam        = true;
    cpus[cpuIdx].switches.assignment  [0] = 0;
    cpus[cpuIdx].switches.interlace   [0] = false;
    cpus[cpuIdx].switches.enable      [0] = false;
    cpus[cpuIdx].switches.init_enable [0] = false;
    cpus[cpuIdx].switches.store_size  [0] = store_sz;

    cpus[cpuIdx].switches.assignment  [1] = 0;
    cpus[cpuIdx].switches.interlace   [1] = false;
    cpus[cpuIdx].switches.enable      [1] = true;
    cpus[cpuIdx].switches.init_enable [1] = false;
    cpus[cpuIdx].switches.store_size  [1] = store_sz;

    cpus[cpuIdx].switches.assignment  [2] = 0;
    cpus[cpuIdx].switches.interlace   [2] = false;
    cpus[cpuIdx].switches.enable      [2] = false;
    cpus[cpuIdx].switches.init_enable [2] = false;
    cpus[cpuIdx].switches.store_size  [2] = store_sz;

    cpus[cpuIdx].switches.assignment  [3] = 0;
    cpus[cpuIdx].switches.interlace   [3] = false;
    cpus[cpuIdx].switches.enable      [3] = false;
    cpus[cpuIdx].switches.init_enable [3] = false;
    cpus[cpuIdx].switches.store_size  [3] = store_sz;

    if (cpus[cpuIdx].tweaks.l68_mode) { // L68
      cpus[cpuIdx].switches.assignment  [4] = 0;
      cpus[cpuIdx].switches.interlace   [4] = false;
      cpus[cpuIdx].switches.enable      [4] = false;
      cpus[cpuIdx].switches.init_enable [4] = false;
      cpus[cpuIdx].switches.store_size  [4] = 3;

      cpus[cpuIdx].switches.assignment  [5] = 0;
      cpus[cpuIdx].switches.interlace   [5] = false;
      cpus[cpuIdx].switches.enable      [5] = false;
      cpus[cpuIdx].switches.init_enable [5] = false;
      cpus[cpuIdx].switches.store_size  [5] = 3;

      cpus[cpuIdx].switches.assignment  [6] = 0;
      cpus[cpuIdx].switches.interlace   [6] = false;
      cpus[cpuIdx].switches.enable      [6] = false;
      cpus[cpuIdx].switches.init_enable [6] = false;
      cpus[cpuIdx].switches.store_size  [6] = 3;

      cpus[cpuIdx].switches.assignment  [7] = 0;
      cpus[cpuIdx].switches.interlace   [7] = false;
      cpus[cpuIdx].switches.enable      [7] = false;
      cpus[cpuIdx].switches.init_enable [7] = false;
      cpus[cpuIdx].switches.store_size  [7] = 3;
    }
    cpus[cpuIdx].switches.enable      [1] = true;

    if (cpus[cpuIdx].executing) {
      cpus[cpuIdx].forceRestart = true;
      wakeCPU (cpuIdx);
    } else {
      cpuResetUnitIdx ((uint) cpuIdx, false);
      //simh_cpu_reset_and_clear_unit (cpuUnits + cpuIdx, 0, NULL, NULL);
    }

  } else if (was && !enable) {
    cpus[cpuIdx].switches = cpus[cpuIdx].isolts_switches_save;

    if (cpus[cpuIdx].executing) {
      cpus[cpuIdx].forceRestart = true;
      wakeCPU (cpuIdx);
    } else {
      cpuResetUnitIdx ((uint) cpuIdx, false);
      //simh_cpu_reset_and_clear_unit (cpuUnits + cpuIdx, 0, NULL, NULL);
    }
  }
  return 0;
}


int cpuCmdSetKips (word36 cpuIdx, word36 kips) {
  if (cpuCmdCheckIdx (cpuIdx)) {
    return -1;
  }
  if (kips > 100000 || kips < 1) {
    simPrintf ("Error: Invalid CPU KIPS; must be 1-100000\n");
    return -1;
  }
#ifdef CMD_VERBOSE
  simPrintf ("SET CPU %c KIPS %012"PRId36".\n", AZ (cpuIdx), kips);
#endif
  cpus[cpuIdx].tweaks.kips = (uint) kips;
  setLUFLimits (cpuIdx, kips); //  Move to cpu reset XXX
  return 0;
}


int cpuCmdSetLuf (word36 cpuIdx, bool enable) {
  if (cpuCmdCheckIdx (cpuIdx)) {
    return -1;
  }
#ifdef CMD_VERBOSE
  simPrintf ("SET LUF %c MAP %s\n", AZ (cpuIdx), enable ? "ENABLE" : "DISABLE");
#endif
  cpus[cpuIdx].tweaks.enableLUF = enable;
  return 0;
}


int cpuCmdSetMap (word36 cpuIdx, bool enable) {
  if (cpuCmdCheckIdx (cpuIdx)) {
    return -1;
  }
#ifdef CMD_VERBOSE
  simPrintf ("SET CPU %c MAP %s\n", AZ (cpuIdx), enable ? "ENABLE" : "DISABLE");
#endif
  cpus[cpuIdx].tweaks.useMap = enable;
  return 0;
}


int cpuCmdSetPortAssignment (word36 cpuIdx, word36 portNumber, word36 assignment) {
  if (cpuCmdCheckIdx (cpuIdx)) {
    return -1;
  }
  if (cpuCmdCheckPort (cpuIdx, portNumber)) {
    return -1;
  }
  if (assignment >= N_SCU_PORTS) {
    simPrintf ("Error: Invalid CPU port assignment; must be 0-7\n");
    return -1;
  }
#ifdef CMD_VERBOSE
  simPrintf ("SET CPU %c PORT %c ASSIGNMENT %"PRIo36"\n", AZ (cpuIdx), AZ (portNumber), assignment);
#endif
  cpus[cpuIdx].switches.assignment [portNumber] = (uint) assignment;
  return 0;
}


int cpuCmdSetPortEnable (word36 cpuIdx, word36 portNumber, bool enable) {
  if (cpuCmdCheckIdx (cpuIdx)) {
    return -1;
  }
  if (cpuCmdCheckPort (cpuIdx, portNumber)) {
    return -1;
  }
#ifdef CMD_VERBOSE
  simPrintf ("SET CPU %c PORT %c %s\n", AZ (cpuIdx), AZ (portNumber), enable ? "ENABLE" : "DISABLE");
#endif
  cpus[cpuIdx].switches.enable [portNumber] = enable;
  return 0;
}


int cpuCmdSetPortInterlaceEnable (word36 cpuIdx, word36 portNumber, int enable) {
  if (cpuCmdCheckIdx (cpuIdx)) {
    return -1;
  }
  if (cpuCmdCheckPort (cpuIdx, portNumber)) {
    return -1;
  }
  if (enable > 1) {
    simPrintf ("Error: Invalid CPU port state ; must be ENABLE or DISABLE\n");
    return -1;
  }
#ifdef CMD_VERBOSE
  simPrintf ("SET CPU %c PORT %c INTERLACE %s\n", AZ (cpuIdx), AZ (portNumber), enable ? "ENABLE" : "DISABLE");
#endif
  cpus[cpuIdx].switches.interlace [portNumber] = enable ? 1 : 0;
  return 0;
}


int cpuCmdSetPortInitEnable (word36 cpuIdx, word36 portNumber, int enable) {
  if (cpuCmdCheckIdx (cpuIdx)) {
    return -1;
  }
  if (cpuCmdCheckPort (cpuIdx, portNumber)) {
    return -1;
  }
  if (enable > 1) {
    simPrintf ("Error: Invalid CPU port initialization state; must be ENABLE or DISABLE\n");
    return -1;
  }
#ifdef CMD_VERBOSE
  simPrintf ("SET CPU %c PORT %c INITIALIZE %s\n", AZ (cpuIdx), AZ (portNumber), enable ? "ENABLE" : "DISABLE");
#endif
  cpus[cpuIdx].switches.init_enable [portNumber] = enable ? 1 : 0;
  return 0;
}


int cpuCmdSetPortStoreSize (word36 cpuIdx, word36 portNumber, word36 storeSize){
  if (cpuCmdCheckIdx (cpuIdx)) {
    return -1;
  }
  if (cpuCmdCheckPort (cpuIdx, portNumber)) {
    return -1;
  }
  if (storeSize > 15) {
    simPrintf ("Error: Invalid CPU port store size; must be 0-7, 32k, 64k, 128k,\n"
            "    256k, 512k, 1024k, 1m, 2048k, 2m, 4096k, 4m\n");
    return -1;
  }

  uint v = (uint) storeSize;

  // Either switch position (0-7) or size (8-15)
  if (v > 7) {
    // by size
   
    // DPS8 and L68 have different mapppings from switch position
    // to memory size.
    if (cpus[cpuIdx].tweaks.l68_mode) {
      // L68
      switch (v) {
        case  8:  v = 0;   break; // 32K
        case  9:  v = 1;   break; // 64K
        case 10:  v = 3;   break; // 128K
        case 11:  v = 7;   break; // 256K
        case 12:  v = 4;   break; // 512K
        case 13:  v = 5;   break; // 1024K
        case 14:  v = 6;   break; // 2048K
        case 15:  v = 2;   break; // 4096K
      }
    } else { // dps8m
      switch (v) {
        case  8:  v = 0;   break; // 32K
        case  9:  v = 1;   break; // 64K
        case 10:  v = 2;   break; // 128K
        case 11:  v = 3;   break; // 256K
        case 12:  v = 4;   break; // 512K
        case 13:  v = 5;   break; // 1024K
        case 14:  v = 6;   break; // 2048K
        case 15:  v = 7;   break; // 4096K
      } // switch
    } // l68 / dps8m
  } // v > 7
#ifdef CMD_VERBOSE
  simPrintf ("SET CPU %c PORT %c STORE_SIZE %d.\n", AZ (cpuIdx), AZ (portNumber), v);
#endif
  cpus[cpuIdx].switches.store_size [portNumber] = v;
  return 0;
}


int cpuCmdSetProm (word36 cpuIdx, bool enable) {
  if (cpuCmdCheckIdx (cpuIdx)) {
    return -1;
  }
#ifdef CMD_VERBOSE
  simPrintf ("SET CPU %c PROM %s\n", AZ (cpuIdx), enable ? "ENABLE" : "DISABLE");
#endif
  cpus[cpuIdx].options.prom_installed = enable;
  return 0;
}


int cpuCmdSetPtwam (word36 cpuIdx, bool enable) {
  if (cpuCmdCheckIdx (cpuIdx)) {
    return -1;
  }
#ifdef CMD_VERBOSE
  simPrintf ("SET CPU %c PTWAM %s\n", AZ (cpuIdx), enable ? "ENABLE" : "DISABLE");
#endif
  cpus[cpuIdx].switches.ptwam_enable = enable;
  return 0;
}


int cpuCmdSetRunStart (word36 cpuIdx, bool enable) {
  if (cpuCmdCheckIdx (cpuIdx)) {
    return -1;
  }
#ifdef CMD_VERBOSE
  simPrintf ("SET CPU %c RUN_START %s\n", AZ (cpuIdx), enable ? "ENABLE" : "DISABLE");
#endif
  cpus[cpuIdx].tweaks.runStart = enable;
  return 0;
}


int cpuCmdSetSdwam (word36 cpuIdx, bool enable) {
  if (cpuCmdCheckIdx (cpuIdx)) {
    return -1;
  }
#ifdef CMD_VERBOSE
  simPrintf ("SET CPU %c SDWAM %s\n", AZ (cpuIdx), enable ? "ENABLE" : "DISABLE");
#endif
  cpus[cpuIdx].switches.sdwam_enable = enable;
  return 0;
}


int cpuCmdSetSpeed (word36 cpuIdx, word36 speed) {
  if (cpuCmdCheckIdx (cpuIdx)) {
    return -1;
  }
  if (speed > 017) {
    simPrintf ("Error: Invalid CPU speed; must be 0-17\n");
    return -1;
  }
#ifdef CMD_VERBOSE
  if (speed == 0)
    simPrintf ("SET CPU %c SPEED 8/70\n", AZ (cpuIdx));
  else if (speed == 0100)
    simPrintf ("SET CPU %c SPEED 8/52\n", AZ (cpuIdx));
  else
    simPrintf ("SET CPU %c SPEED %"PRIo36"\n", AZ (cpuIdx), speed);
#endif
  cpus[cpuIdx].options.proc_speed = (uint) speed;
  return 0;
}


int cpuCmdSetStopnumber (word36 cpuIdx, word36 stopnumber) {
  if (cpuCmdCheckIdx (cpuIdx)) {
    return -1;
  }
  if (stopnumber > 9999) {
    simPrintf ("Error: Invalid Stop Number; must be 0000. to 9999.\n");
    return -1;
  }
#ifdef CMD_VERBOSE
  simPrintf ("SET TRO %c STOPNUMBER %04"PRId36"\n", AZ (cpuIdx), stopnumber);
#endif
  // set up for check stop
  // convert stopnum to bcd
  int64_t d1 = (stopnumber / 1000) % 10;
  int64_t d2 = (stopnumber /  100) % 10;
  int64_t d3 = (stopnumber /   10) % 10;
  int64_t d4 = (stopnumber /    1) % 10;
  word36 d = W36_C (0123000000000);
  putbits36_6 (& d,  9, (word4) d1);
  putbits36_6 (& d, 15, (word4) d2);
  putbits36_6 (& d, 21, (word4) d3);
  putbits36_6 (& d, 27, (word4) d4);
  cpus[cpuIdx].switches.data_switches = d;
  return 0;
}


int cpuCmdSetTro (word36 cpuIdx, bool enable) {
  if (cpuCmdCheckIdx (cpuIdx)) {
    return -1;
  }
#ifdef CMD_VERBOSE
  simPrintf ("SET CPU %c TRO %s\n", AZ (cpuIdx), enable ? "ENABLE" : "DISABLE");
#endif
  cpus[cpuIdx].tweaks.tro_enable = enable;
  return 0;
}


int cpuCmdSetWam (word36 cpuIdx, bool enable) {
  if (cpuCmdCheckIdx (cpuIdx)) {
    return -1;
  }
#ifdef CMD_VERBOSE
  simPrintf ("SET CPU %c WAM %s\n", AZ (cpuIdx), enable ? "ENABLE" : "DISABLE");
#endif
  cpus[cpuIdx].tweaks.enable_wam = enable;
  return 0;
}


int cpuCmdShow (word36 cpuIdx) {
  if (cpuCmdCheckIdx (cpuIdx)) {
    return -1;
  }
#ifdef CMD_VERBOSE
  //simPrintf ("SHOW CPU %c\n", AZ (cpuIdx));
#endif
  simPrintf ("CPU %c: %s\n", AZ (cpuIdx), cpus[cpuIdx].tweaks.l68_mode ? "L68" : "DPS8M");
  simPrintf ("  SWITCHES:\n");
  simPrintf ("    ADDRESS:       %06o\n", cpus[cpuIdx].switches.addr_switches);
  simPrintf ("    CACHE:         %s\n", cpus[cpuIdx].switches.enable_cache ? "ENABLE" : "DISABLE");
  simPrintf ("    DATA:          %012"PRIo36"\n", cpus[cpuIdx].switches.data_switches);
  simPrintf ("    FAULTBASE:     %03o\n", cpus[cpuIdx].switches.FLT_BASE);
  simPrintf ("    MODE:          %s\n", cpus[cpuIdx].switches.procMode == procModeMultics ? "Multics" : "GCOS");
  simPrintf ("    NUMBER:        %o\n", cpus[cpuIdx].switches.cpu_num);
  simPrintf ("    PTWAM:         %s\n", cpus[cpuIdx].switches.ptwam_enable ? "ENABLE" : "DISABLE");
  simPrintf ("    SDWAM:         %s\n", cpus[cpuIdx].switches.sdwam_enable ? "ENABLE" : "DISABLE");

  for (uint p = 0; p < N_CPU_PORTS; p ++) {
    simPrintf ("    PORT: %o\n", p);
    simPrintf ("      %s\n",  cpus[cpuIdx].switches.enable[p] ? "ENABLE" : "DISABLE");
    simPrintf ("      ASSIGNMENT %o\n",  cpus[cpuIdx].switches.assignment[p]);
    simPrintf ("      INTERLACE  %s\n",  cpus[cpuIdx].switches.interlace[p] ? "ENABLE" : "DISABLE");
    simPrintf ("      INITIALIZE %s\n",  cpus[cpuIdx].switches.init_enable[p] ? "ENABLE" : "DISABLE");
    simPrintf ("      STORE_SIZE %d.\n",  cpus[cpuIdx].switches.store_size[p]);
  }

  simPrintf ("  OPTIONS:\n");
  simPrintf ("    CACHE:         %s\n", cpus[cpuIdx].options.cache_installed ? "INSTALLED" : "NOT INSTALLED");
  simPrintf ("    CLOCKSLAVE:    %s\n", cpus[cpuIdx].options.clock_slave_installed ? "INSTALLED" : "NOT INSTALLED");
  simPrintf ("    HEXMODE:       %s\n", cpus[cpuIdx].options.hex_mode_installed ? "ENABLE" : "DISABLE");
  simPrintf ("    PROM:          %s\n", cpus[cpuIdx].options.prom_installed ? "ENABLE" : "DISABLE");
  if (cpus[cpuIdx].options.proc_speed == 0)
    simPrintf ("    SPEED:       8/70\n");
  else if (cpus[cpuIdx].options.proc_speed == 0100)
    simPrintf ("    SPEED:       8/52\n");
  else
    simPrintf ("    SPEED:       %02o\n", cpus[cpuIdx].options.proc_speed);

  simPrintf ("  TWEAKS:\n");
  if (cpus[cpuIdx].tweaks.set_affinity) {
    simPrintf ("    AFFINITY:    %u\n", cpus[cpuIdx].tweaks.affinity);
  } else {
    simPrintf ("    AFFINITY:    OFF\n");
  }
  simPrintf ("    EMCALL:        %s\n", cpus[cpuIdx].tweaks.enable_emcall ? "ENABLE" : "DISABLE");
  simPrintf ("    ISOLTS_MODE:   %s\n", cpus[cpuIdx].tweaks.enable_emcall ? "ENABLE" : "DISABLE");
  simPrintf ("    KIPS:          %u\n", cpus[cpuIdx].tweaks.kips);
  simPrintf ("    LUF:           %s\n", cpus[cpuIdx].tweaks.enableLUF ? "ENABLE" : "DISABLE");
  simPrintf ("    MAP:           %s\n", cpus[cpuIdx].tweaks.useMap ? "ENABLE" : "DISABLE");
  simPrintf ("    RUN_START      %s\n", cpus[cpuIdx].tweaks.useMap ? "ENABLE" : "DISABLE");
  simPrintf ("    TRO:           %s\n", cpus[cpuIdx].tweaks.tro_enable ? "ENABLE" : "DISABLE");
  simPrintf ("    WAM:           %s\n", cpus[cpuIdx].tweaks.enable_wam ? "ENABLE" : "DISABLE");

  return 0;
}


