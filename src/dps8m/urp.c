/*
 * vim: filetype=c:tabstop=4:ai:expandtab
 * SPDX-License-Identifier: ICU
 * scspell-id: 0aeda9d7-f62f-11ec-a611-80ee73e9b8e7
 *
 * ---------------------------------------------------------------------------
 *
 * Copyright (c) 2007-2013 Michael Mondy
 * Copyright (c) 2012-2016 Harry Reed
 * Copyright (c) 2013-2024 Charles Anthony
 * Copyright (c) 2021-2024 The DPS8M Development Team
 *
 * This software is made available under the terms of the ICU License.
 * See the LICENSE.md file at the top-level directory of this distribution.
 *
 * ---------------------------------------------------------------------------
 */

#include <stdio.h>
#include <ctype.h>
#include <unistd.h>

#include "dps8m.h"
#include "iom_channel.h"
#include "iom.h"
#include "urp.h"
#include "faults.h"
#include "cable.h"
#include "utils.h"

//-- // XXX We use this where we assume there is only one unit
//-- #define ASSUME0 0
//--

#define N_PRU_UNITS 1 // default

static struct urpState
  {
    enum urpMode
      {
        urpNoMode, urpSetDiag, urpInitRdData
      } ioMode;
    char deviceName [MAX_DEV_NAME_LEN];
  } urpState [N_URP_UNITS_MAX];

#define UNIT_FLAGS ( UNIT_FIX | UNIT_ATTABLE | UNIT_ROABLE | UNIT_DISABLE | \
                     UNIT_IDLE )
UNIT urp_unit [N_URP_UNITS_MAX] = {
#ifdef NO_C_ELLIPSIS
  { UDATA (NULL, UNIT_FLAGS, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL },
  { UDATA (NULL, UNIT_FLAGS, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL },
  { UDATA (NULL, UNIT_FLAGS, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL },
  { UDATA (NULL, UNIT_FLAGS, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL },
  { UDATA (NULL, UNIT_FLAGS, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL },
  { UDATA (NULL, UNIT_FLAGS, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL },
  { UDATA (NULL, UNIT_FLAGS, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL },
  { UDATA (NULL, UNIT_FLAGS, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL },
  { UDATA (NULL, UNIT_FLAGS, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL },
  { UDATA (NULL, UNIT_FLAGS, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL },
  { UDATA (NULL, UNIT_FLAGS, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL },
  { UDATA (NULL, UNIT_FLAGS, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL },
  { UDATA (NULL, UNIT_FLAGS, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL },
  { UDATA (NULL, UNIT_FLAGS, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL },
  { UDATA (NULL, UNIT_FLAGS, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL },
  { UDATA (NULL, UNIT_FLAGS, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL }
#else
  [0 ... N_URP_UNITS_MAX-1] = {
    UDATA (NULL, UNIT_FLAGS, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL
  }
#endif
};

#define URPUNIT_NUM(uptr) ((uptr) - urp_unit)

static simRc urpShowUnits (UNUSED FILE * st, UNUSED UNIT * uptr, UNUSED int val, UNUSED const void * desc)
  {
    simPrintf("Number of URP units in system is %d\n", urp_dev.numunits);
    return SCPE_OK;
  }

static simRc urpSetUnits (UNUSED UNIT * uptr, UNUSED int32_t value, const char * cptr, UNUSED void * desc)
  {
    if (! cptr)
      return SCPE_ARG;
    int n = atoi (cptr);
    if (n < 0 || n > N_URP_UNITS_MAX)
      return SCPE_ARG;
    urp_dev.numunits = (uint32_t) n;
    return SCPE_OK;
  }

static simRc urpShowDeviceName (UNUSED FILE * st, UNIT * uptr, UNUSED int val, UNUSED const void * desc)
  {
    int n = (int) URPUNIT_NUM (uptr);
    if (n < 0 || n >= N_URP_UNITS_MAX)
      return SCPE_ARG;
    simPrintf ("name     : %s", urpState[n].deviceName);
    return SCPE_OK;
  }

static simRc urpSetDeviceName (UNUSED UNIT * uptr, UNUSED int32_t value, UNUSED const char * cptr, UNUSED void * desc)
  {
    int n = (int) URPUNIT_NUM (uptr);
    if (n < 0 || n >= N_URP_UNITS_MAX)
      return SCPE_ARG;
    if (cptr)
      {
        strncpy (urpState[n].deviceName, cptr, MAX_DEV_NAME_LEN - 1);
        urpState[n].deviceName[MAX_DEV_NAME_LEN - 1] = 0;
      }
    else
      urpState[n].deviceName [0] = 0;
    return SCPE_OK;
  }

#define UNIT_WATCH UNIT_V_UF

static MTAB urp_mod [] =
  {
#ifndef SPEED
    { UNIT_WATCH, 1, "WATCH", "WATCH", 0, 0, NULL, NULL },
    { UNIT_WATCH, 0, "NOWATCH", "NOWATCH", 0, 0, NULL, NULL },
#endif
    {
      MTAB_XTD | MTAB_VDV | MTAB_NMO | MTAB_VALR, /* mask               */
      0,                                          /* match              */
      "NUNITS",                                   /* print string       */
      "NUNITS",                                   /* match string       */
      urpSetUnits,                                /* validation routine */
      urpShowUnits,                               /* display routine    */
      "Number of URP units in the system",        /* value descriptor   */
      NULL                                        /* help               */
    },
    {
      MTAB_XTD | MTAB_VUN | MTAB_VALR | MTAB_NC,  /* mask               */
      0,                                          /* match              */
      "NAME",                                     /* print string       */
      "NAME",                                     /* match string       */
      urpSetDeviceName,                           /* validation routine */
      urpShowDeviceName,                          /* display routine    */
      "Set the device name",                      /* value descriptor   */
      NULL                                        /* help               */
    },

    { 0, 0, NULL, NULL, 0, 0, NULL, NULL }
  };

static simRc urpReset (UNUSED DEVICE * dptr)
  {
    return SCPE_OK;
  }

DEVICE urp_dev = {
    "URP",        /* name                */
    urp_unit,     /* unit                */
    NULL,         /* registers           */
    urp_mod,      /* modifiers           */
    N_PRU_UNITS,  /* number of units     */
    10,           /* address radix       */
    24,           /* address width       */
    1,            /* address increment   */
    8,            /* data radix          */
    36,           /* data width          */
    NULL,         /* examine             */
    NULL,         /* deposit             */
    urpReset,     /* reset               */
    NULL,         /* boot                */
    NULL,         /* attach              */
    NULL,         /* detach              */
    NULL,         /* context             */
    0,            /* flags               */
    0,            /* debug control flags */
    NULL,         /* debug flag names    */
    NULL,         /* memory size change  */
    NULL,         /* logical name        */
    NULL,         /* help                */
    NULL,         /* attach help         */
    NULL,         /* attach context      */
    NULL,         /* description         */
    NULL          /* end                 */
};

/*
 * urp_init()
 */

// Once-only initialization

void urp_init (void)
  {
    (void) memset (urpState, 0, sizeof (urpState));
  }

static iomCmdRc urpCmd (chnlState_t * chnlp) {
 uint ctlrUnitIdx = get_ctlr_idx (chnlp);
 uint devUnitIdx = cables->urp_to_urd[ctlrUnitIdx][chnlp->IDCW_DEV_CODE].unit_idx;
 //UNIT * unitp = & urp_unit [devUnitIdx];
 //int urp_unit_num = (int) URPUNIT_NUM (unitp);
 struct urpState * statep = & urpState[devUnitIdx];

 // IDCW?
 if (IS_IDCW (chnlp)) {
    // IDCW
    statep->ioMode = urpNoMode;

    switch (chnlp->IDCW_DEV_CMD) {
      case 000: // CMD 00 Request status
        chnlp->stati = 04000;
        break;

      case 006: // CMD 005 Initiate read data xfer (load_mpc.pl1)
        statep->ioMode = urpInitRdData;
        chnlp->stati = 04000;
        break;

// 011 punch binary
// 031 set diagnostic mode

      case 031: // CMD 031 Set Diagnostic Mode (load_mpc.pl1)
        statep->ioMode = urpSetDiag;
        chnlp->stati = 04000;
        break;

      case 040: // CMD 40 Reset status
        chnlp->stati = 04000;
        chnlp->isRead = false;
        break;

      default:
        chnlp->stati = 04501; // cmd reject, invalid opcode
        chnlp->chanStatus = chanStatIncorrectDCW;
        if (chnlp->IDCW_DEV_CMD != 051) // ignore bootload console probe
          simWarn ("%s: URP unrecognized device command  %02o\n", __func__, chnlp->IDCW_DEV_CMD);
        return IOM_CMD_ERROR;
    } // switch IDCW_DEV_CMD

    return IOM_CMD_PROCEED;
  } // if IDCW

  // Not IDCW; TDCW are captured in IOM, so must be IOTD, IOTP or IOTNP
  switch (statep->ioMode) {
    case urpNoMode:
      //simPrintf ("%s: Unexpected IOTx\n", __func__);
      //simWarn ("%s: Unexpected IOTx\n", __func__);
      //return IOM_CMD_ERROR;
      break;

    case urpSetDiag:
      // We don't use the DDCW, so just pretend we do. BUG
      chnlp->stati = 04000;
      break;

    case urpInitRdData:
      // We don't use the DDCW, so just pretend we do. BUG
      chnlp->stati = 04000;
      break;

     default:
      simWarn ("%s: Unrecognized ioMode %d\n", __func__, statep->ioMode);
      return IOM_CMD_ERROR;
  }
  return IOM_CMD_PROCEED;
}

iomCmdRc urp_iom_cmd (chnlState_t * chnlp) {
  uint iomUnitIdx = chnlp->myIomIdx;
  uint chan = chnlp->myChanIdx;
  uint devCode = chnlp->IDCW_DEV_CODE;
  if (devCode == 0)
    return urpCmd (chnlp);
  uint urpUnitIdx = cables->iom_to_ctlr[iomUnitIdx][chan].ctlr_unit_idx;
  iom_cmd_t * cmd = cables->urp_to_urd[urpUnitIdx][devCode].iom_cmd;
  if (! cmd) {
    //simWarn ("URP can't find device handler\n");
    //return IOM_CMD_ERROR;
    chnlp->stati = 04502; // invalid device code
    return IOM_CMD_DISCONNECT;
  }
  return cmd (chnlp);
}
