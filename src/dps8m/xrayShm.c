#include <stdio.h>
#include <sys/mman.h>

#include "dps8m.h"
#include "xray.h"

void * openShm (char * key) {
  void * p;

  if (strcmp ("", key) == 0) {
    (void) fprintf (stderr, "FATAL: Invalid statefile name specified!\n");
    exit (EXIT_FAILURE);
  }

  int fd = open (filebuf, O_RDWR /*| O_CREAT*/, S_IRUSR | S_IWUSR);
  if (fd == -1) {
    (void) fprintf (stderr, "FATAL: Opening '%s' failed: %s (Error %d); is dps8m running?\n",
                    filebuf, strerror (errno), errno);
    exit (EXIT_FAILURE);
  }

  struct stat sb;
  if (fstat (fd, & sb) == -1) { /* To obtain file size */
    (void) fprintf (stderr, "FATAL: fstat failed: %s (Error %d)\r\n", strerror (errno), errno);
    exit (EXIT_FAILURE);
  }
  p = mmap (NULL, sb.st_size, PROT_READ, MAP_SHARED, fd, 0);

  if (p == MAP_FAILED) {
    (void) fprintf (stderr, "FATAL: mmap failed: %s (Error %d)\r\n", strerror (errno), errno);
    exit (EXIT_FAILURE);
  }

  close (fd);
  return p;
}

