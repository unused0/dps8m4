/*
 * vim: filetype=c:tabstop=4:ai:expandtab
 * SPDX-License-Identifier: ICU
 * scspell-id: 6965b612-f62e-11ec-a432-80ee73e9b8e7
 *
 * ---------------------------------------------------------------------------
 *
 * Copyright (c) 2012-2016 Harry Reed
 * Copyright (c) 2012 Dave Jordan
 * Copyright (c) 2013-2024 Charles Anthony
 * Copyright (c) 2016 Jean-Michel Merliot
 * Copyright (c) 2021-2024 The DPS8M Development Team
 *
 * This software is made available under the terms of the ICU License.
 * See the LICENSE.md file at the top-level directory of this distribution.
 *
 * ---------------------------------------------------------------------------
 */

#if !defined(DPS8M_H)
# define DPS8M_H

# if defined(__FAST_MATH__)
#  error __FAST_MATH__ is unsupported
# endif /* if defined(__FAST_MATH__) */

# include <stdio.h>
# include <stdbool.h>
# include <stdatomic.h>
# include <errno.h>
# include <inttypes.h>
# include <sys/stat.h>
# include <sys/time.h>
# include <setjmp.h>  // for setjmp/longjmp used by interrupts & faults

#include "datatypes.h"

# if (defined(__APPLE__) && defined(__MACH__)) || defined(__ANDROID__)
#  include <libgen.h>  // needed for macOS and Android
# endif

# if defined(__FreeBSD__) || defined(__OpenBSD__) || defined(__NetBSD__) || defined(__DragonFly__) || (defined(__APPLE__) && defined(__MACH__)) || defined(__ANDROID__) || defined(_AIX)
#  undef setjmp
#  define setjmp _setjmp
#  undef longjmp
#  define longjmp _longjmp
# endif


#include "config.h"

typedef struct cpu_state_s cpuState_t;

# include "sim_defs.h"                                   /* simulator defns */
# include "sim_tape.h"

# if defined (__MINGW64__) || \
    defined (__MINGW32__)  || \
    defined (__GNUC__)     || \
    defined (__clang_version__)
#  define NO_RETURN __attribute__ ((noreturn))
#  define UNUSED    __attribute__ ((unused))
# else
#  define NO_RETURN
#  define UNUSED
# endif

//#define INT32_C(x) ((int32_t) (x)
//#define UINT32_C(x) ((uint32_t) (x)
//#define INT64_C(x) ((int64_t) (x)
//#define UINT64_C(x) ((uint64_t) (x)
# ifndef INT128_C
#  define INT128_C(x)      x ## LL
# endif
# ifndef UINT128_C
#  define UINT128_C(x)      x ## ULL
# endif

#define W36_C UINT64_C

# include "simh.h"
# include "math128.h"
# include "hw_consts.h"
# include "em_consts.h"

// Misc constants and macros

# define ARRAY_SIZE(a) ( sizeof(a) / sizeof((a)[0]) )

# if defined(FREE)
#  undef FREE
# endif /* if defined(FREE) */
# define FREE(p) do  \
  {                  \
    free((p));       \
    (p) = NULL;      \
  } while(0)

# if defined(__GNUC__) || defined(__clang_version__)
#  if !defined(LIKELY)
#   define LIKELY(x) __builtin_expect(!!(x), 1)
#  endif
#  if !defined(UNLIKELY)
#   define UNLIKELY(x) __builtin_expect(!!(x), 0)
#  endif
# else
#  if !defined(LIKELY)
#   define LIKELY(x) (x)
#  endif
#  if !defined(UNLIKELY)
#   define UNLIKELY(x) (x)
#  endif
# endif

# if defined(USE_BACKTRACE) && defined(SIGUSR2)
# define BACKTRACE (void) raise(SIGUSR2);
#else
# define BACKTRACE
# endif

# define BUG_CHECK(p,m) \
  do { \
    if (p) { \
      (void) fprintf(stderr, "\nFATAL: %s Aborting at %s[%s:%d]\n", \
                              m, __func__, __FILE__, __LINE__); \
      BACKTRACE; \
      abort (); \
    } \
  } while (0)

# define MALLOC_CHECK(p) BUG_CHECK ((p) == NULL, "Out of memory!")


// Basic STDIO for MinGW
# if !defined(__CYGWIN__)
#  if defined(__MINGW32__) || defined(__MINGW64__) || defined(CROSS_MINGW32) || defined(CROSS_MINGW64)
#   define WIN_STDIO    1
#  endif /* if defined(__MINGW32__) || defined(__MINGW64__) || defined(CROSS_MINGW32) || defined(CROSS_MINGW64) */
# endif /* if !defined(__CYGWIN__) */

#define PRIo36 PRIo64
#define PRId36 PRId64

# include "sys.h"
# include "cpu.h"
# include "scu.h"
# include "hdbg.h"
# include "test.h"

#define INV "\033[7m"
#define RES "\033[0m"
//#define RED0(format) printf ("\033[7m"format"\033[0m")
//#define RED(format, ...) printf ("\033[7m"format"\033[0m", __VA_ARGS__)

// Allows empty arg list
//#define VA_ARGS(...) , ##__VA_ARGS__
// #define CALL(f,...) FN(f)->call((ref(new LinkedList()) VA_ARGS(__VA_ARGS__)))

#endif // ifdef DPS8M_H
