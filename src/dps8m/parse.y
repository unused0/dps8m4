/* definitions */
%{
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#define YYERROR_VERBOSE

#include "dps8m.h"

extern int yylineno;

extern ctlr_t ctlrs[N_CTLRS];

extern int yylex();
extern int yyparse();
void yyerror (const char *str);

static word36 iomIdx;
static enum ctlrTypes ctlrType;
static word36 ctlrIdx;
static word36 ctlrModel;
static word36 prphIdx;
static enum prphTypes prphType;
static word36 chan, nchan;
static word36 chan0, nchan0;
static word36 chan1, nchan1;
static word36 chan2, nchan2;
static word36 chan3, nchan3;
static word36 model;
static word36 model0, d0;
static word36 model1, d1;
static word36 model2, d2;
static word36 model3, d3;
static word36 model4, d4;
static word36 state;
static word36 lineLength;
static word36 train;
static enum iomModels iomModel;
static word36 cpuIdx;
static enum cpuTypes cpuType;
static word36 cpuModel;
static word36 cacheSize;
static word36 memIdx;
static word36 memsize;
static word36 opcState;
static word36 position;
static word36 portNumber;
static word36 scuIdx;
static int maskRegister;
static bool enable;
static uint banklist;

#define iffail(x) if (x) YYERROR
%}

%token A01_
%token A1_
%token ADDRESS
%token AFFINITY_
%token ASSIGNMENT
%token ALT

%token B01_
%token B1_

%token CACHE
%token CCU
%token CHNL
%token CLOCKSLAVE
%token COMMA
%token CONFIG
%token CPU
%token CPUx
%token CYCLIC

%token DATA
%token DIA
%token DISABLE
%token DOWN
%token DPS
%token DPS8
%token DSK

%token ELAPSED_DAYS
%token EMCALL
%token ENABLE
%token ERROR

%token ID
%token INTERLACE
%token INITIALIZE

%token FAULTBASE
%token FIPS
%token FNP

%token GCOS

%token HEXMODE

%token INSTALLED
%token IOM
%token IMU
%token INOP
%token IO
%token IPC
%token ISOLTS_MODE

%token KIPS

%token L68
%token LOWER
%token LSS
%token LUF

%token MANUAL
%token MAP
%token MASK
%token MASKx
%token MEM
%token MODE
%token MPC
%token MSP
%token MTP
%token MULTICS

%token NEA
%token NOT
%token NUMBER_OVALUE
%token NUMBER_DVALUE
%token NUMBER

%token OFF
%token OFFLINE
%token ON
%token ONLINE
%token OPC

%token PORTx
%token PORT
%token PROGRAM
%token PRPH
%token PROM
%token PRT
%token PTWAM
%token PUN

%token RDR
%token RESET
%token RUN_START

%token S852
%token S870
%token SCU
%token SCUx
%token SDWAM
%token SEMICOLON
%token SET
%token SHOW
%token SPEED_ 
%token STATE 
%token STEADY_CLOCK 
%token STOP_NUMBER
%token STORE_SIZE

%token TAP
%token TRO

%token UP
%token URP

%token WAM

%token Y2K

%define api.value.type { word36 }
%code requires {
  #include "dps8m.h"
}

%start Cmds

%% 
// cpu[a-h] port state type model cache_size
//
// config ipc fips iom chn nchan
/* rules */ 
Cmds : prepare Cmd 
     | Cmds SEMICOLON prepare Cmd 
     ;

prepare : %empty { banklist = 0; }

///
/// SET CPU
///

Cmd  : SET Cpu L68                          { iffail (cpuCmdSetL68 (cpuIdx, 1)); }
     | SET Cpu DPS8                         { iffail (cpuCmdSetL68 (cpuIdx, 0)); }
     | SET Cpu ADDRESS NUMBER_VALUE         { iffail (cpuCmdSetAddress (cpuIdx, yylval)); }
     | SET Cpu AFFINITY_ OFF                { iffail (cpuCmdSetAffinity (cpuIdx, false, 0)); }
     | SET Cpu AFFINITY_ NUMBER_DVALUE      { iffail (cpuCmdSetAffinity (cpuIdx, true, yylval)); }
     | SET Cpu CACHE e_d                    { iffail (cpuCmdSetCache (cpuIdx, enable)); }
     | SET Cpu CACHE INSTALLED              { iffail (cpuCmdSetCacheInstalled (cpuIdx, true)); }
     | SET Cpu CACHE NOT INSTALLED          { iffail (cpuCmdSetCacheInstalled (cpuIdx, false)); }
     | SET Cpu CLOCKSLAVE INSTALLED         { iffail (cpuCmdSetClockslaveInstalled (cpuIdx, true)); }
     | SET Cpu CLOCKSLAVE NOT INSTALLED     { iffail (cpuCmdSetClockslaveInstalled (cpuIdx, false)); }
     | SET Cpu DATA NUMBER_VALUE            { iffail (cpuCmdSetData (cpuIdx, yylval)); }
     | SET Cpu DATA NUMBER_DVALUE { position = yylval; } ON
                                            { iffail (cpuCmdSetDataOne (cpuIdx, position, 1)); }
     | SET Cpu DATA NUMBER_DVALUE { position = yylval; } UP
                                            { iffail (cpuCmdSetDataOne (cpuIdx, position, 1)); }
     | SET Cpu DATA NUMBER_DVALUE { position = yylval; } OFF
                                            { iffail (cpuCmdSetDataOne (cpuIdx, position, 0)); }
     | SET Cpu DATA NUMBER_DVALUE { position = yylval; } DOWN
                                            { iffail (cpuCmdSetDataOne (cpuIdx, position, 0)); }
     | SET Cpu DATA NUMBER_DVALUE { position = yylval; } NUMBER_VALUE
                                            { iffail (cpuCmdSetDataOne (cpuIdx, position, yylval)); }
     | SET Cpu EMCALL e_d                   { iffail (cpuCmdSetEmcall (cpuIdx, enable)); }
     | SET Cpu FAULTBASE MULTICS            { iffail (cpuCmdSetFaultbase  (cpuIdx, 2)); }
     | SET Cpu FAULTBASE NUMBER_VALUE       { iffail (cpuCmdSetFaultbase  (cpuIdx, yylval)); }
     | SET Cpu HEXMODE e_d                  { iffail (cpuCmdSetHexMode (cpuIdx, enable)); }
     | SET Cpu ISOLTS_MODE e_d              { iffail (cpuCmdSetIsoltsMode (cpuIdx, enable)); }
     | SET Cpu KIPS NUMBER_VALUE            { iffail (cpuCmdSetKips (cpuIdx, yylval)); }
     | SET Cpu LUF e_d                      { iffail (cpuCmdSetLuf (cpuIdx, enable)); }
     | SET Cpu MAP e_d                      { iffail (cpuCmdSetMap (cpuIdx, enable)); }
     | SET Cpu MODE GCOS                    { iffail (cpuCmdSetMode       (cpuIdx, procModeGCOS)); }
     | SET Cpu MODE MULTICS                 { iffail (cpuCmdSetMode       (cpuIdx, procModeMultics)); }
     | SET Cpu NUMBER NUMBER_VALUE          { iffail (cpuCmdSetNumber (cpuIdx, yylval)); }
     | SET Cpu Port e_d  { iffail (cpuCmdSetPortEnable (cpuIdx, portNumber, enable)); }
     | SET Cpu Port ASSIGNMENT NUMBER_VALUE { iffail (cpuCmdSetPortAssignment (cpuIdx, portNumber, yylval)); }
     | SET Cpu Port INITIALIZE e_d          { iffail (cpuCmdSetPortInitEnable (cpuIdx, portNumber, enable)); }
     | SET Cpu Port INTERLACE e_d           { iffail (cpuCmdSetPortInterlaceEnable (cpuIdx, portNumber, enable)); }
     | SET Cpu Port STORE_SIZE NUMBER_VALUE { iffail (cpuCmdSetPortStoreSize (cpuIdx, portNumber, yylval)); }
     | SET Cpu PROM e_d                     { iffail (cpuCmdSetProm (cpuIdx, enable)); }
     | SET Cpu PTWAM e_d                    { iffail (cpuCmdSetPtwam (cpuIdx, enable)); }
     | SET Cpu RUN_START e_d                { iffail (cpuCmdSetRunStart (cpuIdx, enable)); }
     | SET Cpu SDWAM e_d                    { iffail (cpuCmdSetSdwam (cpuIdx, enable)); }
     | SET Cpu SPEED_ NUMBER_VALUE          { iffail (cpuCmdSetSpeed (cpuIdx, yylval)); }
     | SET Cpu SPEED_ S852                  { iffail (cpuCmdSetSpeed (cpuIdx, 0100)); }
     | SET Cpu SPEED_ S870                  { iffail (cpuCmdSetSpeed (cpuIdx, 0)); }
     | SET Cpu STOP_NUMBER NUMBER_DVALUE    { iffail (cpuCmdSetStopnumber (cpuIdx, yylval)); }
     | SET Cpu TRO e_d                      { iffail (cpuCmdSetTro (cpuIdx, enable)); }
     | SET Cpu WAM e_d                      { iffail (cpuCmdSetWam (cpuIdx, enable)); }

///
/// SHOW CPU
///

     | SHOW Cpu   { iffail (cpuCmdShow (cpuIdx)); }

///
/// SET SCU
///

     | SET Scu CYCLIC NUMBER_VALUE  { iffail (scuCmdSetCyclic (scuIdx, yylval)); }
     | SET Scu ELAPSED_DAYS NUMBER_DVALUE
                                    { iffail (scuCmdSetElapseddays (scuIdx, yylval)); }
     | SET Scu INTERLACE e_d        { iffail (scuCmdSetInterlace (scuIdx, enable)); }
     | SET Scu LSS NUMBER_VALUE     { iffail (scuCmdSetLSS (scuIdx, yylval)); }
     | SET Scu LOWER e_d            { iffail (scuCmdSetLower (scuIdx, enable)); }
     | SET Scu Mask OFF             { iffail (scuCmdSetMask (scuIdx, maskRegister, -1)); }
     | SET Scu Mask NUMBER_VALUE    { iffail (scuCmdSetMask (scuIdx, maskRegister, yylval)); }
     | SET Scu MODE MANUAL          { iffail (scuCmdSetMode (scuIdx, 0)); }
     | SET Scu MODE PROGRAM         { iffail (scuCmdSetMode (scuIdx, 1)); }
     | SET Scu NEA NUMBER_VALUE     { iffail (scuCmdSetNEA (scuIdx, yylval)); }
     | SET Scu OFFLINE              { iffail (scuCmdSetOnline (scuIdx, 017, false)); }
     | SET Scu ONLINE               { iffail (scuCmdSetOnline (scuIdx, 017, true)); }
     | SET Scu Banklist OFFLINE     { iffail (scuCmdSetOnline (scuIdx, banklist, false)); }
     | SET Scu Banklist ONLINE      { iffail (scuCmdSetOnline (scuIdx, banklist, true)); }
     | SET Scu Port e_d             { iffail (scuCmdSetPort (scuIdx, portNumber, enable)); }
     | SET Scu STEADY_CLOCK e_d     { iffail (scuCmdSetSteadyClock (scuIdx, enable)); }
     | SET Scu STEADY_CLOCK Y2K     { iffail (scuCmdSetY2K (scuIdx, enable)); }
// int
// lwr
// elapsed days
// steady clock
// bullet time
// y2k

///
/// SHOW SCU
///

     | SHOW Scu         { iffail (scuCmdShow (scuIdx)); }
     | SHOW Scu CONFIG  { iffail (scuCmdShowConfig (scuIdx)); }
     | SHOW Scu STATE   { iffail (scuCmdShowState (scuIdx)); }

///
/// CONFIG
///

       // CONFIG CPU ID
       // CONFIG CPU ID port state type model cache  AM81 7-10 */
     | CONFIG Cpu NUMBER_VALUE { portNumber = yylval; } State CpuType CpuModel CacheSize
         { iffail (configCmdCpu (cpuIdx, portNumber, state, cpuType, cpuModel, cacheSize)); }

       // CONFIG MEMx
       // CALBLE MEM memsize state
     | CONFIG Mem Memsize State
         { iffail (configCmdMem (memIdx, memsize, state)); }

       // CONFIG IPC
     | CONFIG IpcCtlr FIPS Iom Chan Nchan
         { iffail (configCmdIpcCtlr (iomIdx, chan, nchan)); }

       // CONFIG  MSPx, MTPx, URPx
     | CONFIG MPC Ctlr CtlrModel Iom ChanSet0
         { iffail (configCmdCtlr (ctlrIdx, ctlrType, ctlrModel, iomIdx, chan0, nchan0, 0, 0, 0, 0, 0, 0)); }

     | CONFIG MPC Ctlr CtlrModel Iom ChanSet0 ChanSet1
         { iffail (configCmdCtlr (ctlrIdx, ctlrType, ctlrModel, iomIdx, chan0, nchan0, chan1, nchan1, 0, 0, 0, 0)); }

     | CONFIG MPC Ctlr CtlrModel Iom ChanSet0 ChanSet1 ChanSet2
         { iffail (configCmdCtlr (ctlrIdx, ctlrType, ctlrModel, iomIdx, chan0, nchan0, chan1, nchan1, chan2, nchan2, 0, 0)); }

     | CONFIG MPC Ctlr CtlrModel Iom ChanSet0 ChanSet1 ChanSet2 ChanSet3
         { iffail (configCmdCtlr (ctlrIdx, ctlrType, ctlrModel, iomIdx, chan0, nchan0, chan1, nchan1, chan2, nchan2, chan3, nchan3)); }
 
       /* CCUx DIAx PUNx RDRx */
     | CONFIG PRPH Prph0 Iom Chan PrphModel
         { iffail (configCmdPrph0 (prphIdx, prphType, iomIdx, chan, model)); }

       /* DSKx TAPx */
       /* prph dskn iom channel nchan model1 d1 {model 2 d2 ... model5 d5} */
     | CONFIG PRPH Prph1 Iom Chan Nchan PrphModelN0 
         { iffail (configCmdPrph1 (prphIdx, prphType, iomIdx, chan, nchan, 1, model0, d0, 0, 0, 0, 0, 0, 0, 0, 0)); }

     | CONFIG PRPH Prph1 Iom Chan Nchan PrphModelN0 PrphModelN1
         { iffail (configCmdPrph1 (prphIdx, prphType, iomIdx, chan, nchan, 2, model0, d0, model1, d1, 0, 0, 0, 0, 0, 0)); }

     | CONFIG PRPH Prph1 Iom Chan Nchan PrphModelN0 PrphModelN1 PrphModelN2
         { iffail (configCmdPrph1 (prphIdx, prphType, iomIdx, chan, nchan, 3, model0, d0, model1, d1, model2, d2, 0, 0, 0, 0)); }

     | CONFIG PRPH Prph1 Iom Chan Nchan PrphModelN0 PrphModelN1 PrphModelN2 PrphModelN3
         { iffail (configCmdPrph1 (prphIdx, prphType, iomIdx, chan, nchan, 4, model0, d0, model1, d1, model2, d2, model3, d3, 0, 0)); }

     | CONFIG PRPH Prph1 Iom Chan Nchan PrphModelN0 PrphModelN1 PrphModelN2 PrphModelN3 PrphModelN4
         { iffail (configCmdPrph1 (prphIdx, prphType, iomIdx, chan, nchan, 5, model0, d0, model1, d1, model2, d2, model3, d3, model4, d4)); }

       /* FNPx */
     | CONFIG PRPH Prph2 Iom Chan PrphModel State
         { iffail (configCmdPrph2 (prphIdx, prphType, iomIdx, chan, model, state)); }

       /* OPCx */
     | CONFIG PRPH Prph3 Iom Chan PrphModel LineLength OpcState
         { iffail (configCmdPrph3 (prphIdx, prphType, iomIdx, chan, model, lineLength, opcState, false)); }

     | CONFIG PRPH Prph3 Iom Chan PrphModel LineLength OpcState Option
         { iffail (configCmdPrph3 (prphIdx, prphType, iomIdx, chan, model, lineLength, opcState, true)); }

       /* PRTx */
     | CONFIG PRPH Prph4 Iom Chan PrphModel Train LineLength
         { iffail (configCmdPrph4 (prphIdx, prphType, iomIdx, chan, model, train, lineLength)); }


  /* CHNL device_name iom1 chn1 nchan1 {... iom4 chn4 nchan4 }
       device name must match the device nuame of the disk or tape
      subsystem on a prph record  AM81 7-5 */

     | CONFIG CHNL Prph1 Iom ChanSet0
         { iffail (configCmdChnl (prphIdx, prphType, iomIdx,  chan0, nchan0, 0, 0, 0, 0, 0, 0)); }

     | CONFIG CHNL Prph1 Iom ChanSet0 ChanSet1
         { iffail (configCmdChnl (prphIdx, prphType, iomIdx,  chan0, nchan0, chan1, nchan1, 0, 0, 0, 0)); }

     | CONFIG CHNL Prph1 Iom ChanSet0 ChanSet1 ChanSet2
         { iffail (configCmdChnl (prphIdx, prphType, iomIdx,  chan0, nchan0, chan1, nchan1, chan2, nchan2, 0, 0)); }

     | CONFIG CHNL Prph1 Iom ChanSet0 ChanSet1 ChanSet2 ChanSet3
         { iffail (configCmdChnl (prphIdx, prphType, iomIdx,  chan0, nchan0, chan1, nchan1, chan2, nchan2, chan3, nchan3)); }

  /* IOMx  port model state */
     | CONFIG Iom NUMBER_VALUE { portNumber = yylval; }  IomModel State
         { iffail (configCmdIom (iomIdx, portNumber, model, state)); }

  /* MEM port size state */

     | CONFIG RESET { configCmdReset (); }

  /* empty */ 
     | prepare
     ;


///
/// Bits and pieces
///

IpcCtlr : IPC        { ctlrType = ctlrIPC; ctlrIdx = 0; }
        ;

Ctlr : MTP ID       { ctlrType = ctlrMTP; ctlrIdx = yylval; }
     | MSP ID       { ctlrType = ctlrMSP; ctlrIdx = yylval; }
     | URP ID       { ctlrType = ctlrURP; ctlrIdx = yylval; }
     ;

Prph0 : CCU ID     { prphType = prphCCU; prphIdx = yylval; }
      | DIA ID    { prphType = prphDIA; prphIdx = yylval; }
      | PUN ID    { prphType = prphPUN; prphIdx = yylval; }
      | RDR ID    { prphType = prphRDR; prphIdx = yylval; }
      ;

Prph1 : DSK ID   { prphType = prphDSK; prphIdx = yylval; }
      | TAP ID   { prphType = prphTAP; prphIdx = yylval; }
      ;

Prph2 : FNP ID   { prphType = prphFNP; /* FNPs are A-H (0-7) */ iffail (yylval > 8); prphIdx = yylval; }
      ;

Prph3 : OPC ID   { prphType = prphOPC; prphIdx = yylval; }
      ;

Prph4 : PRT ID   { prphType = prphPRT; prphIdx = yylval; }
      ;

CtlrModel : NUMBER_VALUE  { ctlrModel = yylval; }
          ;

ChanSet0 : NUMBER_VALUE { chan0 = yylval; } NUMBER_VALUE { nchan0 = yylval; }
         ;
ChanSet1 : NUMBER_VALUE { chan1 = yylval; } NUMBER_VALUE { nchan1 = yylval; }
         ;
ChanSet2 : NUMBER_VALUE { chan2 = yylval; } NUMBER_VALUE { nchan2 = yylval; }
         ;
ChanSet3 : NUMBER_VALUE { chan3 = yylval; } NUMBER_VALUE { nchan3 = yylval; }
         ;

PrphModel  : NUMBER_VALUE { model  = yylval; }
         ;
PrphModelN0 : NUMBER_VALUE { model0 = yylval; } NUMBER_VALUE { d0 = yylval; }
         ;
PrphModelN1 : NUMBER_VALUE { model1 = yylval; } NUMBER_VALUE { d1 = yylval; }
         ;
PrphModelN2 : NUMBER_VALUE { model2 = yylval; } NUMBER_VALUE { d2 = yylval; }
         ;
PrphModelN3 : NUMBER_VALUE { model3 = yylval; } NUMBER_VALUE { d3 = yylval; }
         ;
PrphModelN4 : NUMBER_VALUE { model4 = yylval; } NUMBER_VALUE { d4 = yylval; }
         ;

Chan : NUMBER_VALUE { chan = yylval; }
     ;

Nchan : NUMBER_VALUE { nchan = yylval; }
      ;

Iom : IOM ID        { /* IOMs are A-D (0-3) */ iffail (yylval > 3); iomIdx = yylval; }
    ;

Mem : MEM ID        { /* SCUs are A-D (0-3) */ iffail (yylval > 3); memIdx = yylval; }
    ;

State : ON        { state = stateOn; }
      | OFF       { state = stateOff; }
      ;

OpcState : ON        { opcState = opcStateOn; }
         | ALT       { opcState = opcStateAlt; }
         | IO        { opcState = opcStateIO; }
         | INOP      { opcState = opcStateInop; }
         ;

LineLength : NUMBER_VALUE { lineLength = yylval; }
           ;

Train : NUMBER_VALUE { train = yylval; }
           ;

Option : MASK
       ;

Cpu : CPU ID { /* CPUs are A-H (0-7) */ iffail (yylval > 7); cpuIdx = yylval; }
    | CPUx {iffail (yylval > 7); cpuIdx = yylval; }
    ;

Scu : SCU ID { /* SCUs are A-H (0-7) */ iffail (yylval > 7); scuIdx = yylval; }
    | SCUx {iffail (yylval > 7); scuIdx = yylval; }
    ;

Port : PORT ID { portNumber = yylval; }
     | PORTx { portNumber = yylval; }
     ;

IomModel : IOM { iomModel = iomIOM; }
         | IMU { iomModel = iomIMU; }
         ;

CpuType  : L68    { cpuType = cpuL68; }
         | DPS8   { cpuType = cpuDPS8; }
         | DPS    { cpuType = cpuDPS; }
         ;

CpuModel : NUMBER_VALUE { cpuModel = yylval; }
         ;

CacheSize : NUMBER_VALUE { cacheSize = yylval; }
          ;

Memsize : NUMBER_VALUE { memsize = yylval; }
        ;

Mask    : MASK ID { iffail (yyval > 1); maskRegister = yylval; }
        | MASKx   { iffail (yyval > 1); maskRegister = yylval; }

NUMBER_VALUE : NUMBER_OVALUE | NUMBER_DVALUE ;

e_d : ENABLE { enable = true; }
    | DISABLE { enable = false; }

Bankitem : A1_  { banklist |= 004; }
         | B1_  { banklist |= 001; }
         | ID   { if (yylval == 0)
                    banklist |= 010;
                  else if (yylval == 1)
                    banklist |= 002;
                  else
                    banklist |= 020; }
         ;

Banklist : Bankitem 
     | Banklist Bankitem
     ;
%% 

/* auxiliary routines */

//#include "lex.yy.c" 
 
/* yacc error handler */

void yyerror (const char *str) {
  fprintf (stderr, "error: %s\n", str);
  //fprintf (stderr, "error: %s\n", yytext);
}
 
int yywrap (void) {
  return 1;
} 
  
/* Declarations */
void set_input_string (const char * in);
void end_lexical_scan (void);

/* This function parses a string */
int parse_string (const char * in) {
  //yydebug = 1;
  set_input_string (in);
  int rv = yyparse ();
  end_lexical_scan ();
  return rv;
}

