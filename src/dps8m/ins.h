/*
 * vim: filetype=c:tabstop=4:ai:expandtab
 * SPDX-License-Identifier: ICU
 * scspell-id: 838fb98b-f62e-11ec-8e20-80ee73e9b8e7
 *
 * ---------------------------------------------------------------------------
 *
 * Copyright (c) 2007-2013 Michael Mondy
 * Copyright (c) 2012-2016 Harry Reed
 * Copyright (c) 2013-2024 Charles Anthony
 * Copyright (c) 2021-2024 The DPS8M Development Team
 *
 * This software is made available under the terms of the ICU License.
 * See the LICENSE.md file at the top-level directory of this distribution.
 *
 * ---------------------------------------------------------------------------
 */

void tidy_cu (cpuState_t * cpup);
void cu_safe_store(cpuState_t * cpup);
#if defined(MATRIX)
void initializeTheMatrix (void);
void addToTheMatrix (uint32_t opcode, bool opcodeX, bool a, word6 tag);
simRc display_the_matrix (int32_t arg, const char * buf);
#endif /* if defined(MATRIX) */
simRc prepareComputedAddress (void);   // new
void cu_safe_restore(cpuState_t * cpup);
void fetchInstruction(cpuState_t * cpup, word18 addr);
simRc executeInstruction (cpuState_t * cpup);
void doRCU (cpuState_t * cpup) NO_RETURN;
void traceInstruction (cpuState_t * cpup);
bool tstOVFfault (cpuState_t * cpup);
bool chkOVF (cpuState_t * cpup);
