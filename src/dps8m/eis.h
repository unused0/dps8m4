/*
 * vim: filetype=c:tabstop=4:ai:expandtab
 * SPDX-License-Identifier: ICU
 * scspell-id: 359ee1b9-f62e-11ec-b168-80ee73e9b8e7
 *
 * ---------------------------------------------------------------------------
 *
 * Copyright (c) 2007-2013 Michael Mondy
 * Copyright (c) 2012-2016 Harry Reed
 * Copyright (c) 2013-2024 Charles Anthony
 * Copyright (c) 2021-2024 The DPS8M Development Team
 *
 * This software is made available under the terms of the ICU License.
 * See the LICENSE.md file at the top-level directory of this distribution.
 *
 * ---------------------------------------------------------------------------
 */

void setupEISoperands (cpuState_t * cpup);
void abd (cpuState_t * cpup);
void awd (cpuState_t * cpup);
void a4bd (cpuState_t * cpup);
void axbd (cpuState_t * cpup, uint sz);

void sbd (cpuState_t * cpup);
void swd (cpuState_t * cpup);
void s4bd (cpuState_t * cpup);
void s9bd (cpuState_t * cpup);
void sxbd (cpuState_t * cpup, uint sz);
void asxbd (cpuState_t * cpup, uint sz, bool sub);

void s4bd (cpuState_t * cpup);
void cmpc (cpuState_t * cpup);
void scd (cpuState_t * cpup);
void scdr (cpuState_t * cpup);
void scm (cpuState_t * cpup);
void scmr (cpuState_t * cpup);
void tct (cpuState_t * cpup);
void tctr (cpuState_t * cpup);
void mlr (cpuState_t * cpup);
void mrl (cpuState_t * cpup);
void mve (cpuState_t * cpup);
void mvne (cpuState_t * cpup);
void mvt (cpuState_t * cpup);
void cmpn (cpuState_t * cpup);
void mvn (cpuState_t * cpup);
//void csl (bool isSZTL);
void csl (cpuState_t * cpup);
void sztl (cpuState_t * cpup);
void csr (cpuState_t * cpup);
void sztr (cpuState_t * cpup);
void cmpb (cpuState_t * cpup);
void btd (cpuState_t * cpup);
void dtb (cpuState_t * cpup);
void ad2d (cpuState_t * cpup);
void ad3d (cpuState_t * cpup);
void sb2d (cpuState_t * cpup);
void sb3d (cpuState_t * cpup);
void mp2d (cpuState_t * cpup);
void mp3d (cpuState_t * cpup);
void dv2d (cpuState_t * cpup);
void dv3d (cpuState_t * cpup);
