
#define AZ(n) ((char) ((n) + 'A'))

typedef unsigned int uint;

enum states { stateOff, stateOn };

enum opcStates { opcStateOn, opcStateAlt, opcStateIO, opcStateInop };

///
/// SCU
///

// Is a SCU port connected to a CPU or an IOM?
enum scuPortConnectionType { scuPortCPU, scuPortIOM };

// Since all SCU ports must be configured the same for all SCUs,
// abstract the port connection data out of per-scu and into
// a generic SCU
// Also, AM81 pg 7-14 mem sez:
//   These mem records must me placed
//   in the config file in the order in which the memories are
//   configured, the lowest order memory (lowest address) first
//   and the highest (highest address) last.
// So we need to track the order so that we can generate a
// correct config deck

typedef struct scuPort_s {
  bool portConnected[N_SCU_PORTS];
  enum scuPortConnectionType scuPortConnectionsType[N_SCU_PORTS];
  // Which CPU or IOM port is the SCU port conencted to?
  uint scuPortConnectionIdx[N_SCU_PORTS];
  uint nConnected; // Track the number of connected SCUs to record the order
} scuPort_t;

// For each SCU
typedef struct scu_s {
  uint connectionOrder; // The order in which this scu was connected; driven by nConnected
  uint size;
  enum states state;
} scu_t2;

///
/// CPU
///

enum cpuTypes { cpuL68, cpuDPS8, cpuDPS };

///
/// Controllers
///

// enums are started at 0; so N_CTLR_TYPES in the number of enumerated items,
// not including itself

enum ctlrTypes { ctlrIPC, ctlrMTP, ctlrMSP, ctlrURP, ctlrDIA, N_CTLR_TYPES };
enum prphTypes { prphCCU, prphDIA, prphFNP, prphOPC, prphPUN, prphRDR, prphPRT, prphDSK, prphTAP, N_PRPH_TYPES };

typedef struct ctlr_s {
  bool ctlrConnected; // Connected to an IOM channel

  uint ctlrIdx; // Which controller am I?  (A-Z)
  enum ctlrTypes ctlrType; // Is this needed?
  uint ctlrModel;
  // This is more commplicated
  //uint iomIdx; // Which IOM am I connected to?
  //uint chan; // Which chan of that IOM
} ctlr_t;

typedef struct prph_s {
  bool prphConnected; // Connected to an IOM channel

  uint prphIdx; // Which controller am I?  (A-Z)
  enum prphTypes prphType;
  uint prphModel;
  // This is more commplicated
  //uint iomIdx; // Which IOM am I connected to?
  //uint chan; // Which chan of that IOM
} prph_t;

///
/// IOM
///

enum iomModels { iomIOM, iomIMU };

typedef struct iom_s {
   bool iomConnected;
   bool channelsConnected [N_IOM_CHANS]; // Channel is connected to a controller
   enum ctlrTypes ctlrTypes [N_IOM_CHANS]; // What type of controller
   uint ctlrIdxs [N_IOM_CHANS]; // Which controller is the channel connected to
   enum iomModels iomModel; // IOM or IMU
   enum states state;  // Track this for automated config deck updates
} iom_t;

///
/// Cables
///

typedef struct {
  ctlr_t ctlrs[N_CTLR_TYPES][N_CTLRS];
  prph_t prphs[N_PRPH_TYPES][N_PRPHS];
  iom_t ioms[N_IOMS];
  scuPort_t scuPorts;
  scu_t2 scus[N_SCUS];
  int nIPCs; // Number of IPC controllers.
} config_t;

extern config_t config;

///
/// Function prototypes
///

int parse_string(const char * in);

void configCmdReset (void);
// MSPx, MTPx, URPx
int configCmdCtlr (word36 ctlrIdx, enum ctlrTypes ctlrType, word36 ctlrModel, word36 iomIdx,
                word36 chan0, word36 nchan0,
                word36 chan1, word36 nchan1,
                word36 chan2, word36 nchan2,
                word36 chan3, word36 nchan3);
/* IPC */
int configCmdIpcCtlr (word36 iomIdx, word36 chan, word36 nchan);
/* CCUx DIAx PUNx RDRx */
int configCmdPrph0 (word36 prphIdx, enum prphTypes prphType, word36 iomIdx, word36 chan, word36 model);
/* TAPx, DSKx */
int configCmdPrph1 (word36 prphIdx, enum prphTypes prphType, word36 iomIdx, word36 chan, word36 nchan, word36 nsets, word36 model0, word36 d0, word36 model1, word36 d1, word36 model2, word36 d2, word36 model3, word36 d3, word36 model4, word36 d4);
/* FNPx */
int configCmdPrph2 (word36 prphIdx, enum prphTypes prphType, word36 iomIdx, word36 chan, word36 model, enum states state);
/* OPCx */
int configCmdPrph3 (word36 prphIdx, enum prphTypes prphType, word36 iomIdx, word36 chan, word36 model, word36 lineLength, enum opcStates opcState, bool mask);
/* PRTx */
int configCmdPrph4 (word36 prphIdx, enum prphTypes prphType, word36 iomIdx, word36 chan, word36 model, word36 train, word36 lineLength);
/* IOMx */
int configCmdIom (word36 iomIdx, word36 port, enum iomModels iomModel, enum states state);
/* CPUx */
int configCmdCpu (word36 cpuIdx, word36 port, enum states state, enum cpuTypes cpuType, word36 cpuModel, word36 cacheSize);
/* MEMx */
int configCmdMem (word36 memIdx, word36 memsize, enum states state);
/* CHNL */
int configCmdChnl (word36 prphIdx, enum prphTypes prphType, word36 iomIdx, word36 chan0, word36 nchan0, word36 chan1, word36 nchan1, word36 chan2, word36 nchan2, word36 chan3, word36 nchan3);

int evalNumber (const char * s, word36 * v);

int cmdExecute (const char * cmd);
