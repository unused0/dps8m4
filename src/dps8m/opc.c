/*
 * vim: filetype=c:tabstop=4:ai:expandtab
 * SPDX-License-Identifier: ICU
 * SPDX-License-Identifier: Multics
 * scspell-id: 6421764a-f62d-11ec-b542-80ee73e9b8e7
 *
 * ---------------------------------------------------------------------------
 *
 * Copyright (c) 2007-2013 Michael Mondy
 * Copyright (c) 2012-2016 Harry Reed
 * Copyright (c) 2013-2024 Charles Anthony
 * Copyright (c) 2021-2024 The DPS8M Development Team
 *
 * This software is made available under the terms of the ICU License.
 * See the LICENSE.md file at the top-level directory of this distribution.
 *
 * ---------------------------------------------------------------------------
 *
 * This source file may contain code comments that adapt, include, and/or
 * incorporate Multics program code and/or documentation distributed under
 * the Multics License.  In the event of any discrepancy between code
 * comments herein and the original Multics materials, the original Multics
 * materials should be considered authoritative unless otherwise noted.
 * For more details and historical background, see the LICENSE.md file at
 * the top-level directory of this distribution.
 *
 * ---------------------------------------------------------------------------
 */

#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#if !defined(__MINGW64__) && !defined(__MINGW32__)
# include <termios.h>
#endif /* if !defined(__MINGW64__) && !defined(__MINGW32__) */
#include <ctype.h>

#include "dps8m.h"
#include "iom_channel.h"
#include "iom.h"
#include "cmd.h"
#include "opc.h"
#include "cable.h"
#include "faults.h"
#include "tap.h"  // attachTape
#include "dsk.h"  // attachDisk
#include "utils.h"
#include "threadz.h"
#include "main.h"

#include "libtelnet.h"
#if defined(CONSOLE_FIX)
# include "threadz.h"
#endif /* if defined(CONSOLE_FIX) */

#if defined(SIM_NAME)
# undef SIM_NAME
#endif /* if defined(SIM_NAME) */
#define SIM_NAME "DPS8M"

#define ASSUME0 0

// config switch -- The bootload console has a 30-second timer mechanism. When
// reading from the console, if no character is typed within 30 seconds, the
// read operation is terminated. The timer is controlled by an enable switch,
// must be set to enabled during Multics and BCE

static simRc opc_reset (DEVICE * dptr);
static simRc opc_show_nunits (FILE *st, UNIT *uptr, int val,
                                 const void *desc);
static simRc opc_set_nunits (UNIT * uptr, int32_t value, const char * cptr,
                                void * desc);
static simRc opc_autoinput_set (UNIT *uptr, int32_t val, const char *cptr,
                                void *desc);
static simRc opc_autoinput_show (FILE *st, UNIT *uptr, int val,
                                 const void *desc);
static simRc opc_set_config (UNUSED UNIT *  uptr, UNUSED int32_t value,
                              const char * cptr, UNUSED void * desc);
static simRc opc_show_config (UNUSED FILE * st, UNUSED UNIT * uptr,
                               UNUSED int  val, UNUSED const void * desc);
static simRc opc_set_console_port (UNIT * uptr, UNUSED int32_t value,
                                    const char * cptr, UNUSED void * desc);
static simRc opc_show_console_port (UNUSED FILE * st, UNIT * uptr,
                                       UNUSED int val, UNUSED const void * desc);
static simRc opc_set_console_address (UNIT * uptr, UNUSED int32_t value,
                                    const char * cptr, UNUSED void * desc);
static simRc opc_show_console_address (UNUSED FILE * st, UNIT * uptr,
                                       UNUSED int val, UNUSED const void * desc);
static simRc opc_set_console_pw (UNIT * uptr, UNUSED int32_t value,
                                    const char * cptr, UNUSED void * desc);
static simRc opc_show_console_pw (UNUSED FILE * st, UNIT * uptr,
                                       UNUSED int val, UNUSED const void * desc);
static simRc opc_set_device_name (UNIT * uptr, UNUSED int32_t value,
                                   const char * cptr, UNUSED void * desc);
static simRc opc_show_device_name (UNUSED FILE * st, UNIT * uptr,
                                    UNUSED int val, UNUSED const void * desc);

static MTAB opc_mtab[] =
  {
    {
       MTAB_unit_nouc,     /* Mask          */
       0,                  /* Match         */
       "AUTOINPUT",        /* Print string  */
       "AUTOINPUT",        /* Match pstring */
       opc_autoinput_set,
       opc_autoinput_show,
       NULL,
       NULL
    },

    {
      MTAB_dev_valr,                        /* Mask               */
      0,                                    /* Match              */
      "NUNITS",                             /* Print string       */
      "NUNITS",                             /* Match string       */
      opc_set_nunits,                       /* Validation routine */
      opc_show_nunits,                      /* Display routine    */
      "Number of OPC units in the system",  /* Value descriptor   */
      NULL                                  /* Help               */
    },

    {
      MTAB_unit_uc,                   /* Mask               */
      0,                              /* Match              */
      (char *) "CONFIG",              /* Print string       */
      (char *) "CONFIG",              /* Match string       */
      opc_set_config,                 /* Validation routine */
      opc_show_config,                /* Display routine    */
      NULL,                           /* Value descriptor   */
      NULL,                           /* Help               */
    },
    {
      MTAB_XTD | MTAB_VUN | \
      MTAB_VALR | MTAB_NC,            /* Mask               */
      0,                              /* Match              */
      "NAME",                         /* Print string       */
      "NAME",                         /* Match string       */
      opc_set_device_name,            /* Validation routine */
      opc_show_device_name,           /* Display routine    */
      "Set the device name",          /* Value descriptor   */
      NULL                            /* Help               */
    },

    {
      MTAB_unit_valr_nouc,            /* Mask               */
      0,                              /* Match              */
      "PORT",                         /* Print string       */
      "PORT",                         /* Match string       */
      opc_set_console_port,           /* validation routine */
      opc_show_console_port,          /* Display routine    */
      "Set the console port number",  /* Value descriptor   */
      NULL                            /* Help               */
    },

    {
      MTAB_unit_valr_nouc,            /* Mask               */
      0,                              /* Match              */
      "ADDRESS",                      /* Print string       */
      "ADDRESS",                      /* Match string       */
      opc_set_console_address,        /* Validation routine */
      opc_show_console_address,       /* Display routine    */
      "Set the console IP Address",   /* Value descriptor   */
      NULL                            /* Help               */
    },

    {
      MTAB_unit_valr_nouc,            /* Mask               */
      0,                              /* Match              */
      "PW",                           /* Print string       */
      "PW",                           /* Match string       */
      opc_set_console_pw,             /* Validation routine */
      opc_show_console_pw,            /* Display routine    */
      "Set the console password",     /* Value descriptor   */
      NULL                            /* Help               */
    },

    MTAB_eol
};

// Multics only supports a single operator console; but
// it is possible to run multiple Multics instances in a
// cluster. It is also possible to route message output to
// alternate console(s) as I/O devices.

#define N_OPC_UNITS 1 // default
#define OPC_UNIT_IDX(uptr) ((uptr) - opc_unit)

// sim_activate counts in instructions, is dependent on the execution
// model
// The sim_activate calls are done by the controller thread, which
// has a 1000Hz cycle rate.
// 1K ~= 1 sec
# define ACTIVATE_1SEC 1000

static simRc opc_svc (UNIT * unitp);

UNIT opc_unit[N_OPC_UNITS_MAX] = {
#if defined(NO_C_ELLIPSIS)
  { UDATA (& opc_svc, 0, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL },
  { UDATA (& opc_svc, 0, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL },
  { UDATA (& opc_svc, 0, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL },
  { UDATA (& opc_svc, 0, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL },
  { UDATA (& opc_svc, 0, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL },
  { UDATA (& opc_svc, 0, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL },
  { UDATA (& opc_svc, 0, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL },
  { UDATA (& opc_svc, 0, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL }
#else
  [0 ... N_OPC_UNITS_MAX - 1] = {
    UDATA (& opc_svc, 0, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL
  }
#endif
};

DEVICE opc_dev = {
    "OPC",         /* Name                */
    opc_unit,      /* Units               */
    NULL,          /* Registers           */
    opc_mtab,      /* Modifiers           */
    N_OPC_UNITS,   /* #units              */
    10,            /* Address radix       */
    8,             /* Address width       */
    1,             /* Address increment   */
    8,             /* Address width       */
    8,             /* Data width          */
    NULL,          /* Examine routine     */
    NULL,          /* Deposit routine     */
    opc_reset,     /* Reset routine       */
    NULL,          /* Boot routine        */
    NULL,          /* Attach routine      */
    NULL,          /* Detach routine      */
    NULL,          /* Context             */
    0,             /* Flags               */
    0,             /* Debug control flags */
    NULL,          /* Debug flag names    */
    NULL,          /* Memory size change  */
    NULL,          /* Logical name        */
    NULL,          /* Help                */
    NULL,          /* Attach help         */
    NULL,          /* Help context        */
    NULL,          /* Description         */
    NULL           /* End                 */
};

enum console_model { m6001 = 0, m6004 = 1, m6601 = 2 };

// Hangs off the device structure
typedef struct opc_state_t
  {
    // Track progress of reads through the autoinput buffer
    unsigned char *tailp;
    unsigned char *readp;

    // Autoinput buffer pointers
    unsigned char *auto_input;
    unsigned char *autop;

    // stuff saved from the Read ASCII command
    time_t startTime;
    UNIT * unitp;

    // telnet connection to console
    uv_access console_access;

    enum console_model model;
    enum console_mode { opc_no_mode, opc_read_mode, opc_write_mode } io_mode;

    // stuff saved from the Read ASCII command
    uint tally;
    uint daddr;
    int chan;

    // ^T
    // Generate "accept" command when dial_ctl announces dialin console
    int autoaccept;
    // Replace empty console input with "@"
    int noempty;
    // ATTN flushes typeahead buffer
    int attn_flush;
    // Run in background (don't read the keyboard(
    int bg;
    int rcpwatch;

    int simh_buffer_cnt;

    // Track the carrier position to allow tab expansion
    // (If the left margin is 1, then the tab stops are 11, 21, 31, 41, ...)
    int carrierPosition;

    bool echo;

    bool attn_pressed;
    bool simh_attn_pressed;

    bool bcd;

    // Handle escape sequence
    bool escapeSequence;

    char device_name [MAX_DEV_NAME_LEN];

// Multics does console reads with a tally of 64 words; so 256 characters + NUL.
// If the tally is smaller than the contents of the buffer, sendConsole will
// issue a warning and discard the excess.
#define bufsize 257
    unsigned char keyboardLineBuffer[bufsize];
    bool tabStops [bufsize];

#define simh_buffer_sz 4096
    char simh_buffer[simh_buffer_sz];
 } opc_state_t;

static opc_state_t console_state[N_OPC_UNITS_MAX];

static char * bcd_code_page =
  "01234567"
  "89[#@;>?"
  " ABCDEFG"
  "HI&.](<\\"
  "^JKLMNOP"
  "QR-$*);'"
  "+/STUVWX"
  "YZ_,%=\"!";

//
// Typeahead buffer
//

#if !defined(TA_BUFFER_SIZE)
# define TA_BUFFER_SIZE 65536
#endif

static int ta_buffer[TA_BUFFER_SIZE];
static uint ta_cnt  = 0;
static uint ta_next = 0;
static bool ta_ovf  = false;

static void ta_flush (void)
  {
    ta_cnt = ta_next = 0;
    ta_ovf = false;
  }

static void ta_push (int c)
  {
    // discard overflow
    if (ta_cnt >= TA_BUFFER_SIZE)
      {
        if (! ta_ovf)
          sim_print ("typeahead buffer overflow");
        ta_ovf = true;
        return;
      }
    ta_buffer [ta_cnt ++] = c;
  }

static int ta_peek (void)
  {
    if (ta_next >= ta_cnt)
      return SCPE_OK;
    int c = ta_buffer[ta_next];
    return c;
  }

static int ta_get (void)
  {
    if (ta_next >= ta_cnt)
      return SCPE_OK;
    int c = ta_buffer[ta_next ++];
    if (ta_next >= ta_cnt)
      ta_flush ();
    return c;
  }

static simRc opc_reset (UNUSED DEVICE * dptr)
  {
    for (uint i = 0; i < N_OPC_UNITS_MAX; i ++)
      {
        console_state[i].io_mode         = opc_no_mode;
        console_state[i].tailp           = console_state[i].keyboardLineBuffer;
        console_state[i].readp           = console_state[i].keyboardLineBuffer;
        console_state[i].carrierPosition = 1;
        (void) memset (console_state[i].tabStops, 0, sizeof (console_state[i].tabStops));
        console_state[i].escapeSequence  = false;
      }
    return SCPE_OK;
  }

int check_attn_key (void)
  {
    for (uint i = 0; i < opc_dev.numunits; i ++)
      {
        opc_state_t * csp = console_state + i;
        if (csp->attn_pressed)
          {
             csp->attn_pressed = false;
             sim_usleep (1000);
             return (int) i;
          }
      }
    return -1;
  }

// Once-only initialization

void console_init (void)
  {
    opc_reset (& opc_dev);
    for (uint i = 0; i < N_OPC_UNITS_MAX; i ++)
      {
        opc_state_t * csp      = console_state + i;
        csp->model             = m6001;
        csp->auto_input        = NULL;
        csp->autop             = NULL;
        csp->attn_pressed      = false;
        csp->simh_attn_pressed = false;
        csp->simh_buffer_cnt   = 0;
        strcpy (csp->console_access.pw, "MulticsRulez");

        csp->autoaccept      = 0;
        csp->noempty         = 0;
        csp->attn_flush      = 1;
        csp->bg              = 0;
        csp->rcpwatch        = 1;
        csp->carrierPosition = 1;
        csp->escapeSequence  = 1;
        (void) memset (csp->tabStops, 0, sizeof (csp->tabStops));
      }
  }

// Once-only shutdown

void console_exit (void) {
  for (uint i = 0; i < N_OPC_UNITS_MAX; i ++) {
    opc_state_t * csp = console_state + i;
    if (csp->auto_input) {
      FREE (csp->auto_input);
      csp->auto_input = NULL;
    }
    if (csp->console_access.telnetp) {
      simWarn ("console_exit freeing console %u telnetp %p\n", i, csp->console_access.telnetp);
      telnet_free (csp->console_access.telnetp);
      csp->console_access.telnetp = NULL;
    }
  }
}

static int opc_autoinput_set (UNIT * uptr, UNUSED int32_t val,
                                const char *  cptr, UNUSED void * desc)
  {
    int devUnitIdx = (int) OPC_UNIT_IDX (uptr);
    opc_state_t * csp = console_state + devUnitIdx;

    if (cptr)
      {
        unsigned char * new = (unsigned char *) strdupesc (cptr);
        if (csp-> auto_input)
          {
            size_t nl = strlen ((char *) new);
            size_t ol = strlen ((char *) csp->auto_input);

            unsigned char * old = realloc (csp->auto_input, nl + ol + 1);
            MALLOC_CHECK (old);
            strcpy ((char *) old + ol, (char *) new);
            csp->auto_input = old;
            FREE (new);
          }
        else
          csp->auto_input = new;
      }
    else
      {
        if (csp->auto_input)
          FREE (csp->auto_input);
        csp->auto_input = NULL;
      }
    csp->autop = csp->auto_input;
    return SCPE_OK;
  }

int clear_opc_autoinput (int32_t flag, UNUSED const char * cptr)
  {
    opc_state_t * csp = console_state + flag;
    if (csp->auto_input)
      FREE (csp->auto_input);
    csp->auto_input = NULL;
    csp->autop = csp->auto_input;
    return SCPE_OK;
  }

int add_opc_autoinput (int32_t flag, const char * cptr)
  {
    opc_state_t * csp = console_state + flag;
    unsigned char * new = (unsigned char *) strdupesc (cptr);
    if (csp->auto_input)
      {
        size_t nl = strlen ((char *) new);
        size_t ol = strlen ((char *) csp->auto_input);

        unsigned char * old = realloc (csp->auto_input, nl + ol + 1);
        MALLOC_CHECK (old);
        strcpy ((char *) old + ol, (char *) new);
        csp->auto_input = old;
        FREE (new);
      }
    else
      csp->auto_input = new;
    csp->autop = csp->auto_input;
    return SCPE_OK;
  }

// autostream token
//   text
// token

int add_opc_autostream (int32_t flag, const char * cptr)
  {
    opc_state_t * csp = console_state + flag;
    for (;;)
      {
        char cbuf [4 * CBUFSIZE];
        char * p = read_line (cbuf, sizeof (cbuf) - 1, sim_fpin);
        if (! p) // eof
          break;
        if (strcmp (cptr, p) == 0)
          break;
        strcat (p, "\n");
        unsigned char * new = (unsigned char *) strdupesc (p);
        if (csp->auto_input)
          {
            size_t nl = strlen ((char *) new);
            size_t ol = strlen ((char *) csp->auto_input);

            unsigned char * old = realloc (csp->auto_input, nl + ol + 1);
            strcpy ((char *) old + ol, (char *) new);
            csp->auto_input = old;
            free (new);
          }
        else
          csp->auto_input = new;
      }
    csp->autop = csp->auto_input;
    return SCPE_OK;
  }

static int opc_autoinput_show (UNUSED FILE * st, UNIT * uptr,
                                 UNUSED int val, UNUSED const void * desc)
  {
    int conUnitIdx = (int) OPC_UNIT_IDX (uptr);
    opc_state_t * csp = console_state + conUnitIdx;
    if (csp->auto_input)
      sim_print ("autoinput: '%s'", csp->auto_input);
    else
      sim_print ("autoinput: empty");
    return SCPE_OK;
  }

static simRc console_attn (UNUSED UNIT * uptr);

static UNIT attn_unit[N_OPC_UNITS_MAX] = {
#if defined(NO_C_ELLIPSIS)
  { UDATA (& console_attn, 0, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL },
  { UDATA (& console_attn, 0, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL },
  { UDATA (& console_attn, 0, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL },
  { UDATA (& console_attn, 0, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL },
  { UDATA (& console_attn, 0, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL },
  { UDATA (& console_attn, 0, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL },
  { UDATA (& console_attn, 0, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL },
  { UDATA (& console_attn, 0, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL }
#else
  [0 ... N_OPC_UNITS_MAX - 1] = {
    UDATA (& console_attn, 0, 0), 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL
  }
#endif
};

static simRc console_attn (UNUSED UNIT * uptr)
  {
    uint con_unit_idx  = (uint) (uptr - attn_unit);
    uint ctlr_port_num = 0; // Consoles are single ported
    uint iomUnitIdx  = cables->opc_to_iom[con_unit_idx][ctlr_port_num].iom_unit_idx;
    uint chanNum      = cables->opc_to_iom[con_unit_idx][ctlr_port_num].chan_num;
    uint devCode      = 0; // Only a single console on the controller
    chnlState_t * chnlp = & iomChanData[iomUnitIdx][chanNum];

    send_special_interrupt (chnlp, devCode, 0, 0);
    return SCPE_OK;
  }

void console_attn_idx (int conUnitIdx)
  {
    console_attn (attn_unit + conUnitIdx);
  }

#if 0
#if !defined(__MINGW64__) && !defined(__MINGW32__)
static struct termios ttyTermios;
static bool ttyTermiosOk = false;

static void newlineOff (void)
  {
    if (! isatty (0))
      return;
    if (! ttyTermiosOk)
      {
        int rc = tcgetattr (0, & ttyTermios); /* get old flags */
        if (rc)
           return;
        ttyTermiosOk = true;
      }
    struct termios runtty;
    runtty = ttyTermios;
    runtty.c_oflag &= (unsigned int) ~OPOST; /* no output edit */
# if defined(__ANDROID__)
#  define TCSA_TYPE TCSANOW
    (void)fflush(stdout);
    (void)fflush(stderr);
# else
#  define TCSA_TYPE TCSAFLUSH
# endif
    tcsetattr (0, TCSA_TYPE, & runtty);
  }

static void newlineOn (void)
  {
    if (! isatty (0))
      return;
    if (! ttyTermiosOk)
      return;
# if defined(__ANDROID__)
    (void)fflush(stdout);
    (void)fflush(stderr);
# endif
    tcsetattr (0, TCSA_TYPE, & ttyTermios);
  }
#endif /* if !defined(__MINGW64__) && !defined(__MINGW32__) */
#endif

static void handleRCP (uint con_unit_idx, char * text)
  {
// It appears that Cygwin doesn't grok "%ms"
#if 0
    char * label = NULL;
    char * with = NULL;
    char * drive = NULL;
// 1750.1  RCP: Mount Reel 12.3EXEC_CF0019_1 without ring on tapa_01
    int rc = sscanf (text, "%*d.%*d RCP: Mount Reel %ms %ms ring on %ms",
                & label, & with, & drive);
#endif
    //size_t len = strlen (text);
    //char   label [len + 1];
    //char   with  [len + 1];
    //char   drive [len + 1];
#define TOKLEN 128
#define TOKLENQ "128"
    char   label [TOKLEN + 1];
    char   with  [TOKLEN + 1];
    char   drive [TOKLEN + 1];
    int rc = sscanf (text, "%*d.%*d RCP: Mount Reel %" TOKLENQ "s %" TOKLENQ "s ring on %" TOKLENQ "s",
                     label, with, drive);
    if (rc == 3)
      {
        bool withring = (strcmp (with, "with") == 0);
        char labelDotTap[strlen (label) + strlen (".tap") + 1];
        strcpy (labelDotTap, label);
        strcat (labelDotTap, ".tap");
        attachTape (labelDotTap, withring, drive);
        return;
      }

    rc = sscanf (text, "%*d.%*d RCP: Remount Reel %" TOKLENQ "s %" TOKLENQ "s ring on %" TOKLENQ "s",
                label, with, drive);
    if (rc == 3)
      {
        bool withring = (strcmp (with, "with") == 0);
        char labelDotTap [strlen (label) + strlen (".tap") + 1];
        strcpy (labelDotTap, label);
        strcat (labelDotTap, ".tap");
        attachTape (labelDotTap, withring, drive);
        return;
      }

// 1654.1  RCP: Mount Pack test without protect on dskb_05 for Repair.SysAdmin.a

    rc = sscanf (text, "%*d.%*d RCP: Mount Pack %s %s protect on %s",
                label, with, drive);
    if (rc == 3)
      {
        bool with_protect = (strcmp (with, "with") == 0);
        char label_dot_dsk [strlen (label) + strlen (".dsk") + 1];
        strcpy (label_dot_dsk, label);
        strcat (label_dot_dsk, ".dsk");
        attach_disk (label_dot_dsk, with_protect, drive);
        return;
      }

//  1629  as   dial_ctl_: Channel d.h000 dialed to Initializer

    if (console_state[con_unit_idx].autoaccept)
      {
        rc = sscanf (text, "%*d  as   dial_ctl_: Channel %" TOKLENQ "s dialed to Initializer",
                     label);
        if (rc == 1)
          {
            //simPrintf (" dial system <%s>\n", label);
            opc_autoinput_set (opc_unit + con_unit_idx, 0, "accept ", NULL);
            opc_autoinput_set (opc_unit + con_unit_idx, 0, label, NULL);
            opc_autoinput_set (opc_unit + con_unit_idx, 0, "\r", NULL);
// XXX This is subject to race conditions
            if (console_state[con_unit_idx].io_mode != opc_read_mode)
              console_state[con_unit_idx].attn_pressed = true;
            return;
          }
      }
  }

#if OSCAR == 1
static void oscar (char * text) { 
  char prefix[] = "log oscar ";
  if (strncmp (text, prefix, strlen (prefix)))
    return; 
  //do_cmd (0, text + strlen (prefix));
  char * cptr = text + strlen (prefix);
  char gbuf[257];
  cptr = (char *) get_glyph (cptr, gbuf, 0);         /* get command glyph */
  if (strlen (gbuf) == 0)
    return;
  CTAB *cmdp;
  if ((cmdp = find_cmd (gbuf))) {                     /* lookup command */
    simRc stat = cmdp->action (cmdp->arg, cptr);  /* if found, exec */
    if (stat == SCPE_OK)
      simPrintf ("oscar thinks that's ok.\n");
    else
      simWarn ("oscar thinks %d\n", stat);
    }
  else
    simPrintf ("oscar says huh?\n");
}
#endif

// Send entered text to the IOM.
static void sendConsole (int conUnitIdx, word12 stati)
  {
    opc_state_t * csp   = console_state + conUnitIdx;
    uint tally          = csp->tally;
    uint ctlr_port_num  = 0; // Consoles are single ported
    uint iomUnitIdx     = cables->opc_to_iom[conUnitIdx][ctlr_port_num].iom_unit_idx;
    uint chan_num       = cables->opc_to_iom[conUnitIdx][ctlr_port_num].chan_num;
    chnlState_t * chnlp = & iomChanData[iomUnitIdx][chan_num];

    //ASSURE (csp->io_mode == opc_read_mode);
    if (csp->io_mode != opc_read_mode)
      {
        simWarn ("%s called with io_mode != opc_read_mode (%d)\n",
                  __func__, csp->io_mode);
        return;
      }

#if OSCAR == 1
    char text[257];
    int ntext;
    for (ntext = 0; ntext < 256; ntext ++)
      {
        if (csp->readp + ntext >= csp->tailp)
          break;
        text[ntext] = (char) (* (csp->readp + ntext));
      }
    text[ntext] = 0;
    oscar (text);
#endif

    uint n_chars = (uint) (csp->tailp - csp->readp);
    uint n_words;
    if (csp->bcd)
        // + 2 for the !1 newline
        n_words = ((n_chars+2) + 5) / 6;
      else
        n_words = (n_chars + 3) / 4;
    // The "+1" is for them empty line case below
    word36 buf[n_words + 1];
    // Make Oracle lint happy
    (void) memset (buf, 0, sizeof (word36) * (n_words + 1));
    word36 * bufp = buf;

    // XXX
    // Multics doesn't seem to like empty lines; it the line buffer
    // is empty and there is room in the I/O buffer, send a line kill.
    if ((!csp->bcd) && csp->noempty && n_chars == 0 && tally)
      {
        n_chars = 1;
        n_words = 1;
        putbits36_9 (bufp, 0, '@');
        tally --;
      }
    else
      {
        int bcd_nl_state = 0;
        while (tally && csp->readp < csp->tailp)
          {
            if (csp->bcd)
              {
                //* bufp = 0171717171717ul;
                //* bufp = 0202020202020ul;
                * bufp = 0;
                for (uint charno = 0; charno < 4; ++ charno)
                  {
                    unsigned char c;
                    if (csp->readp >= csp->tailp)
                      {
                        if (bcd_nl_state == 0)
                          {
                            c = '!';
                            bcd_nl_state = 1;
                          }
                        else if (bcd_nl_state == 1)
                          {
                            c = '1';
                            bcd_nl_state = 2;
                          }
                        else
                          break;
                      }
                    else
                      c = (unsigned char) (* csp->readp ++);
                    c = (unsigned char) toupper (c);
                    int i;
                    for (i = 0; i < 64; i ++)
                      if (bcd_code_page[i] == c)
                        break;
                    if (i >= 64)
                      {
                        simWarn ("Character %o does not map to BCD; replacing with '?'\n", c);
                        i = 017;
                      }
                    putbits36_6 (bufp, charno * 6, (word6) i);
                  }
              }
            else
              {
                * bufp = 0ul;
                for (uint charno = 0; charno < 4; ++ charno)
                  {
                    if (csp->readp >= csp->tailp)
                      break;
                    unsigned char c = (unsigned char) (* csp->readp ++);
                    c &= 0177;  // Multics get consternated about this
                    putbits36_9 (bufp, charno * 9, c);
                  }
              }
            bufp ++;
            tally --;
          }
        if (csp->readp < csp->tailp)
          {
            simWarn ("opc_iom_io: discarding %d characters from end of line\n",
                      (int) (csp->tailp - csp->readp));
          }
      }

    iomIndirectDataService (chnlp, buf, & n_words, true);

    chnlp->charPos = n_chars % 4;
    chnlp->stati   = (word12) stati;

    csp->readp   = csp->keyboardLineBuffer;
    csp->tailp   = csp->keyboardLineBuffer;
    csp->io_mode = opc_no_mode;

    send_terminate_interrupt (chnlp);
  }

static void console_putchar (int conUnitIdx, char ch);
static void console_putstr  (int conUnitIdx, char * str);

static char * consoleID[8] = {
  "OPCA",
  "OPCB",
  "OPCC",
  "OPCD",
  "OPCE",
  "OPCF",
  "OPCG",
  "OPCH"
};

// Process characters entered on keyboard or autoinput
static void consoleProcessIdx (int conUnitIdx)
  {
    opc_state_t * csp = console_state + conUnitIdx;
    int c;

//// Move data from keyboard buffers into type-ahead buffer

    for (;;)
      {
        c = csp->bg ? SCPE_OK : sim_poll_kbd ();
        if (c == SCPE_OK)
          c = accessGetChar (& csp->console_access);

        // Check for stop signaled by scp

        if (breakEnable && stop_cpu)
          {
            console_putstr (conUnitIdx,  consoleID[conUnitIdx]);
            console_putstr (conUnitIdx,  " Got <sim stop> \n");
            return;
          }

        // Check for ^E
        //   (Windows doesn't handle ^E as a signal; need to explicitly test
        //   for it.)

        if (breakEnable && c == SCPE_STOP)
          {
            console_putstr (conUnitIdx,  consoleID[conUnitIdx]);
            console_putstr (conUnitIdx,  " Got <sim stop> \n");
            stop_cpu = 1;
            return; // User typed ^E to stop simulation
          }

        // Check for scp break

        if (breakEnable && c == SCPE_BREAK)
          {
            console_putstr (conUnitIdx,  consoleID[conUnitIdx]);
            console_putstr (conUnitIdx,  " Got <sim stop> \n");
            stop_cpu = 1;
            return; // User typed ^E to stop simulation
          }

        // End of available input

        if (c == SCPE_OK)
          break;

        // sanity test

        if (c < SCPE_KFLAG)
          {
            simPrintf ("impossible %d %o\n", c, c);
            continue; // Should be impossible
          }

        // translate to ascii

        int ch = c - SCPE_KFLAG;

        // XXX This is subject to race conditions
        // If the console is not in read mode and ESC or ^A
        // is pressed, signal Multics for ATTN.
        if (csp->io_mode != opc_read_mode)
          {
            if (ch == '\033' || ch == '\001') // escape or ^A
              {
                if (csp->attn_flush)
                  ta_flush ();
                csp->attn_pressed = true;
                continue;
              }
          }

#if 0
        // If Multics has gone seriously awry (eg crash
        // to BCE during boot, the autoinput will become
        // wedged waiting for the expect string 'Ready'
        // Allow the user to signal ATTN when wedged
        // by checking to see if we are waiting on an expect string

        if ((ch == '\033' || ch == '\001') &&  // ESC or ^A
            csp->autop && (*csp->autop == 030 || *csp->autop == 031)) // ^X ^Y
          {
            // User pressed ATTN while expect waiting
            // Clear the autoinput buffer; these will cancel the
            // expect wait and any remaining script, returning
            // control of the console to the user.
            // Assuming opc0.
            clear_opc_autoinput (con_unit_idx, NULL);
            ta_flush ();
            simPrintf ("\nAutoinput and typeahead buffers flushed\n");
            continue;
          }
#endif
        // ^S

        if (ch == 023) // ^S scp command
          {
            if (! csp->simh_attn_pressed)
              {
                ta_flush ();
                csp->simh_attn_pressed = true;
                csp->simh_buffer_cnt = 0;
                console_putstr (conUnitIdx, "^S\n" SIM_NAME "> ");
              }
            continue;
          }

//// ^P  Prompt

        // ATTN is ambiguous; in read mode, it cancels the read.
        // In write mode, is discards output and asks for a prompt.

        // ^P will check the mode, and if it is not input, will do an
        // ATTN signal. There is still a small race window; if Multics
        // is in the process of starting read mode, it may end up canceling
        // the read.

        if (ch == 020) { // ^P
          sim_usleep (1000);
          if (csp->io_mode != opc_read_mode) {
            if (csp->attn_flush)
              ta_flush ();
            csp->attn_pressed = true;
          }
          continue;
        }

//// ^T

        if (ch == 024) // ^T
          {
            char buf[256];
            char cms[3] = "?RW";
            // XXX Assumes console 0
            (void) sprintf (buf, "^T attn %c %c cnt %u next %u\n",
                            console_state[0].attn_pressed+'0',
                            cms[console_state[0].io_mode],
                            ta_cnt, ta_next);
            console_putstr (conUnitIdx, buf);
            (void) sprintf (buf, "syncClockMode %c syncClockModeMasterIdx %u\n",
                            sys.syncClockMode ? 'T' : 'F',
                            sys.syncClockModeMasterIdx);
            console_putstr (conUnitIdx, buf);
            for (int i = 0; i < N_CPU_UNITS_MAX; i ++) {
              (void) sprintf (buf, "CPU %c %s %05o:%06o\n"
                                   "      executing %c syncClockModeMaster %c workAllocation %4ld\n"
                                   "      inMultics %c syncClockModeCache %c isSlave %c\n",
                              "ABCDEFGH"[i],
                              cycleString (cpus[i].cycle),
                              cpus[i].PPR.PSR,
                              cpus[i].PPR.IC,
                              cpus[i].executing ? 'T' : 'F',
                              cpus[i].syncClockModeMaster ? 'T' : 'F',
                              cpus[i].workAllocation,
                              cpus[i].inMultics ? 'T' : 'F',
                              cpus[i].syncClockModeCache ? 'T' : 'F',
                              cpus[i].isSlave ? 'T' : 'F');
              console_putstr (conUnitIdx, buf);
            }
            continue;
          }

//// In ^S mode (accumulating a scp command)?

        if (csp->simh_attn_pressed)
          {
            ta_get ();
            if (ch == '\177' || ch == '\010')  // backspace/del
              {
                if (csp->simh_buffer_cnt > 0)
                  {
                    -- csp->simh_buffer_cnt;
                    csp->simh_buffer[csp->simh_buffer_cnt] = 0;
                    console_putstr (conUnitIdx,  "\b \b");
                  }
                return;
              }

            //// scp ^R

            if (ch == '\022')  // ^R
              {
                console_putstr (conUnitIdx, "^R\n" SIM_NAME "> ");
                for (int i = 0; i < csp->simh_buffer_cnt; i ++) {
                  console_putchar (conUnitIdx, (char) (csp->simh_buffer[i]));
#ifdef REALOPC
                  usleep (303030);
#endif
                }
                return;
              }

            //// scp ^U

            if (ch == '\025')  // ^U
              {
                console_putstr (conUnitIdx, "^U\n" SIM_NAME "> ");
                csp->simh_buffer_cnt = 0;
                return;
              }

            //// scp CR/LF

            if (ch == '\012' || ch == '\015')  // CR/LF
              {
                console_putstr (conUnitIdx,  "\n");
                csp->simh_buffer[csp->simh_buffer_cnt] = 0;

                char * cptr = csp->simh_buffer;
                char gbuf[simh_buffer_sz];
                cptr = (char *) get_glyph (cptr, gbuf, 0); /* get command glyph */
                if (strlen (gbuf))
                  {
                    CTAB *cmdp;
                    if ((cmdp = find_cmd (gbuf))) /* lookup command */
                      {
                        simRc stat = cmdp->action (cmdp->arg, cptr);
                           /* if found, exec */
                        if (stat != SCPE_OK)
                          {
                            char buf[4096];
                            sprintf (buf, "\n%s returned %d '%s'\n",
                              SIM_NAME, stat, sim_error_text (stat));
                            console_putstr (conUnitIdx,  buf);
                          }
                      }
                    else
                       console_putstr (conUnitIdx,
                         "\nUnrecognized " SIM_NAME " command.\n");
                  }
                csp->simh_buffer_cnt   = 0;
                csp->simh_buffer[0]    = 0;
                csp->simh_attn_pressed = false;
                return;
              }

            //// scp ESC/^D/^Z

            if (ch == '\033' || ch == '\004' || ch == '\032')  // ESC/^D/^Z
              {
                console_putstr (conUnitIdx,  "\n" SIM_NAME " cancel\n");
                // Empty input buffer
                csp->simh_buffer_cnt   = 0;
                csp->simh_buffer[0]    = 0;
                csp->simh_attn_pressed = false;
                return;
              }

            //// scp isprint?

            if (isprint (ch))
              {
                // silently drop buffer overrun
                if (csp->simh_buffer_cnt + 1 >= simh_buffer_sz)
                  return;
                csp->simh_buffer[csp->simh_buffer_cnt ++] = (char) ch;
                console_putchar (conUnitIdx, (char) ch);
#ifdef REALOPC
                usleep (303030);
#endif
                return;
              }
            return;
          } // if (simh_attn_pressed)

        // Save the character

        ta_push (c);
      }

//// Check for stop signaled by scp

    if (breakEnable && stop_cpu)
      {
        console_putstr (conUnitIdx,  consoleID[conUnitIdx]);
        console_putstr (conUnitIdx,  " Got <sim stop> \n");
        return;
      }

//// Console is reading and autoinput is ready
////   Move line of text from autoinput buffer to console buffer

    if (csp->io_mode == opc_read_mode &&
        csp->autop != NULL)
      {
        int announce = 1;
        for (;;)
          {
            if (csp->tailp >= csp->keyboardLineBuffer + sizeof (csp->keyboardLineBuffer))
             {
                simWarn ("getConsoleInput: Buffer full; flushing autoinput.\n");
                sendConsole (conUnitIdx, 04000); // Normal status
                return;
              }
            unsigned char c = * (csp->autop);
            if (c == 4) // eot
              {
                FREE (csp->auto_input);
                csp->auto_input = NULL;
                csp->autop      = NULL;
                // Empty input buffer
                csp->readp      = csp->keyboardLineBuffer;
                csp->tailp      = csp->keyboardLineBuffer;
                sendConsole (conUnitIdx, 04310); // Null line, status operator
                                                 // distracted
                console_putstr (conUnitIdx,  consoleID[conUnitIdx]);
                console_putstr (conUnitIdx,  " RELEASED\n");
                sim_usleep (1000);
                return;
              }
            if (c == 030 || c == 031) // ^X ^Y
              {
                // an expect string is in the autoinput buffer; wait for it
                // to be processed
                return;
              }
            if (c == 0)
              {
                FREE (csp->auto_input);
                csp->auto_input = NULL;
                csp->autop      = NULL;
                goto eol;
              }
            if (announce)
              {
                console_putstr (conUnitIdx,  consoleID[conUnitIdx]);
                console_putstr (conUnitIdx,  " [auto-input] ");
                announce = 0;
              }
            csp->autop ++;

            if (c == '\012' || c == '\015')
              {
eol:
                if (csp->echo)
                  console_putstr (conUnitIdx,  "\n");
                sendConsole (conUnitIdx, 04000); // Normal status
                return;
              }
            else
              {
                * csp->tailp ++ = c;
                if (csp->echo) {
                  console_putchar (conUnitIdx, (char) c);
#ifdef REALOPC
                  usleep (303030);
#endif
                }
              }
          } // for (;;)
      } // if (autop)

//// Read mode and nothing in console buffer
////   Check for timeout

    if (csp->io_mode == opc_read_mode &&
        csp->tailp == csp->keyboardLineBuffer)
      {
        if (csp->startTime + 30 <= time (NULL))
          {
            console_putstr (conUnitIdx,  consoleID[conUnitIdx]);
            console_putstr (conUnitIdx,  " TIMEOUT\n");
            sim_usleep (1000);
            csp->readp = csp->keyboardLineBuffer;
            csp->tailp = csp->keyboardLineBuffer;
            sendConsole (conUnitIdx, 04310); // Null line, status operator
                                             // distracted
          }
      }

//// Peek at the character in the typeahead buffer

    c = ta_peek ();

    // No data
    if (c == SCPE_OK)
        return;

    // Convert from scp encoding to ASCII
    if (c < SCPE_KFLAG)
      {
        simPrintf ("impossible %d %o\n", c, c);
        return; // Should be impossible
      }

    // translate to ascii

    int ch = c - SCPE_KFLAG;

// XXX This is subject to race conditions
    if (csp->io_mode != opc_read_mode)
      {
        if (ch == '\033' || ch == '\001') // escape or ^A
          {
            ta_get ();
            csp->attn_pressed = true;
          }
        return;
      }

    if (ch == '\177' || ch == '\010')  // backspace/del
      {
        ta_get ();
        if (csp->tailp > csp->keyboardLineBuffer)
          {
            * csp->tailp = 0;
            -- csp->tailp;
            if (csp->echo)
              console_putstr (conUnitIdx,  "\b \b");
          }
        return;
      }

    if (ch == '\022')  // ^R
      {
        ta_get ();
        if (csp->echo)
          {
            console_putstr (conUnitIdx,  "^R\n");
            for (unsigned char * p = csp->keyboardLineBuffer; p < csp->tailp; p ++) {
              console_putchar (conUnitIdx, (char) (*p));
#ifdef REALOPC
              usleep (303030);
#endif
            }
            return;
          }
      }

    if (ch == '\025')  // ^U
      {
        ta_get ();
        console_putstr (conUnitIdx,  "^U\n");
        csp->tailp = csp->keyboardLineBuffer;
        return;
      }

    if (ch == '\030')  // ^X
      {
        ta_get ();
        console_putstr (conUnitIdx,  "^X\n");
        csp->tailp = csp->keyboardLineBuffer;
        return;
      }

    if (ch == '\012' || ch == '\015')  // CR/LF
      {
        ta_get ();
        if (csp->echo)
          console_putstr (conUnitIdx,  "\n");
        sendConsole (conUnitIdx, 04000); // Normal status
        return;
      }

    if (ch == '\033' || ch == '\004' || ch == '\032')  // ESC/^D/^Z
      {
        ta_get ();
        console_putstr (conUnitIdx,  "\n");
        // Empty input buffer
        csp->readp = csp->keyboardLineBuffer;
        csp->tailp = csp->keyboardLineBuffer;
        sendConsole (conUnitIdx, 04310); // Null line, status operator
                                         // distracted
        console_putstr (conUnitIdx,  consoleID[conUnitIdx]);
        console_putstr (conUnitIdx,  " RELEASED\n");
        sim_usleep (1000);
        return;
      }

    if (isprint (ch))
      {
        // silently drop buffer overrun
        ta_get ();
        if (csp->tailp >= csp->keyboardLineBuffer + sizeof (csp->keyboardLineBuffer))
          return;

        * csp->tailp ++ = (unsigned char) ch;
        if (csp->echo) {
          console_putchar (conUnitIdx, (char) ch);
#ifdef REALOPC
          usleep (303030);
#endif
        }
        return;
      }
    // ignore other chars...
    ta_get ();
    return;
  }

void consoleProcess (void)
  {
    for (int conUnitIdx = 0; conUnitIdx < (int) opc_dev.numunits; conUnitIdx ++)
      consoleProcessIdx (conUnitIdx);
  }

/*
 * opc_iom_cmd ()
 *
 * Handle a device command.  Invoked by the IOM while processing a PCW
 * or IDCW.
 */

iomCmdRc opc_iom_cmd (chnlState_t * chnlp) {
  iomCmdRc rc = IOM_CMD_PROCEED;

  lock_libuv ();

  uint con_unit_idx   = get_ctlr_idx (chnlp);
  UNIT * unitp        = & opc_unit[con_unit_idx];
  opc_state_t * csp   = console_state + con_unit_idx;

  chnlp->dev_code = chnlp->IDCW_DEV_CODE;
  chnlp->stati = 0;
  //int conUnitIdx = (int) d->devUnitIdx;

  // The 6001 only executes the PCW DCW command; the 6601 executes
  // the PCW DCW and (at least) the first DCW list item.
  // When Multics uses the 6601, the PCW DCW is always 040 RESET.
  // The 040 RESET will trigger the DCW list read.
  // will change this.

  // IDCW?
  if (IS_IDCW (chnlp)) {
    // IDCW
    switch (chnlp->IDCW_DEV_CMD) {
      case 000: // CMD 00 Request status
        csp->io_mode = opc_no_mode;
        chnlp->stati     = 04000;
        rc = IOM_CMD_DISCONNECT;
        break;

      case 003:               // Read BCD
        csp->io_mode = opc_read_mode;
        chnlp->recordResidue --;
        csp->echo    = true;
        csp->bcd     = true;
        chnlp->stati     = 04000;
        break;

      case 013:               // Write BCD
        chnlp->isRead    = false;
        csp->bcd     = true;
        csp->io_mode = opc_write_mode;
        chnlp->recordResidue --;
        chnlp->stati     = 04000;
        break;

      case 023:               // Read ASCII
        csp->io_mode = opc_read_mode;
        chnlp->recordResidue --;
        csp->echo    = true;
        csp->bcd     = false;
        chnlp->stati     = 04000;
        break;

      case 033:               // Write ASCII
        chnlp->isRead    = false;
        csp->bcd     = false;
        csp->io_mode = opc_write_mode;
        chnlp->recordResidue --;
        chnlp->stati     = 04000;
        break;

// Model 6001 vs. 6601.
//
// When Multics switches to 6601 mode, the PCW DCW is always 040 RESET; the
// bootload and 6001 code never does that. Therefore, we use the 040
// as an indication that the DCW list should be processed.
// All of the other device commands will return IOM_CMD_DISCONNECT, stopping
// parsing of the channel command program. This one will return IOM_CMD_PROCEED,
// causing the parser to move to the DCW list.

      case 040:               // Reset
        chnlp->stati = 04000;
        // T&D probing
        //if (chnlp->IDCW_DEV_CODE == 077) {
          // T&D uses dev code 77 to test for the console device;
          // it ignores dev code, and so returns OK here.
          //chnlp->stati = 04502; // invalid device code
          // if (chnlp->IDCW_CHAN_CTRL == 0) { simWarn ("%s: TERMINATE_BUG\n", __func__); return IOM_CMD_DISCONNECT; }
        //}
        break;

      case 043:               // Read ASCII unechoed
        csp->io_mode = opc_read_mode;
        chnlp->recordResidue --;
        csp->echo    = false;
        csp->bcd     = false;
        chnlp->stati     = 04000;
        break;

      case 051:               // Write Alert -- Ring Bell
        chnlp->isRead = false;
        console_putstr ((int) con_unit_idx,  consoleID[con_unit_idx]);
        console_putstr ((int) con_unit_idx,  " ALERT\n");
        console_putchar ((int) con_unit_idx, '\a');
#ifdef REALOPC
        usleep (303030);
#endif
        chnlp->stati  = 04000;
        rc = IOM_CMD_DISCONNECT;
        break;

      case 057:               // Read ID (according to AN70-1)
        // FIXME: No support for Read ID; appropriate values are not known
        //[CAC] Looking at the bootload console code, it seems more
        // concerned about the device responding, rather than the actual
        // returned value. Make some thing up.
        chnlp->stati = 04500;
        rc = IOM_CMD_DISCONNECT;
        break;

      case 060:               // LOCK MCA
        console_putstr ((int) con_unit_idx,  consoleID[con_unit_idx]);
        console_putstr ((int) con_unit_idx,  " LOCK\n");
        chnlp->stati = 04000;
        sim_usleep (1000);
        rc = IOM_CMD_DISCONNECT;
        break;

      case 063:               // UNLOCK MCA
        console_putstr ((int) con_unit_idx,  consoleID[con_unit_idx]);
        console_putstr ((int) con_unit_idx,  " UNLOCK\n");
        chnlp->stati = 04000;
        sim_usleep (1000);
        rc = IOM_CMD_DISCONNECT;
        break;

      default:
        chnlp->stati = 04501; // command reject, invalid instruction code
        rc = IOM_CMD_ERROR;
        goto done;
    } // switch IDCW_DEV_CMD
    goto done;
  } // IDCW

  // Not IDCW; TDCW are captured in IOM, so must be IOTD or IOTP
  switch (csp->io_mode) {
    case opc_no_mode:
      simWarn ("%s: Unexpected IOTx\n", __func__);
      rc = IOM_CMD_ERROR;
      goto done;

    case opc_read_mode: {
        if (csp->tailp != csp->keyboardLineBuffer) {
          simWarn ("%s: Discarding previously buffered input.\n", __func__);
        }
        uint tally = chnlp->DDCW_TALLY;
        uint daddr = chnlp->DDCW_ADDR;

        if (tally == 0) {
          tally = 4096;
        }

        csp->tailp     = csp->keyboardLineBuffer;
        csp->readp     = csp->keyboardLineBuffer;
        csp->startTime = time (NULL);
        csp->tally     = tally;
        csp->daddr     = daddr;
        csp->unitp     = unitp;
        csp->chan      = (int) chnlp->myIomIdx;

        // If Multics has gone seriously awry (eg crash
        // to BCE during boot), the autoinput will become
        // wedged waiting for the expect string 'Ready'.
        // We just went to read mode; if we are waiting
        // on an expect string, it is never coming because
        // console access is blocked by the expect code.
        // Throw out the script if this happens....

        // If there is autoinput and it is at ^X or ^Y
        if (csp->autop && (*csp->autop == 030 || *csp->autop == 031)) { // ^X ^Y
          // We are wedged.
          // Clear the autoinput buffer; this will cancel the
          // expect wait and any remaining script, returning
          // control of the console to the user.
          // Assuming opc0.
          clear_opc_autoinput (ASSUME0, NULL);
          ta_flush ();
          simPrintf \
              ("\nScript wedged and abandoned; autoinput and typeahead buffers flushed\n");
        }
        rc = IOM_CMD_PENDING; // command in progress; do not send terminate interrupt
        goto done;
      } // case opc_read_mode

    case opc_write_mode: {
        uint tally = chnlp->DDCW_TALLY;

// We would hope that number of valid characters in the last word
// would be in DCW_18_20_CP, but it seems to reliably be zero.

        if (tally == 0) {
          tally = 4096;
        }

        word36 buf[tally];
        iomIndirectDataService (chnlp, buf, & tally, false);
        chnlp->initiate = false;

#if 0
  simPrintf ("\n");
  for (uint i = 0; i < tally; i ++) {
    simPrintf ("%012"PRIo64"  \"", buf[i]);
    for (uint j = 0; j < 36; j += 9) {
      word9 ch = getbits36_9 (buf[i], j);
      if (ch < 256 && isprint ((char) ch))
        simPrintf ("%c", ch);
      else
        simPrintf ("\\%03o", ch);
    }
   simPrintf ("\"\n");
  }
  simPrintf ("\n");
#endif
#if 0
if (csp->bcd) {
  simPrintf ("\n");
  for (uint i = 0; i < tally; i ++) {
    simPrintf ("%012"PRIo64"  \"", buf[i]);
    for (uint j = 0; j < 36; j += 6) {
      word6 ch = getbits36_6 (buf[i], j);
      simPrintf ("%c", bcd_code_page[ch]);
    }
   simPrintf ("\"\n");
  }
  simPrintf ("\n");
}
#endif

        // Tally is in words, not chars.
        char text[tally * 4 + 1];
        char * textp = text;
        word36 * bufp = buf;
        * textp = 0;
#if !defined(__MINGW64__)
        //newlineOff ();
#endif
        // 0 == no escape character seen
        // 1 ==  ! seen
        // 2 == !! seen
        int escape_cnt = 0;

        while (tally) {
          word36 datum = * bufp ++;
          tally --;
          if (csp->bcd) {
            // Lifted from tolts_util_.pl1:bci_to_ascii
            for (int i = 0; i < 6; i ++) {
              word36 narrow_char = datum >> 30; // slide leftmost char
                                                //  into low byte
              datum = datum << 6; // lose the leftmost char
              narrow_char &= 077;
              char ch = bcd_code_page [narrow_char];
              if (escape_cnt == 2) {
                console_putchar ((int) con_unit_idx, ch);
#ifdef REALOPC
                usleep (66666);
#endif
                * textp ++ = ch;
                escape_cnt = 0;
              } else if (ch == '!') {
                escape_cnt ++;
              } else if (escape_cnt == 1) {
                uint lp = (uint)narrow_char;
                // !0 is mapped to !1
                // !1 to !9, ![, !#, !@, !;, !>, !?    1 to 15 newlines
                if (lp == 060 /* + */ || lp == 075 /* = */) { // POLTS
                  chnlp->stati = 04320;
                  goto done;
                }
                if (lp == 0)
                  lp = 1;
                if (lp >= 16) {
                  console_putstr ((int) con_unit_idx, "\f");
                  //* textp ++ = '\f';
                } else {
                  for (uint i = 0; i < lp; i ++) {
                    console_putstr ((int) con_unit_idx, "\n");
                    //* textp ++ = '\r';
                    //* textp ++ = '\n';
                  }
                }
                escape_cnt = 0;
              } else if (ch == '?') {
                escape_cnt = 0;
              } else {
                console_putchar ((int) con_unit_idx, ch);
#ifdef REALOPC
                usleep (66666);
#endif
                * textp ++ = ch;
                escape_cnt = 0;
              }
            }
          } else {
            for (int i = 0; i < 4; i ++) {
              word36 wide_char = datum >> 27; // slide leftmost char
                                              //  into low byte
              datum = datum << 9; // lose the leftmost char
              char ch = wide_char & 0x7f;
              if (ch != 0177 && ch != 0) {
                console_putchar ((int) con_unit_idx, ch);
#ifdef REALOPC
                usleep (66666);
#endif
                * textp ++ = ch;
              }
            }
          }
        }
        * textp ++ = 0;

        // autoinput expect
        if (csp->autop && * csp->autop == 030) {
          //   ^xstring\0
          //size_t expl = strlen ((char *) (csp->autop + 1));
          //   ^xstring^x
          size_t expl = strcspn ((char *) (csp->autop + 1), "\030");

          if (strncmp (text, (char *) (csp->autop + 1), expl) == 0) {
            csp->autop += expl + 2;
            sim_usleep (1000);
            sim_activate (& attn_unit[con_unit_idx], ACTIVATE_1SEC);
          }
        }
        // autoinput expect
        if (csp->autop && * csp->autop == 031) {
          //   ^ystring\0
          //size_t expl = strlen ((char *) (csp->autop + 1));
          //   ^ystring^y
          size_t expl = strcspn ((char *) (csp->autop + 1), "\031");

          char needle [expl + 1];
          strncpy (needle, (char *) csp->autop + 1, expl);
          needle [expl] = 0;
          if (strstr (text, needle)) {
            csp->autop += expl + 2;
            sim_usleep (1000);
            sim_activate (& attn_unit[con_unit_idx], ACTIVATE_1SEC);
          }
        }
        if (csp->rcpwatch)
          handleRCP (con_unit_idx, text);
#if !defined(__MINGW64__)
        //newlineOn ();
#endif
        chnlp->stati = 04000;
        rc = IOM_CMD_DISCONNECT;
        goto done;
      } // case opc_write_mode
  } // switch io_mode

done:
  unlock_libuv ();
  return rc;
} // opc_iom_cmd

static simRc opc_svc (UNIT * unitp)
  {
    int con_unit_idx   = (int) OPC_UNIT_IDX (unitp);
    uint ctlr_port_num = 0; // Consoles are single ported
    uint iom_unit_idx  = cables->opc_to_iom[con_unit_idx][ctlr_port_num].iom_unit_idx;
    uint chan_num      = cables->opc_to_iom[con_unit_idx][ctlr_port_num].chan_num;
    chnlState_t * chnlp = & iomChanData[iom_unit_idx][chan_num];

    opc_iom_cmd (chnlp);
    return SCPE_OK;
  }

static simRc opc_show_nunits (UNUSED FILE * st, UNUSED UNIT * uptr,
                                 UNUSED int val, UNUSED const void * desc)
  {
    sim_print ("%d units\n", opc_dev.numunits);
    return SCPE_OK;
  }

static simRc opc_set_nunits (UNUSED UNIT * uptr, int32_t UNUSED value,
                                const char * cptr, UNUSED void * desc)
  {
    if (! cptr)
      return SCPE_ARG;
    int n = atoi (cptr);
    if (n < 0 || n > N_OPC_UNITS_MAX)
      return SCPE_ARG;
    opc_dev.numunits = (uint32_t) n;
    return SCPE_OK;
  }

static config_value_list_t cfg_on_off[] =
  {
    { "off",     0 },
    { "on",      1 },
    { "disable", 0 },
    { "enable",  1 },
    { NULL,      0 }
  };

static config_value_list_t cfg_model[] =
  {
    { "m6001", m6001 },
    { "m6004", m6004 },
    { "m6601", m6601 },
    { NULL,    0 }
  };

static configList_t opc_config_list[] =
  {
   { "autoaccept", 0, 1, cfg_on_off },
   { "noempty",    0, 1, cfg_on_off },
   { "attn_flush", 0, 1, cfg_on_off },
   { "model",      1, 0, cfg_model },
   { "bg", 1, 0, cfg_on_off },
   { "rcpwatch", 1, 0, cfg_on_off },
   { NULL,         0, 0, NULL }
  };

static simRc opc_set_config (UNUSED UNIT *  uptr, UNUSED int32_t value,
                              const char * cptr, UNUSED void * desc)
  {
    int devUnitIdx           = (int) OPC_UNIT_IDX (uptr);
    opc_state_t * csp        = console_state + devUnitIdx;
// XXX Minor bug; this code doesn't check for trailing garbage
    configState_t cfg_state = { NULL, NULL };

    for (;;)
      {
        int64_t v;
        int rc = cfgParse (__func__, cptr, opc_config_list,
                           & cfg_state, & v);
        if (rc == CFG_DONE)
          break;

        if (rc == CFG_ERROR)
          {
            cfgParseDone (& cfg_state);
            return SCPE_ARG;
          }
        const char * p = opc_config_list[rc].name;

        if (strcmp (p, "autoaccept") == 0)
          {
            csp->autoaccept = (int) v;
            continue;
          }

        if (strcmp (p, "noempty") == 0)
          {
            csp->noempty = (int) v;
            continue;
          }

        if (strcmp (p, "attn_flush") == 0)
          {
            csp->attn_flush = (int) v;
            continue;
          }

        if (strcmp (p, "model") == 0)
          {
            csp->model = (enum console_model) v;
            continue;
          }

        if (strcmp (p, "bg") == 0)
          {
            csp->bg = (int) v;
            continue;
          }
 
        if (strcmp (p, "rcpwatch") == 0)
          {
            csp->rcpwatch = (int) v;
            continue;
          }
        simWarn ("error: opc_set_config: Invalid cfgParse rc <%d>\n",
                  rc);
        cfgParseDone (& cfg_state);
        return SCPE_ARG;
      } // process statements
    cfgParseDone (& cfg_state);
    return SCPE_OK;
  }

static simRc opc_show_config (UNUSED FILE * st, UNUSED UNIT * uptr,
                               UNUSED int  val, UNUSED const void * desc)
  {
    int devUnitIdx    = (int) OPC_UNIT_IDX (uptr);
    opc_state_t * csp = console_state + devUnitIdx;
    sim_msg ("flags    : ");
    sim_msg ("autoaccept=%d, ", csp->autoaccept);
    sim_msg ("noempty=%d, ",    csp->noempty);
    sim_msg ("attn_flush=%d",   csp->attn_flush);
    sim_msg ("model:  %d\n", csp->model);
    sim_msg ("bg:  %d\n", csp->bg);
    sim_msg ("rcpwatch:  %d\n", csp->rcpwatch);
    return SCPE_OK;
  }

static simRc opc_show_device_name (UNUSED FILE * st, UNIT * uptr,
                                    UNUSED int val, UNUSED const void * desc)
  {
    int n = (int) OPC_UNIT_IDX (uptr);
    if (n < 0 || n >= N_OPC_UNITS_MAX)
      return SCPE_ARG;
    simPrintf("name     : OPC%d", n);
    return SCPE_OK;
  }

static simRc opc_set_device_name (UNIT * uptr, UNUSED int32_t value,
                                   const char * cptr, UNUSED void * desc)
  {
    int n = (int) OPC_UNIT_IDX (uptr);
    if (n < 0 || n >= N_OPC_UNITS_MAX)
      return SCPE_ARG;
    if (cptr)
      {
        strncpy (console_state[n].device_name, cptr, MAX_DEV_NAME_LEN-1);
        console_state[n].device_name[MAX_DEV_NAME_LEN-1] = 0;
      }
    else
      console_state[n].device_name[0] = 0;
    return SCPE_OK;
  }

static simRc opc_set_console_port (UNIT * uptr, UNUSED int32_t value,
                                    const char * cptr, UNUSED void * desc)
  {
    int dev_idx = (int) OPC_UNIT_IDX (uptr);
    if (dev_idx < 0 || dev_idx >= N_OPC_UNITS_MAX)
      return SCPE_ARG;

    if (cptr)
      {
        int port = atoi (cptr);
        if (port < 0 || port > 65535) // 0 is 'disable'
          return SCPE_ARG;
        console_state[dev_idx].console_access.port = port;
        if (port != 0)
          sim_msg ("[OPC emulation: TELNET server port set to %d]\n", (int)port);
      }
    else
      console_state[dev_idx].console_access.port = 0;
    return SCPE_OK;
  }

static simRc opc_show_console_port (UNUSED FILE * st, UNIT * uptr,
                                       UNUSED int val, UNUSED const void * desc)
  {
    int dev_idx = (int) OPC_UNIT_IDX (uptr);
    if (dev_idx < 0 || dev_idx >= N_OPC_UNITS_MAX)
      return SCPE_ARG;
    if (console_state[dev_idx].console_access.port)
      simPrintf("port     : %d", console_state[dev_idx].console_access.port);
    else
      simPrintf("port     : disabled");
    return SCPE_OK;
  }

static simRc opc_set_console_address (UNIT * uptr, UNUSED int32_t value,
                                    const char * cptr, UNUSED void * desc)
  {
    int dev_idx = (int) OPC_UNIT_IDX (uptr);
    if (dev_idx < 0 || dev_idx >= N_OPC_UNITS_MAX)
      return SCPE_ARG;

    if (console_state[dev_idx].console_access.address)
      {
        FREE (console_state[dev_idx].console_access.address);
        console_state[dev_idx].console_access.address = NULL;
      }

    if (cptr)
      {
        console_state[dev_idx].console_access.address = strdup (cptr);
        MALLOC_CHECK (console_state[dev_idx].console_access.address);
        sim_msg ("[OPC emulation: OPC%d server address set to %s]\n",
                 dev_idx, console_state[dev_idx].console_access.address);
      }

    return SCPE_OK;
  }

static simRc opc_show_console_address (UNUSED FILE * st, UNIT * uptr,
                                       UNUSED int val, UNUSED const void * desc)
  {
    int dev_idx = (int) OPC_UNIT_IDX (uptr);
    if (dev_idx < 0 || dev_idx >= N_OPC_UNITS_MAX)
      return SCPE_ARG;
    if (console_state[dev_idx].console_access.address)
      simPrintf("address  : %s", console_state[dev_idx].console_access.address);
    else
      simPrintf("address  : any");
    return SCPE_OK;
  }

static simRc opc_set_console_pw (UNIT * uptr, UNUSED int32_t value,
                                    const char * cptr, UNUSED void * desc)
  {
    long dev_idx = (int) OPC_UNIT_IDX (uptr);
    if (dev_idx < 0 || dev_idx >= N_OPC_UNITS_MAX)
      return SCPE_ARG;

    if (cptr && (strlen(cptr) > 0))
      {
        char token[TOKLEN + 1];
        int rc = sscanf (cptr, "%" TOKLENQ "s", token);
        if (rc != 1)
          return SCPE_ARG;
        if (strlen (token) > PW_SIZE)
          return SCPE_ARG;
        strcpy (console_state[dev_idx].console_access.pw, token);
      }
    else
      {
        sim_msg ("no password\n");
        console_state[dev_idx].console_access.pw[0] = 0;
      }

    return SCPE_OK;
  }

static simRc opc_show_console_pw (UNUSED FILE * st, UNIT * uptr,
                                       UNUSED int val, UNUSED const void * desc)
  {
    int dev_idx = (int) OPC_UNIT_IDX (uptr);
    if (dev_idx < 0 || dev_idx >= N_OPC_UNITS_MAX)
      return SCPE_ARG;
    simPrintf("password : %s", console_state[dev_idx].console_access.pw);
    return SCPE_OK;
  }

static void console_putstr (int conUnitIdx, char * str)
  {
    size_t l = strlen (str);
    for (size_t i = 0; i < l; i ++) {
      sim_putchar (str[i]);
    }
    fflush (sim_log);
    opc_state_t * csp = console_state + conUnitIdx;
    if (csp->console_access.loggedOn)
      accessStartWrite (csp->console_access.client, str,
                           (ssize_t) l);
  }

static void consolePutchar0 (int conUnitIdx, char ch) {
  opc_state_t * csp = console_state + conUnitIdx;
  sim_putchar (ch);
  fflush (sim_log);
  if (csp->console_access.loggedOn)
    accessStartWrite (csp->console_access.client, & ch, 1);
}

static void console_putchar (int conUnitIdx, char ch) {
  opc_state_t * csp = console_state + conUnitIdx;
  if (csp->escapeSequence) { // Prior character was an escape
    csp->escapeSequence = false;
    if (ch == '1') { // Set tab
      if (csp->carrierPosition >= 1 && csp->carrierPosition <= 256) {
        csp->tabStops[csp->carrierPosition] = true;
      }
    } else if (ch == '2') { // Clear all tabs
      (void) memset (csp->tabStops, 0, sizeof (csp->tabStops));
    } else { // Unrecognized
      simWarn ("Unrecognized escape sequence \\033\\%03o\n", ch);
    }
  } else if (isprint (ch)) {
    consolePutchar0 (conUnitIdx, ch);
    csp->carrierPosition ++;
  } else if (ch == '\t') {  // Tab
    while (csp->carrierPosition < bufsize - 1) {
      consolePutchar0 (conUnitIdx, ' ');
      csp->carrierPosition ++;
      if (csp->tabStops[csp->carrierPosition])
        break;
    }
  } else if (ch == '\b') { // Backspace
      consolePutchar0 (conUnitIdx, ch);
      csp->carrierPosition --;
  } else if (ch == '\f' || ch == '\r') { // Formfeed, Carriage return
      consolePutchar0 (conUnitIdx, ch);
      csp->carrierPosition = 1;
  } else if (ch == '\033') { // Escape
      csp->escapeSequence = true;
  } else { // Non-printing and we don't recognize a carriage motion character, so just print it...
      consolePutchar0 (conUnitIdx, ch);
  }
}

static void consoleConnectPrompt (uv_tcp_t * client)
  {
    accessStartWriteStr (client, "password: \r\n");
    uv_access * console_access = (uv_access *) client->data;
    console_access->pwPos      = 0;
  }

void startRemoteConsole (void)
  {
    for (int conUnitIdx = 0; conUnitIdx < N_OPC_UNITS_MAX; conUnitIdx ++)
      {
        console_state[conUnitIdx].console_access.connectPrompt = consoleConnectPrompt;
        console_state[conUnitIdx].console_access.connected     = NULL;
        console_state[conUnitIdx].console_access.useTelnet     = true;
#if defined(CONSOLE_FIX)
        lock_libuv ();
#endif
        uv_open_access (& console_state[conUnitIdx].console_access);
#if defined(CONSOLE_FIX)
        unlock_libuv ();
#endif
      }
  }
