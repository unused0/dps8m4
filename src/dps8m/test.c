#include "dps8m.h"

config_t config;

static int ccuModels [] = {
  401
};
static const int nCcuModels = sizeof (ccuModels) / sizeof (int);

static int dskModels [] = {
  400, 402, 451, 500, 501, 3380, 3381, 0
};
static const int nDskModels = sizeof (dskModels) / sizeof (int);

//static int fnpModels [] = {
//  6670
//};
//static const int nFnpModels = sizeof (fnpModels) / sizeof (int);

//static int opcModels [] = {
//  6001, 6004, 6601
//};
//static const int nOpcModels = sizeof (opcModels) / sizeof (int);

//static int prtModels [] = {
//  401, 402, 901, 1000, 1200, 1201, 1600
//};
//static const int nPrtModels = sizeof (prtModels) / sizeof (int);

static int punModels [] = {
  120, 121, 201, 300, 302, 201
};
static const int nPunModels = sizeof (punModels) / sizeof (int);

static int rdrModels [] = {
  201, 301, 500, 501, 1050
};
static const int nRdrModels = sizeof (rdrModels) / sizeof (int);

static int tapModels [] = {
  500, 507, 600, 610, 630, 8200, 0
};
static const int nTapModels = sizeof (tapModels) / sizeof (int);


static bool findModel (int model, int * models, int nmodels) {
  for (int i = 0; i < nmodels; i ++) {
    if (model == models[i]) {
      return true;
    }
  }
  //simPrintf ("Model %"PRId36". not known\n", model);
  return false;
}


static bool findModels (int * modelList, int nModelList, int * models, int nmodels) {
  for (int i = 0; i < nModelList; i ++) {
    bool found = false;
    if (findModel (modelList[i], models, nmodels)) {
      found = true;
      break;
    }
    if (! found) {
      simPrintf ("Model %d. not known.\n", modelList[i]);
      return false;
    }
  }
  return true;
}


char * ctlrTypeStr (enum ctlrTypes ctlrType) {
  switch (ctlrType) {
    case ctlrIPC: return "IPC";
    case ctlrMTP: return "MTP";
    case ctlrMSP: return "MSP";
    case ctlrURP: return "URP";
    default: return "???";
  }
}

char * prphTypeStr (enum prphTypes prphType) {
  switch (prphType) {
    case prphCCU: return "CCU";
    case prphDIA: return "DIA";
    case prphFNP: return "FNP";
    case prphOPC: return "OPC";
    case prphPUN: return "PUN";
    case prphRDR: return "RDR";
    case prphPRT: return "PRT";
    case prphDSK: return "DSK";
    case prphTAP: return "TAP";
    default: return "???";
  }
}


char * stateStr (enum states state) {
  switch (state) {
    case stateOff: return "OFF";
    case stateOn: return "ON";
    default: return "???";
  }
}


char * opcStateStr (enum opcStates opcState) {
  switch (opcState) {
    case opcStateOn: return "ON";
    case opcStateAlt: return "ALT";
    case opcStateIO: return "IO";
    case opcStateInop: return "INOP";
    default: return "???";
  }
}
// nnnn  octal
// nnnn. decimal

int evalNumber (const char * s, word36 * v) {
  long long n;
  char * endptr;
  size_t l = strlen (s);
  if (s[l-1] == '.') { // decimal
    n = strtoll (s, & endptr, 10); 
    if (endptr != NULL && *endptr == '.') {  // valid
      if (n <= MASK36) { // Can't be negative because lexer ignores minus signs
        * v = (word36) n;
        return 0; // ok
      }
    }
   } else { // octal
    n = strtoll (s, & endptr, 8); 
    if (endptr != NULL && *endptr == '\0') {  // valid
      if (n <= MASK36) { // Can't be negative because lexer ignores minus signs
        * v = (word36) n;
        return 0; // ok
      }
    }
  }
  * v = 0;
  simPrintf ("Can't grok '%s' an a number.\n", s);
  return -1; // error
}

///
/// RIPOUT
///

void configCmdReset (void) {
#ifdef CMD_VERBOSE
  simPrintf ("CONFIG RESET\n");
#endif
  memset (& config, 0, sizeof (config));
}


///
/// MPC
///

int configCmdCtlr (word36 ctlrIdx, enum ctlrTypes ctlrType, word36 ctlrModel, word36 iomIdx,
                   word36 chan0, word36 nchan0,
                   word36 chan1, word36 nchan1,
                   word36 chan2, word36 nchan2,
                   word36 chan3, word36 nchan3) {

#ifdef CMD_VERBOSE
  simPrintf ("CONFIG MPC %s%c %"PRId36". IOM%c %"PRIo36" %"PRId36". %"PRIo36" %"PRId36". %"PRIo36" %"PRId36". %"PRIo36" %"PRId36".\n",
#endif
            ctlrTypeStr (ctlrType), AZ (ctlrIdx), ctlrModel, AZ (iomIdx),
            chan0, nchan0, chan1, nchan1, chan2, nchan2, chan3, nchan3);

  if (config.ctlrs[ctlrType][ctlrIdx].ctlrConnected) {
    simPrintf ("Error: Controller %s%c already connected.\n", ctlrTypeStr (ctlrType), AZ (ctlrIdx));
    return -1;
  }

  if (! config.ioms[iomIdx].iomConnected) {
    simPrintf ("Error: IOM%c not connected\n", AZ (iomIdx));
    return -1;
  }

  int chans[4] = { chan0, chan1, chan2, chan3 };
  int nchans[4] = { nchan0, nchan1, nchan2, nchan3 };

  for (int chanSet = 0; chanSet < 4; chanSet ++) {
    for (int n = 0; n < nchans[chanSet]; n ++) {
      int chnl = chans[chanSet] + n;
      if (chnl < N_RESERVED_CHANS || chnl >= N_IOM_CHANS) {
        simPrintf ("Error: Channel number %o invalid.\n", chnl);
        return -1;
      }
      if (config.ioms[iomIdx].channelsConnected[chnl]) {
        simPrintf ("Error: Channel number %o in use.\n", chnl);
        return -1;
      }
    }
  }

  // Cable iom to controller
  for (int chanSet = 0; chanSet < 4; chanSet ++) {
    for (int n = 0; n < nchans[chanSet]; n ++) {
      int chnl = chans[chanSet] + n;
      config.ioms[iomIdx].channelsConnected[chnl] = true;
      config.ioms[iomIdx].ctlrTypes[chnl] = ctlrType;
      config.ioms[iomIdx].ctlrIdxs[chnl] = ctlrIdx;
    }
  }

  // Configure contoller
  config.ctlrs[ctlrType][ctlrIdx].ctlrType = ctlrType;
  config.ctlrs[ctlrType][ctlrIdx].ctlrModel = ctlrModel;
  config.ctlrs[ctlrType][ctlrIdx].ctlrConnected = true;
  config.ctlrs[ctlrType][ctlrIdx].ctlrIdx = ctlrIdx;

  return 0;
}

int configCmdIpcCtlr (word36 iomIdx, word36 chan, word36 nchan) {
  enum ctlrTypes ctlrType = ctlrIPC;
  int ctlrModel = 0;

#ifdef CMD_VERBOSE
  simPrintf ("CONFIG IPC FIPS IOM%c %"PRIo36" %"PRId36".\n", AZ (iomIdx), chan, nchan);
#endif

  if (! config.ioms[iomIdx].iomConnected) {
    simPrintf ("Error: IOM%c not connected\n", AZ (iomIdx));
    return -1;
  }

  int ctlrIdx = config.nIPCs; 
  if (ctlrIdx >= N_CTLRS) {
    simPrintf ("Error: Too many IPCs.\n");
    return -1;
  }

  // New controller allocated, so never connected...
  //if (config.ctlrs[cltrType][ctlrIdx].ctlrConnected) {
  //  simPrintf ("Error: Controller %s%c already connected.\n", ctlrTypeStr (ctlrType), AZ (ctlrIdx));
  //  return;
  //}

  for (int n = 0; n < nchan; n ++) {
    int chnl = chan + n;
    if (chnl < N_RESERVED_CHANS || chnl >= N_IOM_CHANS) {
      simPrintf ("Error: Channel number %o invalid.\n", chnl);
      return -1;
    }
    if (config.ioms[iomIdx].channelsConnected[chnl]) {
      simPrintf ("Error: Channel number %o in use.\n", chnl);
      return -1;
    }
  }

  // Cable iom to controller
  for (int n = 0; n < nchan; n ++) {
    int chnl = chan + n;
    config.ioms[iomIdx].channelsConnected[chnl] = true;
    config.ioms[iomIdx].ctlrIdxs[chnl] = ctlrIdx;
  }

  // Configure contoller
  config.ctlrs[ctlrType][ctlrIdx].ctlrType = ctlrType;
  config.ctlrs[ctlrType][ctlrIdx].ctlrModel = ctlrModel;
  config.ctlrs[ctlrType][ctlrIdx].ctlrConnected = true;
  config.ctlrs[ctlrType][ctlrIdx].ctlrIdx = ctlrIdx;

  // Allocate controller
  config.nIPCs ++; 

  return 0;
}

int configCmdChnl (word36 prphIdx, enum prphTypes prphType, word36 iomIdx, word36 chan0, word36 nchan0, word36 chan1, word36 nchan1, word36 chan2, word36 nchan2, word36 chan3, word36 nchan3) {
#ifdef CMD_VERBOSE
  simPrintf ("CONFIG CHNL %s%c IOM%c %"PRIo36" %"PRId36". %"PRIo36" %"PRId36". %"PRIo36" %"PRId36". %"PRIo36" %"PRId36".\n", 
#endif
            prphTypeStr (prphType), AZ (prphIdx), AZ (iomIdx), 
            chan0, nchan0, chan1, nchan1, chan2, nchan2, chan3, nchan3);

  if (! config.ioms[iomIdx].iomConnected) {
    simPrintf ("Error: IOM%c not connected\n", AZ (iomIdx));
    return -1;
  }

  //bool found = false;
  //enum ctlrTypes ctlrType;
  //int ctlrIdx;

#if 0
  for (int iom = 0; iom < N_IOMS; iom ++) {
    if (! config.ioms[iom].connected)
      continue;
    if (config.ioms[iom].ctlrTypes == ctlrType && config.ioms[iom].ctlrIdxs == ctlrIdx) {
    }
  }
#endif

// XXX

// Need to figure out the controller type and idx (one of MTP, MSP, IPC)

#if 0
  if (! config.ctlrs[ctlrType][ctlrIdx].ctlrConnected) {
    simPrintf ("Error: %s%c not connected\n", prphTypeStr (prphType), AZ (prphIdx));
    return;
  }

  int chans[4] = { chan0, chan1, chan2, chan3 };
  int nchans[4] = { nchan0, nchan1, nchan2, nchan3 };

  for (int chanSet = 0; chanSet < 4; chanSet ++) {
    for (int n = 0; n < nchans[chanSet]; n ++) {
      int chnl = chans[chanSet] + n;
      if (chnl < N_RESERVED_CHANS || chnl >= N_IOM_CHANS) {
        simPrintf ("Error: Channel number %o invalid.\n", chnl);
        return;
      }
      if (config.ioms[iomIdx].channelsConnected[chnl]) {
        simPrintf ("Error: Channel number %o in use.\n", chnl);
        return;
      }
    }
  }

  // Cable iom to controller
  for (int chanSet = 0; chanSet < 4; chanSet ++) {
    for (int n = 0; n < nchans[chanSet]; n ++) {
      int chnl = chans[chanSet] + n;
      config.ioms[iomIdx].channelsConnected[chnl] = true;
      config.ioms[iomIdx].ctlrIdx[chnl] = ctlrIdx;
    }
  }
#endif
  return 0;
}

#if 0
static int configCmdPrphCommon (word36 prphIdx, enum prphTypes prphType) {

  
  if (config.ctlrs[ctlrType][ctlrIdx].ctlrConnected) {
    simPrintf ("Error: Controller %s%c already connected.\n", ctlrTypeStr (ctlrType), AZ (ctlrIdx));
    return;
  }

  for (int chanSet = 0; chanSet < 4; chanSet ++) {
    for (int n = 0; n < nchans[chanSet]; n ++) {
      int chnl = chans[chanSet] + n;
      if (chnl < N_RESERVED_CHANS || chnl >= N_IOM_CHANS) {
        simPrintf ("Error: Channel number %o invalid.\n", chnl);
        return;
      }
      if (config.ioms[iomIdx].channelsConnected[chnl]) {
        simPrintf ("Error: Channel number %o in use.\n", chnl);
        return;
      }
    }
  }

  
  // Cable iom to controller
  for (int chanSet = 0; chanSet < 4; chanSet ++) {
    for (int n = 0; n < nchans[chanSet]; n ++) {
      int chnl = chans[chanSet] + n;
      config.ioms[iomIdx].channelsConnected[chnl] = true;
      config.ioms[iomIdx].ctlrIdx[chnl] = ctlrIdx;
    }
  }

  // Configure contoller
  config.ctlrs[ctlrType][ctlrIdx].ctlrType = ctlrType;
  config.ctlrs[ctlrType][ctlrIdx].ctlrModel = ctlrModel;
  config.ctlrs[ctlrType][ctlrIdx].ctlrConnected = true;
  config.ctlrs[ctlrType][ctlrIdx].ctlrIdx = ctlrIdx;
}
#endif

/* CCU DIA PUN RDR */
int configCmdPrph0 (word36 prphIdx, enum prphTypes prphType, word36 iomIdx, word36 chan, word36 model) {
#ifdef CMD_VERBOSE
  simPrintf ("CONFIG PRPH %s%c IOM%c %"PRIo36" %"PRId36".\n",
          prphTypeStr (prphType), AZ (prphIdx), AZ (iomIdx), chan, model);
#endif

  if (! config.ioms[iomIdx].iomConnected) {
    simPrintf ("Error: IOM%c not connected\n", AZ (iomIdx));
    return -1;
  }

  if (prphType == prphDIA) {
    if (config.ioms[iomIdx].channelsConnected[chan]) {
      simPrintf ("Error: IOM%c channel %"PRIo36" connected to a controller\n", AZ (iomIdx), chan);
      return -1;
    }
    if (config.ctlrs[ctlrDIA][prphIdx].ctlrConnected) {
      simPrintf ("Error: DIA controller in use\n");
      return -1;
    }
  } else {
    if (! config.ioms[iomIdx].channelsConnected[chan]) {
      simPrintf ("Error: IOM%c channel %"PRIo36" not connected to a controller\n", AZ (iomIdx), chan);
      return -1;
    }
  }

  enum ctlrTypes ctlrType;
  int ctlrIdx;

  switch (prphType) {
    case prphCCU: {
      if (! findModel (model, ccuModels, nCcuModels)) {
        simPrintf ("Invalid CCU model number %"PRId36".\n", model);
        return true;
      }
      if (config.ioms[iomIdx].ctlrTypes[chan] != ctlrURP) {
        simPrintf ("Invalid CCU controller\n");
        return true;
      }
      ctlrType = config.ioms[iomIdx].ctlrTypes[chan];
      ctlrIdx = config.ioms[iomIdx].ctlrIdxs[chan];
      break;
    }

    case prphDIA: {
      ctlrType = ctlrDIA;
      ctlrIdx = prphIdx;
      break;
    }

    case prphPUN: {
      if (! findModel (model, punModels, nPunModels)) {
        simPrintf ("Invalid PUN model number %"PRId36".\n", model);
        return -1;
      }
      if (config.ioms[iomIdx].ctlrTypes[chan] != ctlrURP) {
        simPrintf ("Invalid PUN controller\n");
        return -1;
      }
      ctlrType = config.ioms[iomIdx].ctlrTypes[chan];
      ctlrIdx = config.ioms[iomIdx].ctlrIdxs[chan];
      break;
    }

    case prphRDR: {
      if (! findModel (model, rdrModels, nRdrModels)) {
        simPrintf ("Invalid RDR model number %"PRId36".\n", model);
        return -1;
      }
      if (config.ioms[iomIdx].ctlrTypes[chan] != ctlrURP) {
        simPrintf ("Invalid RDR controller\n");
        return -1;
      }
      ctlrType = config.ioms[iomIdx].ctlrTypes[chan];
      ctlrIdx = config.ioms[iomIdx].ctlrIdxs[chan];
      break;
    }

    default: {
      simPrintf ("CONFIG internal error 1201 in %s(); prphType %d???\n", __func__, prphType);
      return -1;
    }
  }

  // Cable in

  // Special case for DIA; there is no MPC card for it.
  if (prphType == prphDIA) {
    config.ioms[iomIdx].channelsConnected[chan] = true;
    config.ioms[iomIdx].ctlrTypes[chan] = ctlrDIA;
    config.ioms[iomIdx].ctlrIdxs[chan] = ctlrIdx;
    config.ctlrs[ctlrType][ctlrIdx].ctlrConnected = true;
    config.ctlrs[ctlrType][ctlrIdx].ctlrType = ctlrType;
    config.ctlrs[ctlrType][ctlrIdx].ctlrModel = model;
  }

  // XXX
  return 0;
}

/* DSK TAP */
int configCmdPrph1 (word36 prphIdx, enum prphTypes prphType, word36 iomIdx, word36 chan, word36 nchan, word36 nsets, word36 model0, word36 d0, word36 model1, word36 d1, word36 model2, word36 d2, word36 model3, word36 d3, word36 model4, word36 d4) {

#ifdef CMD_VERBOSE
  simPrintf ("CONFIG PRPH %s%c IOM%c %"PRIo36" %"PRId36". %"PRId36" %"PRId36". %"PRId36". %"PRId36". %"PRId36". %"PRId36". %"PRId36". %"PRId36". %"PRId36". %"PRId36". %"PRId36".\n",
          prphTypeStr (prphType), AZ (prphIdx), AZ (iomIdx), chan, nchan,
          nsets, model0, d0, model1, d1, model2, d2, model3, d3, model4, d4);
#endif

  if (! config.ioms[iomIdx].iomConnected) {
    simPrintf ("Error: IOM%c not connected\n", AZ (iomIdx));
    return -1;
  }
  if (! config.ioms[iomIdx].channelsConnected[chan]) {
    simPrintf ("Error: IOM%c channel %"PRIo36" not connected to a controller\n", AZ (iomIdx), chan);
    return -1;
  }

  //enum ctlrTypes ctlrType;
  //int ctlrIdx;

  int models[5] = { model0, model1, model2, model3, model4 };
  //int ds[5] = { d0, d1, d2, d3, d4 };

  switch (prphType) {

    case prphDSK: {
      if (! findModels (models, nsets, dskModels, nDskModels)) {
        //simPrintf ("Invalid DSK model number %"PRId36".\n", model);
        return -1;
      }
      if (config.ioms[iomIdx].ctlrTypes[chan] != ctlrMSP &&
          config.ioms[iomIdx].ctlrTypes[chan] != ctlrIPC) {
        simPrintf ("Invalid DSK controller\n");
        return -1;
      }
      //ctlrType = config.ioms[iomIdx].ctlrTypes[chan];
      //ctlrIdx = config.ioms[iomIdx].ctlrIdxs[chan];
      break;
    }

    case prphTAP: {
      if (! findModels (models, nsets, tapModels, nTapModels)) {
        //simPrintf ("Invalid TAP model number %"PRId36".\n", model);
        return -1;
      }
      if (config.ioms[iomIdx].ctlrTypes[chan] != ctlrMTP &&
          config.ioms[iomIdx].ctlrTypes[chan] != ctlrIPC) {
        //simPrintf ("Invalid TAP controller\n");
        return -1;
      }
      //ctlrType = config.ioms[iomIdx].ctlrTypes[chan];
      //ctlrIdx = config.ioms[iomIdx].ctlrIdxs[chan];
      break;
    }

    default: {
      simPrintf ("CONFIG internal error 1201 in %s(); prphType %d???\n", __func__, prphType);
      return -1;
    }
  }

#if 0
  for (int i = 0; i < 5; i ++) {
    if (nDrives[i]) {
      int model = models[i];

      switch (prphType) {
        case prphDSK: {
          if (! findModel (model, dskModels, nDskModels)) {
            simPrintf ("Invalid DSK model number %d.\n", model);
            return true;
          }
          break;
        }

        case prphTAP: {
          if (! findModel (model, tapModels, nTapModels)) {
            simPrintf ("Invalid TAP model number %d.\n", model);
            return true;
          }
          break;
        }

        default: {
          simPrintf ("CONFIG internal error 1201 in %s(); prphType %d???\n", __func__, prphType);
          return true;
        }
      }
    } // if ndrives
  } // for i 
#endif

  // Cable in
  // XXX
  return 0;   
}

/* FNP */
int configCmdPrph2 (word36 prphIdx, enum prphTypes prphType, word36 iomIdx, word36 chan, word36 model, enum states state) {
#ifdef CMD_VERBOSE
  simPrintf ("CONFIG PRPH %s%c IOM%c %"PRIo36" %"PRId36". %s\n",
          prphTypeStr (prphType), AZ (prphIdx), AZ (iomIdx), chan, model, stateStr (state));
#endif

  if (! config.ioms[iomIdx].iomConnected) {
    simPrintf ("Error: IOM%c not connected\n", AZ (iomIdx));
    return -1;
  }
  return 0;
}

/* OPC */
int configCmdPrph3 (word36 prphIdx, enum prphTypes prphType, word36 iomIdx, word36 chan, word36 model, word36 lineLength, enum opcStates opcState, bool mask) {
#ifdef CMD_VERBOSE
  simPrintf ("CONFIG PRPH %s%c IOM%c %"PRIo36" %"PRId36". %"PRId36". %s %s\n",
          prphTypeStr (prphType), AZ (prphIdx), AZ (iomIdx), chan, model, lineLength, opcStateStr (opcState), mask ? "MASK" : "");
#endif

  if (! config.ioms[iomIdx].iomConnected) {
    simPrintf ("Error: IOM%c not connected\n", AZ (iomIdx));
    return -1;
  }

  return 0;
}

/* PRT */
int configCmdPrph4 (word36 prphIdx, enum prphTypes prphType, word36 iomIdx, word36 chan, word36 model, word36 train, word36 lineLength) {
#ifdef CMD_VERBOSE
  simPrintf ("CONFIG PRPH %s%c IOM%c %"PRIo36" %"PRId36". %"PRId36". %"PRId36".\n",
          prphTypeStr (prphType), AZ (prphIdx), AZ (iomIdx), chan, model, train, lineLength);
#endif

  if (! config.ioms[iomIdx].iomConnected) {
    simPrintf ("Error: IOM%c not connected\n", AZ (iomIdx));
    return -1;
  }

  return 0;
}

int configCmdIom (word36 iomIdx, word36 port, enum iomModels iomModel, enum states state) {
#ifdef CMD_VERBOSE
  simPrintf ("CONFIG IOM%c %"PRId36". %s %s\n",
          AZ (iomIdx), port, iomModel == iomIOM ? "IOM" : "IMU", state == stateOff ? "OFF" : "ON");
#endif

  if (port < 0 || port > 7) {
    simPrintf ("Error: port value of %"PRId36". not in 0..7\n", port);
    return -1;
  }

  if (config.ioms[iomIdx].iomConnected) {
    simPrintf ("Error: IOM%c already connected.\n", AZ (iomIdx));
    return -1;
  }

  if (config.scuPorts.portConnected[port]) {
    simPrintf ("Error: SCU port %"PRId36". already connected.\n", port);
    return -1;
  }

  config.scuPorts.portConnected[port] = true;
  config.scuPorts.scuPortConnectionsType[port] = scuPortCPU;
  config.scuPorts.scuPortConnectionIdx[port] = iomIdx;

  config.ioms[iomIdx].iomConnected = true;
  config.ioms[iomIdx].iomModel = iomModel;
  config.ioms[iomIdx].state = state;

  return 0;
}

int configCmdCpu (word36 cpuIdx, word36 port, enum states state, enum cpuTypes cpuType, word36 cpuModel, word36 cacheSize) {
#ifdef CMD_VERBOSE
  simPrintf ("CONFIG CPU%c %"PRIo36" %s %s %"PRId36". %"PRId36".\n", AZ (cpuIdx), port, state == stateOff ? "OFF" : "ON",
          cpuType == cpuL68 ? "L68" : cpuType == cpuDPS8 ? "DPS8" : "DPS", cpuModel, cacheSize);
#endif

  // XXX
  return 0;
}

int configCmdMem (word36 memIdx, word36 memsize, enum states state) {
#ifdef CMD_VERBOSE
  simPrintf ("CONFIG MEM%c %"PRId36". %s\n", AZ (memIdx), memsize, state == stateOff ? "OFF" : "ON");
#endif
  // XXX
  return 0;
}


int yyparse (void);

int cmdExecute (const char * cmd) {
  return parse_string (cmd);
}

#if 0
int main (int argc, char * argv[]) {
  while (1) {
    char buffer[1024];
    char * p = fgets (buffer, sizeof (buffer), stdin);
    if (p == NULL)
      break;
    buffer[strcspn(buffer, "\r\n")] = 0;
    if (parse_string (buffer))
      printf ("%s\n", buffer);
  }

  //return yyparse ();
  //return parse_string (argv[1]);
}
#endif

