/*
 * vim: filetype=c:tabstop=4:ai:expandtab
 * SPDX-License-Identifier: ICU
 * scspell-id: fe96c073-f62f-11ec-bf37-80ee73e9b8e7
 *
 * ---------------------------------------------------------------------------
 *
 * Copyright (c) 2013-2024 Charles Anthony
 * Copyright (c) 2021-2024 The DPS8M Development Team
 *
 * This software is made available under the terms of the ICU License.
 * See the LICENSE.md file at the top-level directory of this distribution.
 *
 * ---------------------------------------------------------------------------
 */

//
// Thread Wrappers
//

#include <unistd.h>
#include <stdlib.h>
#include <signal.h>

#include "dps8m.h"
#include "iom_channel.h"
#include "iom.h"
#include "faults.h"
#include "utils.h"

#include "threadz.h"
#if defined(__FreeBSD__) || defined(__OpenBSD__)
# include <pthread_np.h>
#endif /* FreeBSD || OpenBSD */

#if defined(__HAIKU__)
# include <OS.h>
# undef pthread_setname_np
# define pthread_setname_np(x,y) rename_thread(find_thread(NULL),y)
#endif /* Haiku */

//
// Resource locks
//

// scp library serializer

#ifndef QUIET_UNUSED
void lock_simh (void)
  {
    pthread_mutex_lock (& simh_lock);
  }

void unlock_simh (void)
  {
    pthread_mutex_unlock (& simh_lock);
  }
#endif

// libuv library serializer

static pthread_mutex_t libuv_lock;

void lock_libuv (void)
  {
    pthread_mutex_lock (& libuv_lock);
  }

void unlock_libuv (void)
  {
    pthread_mutex_unlock (& libuv_lock);
  }

#ifdef TESTING
bool test_libuv_lock (void)
  {
    int rc;
    rc = pthread_mutex_trylock (& libuv_lock);
    if (rc)
      {
         // couldn't lock; presumably already
         return true;
      }
    // lock acquired, it wasn't locked
    rc = pthread_mutex_unlock (& libuv_lock);
    if (rc)
      simPrintf ("test_libuv_lock pthread_mutex_lock libuv_lock %d\n", rc);
    return false;
  }
#endif

// Memory serializer

//   addrmods RMW lock
//     Read()
//     Write()
//
//   CPU -- reset locks
//
//   coreRead/coreWrite locks
//
//   read_operand/write operand RMW lock
//     Read()
//     Write()
//
//  IOM

// local serializer

void lock_ptr (pthread_mutex_t * lock)
  {
    int rc;
    rc = pthread_mutex_lock (lock);
    if (rc)
      simPrintf ("lock_ptr %d\n", rc);
  }

void unlock_ptr (pthread_mutex_t * lock)
  {
    int rc;
    rc = pthread_mutex_unlock (lock);
    if (rc)
      simPrintf ("unlock_ptr %d\n", rc);
  }

// SCU serializer

static pthread_mutex_t scu_lock;

void lock_scu (void)
  {
    int rc;
    rc = pthread_mutex_lock (& scu_lock);
    if (rc)
      simPrintf ("lock_scu pthread_spin_lock scu %d\n", rc);
  }

void unlock_scu (void)
  {
    int rc;
    rc = pthread_mutex_unlock (& scu_lock);
    if (rc)
      simPrintf ("unlock_scu pthread_spin_lock scu %d\n", rc);
  }

// synchronous clock serializer

static pthread_mutex_t syncLock;

void lockSync (void)
  {
    int rc;
    rc = pthread_mutex_lock (& syncLock);
    if (rc)
      simPrintf ("lockSync pthread_spin_lock syncLock %d\n", rc);
  }

void unlockSync (void)
  {
    int rc;
    rc = pthread_mutex_unlock (& syncLock);
    if (rc)
      simPrintf ("unlockSync pthread_spin_lock syncLock %d\n", rc);
  }

// G7 serializer

static pthread_mutex_t g7Lock;

void lockG7 (void)
  {
    //sim_debug (DBG_TRACE, & cpu_dev, "lock_g7\n");
    int rc;
    rc = pthread_mutex_lock (& g7Lock);
    if (rc)
      simPrintf ("lockG7 pthread_mutex_lock g7Lock %d\n", rc);
  }

void unlockG7 (void)
  {
    //sim_debug (DBG_TRACE, & cpu_dev, "unlockG7\n");
    int rc;
    rc = pthread_mutex_unlock (& g7Lock);
    if (rc)
      simPrintf ("lockG7 pthread_mutex_unlock g7Lock %d\n", rc);
  }

// IOM serializer

static pthread_mutex_t iom_lock;

void lock_iom (void)
  {
    int rc;
    rc = pthread_mutex_lock (& iom_lock);
    if (rc)
      simPrintf ("%s pthread_spin_lock iom %d\n", __func__, rc);
  }

void unlock_iom (void)
  {
    int rc;
    rc = pthread_mutex_unlock (& iom_lock);
    if (rc)
      simPrintf ("%s pthread_spin_lock iom %d\n", __func__, rc);
  }

// Debugging tool

#ifdef TESTING
static pthread_mutex_t tst_lock = PTHREAD_MUTEX_INITIALIZER;

void lock_tst (void)
  {
    int rc;
    rc = pthread_mutex_lock (& tst_lock);
    if (rc)
      simPrintf ("lock_tst pthread_mutex_lock tst_lock %d\n", rc);
  }

void unlock_tst (void)
  {
    int rc;
    rc = pthread_mutex_unlock (& tst_lock);
    if (rc)
      simPrintf ("unlock_tst pthread_mutex_lock tst_lock %d\n", rc);
  }

// assertion

bool test_tst_lock (void)
  {
    int rc;
    rc = pthread_mutex_trylock (& tst_lock);
    if (rc)
      {
         // couldn't lock; presumably already
         return true;
      }
    // lock acquired, it wasn't locked
    rc = pthread_mutex_unlock (& tst_lock);
    if (rc)
      simPrintf ("test_tst_lock pthread_mutex_lock tst_lock %d\n", rc);
    return false;
  }
#endif

////////////////////////////////////////////////////////////////////////////////
//
// CPU threads
//
////////////////////////////////////////////////////////////////////////////////

//
// main thread
//   createCPUThread
//
// CPU thread
//   cpuRunningWait
//   sleepCPU
//
// SCU thread
//   wakeCPU
//
// cpuThread:
//
//    while (1)
//      {
//        cpuRunningWait
//        compute...
//        dis:  sleepCPU
//      }

struct cpuThreadz_t cpuThreadz [N_CPU_UNITS_MAX];

// Create a thread with Mach CPU policy

#if defined(__APPLE__)
int pthread_create_with_cpu_policy(
        pthread_t *restrict thread,
        int policy_group,
        const pthread_attr_t *restrict attr,
        void *(*start_routine)(void *), void *restrict arg)
{
# if defined(TESTING)
  sim_msg ("\nAffinity policy group %d requested for thread.\n", policy_group);
# endif /* if defined(TESTING) */
  thread_affinity_policy_data_t policy_data = { policy_group };
  int rv = pthread_create_suspended_np(thread, attr, start_routine, arg);
  mach_port_t mach_thread = pthread_mach_thread_np(*thread);
  if (rv != 0)
    {
      return rv;
    }
  thread_policy_set(
          mach_thread,
          THREAD_AFFINITY_POLICY,
          (thread_policy_t)&policy_data,
          THREAD_AFFINITY_POLICY_COUNT);
  thread_resume(mach_thread);
  return 0;
}
#endif /* if defined(__APPLE__) */

// Create CPU thread

void createCPUThread (uint cpuNum) {
  int rc;
  struct cpuThreadz_t * p = & cpuThreadz[cpuNum];
  if (p->run)
    return;
  cpuResetUnitIdx (cpuNum, false);
  p->cpuThreadArg = (int) cpuNum;
  // initialize sleep switch
  rc = pthread_mutex_init (& p->sleepLock, NULL);
  if (rc)
    simPrintf ("createCPUThread pthread_mutex_init sleepLock %d\n", rc);
  rc = pthread_cond_init (& p->sleepCond, NULL);
  if (rc)
    simPrintf ("createCPUThread pthread_cond_init sleepCond %d\n", rc);
  // initialize run/stop switch
  rc = pthread_mutex_init (& p->runLock, NULL);
  if (rc)
    simPrintf ("createCPUThread pthread_mutex_init runLock %d\n", rc);
  rc = pthread_cond_init (& p->runCond, NULL);
  if (rc)
    simPrintf ("createCPUThread pthread_cond_init runCond %d\n", rc);

  p->run = true;

  // initialize DIS sleep
#if defined(USE_MONOTONIC)
# if defined(__APPLE__) || !defined(CLOCK_MONOTONIC)
  p->sleepClock = CLOCK_REALTIME;
  rc = pthread_cond_init (& p->sleepCond, NULL);
# else
  rc = pthread_condattr_init (& p->sleepCondAttr);
  if (rc)
    simPrintf ("createCPUThread pthread_condattr_init sleepCond %d\n", rc);

  rc = pthread_condattr_setclock (& p->sleepCondAttr, CLOCK_MONOTONIC);
  if (rc) {
    //simPrintf ("createCPUThread pthread_condattr_setclock  sleepCond %d\n", rc);
    p->sleepClock = CLOCK_REALTIME;
  } else {
    p->sleepClock = CLOCK_MONOTONIC;
  }
# endif // ! APPLE
  rc = pthread_cond_init (& p->sleepCond, & p->sleepCondAttr);
#else
  rc = pthread_cond_init (& p->sleepCond, NULL);
#endif
  if (rc)
    simPrintf ("createCPUThread pthread_cond_init sleepCond %d\n", rc);

#if defined(__APPLE__)
  rc = pthread_create_with_cpu_policy(
          & p->cpuThread,
          cpuNum,
          NULL,
          cpuThreadMain,
          & p->cpuThreadArg);
#else
  rc = pthread_create(
          & p->cpuThread,
          NULL,
          cpuThreadMain,
          & p->cpuThreadArg);
#endif /* if defined(__APPLE__) */
  if (rc)
    simPrintf ("createCPUThread pthread_create %d\n", rc);

  char nm [17];
  (void) sprintf (nm, "CPU %c", 'a' + cpuNum);
#if !defined(__NetBSD__)
# if ( defined(__FreeBSD__) || defined(__OpenBSD__) )
  pthread_set_name_np (p->cpuThread, nm);
# else
#  if defined(__APPLE__)
  pthread_setname_np (nm);
#  else
#   if !defined(_AIX)
#    if !defined(__gnu_hurd__)
  pthread_setname_np (p->cpuThread, nm);
#    endif /* if !defined(__gnu_hurd__) */
#   endif /* if !defined(_AIX) */
#  endif /* if defined(__APPLE__) */
# endif /* if defined(__FreeBSD__) || defined(__OpenBSD__) */
#endif /* if !defined(__NetBSD__) */

#ifdef AFFINITY
  if (cpus[cpuNum].tweaks.set_affinity) {
    cpu_set_t cpuset;
    CPU_ZERO (& cpuset);
    CPU_SET (cpus[cpuNum].tweaks.affinity, & cpuset);
    int s = pthread_setaffinity_np (p->cpuThread, sizeof (cpu_set_t), & cpuset);
    if (s)
      simPrintf ("pthread_setaffinity_np %u on CPU %u returned %d\n",
                  cpus[cpuNum].tweaks.affinity, cpuNum, s);
  }
#endif
}

void stopCPUThread (cpuState_t * cpup)
  {
    struct cpuThreadz_t * p = & cpuThreadz[cpu.cpuIdx];
    p->run = false;
    pthread_exit(NULL);
  }

// Called by CPU thread to block on run/sleep

#if 0
void cpuRunningWait (void)
  {
    int rc;
    struct cpuThreadz_t * p = & cpuThreadz[cpu.cpuIdx];
    if (p->run)
      return;
    rc = pthread_mutex_lock (& p->runLock);
    if (rc)
      simPrintf ("cpuRunningWait pthread_mutex_lock %d\n", rc);
    while (! p->run)
      {
        rc = pthread_cond_wait (& p->runCond, & p->runLock);
        if (rc)
          simPrintf ("cpuRunningWait pthread_cond_wait %d\n", rc);
     }
    rc = pthread_mutex_unlock (& p->runLock);
    if (rc)
      simPrintf ("cpuRunningWait pthread_mutex_unlock %d\n", rc);
  }
#endif

// Called by CPU thread to sleep until time up or signaled
// Return time left
unsigned long sleepCPU (cpuState_t * cpup, unsigned long usec) {
  int rc;
  struct cpuThreadz_t * p = & cpuThreadz[cpu.cpuIdx];
  struct timespec startTime, absTime;

#ifdef USE_MONOTONIC
  clock_gettime (p->sleepClock, & startTime);
#else
  clock_gettime (CLOCK_REALTIME, & startTime);
#endif
  absTime = startTime;
  int64_t nsec = ((int64_t) usec) * 1000L + (int64_t)startTime.tv_nsec;
  absTime.tv_nsec = nsec % 1000000000L;
  absTime.tv_sec += nsec / 1000000000L;

  pthread_mutex_lock (& p->sleepLock);
  rc = pthread_cond_timedwait (& p->sleepCond, & p->sleepLock, & absTime);
  pthread_mutex_unlock (& p->sleepLock);

  if (rc == ETIMEDOUT) {
    return 0;
  }

  if (rc) {
    simPrintf ("sleepCPU pthread_cond_timedwait rc %ld  usec %ld TR %lu CPU %lu\n", (long) rc, (long) usec, (unsigned long) cpu.rTR, (unsigned long) cpu.cpuIdx);
  }

  struct timespec newTime;
  struct timespec delta;
#ifdef USE_MONOTONIC
  clock_gettime (p->sleepClock, & newTime);
#else
  clock_gettime (CLOCK_REALTIME, & newTime);
#endif
  timespec_diff (& absTime, & newTime, & delta);

  if (delta.tv_nsec < 0)
    return 0; // safety
  return (unsigned long) delta.tv_nsec / 1000L;
}

// Called to wake sleeping CPU; such as interrupt during DIS

void wakeCPU (uint cpuNum)
  {
    int rc;
    struct cpuThreadz_t * p = & cpuThreadz[cpuNum];

    rc = pthread_cond_signal (& p->sleepCond);
    if (rc)
      simPrintf ("wakeCPU pthread_cond_signal %d\n", rc);
  }

////////////////////////////////////////////////////////////////////////////////
//
// Channel threads
//
////////////////////////////////////////////////////////////////////////////////

// main thread
//   createChnThread    create thread
// IOM thread
//   chnRdyWait         wait for channel started
//   setChnConnect      signal channel to start
// Channel thread
//   chnConnectWait     Channel thread wait for work
//   chnConnectDone     Channel thread signal done working
//
//   IOM thread
//     while (1)
//       {
//         iomInterruptWake
//         work...
//         iomInterruptDone
//      }

struct chnThreadz_t chnThreadz [N_IOM_UNITS_MAX] [MAX_CHANNELS];

// Create channel thread

void createChnThread (chnlState_t * chnlp, const char * devTypeStr, void * (* chnMain) (void *)) {
  uint iomNum = chnlp->myIomIdx;
  uint chnNum = chnlp->myChanIdx;
  int rc;
  struct chnThreadz_t * p = & chnThreadz[iomNum][chnNum];
  p->chnThreadArg = (void *) chnlp;

# ifdef tdbg
  p->inCnt = 0;
  p->outCnt = 0;
# endif
  p->ready = false;
  // initialize interrupt wait
  p->connect = false;
  rc = pthread_mutex_init (& p->connectLock, NULL);
  if (rc)
    simPrintf ("createChnThread pthread_mutex_init connectLock %d\n", rc);
  rc = pthread_cond_init (& p->connectCond, NULL);
  if (rc)
    simPrintf ("createChnThread pthread_cond_init connectCond %d\n", rc);

# if defined(__APPLE__)
  rc = pthread_create_with_cpu_policy (
          & p->chnThread,
          iomNum
          NULL,
          chnMain,
          & p->chnThreadArg);
# else
  rc = pthread_create (
          & p->chnThread,
          NULL,
          chnMain,
          & p->chnThreadArg);
# endif /* if defined(__APPLE__) */
  if (rc)
    simPrintf ("createChnThread pthread_create %d\n", rc);

  char nm [17];
  (void) sprintf (nm, "chn %c/%u %s", 'a' + iomNum, chnNum, devTypeStr);
# if defined(__FreeBSD__) || defined(__OpenBSD__)
  pthread_set_name_np (p->chnThread, nm);
# else
  pthread_setname_np (p->chnThread, nm);
# endif /* if defined(__FreeBSD__) || defined(__OpenBSD__) */
}

// Called by channel thread to block until I/O command presented

void chnConnectWait (chnlState_t * chnlp) {
  int rc;
  uint thisIomIdx  = chnlp->myIomIdx;
  uint thisChanNum = chnlp->myChanIdx;
  struct chnThreadz_t * p = & chnThreadz[thisIomIdx][thisChanNum];

  rc = pthread_mutex_lock (& p->connectLock);
  if (rc)
    simPrintf ("chnConnectWait pthread_mutex_lock %d\n", rc);
  p -> ready = true;
  while (! p->connect) {
    rc = pthread_cond_wait (& p->connectCond, & p->connectLock);
    if (rc)
      simPrintf ("chnConnectWait pthread_cond_wait %d\n", rc);
  }
# ifdef tdbg
  p->outCnt++;
  if (p->inCnt != p->outCnt)
    simPrintf ("chn thread %d in %d out %d\n", thisChanNum,
                p->inCnt, p->outCnt);
# endif
}

// Called by channel thread to signal I/O complete

void chnConnectDone (chnlState_t * chnlp) {
  int rc;
  uint thisIomIdx  = chnlp->myIomIdx;
  uint thisChanNum = chnlp->myChanIdx;
  struct chnThreadz_t * p = & chnThreadz[thisIomIdx][thisChanNum];
  p->connect = false;
  rc = pthread_cond_signal (& p->connectCond);
  if (rc)
    simPrintf ("chnInterruptDone pthread_cond_signal %d\n", rc);
  rc = pthread_mutex_unlock (& p->connectLock);
  if (rc)
    simPrintf ("chnConnectDone pthread_mutex_unlock %d\n", rc);
}

// Signal I/O presented to channel thread

void setChnConnect (chnlState_t * chnlp) {
  int rc;
  uint thisIomIdx  = chnlp->myIomIdx;
  uint thisChanNum = chnlp->myChanIdx;
  struct chnThreadz_t * p = & chnThreadz[thisIomIdx][thisChanNum];
  rc = pthread_mutex_lock (& p->connectLock);
  if (rc)
    simPrintf ("setChnConnect pthread_mutex_lock %d\n", rc);
  while (p->connect)
    {
      rc = pthread_cond_wait(&p->connectCond, &p->connectLock);
      if (rc)
        simPrintf ("setChnInterrupt pthread_cond_wait connectLock %d\n", rc);
    }
# ifdef tdbg
  p->inCnt++;
# endif
  p->connect = true;
  rc = pthread_cond_signal (& p->connectCond);
  if (rc)
    simPrintf ("setChnConnect pthread_cond_signal %d\n", rc);
  rc = pthread_mutex_unlock (& p->connectLock);
  if (rc)
    simPrintf ("setChnConnect pthread_mutex_unlock %d\n", rc);
}

// Block until channel thread ready

void chnRdyWait (chnlState_t * chnlp) {
  uint thisIomIdx  = chnlp->myIomIdx;
  uint thisChanNum = chnlp->myChanIdx;
  struct chnThreadz_t * p = & chnThreadz[thisIomIdx][thisChanNum];
  while (! p -> ready)
    sim_usleep (10000);
 }

void initThreadz (void)
  {
    // chnThreadz is sparse; make sure 'started' is false
    (void) memset (chnThreadz, 0, sizeof (chnThreadz));

#if defined(__FreeBSD__) || defined(__OpenBSD__)
    pthread_mutexattr_t scu_attr;
    pthread_mutexattr_init(&scu_attr);
# if !defined(__OpenBSD__)
    pthread_mutexattr_settype(&scu_attr, PTHREAD_MUTEX_ADAPTIVE_NP);
# endif
    pthread_mutex_init (& scu_lock, &scu_attr);
#else
    pthread_mutex_init (& scu_lock, NULL);
#endif /* FreeBSD || OpenBSD */
    pthread_mutexattr_t iom_attr;
    pthread_mutexattr_init(& iom_attr);
    pthread_mutexattr_settype(& iom_attr, PTHREAD_MUTEX_RECURSIVE);

    pthread_mutex_init (& iom_lock, & iom_attr);

    pthread_mutexattr_t libuv_attr;
    pthread_mutexattr_init(& libuv_attr);
    pthread_mutexattr_settype(& libuv_attr, PTHREAD_MUTEX_RECURSIVE);

    pthread_mutex_init (& libuv_lock, & libuv_attr);

  }

// Set up per-thread signal handlers

void int_handler (int signal);

void setSignals (void)
  {
#if !defined(__MINGW64__) && !defined(__MINGW32__)
    struct sigaction act;
    (void) memset (& act, 0, sizeof (act));
    act.sa_handler = int_handler;
    act.sa_flags = 0;
    sigaction (SIGINT, & act, NULL);
    sigaction (SIGTERM, & act, NULL);
#endif /* if !defined(__MINGW64__) && !defined(__MINGW32__) */
  }

#if 0
// Force cache coherency

static pthread_mutex_t fenceLock = PTHREAD_MUTEX_INITIALIZER;
void fence (void)
  {
    pthread_mutex_lock (& fenceLock);
    pthread_mutex_unlock (& fenceLock);
  }
#endif
