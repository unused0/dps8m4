#include <stdio.h>

#include "dps8m.h"
#include "utils.h"
#include "xray.h"

void fetchSDW (word24 addr, sdw_s * SDW) {
  word36 SDWeven, SDWodd;
  coreReadDWord (addr, & SDWeven, & SDWodd);
  (void) memset (SDW, 0, sizeof (sdw_s));
    
  //SDW->ADDR   = (SDWeven >> 12) & 077777777;
  //SDW->R1     = (SDWeven >> 9)  & 7;
  //SDW->R2     = (SDWeven >> 6)  & 7;
  //SDW->R3     = (SDWeven >> 3)  & 7;
  //SDW->DF     = TSTBIT (SDWeven,  2);
  //SDW->FC     = SDWeven & 3;
  SDW->ADDR  = getbits36 (SDWeven, B_SDWE_ADDR);
  SDW->R1    = getbits36 (SDWeven, B_SDWE_R1);
  SDW->R2    = getbits36 (SDWeven, B_SDWE_R2);
  SDW->R3    = getbits36 (SDWeven, B_SDWE_R3);
  SDW->DF    = getbits36 (SDWeven, B_SDWE_F);
  SDW->FC    = getbits36 (SDWeven, B_SDWE_FC);
 
  // odd word
  //SDW->BOUND  = (SDWodd >> 21) & 037777;
  //SDW->R      = TSTBIT (SDWodd,  20);
  //SDW->E      = TSTBIT (SDWodd,  19);
  //SDW->W      = TSTBIT (SDWodd,  18);
  //SDW->P      = TSTBIT (SDWodd,  17);
  //SDW->U      = TSTBIT (SDWodd,  16);
  //SDW->G      = TSTBIT (SDWodd,  15);
  //SDW->C      = TSTBIT (SDWodd,  14);
  //SDW->EB     = SDWodd & 037777;
  SDW->BOUND = getbits36 (SDWodd,  B_SDWO_BOUND);
  SDW->R     = getbits36 (SDWodd,  B_SDWO_R);
  SDW->E     = getbits36 (SDWodd,  B_SDWO_E);
  SDW->W     = getbits36 (SDWodd,  B_SDWO_W);
  SDW->P     = getbits36 (SDWodd,  B_SDWO_P);
  SDW->U     = getbits36 (SDWodd,  B_SDWO_U);
  SDW->G     = getbits36 (SDWodd,  B_SDWO_G);
  SDW->C     = getbits36 (SDWodd,  B_SDWO_C);
  SDW->EB    = getbits36 (SDWodd,  B_SDWO_EB);
}

void fetchPTW (word24 addr, ptw_s * PTW) {
  word36 ptw = coreReadWord (addr);
  (void) memset (PTW, 0, sizeof (ptw_s));
    //PTW->ADDR = GETHI  (ptw);
    //PTW->U    = TSTBIT (ptw, 9);
    //PTW->M    = TSTBIT (ptw, 6);
    //PTW->DF   = TSTBIT (ptw, 2);
    //PTW->FC   = ptw & 3;
    PTW->ADDR = getbits36 (ptw, B_PTW_ADDR);
    PTW->U    = getbits36 (ptw, B_PTW_U);
    PTW->M    = getbits36 (ptw, B_PTW_M);
    PTW->DF   = getbits36 (ptw, B_PTW_F);
    PTW->FC   = getbits36 (ptw, B_PTW_FC);

} 

void fetchDSBR (word24 addr, dsbr_s * DBSR) {
#if 0
  word36 even, odd;
  coreReadDWord (addr, & even, & odd);
  (void) memset (DBSR, 0, sizeof (dsbr_s));
  DSBR->ADDR = GETHI (even) << 6; // modulo 64
  if (TSTBIT (even, 11) {
    printf ("DSBR BIT 24 !=0\n");
  if (TSTBIT (even, 10) {
    printf ("DSBR BIT 25 !=0\n");
  DSBR->U = TST
#endif
}
