/*
 * vim: filetype=c:tabstop=4:ai:expandtab
 * SPDX-License-Identifier: ICU
 * scspell-id: 1881c7a6-f62f-11ec-b538-80ee73e9b8e7
 *
 * ---------------------------------------------------------------------------
 *
 * Copyright (c) 2007-2013 Michael Mondy
 * Copyright (c) 2012-2016 Harry Reed
 * Copyright (c) 2013-2024 Charles Anthony
 * Copyright (c) 2021-2024 The DPS8M Development Team
 *
 * This software is made available under the terms of the ICU License.
 * See the LICENSE.md file at the top-level directory of this distribution.
 *
 * ---------------------------------------------------------------------------
 */

// strdup to stack local
#define strdupl(d, s) \
  typeof (* s) d[strlen (s) + 1]; strcpy (d, s)

// Interface for cfgParse

enum { CFG_ERROR = -2, CFG_DONE = -1 };

typedef struct config_value_list_s
  {
    const char * value_name;
    int64_t value;
  } config_value_list_t;

typedef struct config_list_s
  {
    const char * name;  // opt name
    int64_t min, max;   // value limits
    config_value_list_t * value_list;
  } configList_t;

typedef struct config_state_s
  {
    char * copy;
    char * statement_save;
  } configState_t;

int cfgParse (const char * tag, const char * cptr, configList_t * clist, configState_t * state, int64_t * result);
void cfgParseDone (configState_t * state);

opcode_s * get_iwb_info (DCDstruct *i);
char * dump_flags(char * buffer, word18 flags);
char *disassemble(char * result, word36 instruction);
char *get_mod_string(char * msg, word6 tag);
word72 convert_to_word72 (word36 even, word36 odd);
void convert_to_word36 (word72 src, word36 *even, word36 *odd);

word36 compl36(cpuState_t * cpup, word36 op1, word18 *flags, bool * ovf);
word18 compl18(cpuState_t * cpup, word18 op1, word18 *flags, bool * ovf);

void copyBytes(int posn, word36 src, word36 *dst);
void copyChars(int posn, word36 src, word36 *dst);

void putByte(word36 *dst, word9 data, int posn);
void putChar(word36 *dst, word6 data, int posn);

void cmp36(cpuState_t * cpup, word36 op1, word36 op2, word18 *flags);
void cmp36wl(cpuState_t * cpup, word36 A, word36 Y, word36 Q, word18 *flags);
void cmp18(cpuState_t * cpup, word18 op1, word18 op2, word18 *flags);
void cmp72(cpuState_t * cpup, word72 op1, word72 op2, word18 *flags);

char *strlower(char *q);
int strmask(char *str, char *mask);
char *Strtok(char *, char *);
char *stripquotes(char *s);
char *trim(char *s);
char *ltrim(char *s);
char *rtrim(char *s);

//
// getbitsNN/setbitsNN/putbitsNN
//
//   Manipulate bitfields.
//     NN      the word size (18, 36, 72).
//     data    the incoming word
//     i       the starting bit number (DPS8M notation, 0 is the MSB)
//     n       the number of bits, starting at i
//     val     the bits
//
//   val = getbitsNN (data, i, n)
//
//       extract n bits from data, starting at i.
//
//           val = getbits36 (data, 0, 1) --> the sign bit
//           val = getbits36 (data, 18, 18) --> the low eighteen bits
//
//   newdata = setbitsNN (data, i, n, val)
//
//       return 'data' with n bits of val inserted.
//
//           newdata = setbits36 (data, 0, 18, 1) --> move '1' into the high
//                                                    18 bits.
//
//   putbitsNN (& data, i, n, val)
//
//        put val into data (equivalent to 'data = setbitsNN (data, ....)'
//
//           putbits (& data, 0, 18, 1) --> set the high 18 bits to '1'.
//

#if 0
static inline word72 getbits72 (word72 x, uint i, uint n)
  {
    // bit 71 is right end, bit zero is 72nd from the right
    int shift = 71 - (int) i - (int) n + 1;
    if (shift < 0 || shift > 71)
      {
        simPrintf ("getbits72: bad args (i=%d,n=%d)\n", i, n);
        return 0;
      }
     else
      return (x >> (unsigned) shift) & ~ ((~ W36_C (0)) << n);
  }
#endif

static inline word36 getbits36(word36 x, uint i, uint n) {
    // bit 35 is right end, bit zero is 36th from the right
    int shift = 35-(int)i-(int)n+1;
    if (shift < 0 || shift > 35) {
        simPrintf ("getbits36: bad args (%012"PRIu64",i=%d,n=%d)\n", x, i, n);
        return 0;
    } else
        return (x >> (unsigned) shift) & ~ ((~ W36_C (0)) << n);
}

#define GETBITS36_N(N)                                                           \
static inline word##N getbits36_##N (word36 x, uint i)                           \
  {                                                                              \
    /* bit 35 is right end, bit zero is 36th from the right */                   \
    int shift = 35-(int)i-(int)N+1;                                              \
    if (shift < 0 || shift > 35) {                                               \
        simPrintf ("getbits36_" #N ": bad args (%012"PRIu64",i=%d)\n", x, i);   \
        return 0;                                                                \
    } else                                                                       \
        return (x >> (unsigned) shift) & MASK##N;                                \
  }

GETBITS36_N (1)
GETBITS36_N (2)
GETBITS36_N (3)
GETBITS36_N (4)
GETBITS36_N (5)
GETBITS36_N (6)
GETBITS36_N (7)
GETBITS36_N (8)
GETBITS36_N (9)
GETBITS36_N (10)
GETBITS36_N (12)
GETBITS36_N (14)
GETBITS36_N (15)
GETBITS36_N (16)
GETBITS36_N (18)
GETBITS36_N (24)
GETBITS36_N (28)

static inline word36 setbits36(word36 x, uint p, uint n, word36 val)
{
    int shift = 36 - (int) p - (int) n;
    if (shift < 0 || shift > 35) {
        simPrintf ("setbits36: bad args (%012"PRIu64",pos=%d,n=%d)\n", x, p, n);
        return 0;
    }
    word36 mask = ~ (( ~ W36_C (0)) <<n);  // n low bits on
    mask <<= (unsigned) shift;  // shift 1s to proper position; result 0*1{n}0*
    // caller may provide val that is too big, e.g., a word with all bits
    // set to one, so we mask val
    word36 result = (x & ~ mask) | ((val&MASKBITS(n)) << (36 - p - n));
    return result;
}

#define SETBITS36_N(N)                                                                              \
static inline word36 setbits36_##N (word36 x, uint p, word##N val)                                  \
{                                                                                                   \
    const int n = N;                                                                                \
    int shift = 36 - (int) p - (int) n;                                                             \
    if (shift < 0 || shift > 35) {                                                                  \
        simPrintf ("setbits36_" #N ": bad args (%012"PRIu64",pos=%d)\n", x, p);                    \
        return 0;                                                                                   \
    }                                                                                               \
    word36 mask = ~ ((~ W36_C (0)) << n);  /* n low bits on */                                      \
    word36 smask = mask << (unsigned) shift;  /* shift 1s to proper position; result 0*1{n}0* */    \
    /* caller may provide val that is too big, e.g., a word with all bits */                        \
    /* set to one, so we mask val */                                                                \
    word36 result = (x & ~ smask) | (((word36) val & mask) << shift);                               \
    return result;                                                                                  \
}

SETBITS36_N(1)
SETBITS36_N(4)
SETBITS36_N(5)
SETBITS36_N(6)
SETBITS36_N(8)
SETBITS36_N(9)
SETBITS36_N(16)
SETBITS36_N(24)

static inline void putbits36 (word36 * x, uint p, uint n, word36 val)
  {
    int shift = 36 - (int) p - (int) n;
    if (shift < 0 || shift > 35)
      {
        simPrintf ("putbits36: bad args (%012"PRIu64",pos=%d,n=%d)\n", *x, p, n);
        return;
      }
    word36 mask = ~ ((~ W36_C (0)) << n);  // n low bits on
    mask <<= (unsigned) shift;  // shift 1s to proper position; result 0*1{n}0*
    // caller may provide val that is too big, e.g., a word with all bits
    // set to one, so we mask val
    * x = (* x & ~mask) | ((val & MASKBITS (n)) << (36 - p - n));
    return;
  }

#define PUTBITS36_N(N)                                                                              \
static inline void putbits36_##N (word36 * x, uint p, word##N val)                                  \
{                                                                                                   \
    const int n = N;                                                                                \
    int shift = 36 - (int) p - (int) n;                                                             \
    if (shift < 0 || shift > 35) {                                                                  \
        simPrintf ("putbits36_" #N ": bad args (%012"PRIu64",pos=%d)\n", *x, p);                   \
        return;                                                                                     \
    }                                                                                               \
    word36 mask = ~ ((~ W36_C (0)) << n);  /* n low bits on */                                      \
    word36 smask = mask << (unsigned) shift;  /* shift 1s to proper position; result 0*1{n}0* */    \
    /* caller may provide val that is too big, e.g., a word with all bits */                        \
    /* set to one, so we mask val */                                                                \
    * x = (* x & ~ smask) | (((word36) val & mask) << shift);                                       \
}

PUTBITS36_N (1)
PUTBITS36_N (2)
PUTBITS36_N (3)
PUTBITS36_N (4)
PUTBITS36_N (5)
PUTBITS36_N (6)
PUTBITS36_N (7)
PUTBITS36_N (8)
PUTBITS36_N (9)
PUTBITS36_N (10)
PUTBITS36_N (12)
PUTBITS36_N (13)
PUTBITS36_N (14)
PUTBITS36_N (15)
PUTBITS36_N (16)
PUTBITS36_N (17)
PUTBITS36_N (18)
PUTBITS36_N (23)
PUTBITS36_N (24)
PUTBITS36_N (28)

static inline void putbits72 (word72 * x, uint p, uint n, word72 val)
  {
    int shift = 72 - (int) p - (int) n;
    if (shift < 0 || shift > 71)
      {
        simPrintf ("putbits72: bad args (pos=%d,n=%d)\n", p, n);
        return;
      }
#if defined(NEED_128)
// n low bits on
    uint64_t lowmask = 0;
    uint64_t highmask = 0;
    if (n >= 64)
      {
        lowmask = MASK64;
        highmask = ~ ((~(uint64_t)0) << (n - 64));
        highmask &= 0377U;
      }
    else
      {
        lowmask = ~ ((~(uint64_t)0) << n);
      }
    word72 mask = construct_128 (highmask, lowmask);
    mask = lshift_128 (mask, (uint) shift);
    word72 notmask = complement_128 (mask);
    * x = or_128 (and_128 (* x, notmask), and_128 (lshift_128 (val, 72 - p - n), mask));
#else
    word72 mask = ~ ((~(word72)0) << n);  // n low bits on
    mask <<= (unsigned) shift;  // shift 1s to proper position; result 0*1{n}0*
    // caller may provide val that is too big, e.g., a word with all bits
    // set to one, so we mask val
    * x = (* x & ~mask) | ((val & MASKBITS72 (n)) << (72 - p - n));
#endif
    return;
  }

//  getbits18 (data, starting bit, number of bits)

static inline word18 getbits18 (word18 x, uint i, uint n)
  {
    // bit 17 is right end, bit zero is 18th from the right
    int shift = 17 - (int) i - (int) n + 1;
    if (shift < 0 || shift > 17)
      {
        simPrintf ("getbits18: bad args (%06o,i=%d,n=%d)\n", x, i, n);
        return 0;
      }
    else
      return (x >> (unsigned) shift) & ~ ((~ W36_C (0)) << n);
  }

//  putbits18 (data, starting bit, number of bits, bits)

static inline void putbits18 (word18 * x, uint p, uint n, word18 val)
  {
    int shift = 18 - (int) p - (int) n;
    if (shift < 0 || shift > 17)
      {
        simPrintf ("putbits18: bad args (%06o,pos=%d,n=%d)\n", * x, p, n);
        return;
      }
    word18 mask = ~ ((~ W36_C (0)) << n);  // n low bits on
    mask <<= (unsigned) shift;  // shift 1s to proper position; result 0*1{n}0*
    // caller may provide val that is too big, e.g., a word with all bits
    // set to one, so we mask val
    * x = (* x & ~mask) | ((val & MASKBITS18 (n)) << (18 - p - n));
    return;
  }

//
// setmask -- set bits from mask
//

static inline void setmask (word36 * v, word36 mask)
  {
    * v |= mask;
  }

//
// clrmask -- clear bits from mask
//

static inline void clrmask (word36 * v, word36 mask)
  {
    * v &= ~mask;
  }

char * strdupesc (const char * str);
word36 extr36 (uint8_t * bits, uint woffset);
void put36 (word36 val, uint8_t * bits, uint woffset);
int extractASCII36FromBuffer (uint8_t * bufp, t_mtrlnt tbc, uint * words_processed, word36 *wordp);
int extractWord36FromBuffer (uint8_t * bufp, t_mtrlnt tbc, uint * words_processed, word36 *wordp);
int insertASCII36toBuffer (uint8_t * bufp, t_mtrlnt tbc, uint * words_processed, word36 word);
int insertWord36toBuffer (uint8_t * bufp, t_mtrlnt tbc, uint * words_processed, word36 word);
void print_int128 (int128 n, char * p);
char * print_int128o (int128 n, char * p);
word36 Add36b (cpuState_t * cpup, word36 op1, word36 op2, word1 carryin, word18 flagsToSet, word18 * flags, bool * ovf);
word36 Sub36b (cpuState_t * cpup, word36 op1, word36 op2, word1 carryin, word18 flagsToSet, word18 * flags, bool * ovf);
word18 Add18b (cpuState_t * cpup, word18 op1, word18 op2, word1 carryin, word18 flagsToSet, word18 * flags, bool * ovf);
word18 Sub18b (cpuState_t * cpup, word18 op1, word18 op2, word1 carryin, word18 flagsToSet, word18 * flags, bool * ovf);
word72 Add72b (cpuState_t * cpup, word72 op1, word72 op2, word1 carryin, word18 flagsToSet, word18 * flags, bool * ovf);
word72 Sub72b (cpuState_t * cpup, word72 op1, word72 op2, word1 carryin, word18 flagsToSet, word18 * flags, bool * ovf);

void timespec_diff(struct timespec *start, struct timespec *stop,
                   struct timespec *result);

void currentTR (word27 * trunits, bool * ovf);
