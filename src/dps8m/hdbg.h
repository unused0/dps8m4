/*
 * vim: filetype=c:tabstop=4:ai:expandtab
 * SPDX-License-Identifier: ICU
 * scspell-id: 5e1e2bcf-f62f-11ec-8cca-80ee73e9b8e7
 *
 * ---------------------------------------------------------------------------
 *
 * Copyright (c) 2016-2024 Charles Anthony
 * Copyright (c) 2021-2024 The DPS8M Development Team
 *
 * This software is made available under the terms of the ICU License.
 * See the LICENSE.md file at the top-level directory of this distribution.
 *
 * ---------------------------------------------------------------------------
 */

#if !defined(HDBG_H)
# define HDBG_H
# if defined(HDBG)

// void hdbg_mark (void);
simRc hdbg_size (int32_t arg, UNUSED const char * buf);
simRc hdbg_print (int32_t arg, UNUSED const char * buf);
simRc hdbg_cpu_mask (UNUSED int32_t arg, const char * buf);
simRc hdbgSegmentNumber (UNUSED int32_t arg, const char * buf);
simRc hdbgBlacklist (UNUSED int32_t arg, const char * buf);
simRc hdbgLive (UNUSED int32_t arg, const char * buf);

void hdbgTrace (cpuState_t * cpup, const char * ctx);
void hdbgPrint (void);
enum hdbgIEFP_e
  {
    hdbgIEFP_abs_bar_read,
    hdbgIEFP_abs_read,
    hdbgIEFP_bar_read,
    hdbgIEFP_read,
    hdbgIEFP_abs_bar_write,
    hdbgIEFP_abs_write,
    hdbgIEFP_bar_write,
    hdbgIEFP_write
  };
void hdbgIEFP (cpuState_t * cpup, enum hdbgIEFP_e type, word15 segno, word18 offset, const char * ctx);
void hdbgMRead (cpuState_t * cpup, word24 addr, word36 data, const char * ctx);
void hdbgMWrite (cpuState_t * cpup, word24 addr, word36 data, const char * ctx);
void hdbgFault (cpuState_t * cpup, _fault faultNumber, _fault_subtype subFault,
                const char * faultMsg, const char * ctx);
void hdbgIntrCellSet (cpuState_t * cpup, uint scuUnitIdx, uint cell, const char * ctx);
void hdbgIntrSet (cpuState_t * cpup, uint inum, uint cpuUnitIdx, uint scuUnitIdx, const char * ctx);
void hdbgIntr (cpuState_t * cpup, uint intr_pair_addr, const char * ctx);
void hdbgNote (cpuState_t * cpup, const char * ctx, const char * fmt, ...)
#  if defined(__GNUC__)
  __attribute__ ((format (printf, 3, 4)))
#  endif /* if defined(__GNUC__) */
;

// Keep sync'd with regNames
enum hregs_t
  {
    hreg_A,
    hreg_Q,
    hreg_E,
    hreg_X0, hreg_X1, hreg_X2, hreg_X3, hreg_X4, hreg_X5, hreg_X6, hreg_X7,
    hreg_AR0, hreg_AR1, hreg_AR2, hreg_AR3, hreg_AR4, hreg_AR5, hreg_AR6, hreg_AR7,
    hreg_PR0, hreg_PR1, hreg_PR2, hreg_PR3, hreg_PR4, hreg_PR5, hreg_PR6, hreg_PR7,
    hreg_Y, hreg_Z,
    hreg_IR,
    hreg_DSBR,
  };
void hdbgRegR (cpuState_t * cpup, enum hregs_t type, word36 data, const char * ctx);
void hdbgRegW (cpuState_t * cpup, enum hregs_t type, word36 data, const char * ctx);
void hdbgPARegR (cpuState_t * cpup, enum hregs_t type, par_s * data, const char * ctx);
void hdbgPARegW (cpuState_t * cpup, enum hregs_t type, par_s * data, const char * ctx);
#  if 0
void hdbgDSBRRegR (enum hregs_t type, dsbr_s * data, const char * ctx);
void hdbgDSBRRegW (enum hregs_t type, dsbr_s * data, const char * ctx);
#  endif
void hdbgAPURead (cpuState_t * cpup, word15 segno, word18 offset, word24 final, word36 data, const char * ctx);
void hdbgAPUWrite (cpuState_t * cpup, word15 segno, word18 offset, word24 final, word36 data, const char * ctx);

#if 0
#  define HDBGMRead(cpup, a, d, c) /* hdbgMRead (cpup, a, d, c) */
#  define HDBGMWrite(cpup, a, d, c) /* hdbgMWrite (cpup, a, d, c) */
#  define HDBGIEFP(cpup, t, s, o, c) /* hdbgIEFP (cpup, t, s, o, c); */
#  define HDBGAPURead(cpup, s, o, f, d, c) /* hdbgAPURead (cpup, s, o, f, d, c) */
#  define HDBGAPUWrite(cpup, s, o, f, d, c) /* hdbgAPUWrite (cpup, s, o, f, d, c) */
#  define HDBGRegAR(cpup, c) /* hdbgRegR (cpup, hreg_A, cpu.rA, c) */
#  define HDBGRegAW(cpup, c) /* hdbgRegW (cpup, hreg_A, cpu.rA, c) */
#  define HDBGRegQR(cpup, c) /* hdbgRegR (cpup, hreg_Q, cpu.rQ, c) */
#  define HDBGRegQW(cpup, c) /* hdbgRegW (cpup, hreg_Q, cpu.rQ, c) */
#  define HDBGRegER(cpup, c) /* hdbgRegR (cpup, hreg_E, cpu.rQ, c) */
#  define HDBGRegEW(cpup, c) /* hdbgRegW (cpup, hreg_E, cpu.rQ, c) */
#  define HDBGRegXR(cpup, i, c) /* hdbgRegR (cpup, hreg_X0+(i), (word36) cpu.rX[i], c) */
#  define HDBGRegXW(cpup, i, c) /* hdbgRegW (cpup, hreg_X0+(i), (word36) cpu.rX[i], c) */
#  define HDBGRegYR(cpup, c) /* hdbgRegR (cpup, hreg_Y, (word36) cpu.CY, c) */
#  define HDBGRegYW(cpup, c) /* hdbgRegW (cpup, hreg_Y, (word36) cpu.CY, c) */
#  define HDBGRegZR(cpup, r, c) /* hdbgRegR (cpup, hreg_Z, (word36) r, c) */
#  define HDBGRegZW(cpup, r, c) /* hdbgRegW (cpup, hreg_Z, (word36) r, c) */
#  define HDBGRegPRR(cpup, i, c) /* hdbgPARegR (cpup, hreg_PR0+(i), & cpu.PAR[i], c) */
#  define HDBGRegPRW(cpup, i, c) /* hdbgPARegW (cpup, hreg_PR0+(i), & cpu.PAR[i], c) */
#  define HDBGRegARR(cpup, i, c) /* hdbgPARegR (cpup, hreg_AR0+(i), & cpu.PAR[i], c) */
#  define HDBGRegARW(cpup, i, c) /* hdbgPARegW (cpup, hreg_AR0+(i), & cpu.PAR[i], c) */
#  if 0
#   define HDBGRegDSBRR(cpup, i, c) /* hdbgDSBRRegR (cpup, hreg_AR0+(i), & cpu.PAR[i], c) */
#   define HDBGRegDSBRW(cpup, i, c) /* hdbgDSBRRegW (cpup, hreg_AR0+(i), & cpu.PAR[i], c) */
#  endif
#  define HDBGRegIR(cpup, c) /* hdbgRegW (cpup, hreg_IR, (word36) cpu.cu.IR, c) */
#  define HDBGTrace(cpup, c) hdbgTrace(cpup,c)
#  define HDBGNote(cpup, c, f, ...) /* hdbgNote(cpup, c, f, __VA_ARGS__) */
#  define HDBGIntr(cpup, i, c) hdbgIntr(cpup, i, c)
#  define HDBGIntrCellSet(cpup, s, c, ctx) hdbgIntrCellSet(cpup, s, c, ctx)
#  define HDBGIntrSet(cpup, i, c, s, ctx) hdbgIntrSet(cpup, i, c, s, ctx)
#  define HDBGFault(cpup, n, s, m, c) hdbgFault(cpup, n, s, m, c)
#  define HDBGPrint() hdbgPrint()
#else
#  define HDBGMRead(cpup, a, d, c) hdbgMRead (cpup, a, d, c)
#  define HDBGMWrite(cpup, a, d, c) hdbgMWrite (cpup, a, d, c)
#  define HDBGIEFP(cpup, t, s, o, c) hdbgIEFP (cpup, t, s, o, c);
#  define HDBGAPURead(cpup, s, o, f, d, c) hdbgAPURead (cpup, s, o, f, d, c)
#  define HDBGAPUWrite(cpup, s, o, f, d, c) hdbgAPUWrite (cpup, s, o, f, d, c)
#  define HDBGRegAR(cpup, c) hdbgRegR (cpup, hreg_A, cpu.rA, c)
#  define HDBGRegAW(cpup, c) hdbgRegW (cpup, hreg_A, cpu.rA, c)
#  define HDBGRegQR(cpup, c) hdbgRegR (cpup, hreg_Q, cpu.rQ, c)
#  define HDBGRegQW(cpup, c) hdbgRegW (cpup, hreg_Q, cpu.rQ, c)
#  define HDBGRegER(cpup, c) hdbgRegR (cpup, hreg_E, cpu.rQ, c)
#  define HDBGRegEW(cpup, c) hdbgRegW (cpup, hreg_E, cpu.rQ, c)
#  define HDBGRegXR(cpup, i, c) hdbgRegR (cpup, hreg_X0+(i), (word36) cpu.rX[i], c)
#  define HDBGRegXW(cpup, i, c) hdbgRegW (cpup, hreg_X0+(i), (word36) cpu.rX[i], c)
#  define HDBGRegYR(cpup, c) hdbgRegR (cpup, hreg_Y, (word36) cpu.CY, c)
#  define HDBGRegYW(cpup, c) hdbgRegW (cpup, hreg_Y, (word36) cpu.CY, c)
#  define HDBGRegZR(cpup, r, c) hdbgRegR (cpup, hreg_Z, (word36) r, c)
#  define HDBGRegZW(cpup, r, c) hdbgRegW (cpup, hreg_Z, (word36) r, c)
#  define HDBGRegPRR(cpup, i, c) hdbgPARegR (cpup, hreg_PR0+(i), & cpu.PAR[i], c)
#  define HDBGRegPRW(cpup, i, c) hdbgPARegW (cpup, hreg_PR0+(i), & cpu.PAR[i], c)
#  define HDBGRegARR(cpup, i, c) hdbgPARegR (cpup, hreg_AR0+(i), & cpu.PAR[i], c)
#  define HDBGRegARW(cpup, i, c) hdbgPARegW (cpup, hreg_AR0+(i), & cpu.PAR[i], c)
#  if 0
#   define HDBGRegDSBRR(cpup, i, c) hdbgDSBRRegR (cpup, hreg_AR0+(i), & cpu.PAR[i], c)
#   define HDBGRegDSBRW(cpup, i, c) hdbgDSBRRegW (cpup, hreg_AR0+(i), & cpu.PAR[i], c)
#  endif
#  define HDBGRegIR(cpup, c) hdbgRegW (cpup, hreg_IR, (word36) cpu.cu.IR, c)
#  define HDBGTrace(cpup, c) hdbgTrace(cpup,c)
#  define HDBGNote(cpup, c, f, ...) hdbgNote(cpup, c, f, __VA_ARGS__)
#  define HDBGIntr(cpup, i, c) /* hdbgIntr(cpup, i, c) */
#  define HDBGIntrCellSet(cpup, s, c, ctx) /* hdbgIntrCellSet(cpup, s, c, ctx) */
#  define HDBGIntrSet(cpup, i, c, s, ctx) /* hdbgIntrSet(cpup, i, c, s, ctx) */
#  define HDBGFault(cpup, n, s, m, c) hdbgFault(cpup, n, s, m, c)
#  define HDBGPrint() hdbgPrint()
#endif
# else /* defined(HDBG) */
#  define HDBGMRead(cpup, a, d, c)
#  define HDBGMWrite(cpup, a, d, c)
#  define HDBGIEFP(cpup, t, s, o, c)
#  define HDBGAPURead(cpup, s, o, f, d, c)
#  define HDBGAPUWrite(cpup, s, o, f, d, c)
#  define HDBGRegAR(cpup, c)
#  define HDBGRegAW(cpup, c)
#  define HDBGRegQR(cpup, c)
#  define HDBGRegQW(cpup, c)
#  define HDBGRegER(cpup, c)
#  define HDBGRegEW(cpup, c)
#  define HDBGRegXR(cpup, i, c)
#  define HDBGRegXW(cpup, i, c)
#  define HDBGRegYR(cpup, c)
#  define HDBGRegYW(cpup, c)
#  define HDBGRegZR(cpup, r, c)
#  define HDBGRegZW(cpup, r, c)
#  define HDBGRegPRR(cpup, i, c)
#  define HDBGRegPRW(cpup, i, c)
#  define HDBGRegARR(cpup, i, c)
#  define HDBGRegARW(cpup, i, c)
#  if 0
#   define HDBGRegDSBRR(cpup, i, c)
#   define HDBGRegDSBRW(cpup, i, c)
#  endif
#  define HDBGRegIR(cpup, c)
#  define HDBGTrace(cpup, c)
#  define HDBGNote(cpup, c, f, ...)
#  define HDBGIntr(cpup, i, c)
#  define HDBGIntrCellSet(cpup, s, c, ctx)
#  define HDBGIntrSet(cpup, i, c, s, ctx)
#  define HDBGFault(cpup, n, s, m, c)
#  define HDBGPrint()
# endif /* defined(HDBG) */
#endif /* if !defined(HDBG_H) */
