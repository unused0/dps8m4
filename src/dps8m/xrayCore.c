#include "dps8m.h"
#include "iom_channel.h"
#include "iom.h"
#include "cable.h"
#include "cmd.h"
#include "tap.h"
#include "state.h"
#include "xray.h"


word36 coreReadWord (word24 addr) {
  return system_state->M[addr];
}

#if 0
word72 coreReadWordDouble (word24 addr) {
  addr &= ~1u;
  word36 even = system_state->M[addr];
  word36 odd = system_state->M[addr + 1];
  return (((word72) even) << 36) | odd;
}
#endif
void coreReadDWord (word24 addr, word36 * even, word36 * odd) {
  addr &= ~1u;
  * even = system_state->M[addr];
  * odd = system_state->M[addr + 1];
}

