%{

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include "parse.tab.h"
#include "dps8m.h"

#define IDX(c) (tolower(c) - 'a')
%}

%option nounput
%option noinput

%%

[ \t]+  { /* ignore all whitespace */ }
"//".*  { /* ignore // comments */ }
"#".*   { /* ignore # comments */ }

";"                                 {                                                  return SEMICOLON; }
","                                 { return COMMA; }

[0-7]+                              { if (evalNumber (yytext, & yylval)) return ERROR; return NUMBER_OVALUE; }
[0-9]+\.                            { if (evalNumber (yytext, & yylval)) return ERROR; return NUMBER_DVALUE; }

(?i:32k)                            { yylval =  8;                                     return NUMBER_OVALUE; }
(?i:64k)                            { yylval =  9;                                     return NUMBER_OVALUE; }
(?i:128k)                           { yylval = 10;                                     return NUMBER_OVALUE; }
(?i:256k)                           { yylval = 11;                                     return NUMBER_OVALUE; }
(?i:512k)                           { yylval = 12;                                     return NUMBER_OVALUE; }
(?i:1024k)                          { yylval = 13;                                     return NUMBER_OVALUE; }
(?i:1m)                             { yylval = 13;                                     return NUMBER_OVALUE; }
(?i:2048k)                          { yylval = 14;                                     return NUMBER_OVALUE; }
(?i:2m)                             { yylval = 14;                                     return NUMBER_OVALUE; }
(?i:4096k)                          { yylval = 15;                                     return NUMBER_OVALUE; }
(?i:4m)                             { yylval = 15;                                     return NUMBER_OVALUE; }
(?i:8\/52)                          {                                                  return S852; }
(?i:8\/70)                          {                                                  return S870; }
(?i:[a-z])                          { yylval = IDX (yytext[0]);                        return ID; }

(?i:a1)                             {                                                  return A1_; }
(?i:address)                        {                                                  return ADDRESS; }
(?i:affinity)                       {                                                  return AFFINITY_; }
(?i:alt)                            {                                                  return ALT; }
(?i:assignment)                     {                                                  return ASSIGNMENT; }

(?i:b1)                             {                                                  return B1_; }

(?i:cache)                          {                                                  return CACHE; }
(?i:clockslave)                     {                                                  return CLOCKSLAVE; }
(?i:config)                         {                                                  return CONFIG; }
(?i:configuration)                  {                                                  return CONFIG; }
(?i:chnl)                           {                                                  return CHNL; }
(?i:ccu)                            { yylval = IDX (yytext[3]);                        return CCU; }
(?i:cpu)                            {                                                  return CPU; }
(?i:cpu[a-h])                       { yylval = IDX (yytext[3]);                        return CPUx; }
(?i:cyclic)                         {                                                  return CYCLIC; }

(?i:data)                           {                                                  return DATA; }
(?i:dia)                            {                                                  return DIA; }
(?i:disable)                        {                                                  return DISABLE; }
(?i:down)                           {                                                  return DOWN; }
(?i:dps8m)                          {                                                  return DPS8; }
(?i:dps8)                           {                                                  return DPS8; }
(?i:dps)                            {                                                  return DPS;  }
(?i:dsk)                            {                                                  return DSK; }

(?i:elapsed_days)                   {                                                  return ELAPSED_DAYS; }
(?i:emcall)                         {                                                  return EMCALL; }
(?i:enable)                         {                                                  return ENABLE; }

(?i:faultbase)                      {                                                  return FAULTBASE; }
(?i:fips)                           {                                                  return FIPS; }
(?i:fnp)                            {                                                  return FNP; }

(?i:gcos)                           {                                                  return GCOS; }

(?i:hexmode)                        {                                                  return HEXMODE; }

(?i:imu)                            {                                                  return IMU; }
(?i:initialize)                     {                                                  return INITIALIZE; }
(?i:inop)                           {                                                  return INOP; }
(?i:installed)                      {                                                  return INSTALLED; }
(?i:interlace)                      {                                                  return INTERLACE; }
(?i:io)                             {                                                  return IO; }
(?i:iom)                            {                                                  return IOM; }
(?i:ipc)                            {                                                  return IPC; }
(?i:isolts_mode)                    {                                                  return ISOLTS_MODE; }

(?i:kips)                           {                                                  return KIPS;  }

(?i:l68)                            {                                                  return L68;  }
(?i:lss)                            {                                                  return LSS;  }
(?i:lower)                            {                                                  return LOWER;  }
(?i:luf)                            {                                                  return LUF;  }

(?i:manual)                         {                                                  return MANUAL; }
(?i:map)                            {                                                  return MAP; }
(?i:mask)                           {                                                  return MASK; }
(?i:mask[a-b])                      { yylval = IDX (yytext[4]);                        return MASKx; }
(?i:mem)                            {                                                  return MEM; }
(?i:mode)                           {                                                  return MODE; }
(?i:mpc)                            {                                                  return MPC; }
(?i:msp)                            {                                                  return MSP; }
(?i:mtp)                            {                                                  return MTP; }
(?i:multics)                        {                                                  return MULTICS; }

(?i:nea)                            {                                                  return NEA; }
(?i:not)                            {                                                  return NOT; }
(?i:number)                         {                                                  return NUMBER; }

(?i:off)                            {                                                  return OFF; }
(?i:offline)                        {                                                  return OFFLINE; }
(?i:on)                             {                                                  return ON; }
(?i:online)                         {                                                  return ONLINE; }
(?i:opc)                            {                                                  return OPC; }

(?i:port)                           {                                                  return PORT; }
(?i:port[a-h])                      { yylval = IDX (yytext[4]);                        return PORTx; }
(?i:program)                        {                                                  return PROGRAM; }
(?i:prom)                           {                                                  return PROM; }
(?i:prph)                           {                                                  return PRPH; }
(?i:prt)                            {                                                  return PRT; }
(?i:ptwam)                          {                                                  return PTWAM; }
(?i:pun)                            {                                                  return PUN; }

(?i:rdr)                            {                                                  return RDR; }

(?i:reset)                          {                                                  return RESET; }
(?i:run_start)                      {                                                  return RUN_START; }

(?i:scu)                            {                                                  return SCU; }
(?i:scu[a-h])                       { yylval = IDX (yytext[3]);                        return SCUx; }
(?i:sdwam)                          {                                                  return SDWAM; }
(?i:set)                            {                                                  return SET; }
(?i:show)                           {                                                  return SHOW; }
(?i:speed)                          {                                                  return SPEED_; }
(?i:state)                          {                                                  return STATE; }
(?i:steady_clock)                   {                                                  return STEADY_CLOCK; }
(?i:stop_number)                    {                                                  return STOP_NUMBER; }
(?i:store_size)                     {                                                  return STORE_SIZE; }

(?i:tap)                            {                                                  return TAP; }
(?i:tro)                            {                                                  return TRO; }

(?i:up)                             {                                                  return UP; }
(?i:urp)                            {                                                  return URP; }

(?i:wam)                            {                                                  return WAM; }

(?i:y2k)                            {                                                  return Y2K; }


. return yytext[0];

%%

void set_input_string (const char * in) {
  yy_scan_string(in);
}

void end_lexical_scan (void) {
  yy_delete_buffer (YY_CURRENT_BUFFER);
}

