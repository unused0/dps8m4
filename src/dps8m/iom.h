/*
 * vim: filetype=c:tabstop=4:ai:expandtab
 * SPDX-License-Identifier: ICU
 * scspell-id: 8e66ce24-f62e-11ec-8690-80ee73e9b8e7
 *
 * ---------------------------------------------------------------------------
 *
 * Copyright (c) 2007-2013 Michael Mondy
 * Copyright (c) 2012-2016 Harry Reed
 * Copyright (c) 2013-2024 Charles Anthony
 * Copyright (c) 2021-2024 The DPS8M Development Team
 *
 * This software is made available under the terms of the ICU License.
 * See the LICENSE.md file at the top-level directory of this distribution.
 *
 * ---------------------------------------------------------------------------
 */

// IDCW Instruction                   18-20  111
// TDCW Transfer                      18-20 !111,  22-23 10
// IOTD IO Transfer and disconnect    18-20 !111,  22-23 00
// IOTP IO Transfer and proceed       18-20 !111,  22-23 01
// IONTP IO Non-Transfer and proceed  18-20 !111,  22-23 11

#if defined(IO_THREADZ)
extern __thread uint this_iom_idx;
extern __thread uint this_chan_num;
//extern __thread bool thisIOMHaveLock;
#endif

typedef struct iomState_s {
  uint64_t connectCnt;
  uint64_t dcwCnt;
  coreLockState_t  iomCoreLockState;
} iomState_t;

extern iomState_t ioms[N_IOM_UNITS_MAX];

extern DEVICE iom_dev;

#if 0
typedef struct pcw_t
  {
    // Word 1
    uint dev_cmd;    // 6 bits; 0..5
    uint dev_code;   // 6 bits; 6..11
    uint ext;        // 6 bits; 12..17; address extension
    uint cp;         // 3 bits; 18..20, must be all ones

// From iom_control.alm:
//  " At this point we would normally set idcw.ext_ctl.  This would allow IOM's
//  " to transfer to DCW lists which do not reside in the low 256K.
//  " Unfortunately, the PSIA does not handle this bit properly.
//  " As a result, we do not set the bit and put a kludge in pc_abs so that
//  " contiguous I/O buffers are always in the low 256K.
//  "
//  " lda       =o040000,dl         " set extension control in IDCW
//  " orsa      ab|0

    uint mask;    // extension control or mask; 1 bit; bit 21
    uint control;    // 2 bits; bit 22..23
    uint chan_cmd;   // 6 bits; bit 24..29;
    // AN87 says: 00 single record xfer, 02 non data xfer,
    // 06 multi-record xfer, 10 single char record xfer
    uint chan_data;  // 6 bits; bit 30..35; often some sort of count
    //

    // Word 2
    uint chan;       // 6 bits; bits 3..8 of word 2
    uint ptPtr;        // 18 bits; bits 9..26 of word 2
    uint ptp;    // 1 bit; bit 27 of word 2
    uint pcw64_pge;    // 1 bit; bit 28 of word 2
    uint aux;    // 1 bit; bit 29 of word 2
  } pcw_t;
#endif

#define IOM_MBX_LPW     0
#define IOM_MBX_LPWX    1
#define IOM_MBX_SCW     2
#define IOM_MBX_DCW     3

//
// iom_cmd_t returns:
//
//  0: ok
//  1; ignored cmd, drop connect.
//  2: did command, don't do DCW list
//  3; command pending, don't sent terminate interrupt
// -1: error

//#define IOM_CMD_OK       0
//#define IOM_CMD_IGNORED  1
//#define IOM_CMD_NO_DCW   2
//#define IOM_CMD_PENDING  3
//#define IOM_CMD_ERROR   -1

typedef enum
  {
     IOM_CMD_ERROR   = -1,
     IOM_CMD_PROCEED =  0,
     IOM_CMD_RESIDUE,
     IOM_CMD_DISCONNECT,
     IOM_CMD_PENDING
  } iomCmdRc;

typedef void iom_connect_t (chnlState_t * chnlp);
typedef iomCmdRc iom_cmd_t (chnlState_t * chnlp);
#if defined(PANEL68)
void do_boot (void);
#endif
#if defined(IO_THREADZ)
void * iom_thread_main (void * arg);
#endif
void * chnThreadMain (void * arg);
void iom_core_read (UNUSED uint iom_unit_idx, word24 addr, word36 *data);
void iom_core_read2 (UNUSED uint iom_unit_idx, word24 addr, word36 *even, word36 *odd);
void iom_core_write (uint iom_unit_idx, word24 addr, word36 data);
void iom_core_write2 (uint iom_unit_idx, word24 addr, word36 even, word36 odd);
void iom_core_read_lock (uint iom_unit_idx, word24 addr, word36 *data);
void iom_core_write_unlock (UNUSED uint iom_unit_idx, word24 addr, word36 data);
simRc iom_unit_reset_idx (uint iom_unit_idx);

#if defined(IO_ASYNC_PAYLOAD_CHAN) || defined(IO_ASYNC_PAYLOAD_CHAN_THREAD)
void iomProcess (void);
#endif

char iomChar (uint iomUnitIdx);
#if defined(TESTING)
void dumpDCW (word36 DCW, word1 LPW_23_REL);
#endif
void iomStats (uint iomNo);
