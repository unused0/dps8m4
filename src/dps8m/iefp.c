/*
 * vim: filetype=c:tabstop=4:ai:expandtab
 * SPDX-License-Identifier: ICU
 * SPDX-License-Identifier: Multics
 * scspell-id: 755f1556-f62e-11ec-8184-80ee73e9b8e7
 *
 * ---------------------------------------------------------------------------
 *
 * Copyright (c) 2007-2013 Michael Mondy
 * Copyright (c) 2012-2016 Harry Reed
 * Copyright (c) 2013-2024 Charles Anthony
 * Copyright (c) 2015 Eric Swenson
 * Copyright (c) 2017 Michal Tomek
 * Copyright (c) 2021-2024 The DPS8M Development Team
 *
 * This software is made available under the terms of the ICU License.
 * See the LICENSE.md file at the top-level directory of this distribution.
 *
 * ---------------------------------------------------------------------------
 *
 * This source file may contain code comments that adapt, include, and/or
 * incorporate Multics program code and/or documentation distributed under
 * the Multics License.  In the event of any discrepancy between code
 * comments herein and the original Multics materials, the original Multics
 * materials should be considered authoritative unless otherwise noted.
 * For more details and historical background, see the LICENSE.md file at
 * the top-level directory of this distribution.
 *
 * ---------------------------------------------------------------------------
 */

#include "dps8m.h"
#include "iom_channel.h"
#include "iom.h"
#include "cable.h"
#include "faults.h"
#include "append.h"
#include "iefp.h"
#include "addrmods.h"
#include "utils.h"

// new Read/Write stuff ...


void ReadAPUDataRead (cpuState_t * cpup, word18 address, word36 * result) {
  cpu.TPR.CA = cpu.iefpFinalAddress = address;
  bool isBAR = get_bar_mode (cpup);

  if (cpu.cu.XSF || cpu.currentInstruction.b29)
    goto B29;

  switch (get_addr_mode (cpup)) {
    case ABSOLUTE_mode: {
      if (isBAR) {
        set_apu_status (cpup, apuStatus_FABS); // XXX maybe...
        cpu.iefpFinalAddress = getBARAddress (cpup, address);
        fauxDoAppendCycle (cpup, APU_DATA_READ);
        coreRead (cpup, cpu.iefpFinalAddress, result, __func__);
        HDBGIEFP (cpup, hdbgIEFP_abs_bar_read, 0, address, "ReadAPUDataRead ABS BAR");
        HDBGMRead (cpup, cpu.iefpFinalAddress, * result, "ReadAPUDataRead ABS BAR");
        return;
      } else {
        set_apu_status (cpup, apuStatus_FABS);
        fauxDoAppendCycle (cpup, APU_DATA_READ);
        coreRead (cpup, address, result, __func__);
        HDBGIEFP (cpup, hdbgIEFP_abs_read, 0, address, "ReadAPUDataRead ABS");
        HDBGMRead (cpup, address, * result, "ReadAPUDataRead ABS");
        return;
      }
    }

    case APPEND_mode: {
B29:;
      if (isBAR) {
        cpu.TPR.CA = getBARAddress (cpup, address);
        cpu.TPR.TSR = cpu.PPR.PSR;
        cpu.TPR.TRR = cpu.PPR.PRR;
        cpu.iefpFinalAddress = doAppendCycleAPUDataRead (cpup, result, 1);
        HDBGIEFP (cpup, hdbgIEFP_bar_read, cpu.TPR.TSR, address, "ReadAPUDataRead BAR");
        HDBGAPURead (cpup, cpu.TPR.TSR, cpu.TPR.CA, cpu.iefpFinalAddress, * result, "ReadAPUDataRead BAR");
        return;
      } else {
        cpu.iefpFinalAddress = doAppendCycleAPUDataRead (cpup, result, 1);
        // XXX Don't trace Multics idle loop
        if (cpu.PPR.PSR != 061 && cpu.PPR.IC != 0307) {
          HDBGIEFP (cpup, hdbgIEFP_read, cpu.TPR.TSR, address, "ReadAPUDataRead");
          HDBGAPURead (cpup, cpu.TPR.TSR, cpu.TPR.CA, cpu.iefpFinalAddress, * result, "ReadAPUDataRead");
        }
        return;
      }
    }
  }
}

void ReadOperandRead (cpuState_t * cpup, word18 address, word36 * result) {
  cpu.TPR.CA = cpu.iefpFinalAddress = address;
  bool isBAR = get_bar_mode (cpup);

  if (cpu.cu.XSF || cpu.currentInstruction.b29)
    goto B29;

  switch (get_addr_mode (cpup)) {
    case ABSOLUTE_mode: {
      if (isBAR) {
        set_apu_status (cpup, apuStatus_FABS); // XXX maybe...
        cpu.iefpFinalAddress = getBARAddress (cpup, address);
        fauxDoAppendCycle (cpup, OPERAND_READ);
        coreRead (cpup, cpu.iefpFinalAddress, result, __func__);
        HDBGIEFP (cpup, hdbgIEFP_abs_bar_read, 0, address, "readOperandRead ABS BAR");
        HDBGMRead (cpup, cpu.iefpFinalAddress, * result, "readOperandRead ABS BAR");
        return;
      } else {
        set_apu_status (cpup, apuStatus_FABS);
        fauxDoAppendCycle (cpup, OPERAND_READ);
        coreRead (cpup, address, result, __func__);
        HDBGIEFP (cpup, hdbgIEFP_abs_read, 0, address, "readOperandRead ABS");
        HDBGMRead (cpup, address, * result, "readOperandRead ABS");
        return;
      }
    }

    case APPEND_mode: {
B29:;
      if (isBAR) {
        cpu.TPR.CA = getBARAddress (cpup, address);
        cpu.TPR.TSR = cpu.PPR.PSR;
        cpu.TPR.TRR = cpu.PPR.PRR;
        cpu.iefpFinalAddress = doAppendCycleOperandRead (cpup, result, 1);
        HDBGIEFP (cpup, hdbgIEFP_bar_read, cpu.TPR.TSR, address, "readOperandRead BAR");
        HDBGAPURead (cpup, cpu.TPR.TSR, cpu.TPR.CA, cpu.iefpFinalAddress, * result, "readOperandRead BAR");

        return;
      } else {
        cpu.iefpFinalAddress = doAppendCycleOperandRead (cpup, result, 1);
        // XXX Don't trace Multics idle loop
        if (cpu.PPR.PSR != 061 && cpu.PPR.IC != 0307) {
          HDBGIEFP (cpup, hdbgIEFP_read, cpu.TPR.TSR, address, "readOperandRead");
          HDBGAPURead (cpup, cpu.TPR.TSR, cpu.TPR.CA, cpu.iefpFinalAddress, * result, "readOperandRead");
        }
        return;
      }
    }
  }
}

void ReadOperandRMW (cpuState_t * cpup, word18 address, word36 * result) {
  cpu.TPR.CA = cpu.iefpFinalAddress = address;
  bool isBAR = get_bar_mode (cpup);

  if (cpu.cu.XSF || cpu.currentInstruction.b29)
    goto B29;

  switch (get_addr_mode (cpup)) {
    case ABSOLUTE_mode: {
      if (isBAR) {
        set_apu_status (cpup, apuStatus_FABS); // XXX maybe...
        cpu.iefpFinalAddress = getBARAddress (cpup, address);
        fauxDoAppendCycle (cpup, OPERAND_RMW);
        coreReadLock (cpup, cpu.iefpFinalAddress, result, __func__);
        HDBGIEFP (cpup, hdbgIEFP_abs_bar_read, 0, address, "ReadOperandRMW ABS BAR");
        HDBGMRead (cpup, cpu.iefpFinalAddress, * result, "ReadOperandRMW ABS BAR");
        return;
      } else {
        set_apu_status (cpup, apuStatus_FABS);
        fauxDoAppendCycle (cpup, OPERAND_RMW);
        coreReadLock (cpup, address, result, __func__);
        HDBGIEFP (cpup, hdbgIEFP_abs_read, 0, address, "ReadOperandRMW ABS");
        HDBGMRead (cpup, address, * result, "ReadOperandRMW ABS");
        return;
      }
    }

    case APPEND_mode: {
B29:;
      if (isBAR) {
        cpu.TPR.CA = getBARAddress (cpup, address);
        cpu.TPR.TSR = cpu.PPR.PSR;
        cpu.TPR.TRR = cpu.PPR.PRR;
        cpu.iefpFinalAddress = doAppendCycleOperandRMW (cpup, result, 1);
        HDBGIEFP (cpup, hdbgIEFP_bar_read, cpu.TPR.TSR, address, "ReadOperandRMW BAR");
        HDBGAPURead (cpup, cpu.TPR.TSR, cpu.TPR.CA, cpu.iefpFinalAddress, * result, "ReadOperandRMW BAR");
        return;
      } else {
        cpu.iefpFinalAddress = doAppendCycleOperandRMW (cpup, result, 1);
        // XXX Don't trace Multics idle loop
        if (cpu.PPR.PSR != 061 && cpu.PPR.IC != 0307) {
          HDBGIEFP (cpup, hdbgIEFP_read, cpu.TPR.TSR, address, "ReadOperandRMW");
          HDBGAPURead (cpup, cpu.TPR.TSR, cpu.TPR.CA, cpu.iefpFinalAddress, * result, "ReadOperandRMW");
        }
        return;
      }
    }
  }
}

void ReadAPUDataRMW (cpuState_t * cpup, word18 address, word36 * result) {
  cpu.TPR.CA = cpu.iefpFinalAddress = address;
  bool isBAR = get_bar_mode (cpup);

  if (cpu.cu.XSF || cpu.currentInstruction.b29)
    goto B29;

  switch (get_addr_mode (cpup)) {
    case ABSOLUTE_mode: {
      if (isBAR) {
        set_apu_status (cpup, apuStatus_FABS); // XXX maybe...
        cpu.iefpFinalAddress = getBARAddress (cpup, address);
        fauxDoAppendCycle (cpup, APU_DATA_RMW);
        coreReadLock (cpup, cpu.iefpFinalAddress, result, __func__);
        HDBGIEFP (cpup, hdbgIEFP_abs_bar_read, 0, address, "ReadAPUDataRMW ABS BAR");
        HDBGMRead (cpup, cpu.iefpFinalAddress, * result, "ReadAPUDataRMW ABS BAR");
        return;
      } else {
        set_apu_status (cpup, apuStatus_FABS);
        fauxDoAppendCycle (cpup, APU_DATA_RMW);
        coreReadLock (cpup, address, result, __func__);
        HDBGIEFP (cpup, hdbgIEFP_abs_read, 0, address, "ReadAPUDataRMW ABS");
        HDBGMRead (cpup, address, * result, "ReadAPUDataRMW ABS");
        return;
      }
    }

    case APPEND_mode: {
B29:;
      if (isBAR) {
        cpu.TPR.CA = getBARAddress (cpup, address);
        cpu.TPR.TSR = cpu.PPR.PSR;
        cpu.TPR.TRR = cpu.PPR.PRR;
        cpu.iefpFinalAddress = doAppendCycleAPUDataRMW (cpup, result, 1);
        HDBGIEFP (cpup, hdbgIEFP_bar_read, cpu.TPR.TSR, address, "ReadAPUDataRMW BAR");
        HDBGAPURead (cpup, cpu.TPR.TSR, cpu.TPR.CA, cpu.iefpFinalAddress, * result, "ReadAPUDataRMW BAR");
        return;
      } else {
        cpu.iefpFinalAddress = doAppendCycleAPUDataRMW (cpup, result, 1);
        // XXX Don't trace Multics idle loop
        if (cpu.PPR.PSR != 061 && cpu.PPR.IC != 0307) {
          HDBGIEFP (cpup, hdbgIEFP_read, cpu.TPR.TSR, address, "ReadAPUDataRMW");
          HDBGAPURead (cpup, cpu.TPR.TSR, cpu.TPR.CA, cpu.iefpFinalAddress, * result, "ReadAPUDataRMW");
        }
        return;
      }
    }
  }
  return ;//SCPE_UNK;
}

void ReadInstructionFetch (cpuState_t * cpup, word18 address, word36 * result) {
  cpu.TPR.CA = cpu.iefpFinalAddress = address;
  bool isBAR = get_bar_mode (cpup);

  if (cpu.cu.XSF)
    goto B29;

  switch (get_addr_mode (cpup)) {
    case ABSOLUTE_mode: {
      if (isBAR) {
        set_apu_status (cpup, apuStatus_FABS); // XXX maybe...
        cpu.iefpFinalAddress = getBARAddress (cpup, address);
        fauxDoAppendCycle (cpup, INSTRUCTION_FETCH);
        coreRead (cpup, cpu.iefpFinalAddress, result, __func__);
        HDBGIEFP (cpup, hdbgIEFP_abs_bar_read, 0, address, "ReadInstructionFetch ABS BAR");
        HDBGMRead (cpup, cpu.iefpFinalAddress, * result, "ReadInstructionFetch ABS BAR");
        return;
      } else {
        set_apu_status (cpup, apuStatus_FABS);
        fauxDoAppendCycle (cpup, INSTRUCTION_FETCH);
        coreRead (cpup, address, result, __func__);
        HDBGIEFP (cpup, hdbgIEFP_abs_read, 0, address, "ReadInstructionFetch ABS");
        HDBGMRead (cpup, address, * result, "ReadInstructionFetch ABS");
        return;
      }
    }

    case APPEND_mode: {
B29:;
      if (isBAR) {
        cpu.TPR.CA = getBARAddress (cpup, address);
        cpu.TPR.TSR = cpu.PPR.PSR;
        cpu.TPR.TRR = cpu.PPR.PRR;
        cpu.iefpFinalAddress = doAppendCycleInstructionFetch (cpup, result, 1);
        HDBGIEFP (cpup, hdbgIEFP_bar_read, cpu.TPR.TSR, address, "ReadInstructionFetch BAR");
        HDBGAPURead (cpup, cpu.TPR.TSR, cpu.TPR.CA, cpu.iefpFinalAddress, * result, "ReadInstructionFetch BAR");
        return;
      } else {
        cpu.iefpFinalAddress = doAppendCycleInstructionFetch (cpup, result, 1);
        // XXX Don't trace Multics idle loop
        if (cpu.PPR.PSR != 061 && cpu.PPR.IC != 0307) {
          HDBGIEFP (cpup, hdbgIEFP_read, cpu.TPR.TSR, address, "ReadInstructionFetch");
          HDBGAPURead (cpup, cpu.TPR.TSR, cpu.TPR.CA, cpu.iefpFinalAddress, * result, "ReadInstructionFetch");
        }
        return;
      }
    }
  }
}

void ReadIndirectWordFetch (cpuState_t * cpup, word18 address, word36 * result) {
  cpu.TPR.CA = cpu.iefpFinalAddress = address;
  bool isBAR = get_bar_mode (cpup);

  if (cpu.cu.XSF || cpu.currentInstruction.b29)
    goto B29;

  switch (get_addr_mode (cpup)) {
    case ABSOLUTE_mode: {
      if (isBAR) {
        set_apu_status (cpup, apuStatus_FABS); // XXX maybe...
        cpu.iefpFinalAddress = getBARAddress (cpup, address);
        fauxDoAppendCycle (cpup, INDIRECT_WORD_FETCH);
        coreRead (cpup, cpu.iefpFinalAddress, result, __func__);
        HDBGIEFP (cpup, hdbgIEFP_abs_bar_read, 0, address, "ReadIndirectWordFetch ABS BAR");
        HDBGMRead (cpup, cpu.iefpFinalAddress, * result, "ReadIndirectWordFetch ABS BAR");
        return;
      } else {
        set_apu_status (cpup, apuStatus_FABS);
        fauxDoAppendCycle (cpup, INDIRECT_WORD_FETCH);
        coreRead (cpup, address, result, __func__);
        HDBGIEFP (cpup, hdbgIEFP_abs_read, 0, address, "ReadIndirectWordFetch ABS");
        HDBGMRead (cpup, address, * result, "ReadIndirectWordFetch ABS");
        return;
      }
  }

  case APPEND_mode: {
B29:;
      if (isBAR) {
        cpu.TPR.CA = getBARAddress (cpup, address);
        cpu.TPR.TSR = cpu.PPR.PSR;
        cpu.TPR.TRR = cpu.PPR.PRR;
        cpu.iefpFinalAddress = doAppendCycleIndirectWordFetch (cpup, result, 1);
        HDBGIEFP (cpup, hdbgIEFP_bar_read, cpu.TPR.TSR, address, "ReadIndirectWordFetch BAR");
        HDBGAPURead (cpup, cpu.TPR.TSR, cpu.TPR.CA, cpu.iefpFinalAddress, * result, "ReadIndirectWordFetch BAR");
        return;
      } else {
        cpu.iefpFinalAddress = doAppendCycleIndirectWordFetch (cpup, result, 1);
        // XXX Don't trace Multics idle loop
        if (cpu.PPR.PSR != 061 && cpu.PPR.IC != 0307) {
            HDBGIEFP (cpup, hdbgIEFP_read, cpu.TPR.TSR, address, "ReadIndirectWordFetch");
            HDBGAPURead (cpup, cpu.TPR.TSR, cpu.TPR.CA, cpu.iefpFinalAddress, * result, "ReadIndirectWordFetch");
          }
          return;
        }
    }
  }
}


void Read2OperandRead (cpuState_t * cpup, word18 address, word36 * result) {
  cpu.TPR.CA = cpu.iefpFinalAddress = address;

  bool isBAR = get_bar_mode (cpup);

  if (cpu.cu.XSF || cpu.currentInstruction.b29)
    goto B29;

  switch (get_addr_mode (cpup)) {
    case ABSOLUTE_mode: {
      if (isBAR) {
        set_apu_status (cpup, apuStatus_FABS); // XXX maybe...
        cpu.iefpFinalAddress = getBARAddress (cpup, address);

        fauxDoAppendCycle (cpup, OPERAND_READ);
        coreRead2 (cpup, cpu.iefpFinalAddress, result + 0, result + 1, __func__);
        HDBGIEFP (cpup, hdbgIEFP_abs_bar_read, 0, address, "Read2OperandRead ABBR");
        HDBGMRead (cpup, cpu.iefpFinalAddress, * result, "Read2OperandRead ABBR evn");
        HDBGMRead (cpup, cpu.iefpFinalAddress+1, * (result+1), "Read2OperandRead ABBR odd");
        return;
      } else {
        set_apu_status (cpup, apuStatus_FABS);
        fauxDoAppendCycle (cpup, OPERAND_READ);
        coreRead2 (cpup, address, result + 0, result + 1, __func__);
        HDBGIEFP (cpup, hdbgIEFP_abs_read, 0, address, "Read2OperandRead AB");
        HDBGMRead (cpup, cpu.iefpFinalAddress, * result, "Read2OperandRead AB evn");
        HDBGMRead (cpup, cpu.iefpFinalAddress+1, * (result+1), "Read2OperandRead AB odd");
        return;
      }
    }

    case APPEND_mode: {
B29:;
      if (isBAR) {
        cpu.TPR.CA = getBARAddress (cpup, address);
        cpu.TPR.TSR = cpu.PPR.PSR;
        cpu.TPR.TRR = cpu.PPR.PRR;
        cpu.iefpFinalAddress = doAppendCycleOperandRead (cpup, result, 2);
        HDBGIEFP (cpup, hdbgIEFP_bar_read, cpu.TPR.TSR, address, "Read2OperandRead BR");
        HDBGAPURead (cpup, cpu.TPR.TSR, cpu.TPR.CA, cpu.iefpFinalAddress, * result, "Read2OperandRead BR evn");
        return;
      } else {
        cpu.iefpFinalAddress = doAppendCycleOperandRead (cpup, result, 2);
        HDBGIEFP (cpup, hdbgIEFP_read, cpu.TPR.TSR, address, "Read2OperandRead");
        HDBGAPURead (cpup, cpu.TPR.TSR, cpu.TPR.CA, cpu.iefpFinalAddress, * result, "Read2OperandRead evn");
        HDBGAPURead (cpup, cpu.TPR.TSR, cpu.TPR.CA + 1, cpu.iefpFinalAddress + 1, * (result+1), "Read2OperandRead odd");
        return;
      }
    }
  }
}

void Read2OperandRMW (cpuState_t * cpup, word18 address, word36 * result) {
  cpu.TPR.CA = cpu.iefpFinalAddress = address;

  bool isBAR = get_bar_mode (cpup);

  if (cpu.cu.XSF || cpu.currentInstruction.b29)
    goto B29;

  switch (get_addr_mode (cpup)) {
    case ABSOLUTE_mode: {
      if (isBAR) {
        set_apu_status (cpup, apuStatus_FABS); // XXX maybe...
        cpu.iefpFinalAddress = getBARAddress (cpup, address);

        fauxDoAppendCycle (cpup, OPERAND_RMW);
        coreRead2 (cpup, cpu.iefpFinalAddress, result + 0, result + 1, __func__);
        HDBGIEFP (cpup, hdbgIEFP_abs_bar_read, 0, address, "Read2OperandRMW ABBR");
        HDBGMRead (cpup, cpu.iefpFinalAddress, * result, "Read2OperandRMW ABBR evn");
        HDBGMRead (cpup, cpu.iefpFinalAddress+1, * (result+1), "Read2OperandRMW ABBR odd");
        return;
      } else {
        set_apu_status (cpup, apuStatus_FABS);
        fauxDoAppendCycle (cpup, OPERAND_RMW);
        coreRead2 (cpup, address, result + 0, result + 1, __func__);
        HDBGIEFP (cpup, hdbgIEFP_abs_read, 0, address, "Read2OperandRMW AB");
        HDBGMRead (cpup, cpu.iefpFinalAddress, * result, "Read2OperandRMW AB evn");
        HDBGMRead (cpup, cpu.iefpFinalAddress+1, * (result+1), "Read2OperandRMW AB odd");
        return;
      }
    }

    case APPEND_mode: {
B29:;
      if (isBAR) {
        cpu.TPR.CA = getBARAddress (cpup, address);
        cpu.TPR.TSR = cpu.PPR.PSR;
        cpu.TPR.TRR = cpu.PPR.PRR;
        cpu.iefpFinalAddress = doAppendCycleOperandRMW (cpup, result, 2);
        HDBGIEFP (cpup, hdbgIEFP_bar_read, cpu.TPR.TSR, address, "Read2OperandRMW BR");
        HDBGAPURead (cpup, cpu.TPR.TSR, cpu.TPR.CA, cpu.iefpFinalAddress, * result, "Read2OperandRMW BR evn");
        HDBGAPURead (cpup, cpu.TPR.TSR, cpu.TPR.CA + 1, cpu.iefpFinalAddress + 1, * (result+1), "Read2OperandRMW BR odd");
        return;
      } else {
        cpu.iefpFinalAddress = doAppendCycleOperandRMW (cpup, result, 2);
        HDBGIEFP (cpup, hdbgIEFP_read, cpu.TPR.TSR, address, "Read2OperandRMW");
        HDBGAPURead (cpup, cpu.TPR.TSR, cpu.TPR.CA, cpu.iefpFinalAddress, * result, "Read2OperandRMW evn");
        HDBGAPURead (cpup, cpu.TPR.TSR, cpu.TPR.CA + 1, cpu.iefpFinalAddress + 1, * (result+1), "Read2OperandRMW odd");
        return;
      }
    }
  }
}

void Read2InstructionFetch (cpuState_t * cpup, word18 address, word36 * result) {
  cpu.TPR.CA = cpu.iefpFinalAddress = address;

  bool isBAR = get_bar_mode (cpup);

  if (cpu.cu.XSF)
    goto B29;

  switch (get_addr_mode (cpup)) {
    case ABSOLUTE_mode: {
      if (isBAR) {
        set_apu_status (cpup, apuStatus_FABS); // XXX maybe...
        cpu.iefpFinalAddress = getBARAddress (cpup, address);

        fauxDoAppendCycle (cpup, INSTRUCTION_FETCH);
        coreRead2 (cpup, cpu.iefpFinalAddress, result + 0, result + 1, __func__);
        HDBGIEFP (cpup, hdbgIEFP_abs_bar_read, 0, address, "Read2InstructionFetch ABBR");
        HDBGMRead (cpup, cpu.iefpFinalAddress, * result, "Read2InstructionFetch ABBR evn");
        HDBGMRead (cpup, cpu.iefpFinalAddress+1, * (result+1), "Read2InstructionFetch ABBR odd");
        return;
      } else {
        set_apu_status (cpup, apuStatus_FABS);
        fauxDoAppendCycle (cpup, INSTRUCTION_FETCH);
        coreRead2 (cpup, address, result + 0, result + 1, __func__);
        HDBGIEFP (cpup, hdbgIEFP_abs_read, 0, address, "Read2InstructionFetch AB");
        HDBGMRead (cpup, cpu.iefpFinalAddress, * result, "Read2InstructionFetch AB evn");
        HDBGMRead (cpup, cpu.iefpFinalAddress+1, * (result+1), "Read2InstructionFetch AB odd");
        return;
      }
    }

    case APPEND_mode: {
B29:;
      if (isBAR) {
        cpu.TPR.CA = getBARAddress (cpup, address);
        cpu.TPR.TSR = cpu.PPR.PSR;
        cpu.TPR.TRR = cpu.PPR.PRR;
        cpu.iefpFinalAddress = doAppendCycleInstructionFetch (cpup, result, 2);
        HDBGIEFP (cpup, hdbgIEFP_bar_read, cpu.TPR.TSR, address, "Read2InstructionFetch BR");
        HDBGAPURead (cpup, cpu.TPR.TSR, cpu.TPR.CA, cpu.iefpFinalAddress, * result, "Read2InstructionFetch BR evn");
        HDBGAPURead (cpup, cpu.TPR.TSR, cpu.TPR.CA + 1, cpu.iefpFinalAddress + 1, * (result+1), "Read2InstructionFetch BR odd");
        return;
      } else {
        cpu.iefpFinalAddress = doAppendCycleInstructionFetch (cpup, result, 2);
        HDBGIEFP (cpup, hdbgIEFP_read, cpu.TPR.TSR, address, "Read2InstructionFetch");
        HDBGAPURead (cpup, cpu.TPR.TSR, cpu.TPR.CA, cpu.iefpFinalAddress, * result, "Read2InstructionFetch evn");
        HDBGAPURead (cpup, cpu.TPR.TSR, cpu.TPR.CA + 1, cpu.iefpFinalAddress + 1, * (result+1), "Read2InstructionFetch odd");
        return;
      }
    }
  }
}

void Read2RTCDOperandFetch (cpuState_t * cpup, word18 address, word36 * result) {
  cpu.TPR.CA = cpu.iefpFinalAddress = address;

  bool isBAR = get_bar_mode (cpup);

  if (isBAR) {
    cpu.TPR.CA = getBARAddress (cpup, address);
    cpu.TPR.TSR = cpu.PPR.PSR;
    cpu.TPR.TRR = cpu.PPR.PRR;
    cpu.iefpFinalAddress = doAppendCycleRTCDOperandFetch (cpup, result, 2);
    HDBGIEFP (cpup, hdbgIEFP_bar_read, cpu.TPR.TSR, address, "Read2 BR");
    HDBGAPURead (cpup, cpu.TPR.TSR, cpu.TPR.CA, cpu.iefpFinalAddress, * result, "Read2 BR evn");
    HDBGAPURead (cpup, cpu.TPR.TSR, cpu.TPR.CA + 1, cpu.iefpFinalAddress + 1, * (result+1), "Read2 BR odd");
    return;
  } else {
    cpu.iefpFinalAddress = doAppendCycleRTCDOperandFetch (cpup, result, 2);
    HDBGIEFP (cpup, hdbgIEFP_read, cpu.TPR.TSR, address, "Read2");
    HDBGAPURead (cpup, cpu.TPR.TSR, cpu.TPR.CA, cpu.iefpFinalAddress, * result, "Read2 evn");
    HDBGAPURead (cpup, cpu.TPR.TSR, cpu.TPR.CA + 1, cpu.iefpFinalAddress + 1, * (result+1), "Read2 odd");
    return;
  }
}

void Read2IndirectWordFetch (cpuState_t * cpup, word18 address, word36 * result) {
  cpu.TPR.CA = cpu.iefpFinalAddress = address;

  bool isBAR = get_bar_mode (cpup);

  if (cpu.cu.XSF || cpu.currentInstruction.b29)
    goto B29;

  switch (get_addr_mode (cpup)) {
    case ABSOLUTE_mode: {
      if (isBAR) {
        set_apu_status (cpup, apuStatus_FABS); // XXX maybe...
        cpu.iefpFinalAddress = getBARAddress (cpup, address);

        fauxDoAppendCycle (cpup, INDIRECT_WORD_FETCH);
        coreRead2 (cpup, cpu.iefpFinalAddress, result + 0, result + 1, __func__);
        HDBGIEFP (cpup, hdbgIEFP_abs_bar_read, 0, address, "Read2IndirectWordFetch ABBR");
        HDBGMRead (cpup, cpu.iefpFinalAddress, * result, "Read2IndirectWordFetch ABBR evn");
        HDBGMRead (cpup, cpu.iefpFinalAddress+1, * (result+1), "Read2IndirectWordFetch ABBR odd");
        return;
      } else {
        set_apu_status (cpup, apuStatus_FABS);
        fauxDoAppendCycle (cpup, INDIRECT_WORD_FETCH);
        coreRead2 (cpup, address, result + 0, result + 1, __func__);
        HDBGIEFP (cpup, hdbgIEFP_abs_read, 0, address, "Read2IndirectWordFetch AB");
        HDBGMRead (cpup, cpu.iefpFinalAddress, * result, "Read2IndirectWordFetch AB evn");
        HDBGMRead (cpup, cpu.iefpFinalAddress+1, * (result+1), "Read2IndirectWordFetch AB odd");
        return;
      }
    }

    case APPEND_mode: {
B29:;
      if (isBAR) {
        cpu.TPR.CA = getBARAddress (cpup, address);
        cpu.TPR.TSR = cpu.PPR.PSR;
        cpu.TPR.TRR = cpu.PPR.PRR;
        cpu.iefpFinalAddress = doAppendCycleIndirectWordFetch (cpup, result, 2);
        HDBGIEFP (cpup, hdbgIEFP_bar_read, cpu.TPR.TSR, address, "Read2IndirectWordFetch BR");
        HDBGAPURead (cpup, cpu.TPR.TSR, cpu.TPR.CA, cpu.iefpFinalAddress, * result, "Read2IndirectWordFetch BR evn");
        HDBGAPURead (cpup, cpu.TPR.TSR, cpu.TPR.CA + 1, cpu.iefpFinalAddress + 1, * (result+1), "Read2IndirectWordFetch BR odd");
        return;
      } else {
        cpu.iefpFinalAddress = doAppendCycleIndirectWordFetch (cpup, result, 2);
        HDBGIEFP (cpup, hdbgIEFP_read, cpu.TPR.TSR, address, "Read2IndirectWordFetch");
        HDBGAPURead (cpup, cpu.TPR.TSR, cpu.TPR.CA, cpu.iefpFinalAddress, * result, "Read2IndirectWordFetch evn");
        HDBGAPURead (cpup, cpu.TPR.TSR, cpu.TPR.CA + 1, cpu.iefpFinalAddress + 1, * (result+1), "Read2IndirectWordFetch odd");
        return;
      }
    }
  }
}

void Read8 (cpuState_t * cpup, word18 address, word36 * result, bool isAR)
  {
    cpu.TPR.CA = cpu.iefpFinalAddress = address;

    bool isBAR = get_bar_mode (cpup);

    if (isAR || cpu.cu.XSF /*get_went_appending ()*/)
      {
        goto B29;
      }

    switch (get_addr_mode (cpup))
      {
        case ABSOLUTE_mode:
          {
            if (isBAR)
              {
                set_apu_status (cpup, apuStatus_FABS); // XXX maybe...
                cpu.iefpFinalAddress = getBARAddress (cpup, address);

                fauxDoAppendCycle (cpup, APU_DATA_READ);
                coreReadN (cpup, cpu.iefpFinalAddress, result, 8, __func__);
                HDBGIEFP (cpup, hdbgIEFP_abs_bar_read, 0, address, "Read8 ABBR");
                for (uint i = 0; i < 8; i ++)
                  HDBGMRead (cpup, cpu.iefpFinalAddress + i, result [i], "Read8 ABBR");
                return;
              }
            else
              {
                set_apu_status (cpup, apuStatus_FABS);
                fauxDoAppendCycle (cpup, APU_DATA_READ);
                coreReadN (cpup, address, result, 8, __func__);
                HDBGIEFP (cpup, hdbgIEFP_abs_read, 0, address, "Read8 ABS");
                for (uint i = 0; i < 8; i ++)
                  HDBGMRead (cpup, address + i, result [i], "Read8 ABS");
                return;
              }
          }

        case APPEND_mode:
          {
B29:;
            if (isBAR)
              {
                cpu.TPR.CA = getBARAddress (cpup, address);
                cpu.TPR.TSR = cpu.PPR.PSR;
                cpu.TPR.TRR = cpu.PPR.PRR;
                cpu.iefpFinalAddress = do_append_cycle (cpup, APU_DATA_READ, result, 8);
                HDBGIEFP (cpup, hdbgIEFP_bar_read, cpu.TPR.TSR, address, "Read8 BAR");
                for (uint i = 0; i < 8; i ++)
                  HDBGAPURead (cpup, cpu.TPR.TSR, cpu.TPR.CA + i, cpu.iefpFinalAddress + i, result[i], "Read8 BAR");
                return;
              }
            else
              {
                cpu.iefpFinalAddress = do_append_cycle (cpup, APU_DATA_READ, result, 8);
                // XXX Don't trace Multics idle loop
                if (cpu.PPR.PSR != 061 && cpu.PPR.IC != 0307)
                  {
                    HDBGIEFP (cpup, hdbgIEFP_read, cpu.TPR.TSR, address, "Read8");
                    for (uint i = 0; i < 8; i ++)
                      HDBGAPURead (cpup, cpu.TPR.TSR, cpu.TPR.CA + i, cpu.iefpFinalAddress + i, result [i], "Read8");
                  }
              }
            return;
          }
      }
    return ;//SCPE_UNK;
  }

void Read16 (cpuState_t * cpup, word18 address, word36 * result)
  {
    address &= paragraphMask; // Round to 8 word boundary
    Read8 (cpup, address, result, cpu.currentInstruction.b29);
    Read8 (cpup, address + 8, result + 8, cpu.currentInstruction.b29);
    return;
  }

void ReadPage (cpuState_t * cpup, word18 address, word36 * result, bool isAR)
  {
    if ((address & PGMK) != 0)
      {
        simWarn ("ReadPage not on boundary %06o\n", address);
      }
    address &= (word18) ~PGMK; // Round to page boundary
    cpu.TPR.CA = cpu.iefpFinalAddress = address;

    bool isBAR = get_bar_mode (cpup);

    if (isAR || cpu.cu.XSF /*get_went_appending ()*/)
      {
        goto B29;
      }

    switch (get_addr_mode (cpup))
      {
        case ABSOLUTE_mode:
          {
            if (isBAR)
              {
                set_apu_status (cpup, apuStatus_FABS); // XXX maybe...
                cpu.iefpFinalAddress = getBARAddress (cpup, address);

                fauxDoAppendCycle (cpup, APU_DATA_READ);
                coreReadN (cpup, cpu.iefpFinalAddress, result, PGSZ, __func__);
                HDBGIEFP (cpup, hdbgIEFP_abs_bar_read, 0, address, "ReadPage AB");
                for (uint i = 0; i < PGSZ; i ++)
                  HDBGMRead (cpup, cpu.iefpFinalAddress + i, result [i], "ReadPage AB");
                return;
              }
            else
              {
                set_apu_status (cpup, apuStatus_FABS);
                fauxDoAppendCycle (cpup, APU_DATA_READ);
                coreReadN (cpup, address, result, PGSZ, __func__);
                HDBGIEFP (cpup, hdbgIEFP_abs_read, 0, address, "ReadPage ABS");
                for (uint i = 0; i < PGSZ; i ++)
                  HDBGMRead (cpup, address + i, result [i], "ReadPage ABS");
                return;
              }
          }

        case APPEND_mode:
          {
B29:;
            if (isBAR)
              {
                cpu.TPR.CA = getBARAddress (cpup, address);
                cpu.TPR.TSR = cpu.PPR.PSR;
                cpu.TPR.TRR = cpu.PPR.PRR;
                cpu.iefpFinalAddress = do_append_cycle (cpup, APU_DATA_READ, result, PGSZ);
                HDBGIEFP (cpup, hdbgIEFP_bar_read, cpu.TPR.TSR, address, "ReadPage B");
                for (uint i = 0; i < PGSZ; i ++)
                  HDBGAPURead (cpup, cpu.TPR.TSR, cpu.TPR.CA + i, cpu.iefpFinalAddress + i, result [i], "ReadPage B");

                return;
              }
            else
              {
                cpu.iefpFinalAddress = do_append_cycle (cpup, APU_DATA_READ, result, PGSZ);
                // XXX Don't trace Multics idle loop
                if (cpu.PPR.PSR != 061 && cpu.PPR.IC != 0307)
                  {
                    HDBGIEFP (cpup, hdbgIEFP_read, cpu.TPR.TSR, address, "ReadPage");
                    for (uint i = 0; i < PGSZ; i ++)
                      HDBGAPURead (cpup, cpu.TPR.TSR, cpu.TPR.CA + i, cpu.iefpFinalAddress + i, result [i], "ReadPage");
                  }
              }
            return;
          }
      }
    return ;//SCPE_UNK;
  }


void WriteAPUDataStore (cpuState_t * cpup, word18 address, word36 data) {
  cpu.TPR.CA = cpu.iefpFinalAddress = address;

  bool isBAR = get_bar_mode (cpup);

  if (cpu.cu.XSF || cpu.currentInstruction.b29)
    goto B29;

  switch (get_addr_mode (cpup)) {
    case ABSOLUTE_mode: {
      if (isBAR) {
        cpu.iefpFinalAddress = getBARAddress (cpup, address);
        set_apu_status (cpup, apuStatus_FABS); // XXX maybe...
        fauxDoAppendCycle (cpup, APU_DATA_STORE);
        coreWrite (cpup, cpu.iefpFinalAddress, data, __func__);
        HDBGIEFP (cpup, hdbgIEFP_abs_bar_write, 0, address, "WriteAPUDataStore ABBR");
        HDBGMWrite (cpup, cpu.iefpFinalAddress, data, "WriteAPUDataStore ABBR");
        return;
      } else {
        set_apu_status (cpup, apuStatus_FABS);
        fauxDoAppendCycle (cpup, APU_DATA_STORE);
        coreWrite (cpup, address, data, __func__);
        HDBGIEFP (cpup, hdbgIEFP_abs_write, 0, address, "WriteAPUDataStore AB");
        HDBGMWrite (cpup, address, data, "WriteAPUDataStore AB");
        return;
      }
    }

    case APPEND_mode: {
B29:
      if (isBAR) {
        cpu.TPR.CA = getBARAddress (cpup, address);
        cpu.TPR.TSR = cpu.PPR.PSR;
        cpu.TPR.TRR = cpu.PPR.PRR;
        cpu.iefpFinalAddress = doAppendCycleAPUDataStore (cpup, & data, 1);
        HDBGIEFP (cpup, hdbgIEFP_bar_write, cpu.TPR.TSR, address, "WriteAPUDataStore BR");
        HDBGAPUWrite (cpup, cpu.TPR.TSR, cpu.TPR.CA, cpu.iefpFinalAddress, data, "WriteAPUDataStore BR");
        return;
      } else {
        cpu.iefpFinalAddress = doAppendCycleAPUDataStore (cpup, & data, 1);
        HDBGIEFP (cpup, hdbgIEFP_write, cpu.TPR.TSR, address, "WriteAPUDataStore");
        HDBGAPUWrite (cpup, cpu.TPR.TSR, cpu.TPR.CA, cpu.iefpFinalAddress, data, "WriteAPUDataStore");
        return;
      }
    }
  }
}

void WriteOperandStore (cpuState_t * cpup, word18 address, word36 data) {
  cpu.TPR.CA = cpu.iefpFinalAddress = address;

  bool isBAR = get_bar_mode (cpup);

  if (cpu.cu.XSF || cpu.currentInstruction.b29)
    goto B29;

  switch (get_addr_mode (cpup)) {
    case ABSOLUTE_mode: {
      if (isBAR) {
        cpu.iefpFinalAddress = getBARAddress (cpup, address);
        set_apu_status (cpup, apuStatus_FABS); // XXX maybe...
        fauxDoAppendCycle (cpup, OPERAND_STORE);
        if (cpu.useZone)
          coreWriteZone (cpup, cpu.iefpFinalAddress, data, __func__);
        else
          coreWrite (cpup, cpu.iefpFinalAddress, data, __func__);
        HDBGIEFP (cpup, hdbgIEFP_abs_bar_write, 0, address, "WriteOperandStore ABBR");
        HDBGMWrite (cpup, cpu.iefpFinalAddress, data, "WriteOperandStore ABBR");
        return;
      } else {
        set_apu_status (cpup, apuStatus_FABS);
        fauxDoAppendCycle (cpup, OPERAND_STORE);
        if (cpu.useZone)
          coreWriteZone (cpup, address, data, __func__);
        else
          coreWrite (cpup, address, data, __func__);
        HDBGIEFP (cpup, hdbgIEFP_abs_write, 0, address, "WriteOperandStore AB");
        HDBGMWrite (cpup, address, data, "WriteOperandStore AB");
        return;
      }
    }

    case APPEND_mode: {
B29:
      if (isBAR) {
        cpu.TPR.CA = getBARAddress (cpup, address);
        cpu.TPR.TSR = cpu.PPR.PSR;
        cpu.TPR.TRR = cpu.PPR.PRR;
        cpu.iefpFinalAddress = doAppendCycleOperandStore (cpup, & data, 1);
        HDBGIEFP (cpup, hdbgIEFP_bar_write, cpu.TPR.TSR, address, "WriteOperandStore BR");
        HDBGAPUWrite (cpup, cpu.TPR.TSR, cpu.TPR.CA, cpu.iefpFinalAddress, data, "WriteOperandStore BR");
        return;
      } else {
        cpu.iefpFinalAddress = doAppendCycleOperandStore (cpup, & data, 1);
        HDBGIEFP (cpup, hdbgIEFP_write, cpu.TPR.TSR, address, "WriteOperandStore");
        HDBGAPUWrite (cpup, cpu.TPR.TSR, cpu.TPR.CA, cpu.iefpFinalAddress, data, "WriteOperandStore");
        return;
      }
    }
  }
}


void Write2OperandStore (cpuState_t * cpup, word18 address, word36 * data) {
  cpu.TPR.CA = cpu.iefpFinalAddress = address;
  bool isBAR = get_bar_mode (cpup);

  if (cpu.cu.XSF || cpu.currentInstruction.b29)
    goto B29;

  switch (get_addr_mode (cpup)) {
    case ABSOLUTE_mode: {
      if (isBAR) {
        cpu.iefpFinalAddress = getBARAddress (cpup, address);
        set_apu_status (cpup, apuStatus_FABS); // XXX maybe...
        fauxDoAppendCycle (cpup, OPERAND_STORE);
        coreWrite2 (cpup, cpu.iefpFinalAddress, data [0], data [1], __func__);
        HDBGIEFP (cpup, hdbgIEFP_abs_bar_write, 0, address, "Write2OperandStore ABBR");
        HDBGMWrite (cpup, cpu.iefpFinalAddress, data [0], "Write2OperandStore ABBR evn");
        HDBGMWrite (cpup, cpu.iefpFinalAddress+1, data [1], "Write2OperandStore ABBR odd");
        return;
      } else {
        set_apu_status (cpup, apuStatus_FABS);
        fauxDoAppendCycle (cpup, OPERAND_STORE);
        coreWrite2 (cpup, address, data [0], data [1], __func__);
        HDBGIEFP (cpup, hdbgIEFP_abs_write, 0, address, "Write2OperandStore AB");
        HDBGMWrite (cpup, address, data [0], "Write2OperandStore AB evn");
        HDBGMWrite (cpup, address+1, data [1], "Write2OperandStore AB odd");
        return;
      }
    }

    case APPEND_mode: {
B29:
      if (isBAR) {
        cpu.TPR.CA = getBARAddress (cpup, address);
        cpu.TPR.TSR = cpu.PPR.PSR;
        cpu.TPR.TRR = cpu.PPR.PRR;
        cpu.iefpFinalAddress = doAppendCycleOperandStore (cpup, data, 2);
        HDBGIEFP (cpup, hdbgIEFP_bar_write, cpu.TPR.TSR, address, "Write2OperandStore BR");
        HDBGAPUWrite (cpup, cpu.TPR.TSR, cpu.TPR.CA, cpu.iefpFinalAddress, data[0], "Write2OperandStore BR evn");
        HDBGAPUWrite (cpup, cpu.TPR.TSR, cpu.TPR.CA + 1, cpu.iefpFinalAddress + 1, data[1], "Write2OperandStore BR odd");
        return;
      } else {
        cpu.iefpFinalAddress = doAppendCycleOperandStore (cpup, data, 2);
        HDBGIEFP (cpup, hdbgIEFP_write, cpu.TPR.TSR, address, "Write2OperandStore");
        HDBGAPUWrite (cpup, cpu.TPR.TSR, cpu.TPR.CA, cpu.iefpFinalAddress, data[0], "Write2OperandStore evn");
        HDBGAPUWrite (cpup, cpu.TPR.TSR, cpu.TPR.CA + 1, cpu.iefpFinalAddress + 1, data[1], "Write2OperandStore odd");
        return;
      }
    }
  }
}

void Write1 (cpuState_t * cpup, word18 address, word36 data, bool isAR)
  {
    cpu.TPR.CA = cpu.iefpFinalAddress = address;
    bool isBAR = get_bar_mode (cpup);
    if (isAR || cpu.cu.XSF /*get_went_appending ()*/)
      goto B29;
    switch (get_addr_mode (cpup))
      {
        case ABSOLUTE_mode:
          {
            if (isBAR)
             {
                cpu.iefpFinalAddress = getBARAddress (cpup, address);
                set_apu_status (cpup, apuStatus_FABS); // XXX maybe...
                fauxDoAppendCycle (cpup, APU_DATA_STORE);
                coreWrite (cpup, cpu.iefpFinalAddress, data, __func__);
                HDBGIEFP (cpup, hdbgIEFP_abs_bar_write, 0, address, "Write1 ABBR");
                HDBGMWrite (cpup, cpu.iefpFinalAddress, data, "Write1 ABBR");
                return;
              }
            else
              {
                set_apu_status (cpup, apuStatus_FABS);
                fauxDoAppendCycle (cpup, APU_DATA_STORE);
                coreWrite (cpup, address, data, __func__);
                HDBGIEFP (cpup, hdbgIEFP_abs_write, 0, address, "Write1 AB");
                HDBGMWrite (cpup, address, data, "Write1 AB");
                return;
              }
          }

        case APPEND_mode:
          {
B29:
            if (isBAR)
              {
                cpu.TPR.CA = getBARAddress (cpup, address);
                cpu.TPR.TSR = cpu.PPR.PSR;
                cpu.TPR.TRR = cpu.PPR.PRR;
                cpu.iefpFinalAddress = do_append_cycle (cpup, APU_DATA_STORE, & data, 1);
                HDBGIEFP (cpup, hdbgIEFP_bar_write, cpu.TPR.TSR, address, "Write1 BR");
                HDBGAPUWrite (cpup, cpu.TPR.TSR, cpu.TPR.CA, cpu.iefpFinalAddress, data, "Write1 BR");
                return;
              }
            else
              {
                cpu.iefpFinalAddress = do_append_cycle (cpup, APU_DATA_STORE, & data, 1);
                HDBGIEFP (cpup, hdbgIEFP_write, cpu.TPR.TSR, address, "Write1");
                HDBGAPUWrite (cpup, cpu.TPR.TSR, cpu.TPR.CA, cpu.iefpFinalAddress, data, "Write1");
                return;
              }
          }
      }
    return ;//SCPE_UNK;
  }

void Write8 (cpuState_t * cpup, word18 address, word36 * data, bool isAR)
  {
    address &= paragraphMask; // Round to 8 word boundarryt
    cpu.TPR.CA = cpu.iefpFinalAddress = address;

    bool isBAR = get_bar_mode (cpup);

    if (isAR || cpu.cu.XSF /*get_went_appending ()*/)
      goto B29;

    switch (get_addr_mode (cpup))
     {
        case ABSOLUTE_mode:
          {
            if (isBAR)
             {
                cpu.iefpFinalAddress = getBARAddress (cpup, address);
                set_apu_status (cpup, apuStatus_FABS); // XXX maybe...
                fauxDoAppendCycle (cpup, APU_DATA_STORE);
                coreWriteN (cpup, cpu.iefpFinalAddress, data, 8, __func__);
                HDBGIEFP (cpup, hdbgIEFP_abs_bar_write, 0, address, "Write8 ABBR");
                for (uint i = 0; i < 8; i ++)
                  HDBGMWrite (cpup, cpu.iefpFinalAddress + i, data [i], "Write8 ABBR");
                return;
              }
            else
              {
                set_apu_status (cpup, apuStatus_FABS);
                fauxDoAppendCycle (cpup, APU_DATA_STORE);
                coreWriteN (cpup, address, data, 8, __func__);
                HDBGIEFP (cpup, hdbgIEFP_abs_write, 0, address, "Write8 AB");
                for (uint i = 0; i < 8; i ++)
                  HDBGMWrite (cpup, address + i, data [i], "Write8 AB");
                return;
              }
          }

        case APPEND_mode:
          {
B29:
            if (isBAR)
              {
                cpu.TPR.CA = getBARAddress (cpup, address);
                cpu.TPR.TSR = cpu.PPR.PSR;
                cpu.TPR.TRR = cpu.PPR.PRR;
                cpu.iefpFinalAddress = do_append_cycle (cpup, APU_DATA_STORE, data, 8);
                HDBGIEFP (cpup, hdbgIEFP_bar_write, cpu.TPR.TSR, address, "Write8 BR");
                for (uint i = 0; i < 8; i ++)
                  HDBGAPUWrite (cpup, cpu.TPR.TSR, cpu.TPR.CA + i, cpu.iefpFinalAddress + i, data [i], "Write8 BR");

                return;
              }
            else
              {
                cpu.iefpFinalAddress = do_append_cycle (cpup, APU_DATA_STORE, data, 8);
                HDBGIEFP (cpup, hdbgIEFP_write, cpu.TPR.TSR, address, "Write8");
                for (uint i = 0; i < 8; i ++)
                  HDBGAPUWrite (cpup, cpu.TPR.TSR, cpu.TPR.CA + i, cpu.iefpFinalAddress + i, data [i], "Write8");

                return;
              }
          }
      }
    return ;//SCPE_UNK;
  }

void Write16 (cpuState_t * cpup, word18 address, word36 * data)
  {
    address &= paragraphMask; // Round to 8 word boundary
    Write8 (cpup, address, data, cpu.currentInstruction.b29);
    Write8 (cpup, address + 8, data + 8, cpu.currentInstruction.b29);
    return;
  }

void Write32 (cpuState_t * cpup, word18 address, word36 * data)
  {
//#define paragraphMask 077777770
    //address &= paragraphMask; // Round to 8 word boundary
    address &= 077777740; // Round to 32 word boundary
    Write8 (cpup, address, data, cpu.currentInstruction.b29);
    Write8 (cpup, address + 8, data + 8, cpu.currentInstruction.b29);
    Write8 (cpup, address + 16, data + 16, cpu.currentInstruction.b29);
    Write8 (cpup, address + 24, data + 24, cpu.currentInstruction.b29);
    return;
  }

void WritePage (cpuState_t * cpup, word18 address, word36 * data, bool isAR)
  {
    if ((address & PGMK) != 0)
      {
        simWarn ("WritePage not on boundary %06o\n", address);
      }
    address &= (word18) ~PGMK; // Round to page boundary
    cpu.TPR.CA = cpu.iefpFinalAddress = address;

    bool isBAR = get_bar_mode (cpup);

    if (isAR || cpu.cu.XSF /*get_went_appending ()*/)
      goto B29;

    switch (get_addr_mode (cpup))
     {
        case ABSOLUTE_mode:
          {
            if (isBAR)
             {
                cpu.iefpFinalAddress = getBARAddress (cpup, address);
                set_apu_status (cpup, apuStatus_FABS); // XXX maybe...
                fauxDoAppendCycle (cpup, APU_DATA_STORE);
                coreWriteN (cpup, cpu.iefpFinalAddress, data, PGSZ, __func__);
                HDBGIEFP (cpup, hdbgIEFP_abs_bar_write, 0, address, "WritePage ABBR");
                for (uint i = 0; i < PGSZ; i ++)
                  HDBGMWrite (cpup, cpu.iefpFinalAddress + i, data [i], "WritePage ABBR");
                return;
              }
            else
              {
                set_apu_status (cpup, apuStatus_FABS);
                fauxDoAppendCycle (cpup, APU_DATA_STORE);
                coreWriteN (cpup, address, data, PGSZ, __func__);
                HDBGIEFP (cpup, hdbgIEFP_abs_write, 0, address, "WritePage AB");
                for (uint i = 0; i < PGSZ; i ++)
                  HDBGMWrite (cpup, address + i, data [i], "WritePage AB");
                return;
              }
          }

        case APPEND_mode:
          {
B29:
            if (isBAR)
              {
                cpu.TPR.CA = getBARAddress (cpup, address);
                cpu.TPR.TSR = cpu.PPR.PSR;
                cpu.TPR.TRR = cpu.PPR.PRR;
                cpu.iefpFinalAddress = do_append_cycle (cpup, APU_DATA_STORE, data, PGSZ);
                HDBGIEFP (cpup, hdbgIEFP_bar_write, cpu.TPR.TSR, address, "WritePage BR");
                for (uint i = 0; i < PGSZ; i ++)
                  HDBGAPUWrite (cpup, cpu.TPR.TSR, cpu.TPR.CA + i, cpu.iefpFinalAddress + i, data [i], "WritePage BR");
                return;
              }
            else
              {
                cpu.iefpFinalAddress = do_append_cycle (cpup, APU_DATA_STORE, data, PGSZ);
                HDBGIEFP (cpup, hdbgIEFP_write, cpu.TPR.TSR, address, "WritePage");
                for (uint i = 0; i < PGSZ; i ++)
                  HDBGAPUWrite (cpup, cpu.TPR.TSR, cpu.TPR.CA + i, cpu.iefpFinalAddress + i, data [i], "WritePage");

                return;
              }
          }
      }
    return ;//SCPE_UNK;
  }

void ReadIndirect (cpuState_t * cpup)
  {
    if (cpu.TPR.CA & 1) // is odd?
      {
        ReadIndirectWordFetch (cpup, cpu.TPR.CA, cpu.itxPair);
        cpu.itxPair[1] = MASK36; // fill with ones for debugging
      }
    else
      {
        Read2IndirectWordFetch (cpup, cpu.TPR.CA, cpu.itxPair);
      }
    return;
  }
