#if !defined(PATH_MAX)
# define PATH_MAX 256
#endif

#if !defined(_POSIX_PATH_MAX)
# define _POSIX_PATH_MAX PATH_MAX
#endif

#if !defined(_XOPEN_PATH_MAX)
# define _XOPEN_PATH_MAX PATH_MAX
#endif

#define MAX3(a, b, c) ((a) > (b) ? ((a) > (c) ? (a) : (c)) : ((b) > (c) ? (b) : (c)))

extern struct system_state_s * system_state;
extern char filebuf [MAX3(PATH_MAX, _POSIX_PATH_MAX, _XOPEN_PATH_MAX)];
extern bool verbose;
extern sdw_s sstSDW;
extern word24 sstPageTableAddr;
extern word18 sstPageTableBound;

//enum symSize {ssWord, ssDWord, ssLowerInt, ssUpperInt};
//enum symType {stAbsaddr, stAddr, stOffet, stNumber};

// xray.c
int xrayDump (long start, long cnt);
int xrayQuit (void);
int xrayPrintSLT (void);

// xrayCore.c
word36 coreReadWord (word24 addr);
void coreReadDWord (word24 addr, word36 * even, word36 * odd);

// xrayShm.c
void * openShm (char * key);

// xraySDW.c
void fetchSDW (word24 addr, sdw_s * SDW);
void fetchPTW (word24 addr, ptw_s * PTW);
void fetchDSBR (word24 addr, dsbr_s * DBSR);
