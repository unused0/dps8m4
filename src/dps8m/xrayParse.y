/* definitions */
%{
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#define YYERROR_VERBOSE

#include "dps8m.h"
#include "xray.h"

extern int yylineno;

extern int yylex();
extern int yyparse();
void yyerror (const char *str);

static word36 start;
static word36 cnt;


#define iffail(x) if (x) YYERROR

%}


%token COMMA

%token DUMP

%token ERROR

%token NUMBER_OVALUE
%token NUMBER_DVALUE
%token NUMBER

%token PRINT 

%token QUIT

%token SLT

%token SEMICOLON


%define api.value.type { word36 }
%code requires {
  #include "dps8m.h"
}

%start Cmds

%%

Cmds : Cmd
     | Cmds SEMICOLON Cmd
     ;

      // dump start, cnt
Cmd : DUMP NUMBER_OVALUE { start = yylval; } NUMBER_OVALUE { cnt = yylval; } { iffail (xrayDump (start, cnt)); }
    | PRINT SLT { iffail (xrayPrintSLT ()); }
    | QUIT { iffail (xrayQuit ()); }
    ;

// NUMBER_VALUE : NUMBER_OVALUE | NUMBER_DVALUE ;

%%

/* auxiliary routines */

//#include "lex.yy.c" 

/* yacc error handler */

void yyerror (const char *str) {
  fprintf (stderr, "error: %s\n", str);
  //fprintf (stderr, "error: %s\n", yytext);
}
 
int yywrap (void) {
  return 1;
}

int evalNumber (const char * s, word36 * v) {
  long long n;
  char * endptr;
  size_t l = strlen (s);
  if (s[l-1] == '.') { // decimal
    n = strtoll (s, & endptr, 10);
    if (endptr != NULL && *endptr == '.') {  // valid
      if (n <= MASK36) { // Can't be negative because lexer ignores minus signs
        * v = (word36) n;
        return 0; // ok
      }
    }
   } else { // octal
    n = strtoll (s, & endptr, 8);
    if (endptr != NULL && *endptr == '\0') {  // valid
      if (n <= MASK36) { // Can't be negative because lexer ignores minus signs
        * v = (word36) n;
        return 0; // ok
      }
    }
  }
  * v = 0;
  printf ("Can't grok '%s' an a number.\n", s);
  return -1; // error
}

/* Declarations */
void set_input_string (const char * in);
void end_lexical_scan (void);

/* This function parses a string */
int parse_string (const char * in) {
  //yydebug = 1;
  set_input_string (in);
  int rv = yyparse ();
  end_lexical_scan ();
  return rv;
}


